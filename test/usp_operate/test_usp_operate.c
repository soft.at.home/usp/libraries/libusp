/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdio.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_function.h>
#include <amxd/amxd_path.h>

#include <amxo/amxo.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_error.h"
#include "usp/uspl_operate.h"
#include "usp/uspl_msghandler.h"
#include "uspl_private.h"
#include "test_usp_operate.h"

static bool test_verify_data(const amxc_var_t* data, const char* field, const char* value) {
    bool rv = false;
    char* field_value = NULL;
    amxc_var_t* field_data = GETP_ARG(data, field);

    printf("Verify event data: check field [%s] contains [%s]\n", field, value);
    fflush(stdout);
    assert_non_null(field_data);

    field_value = amxc_var_dyncast(cstring_t, field_data);
    assert_non_null(field_data);

    rv = (strcmp(field_value, value) == 0);

    free(field_value);
    return rv;
}


static void create_operate_var(amxc_var_t* operate_var) {
    amxc_var_t* input_args = NULL;
    amxc_var_t* table_arg = NULL;
    amxc_var_t* list_arg = NULL;

    amxc_var_add_key(cstring_t, operate_var, "command", "dummy_cmd");
    amxc_var_add_key(cstring_t, operate_var, "command_key", "dummy_cmd_key");
    amxc_var_add_key(bool, operate_var, "send_resp", true);
    input_args = amxc_var_add_key(amxc_htable_t, operate_var, "input_args", NULL);

    amxc_var_add_key(cstring_t, input_args, "string", "text");
    amxc_var_add_key(bool, input_args, "bool", true);
    amxc_var_add_key(uint32_t, input_args, "number", 852);
    table_arg = amxc_var_add_key(amxc_htable_t, input_args, "table", NULL);
    amxc_var_add_key(uint32_t, table_arg, "value", 853);
    list_arg = amxc_var_add_key(amxc_llist_t, input_args, "list", NULL);
    amxc_var_add(bool, list_arg, true);
}

void test_uspl_operate_new(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t operate_var;

    Usp__Operate* operate = NULL;
    Usp__Operate__InputArgsEntry* input_args_entry = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&operate_var);
    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    create_operate_var(&operate_var);
    amxc_var_dump(&operate_var, STDOUT_FILENO);

    assert_int_equal(uspl_operate_new(usp_tx, &operate_var), 0);
    assert_string_equal(usp_tx->msg_id, "0");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__OPERATE);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Operate message
    assert_int_equal(usp_rx->msg->header->msg_type, USP__HEADER__MSG_TYPE__OPERATE);
    assert_int_equal(usp_rx->msg->body->msg_body_case, USP__BODY__MSG_BODY_REQUEST);
    assert_int_equal(usp_rx->msg->body->request->req_type_case, USP__REQUEST__REQ_TYPE_OPERATE);
    operate = usp_rx->msg->body->request->operate;
    assert_non_null(operate);
    assert_string_equal(operate->command, "dummy_cmd");
    assert_string_equal(operate->command_key, "dummy_cmd_key");
    assert_true(operate->send_resp);

    assert_int_equal(operate->n_input_args, 5);
    assert_non_null(operate->input_args);

    input_args_entry = operate->input_args[0];
    assert_string_equal(input_args_entry->key, "table");
    assert_string_equal(input_args_entry->value, "{\"value\":853}");

    input_args_entry = operate->input_args[1];
    assert_string_equal(input_args_entry->key, "number");
    assert_string_equal(input_args_entry->value, "852");

    input_args_entry = operate->input_args[2];
    assert_string_equal(input_args_entry->key, "list");
    assert_string_equal(input_args_entry->value, "[true]");

    input_args_entry = operate->input_args[3];
    assert_string_equal(input_args_entry->key, "string");
    assert_string_equal(input_args_entry->value, "text");

    input_args_entry = operate->input_args[4];
    assert_string_equal(input_args_entry->key, "bool");
    assert_string_equal(input_args_entry->value, "true");

    amxc_var_clean(&operate_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_operate_new_invalid(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    amxc_var_t operate_var;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&operate_var);
    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);

    assert_int_equal(uspl_operate_new(NULL, &operate_var), -1);
    assert_int_equal(uspl_operate_new(usp_tx, NULL), -1);

    assert_int_equal(uspl_operate_new(usp_tx, &operate_var), -1);
    amxc_var_add_key(cstring_t, &operate_var, "command", "dummy_cmd");
    assert_int_equal(uspl_operate_new(usp_tx, &operate_var), -1);

    uspl_tx_delete(&usp_tx);
    amxc_var_clean(&operate_var);
}

void test_uspl_operate_extract(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t operate_var;
    amxc_var_t result;
    int rv = 0;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&operate_var);
    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    create_operate_var(&operate_var);

    assert_int_equal(uspl_operate_new(usp_tx, &operate_var), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    amxc_var_init(&result);
    assert_int_equal(uspl_operate_extract(usp_rx, &result), 0);
    assert_int_equal(amxc_var_compare(&operate_var, &result, &rv), 0);
    assert_int_equal(rv, 0);

    amxc_var_clean(&result);
    amxc_var_clean(&operate_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_operate_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t output_var;
    Usp__Operate* operate = NULL;

    usp_rx = calloc(1, sizeof(uspl_rx_t));
    amxc_var_init(&output_var);

    assert_int_equal(uspl_operate_extract(NULL, &output_var), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_operate_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_operate_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_operate_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_operate_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_operate_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_operate_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request = malloc(sizeof(Usp__Request));
    usp__request__init(usp_rx->msg->body->request);
    assert_int_equal(uspl_operate_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request->req_type_case = USP__REQUEST__REQ_TYPE_OPERATE;
    assert_int_equal(uspl_operate_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    usp_rx->msg->body->request->operate = malloc(sizeof(Usp__Operate));
    usp__operate__init(usp_rx->msg->body->request->operate);
    operate = usp_rx->msg->body->request->operate;
    operate->command = NULL;
    operate->command_key = NULL;
    assert_int_equal(uspl_operate_extract(usp_rx, &output_var), USP_ERR_INVALID_ARGUMENTS);

    operate->command = "";
    assert_int_equal(uspl_operate_extract(usp_rx, &output_var), USP_ERR_INVALID_ARGUMENTS);
    operate->command = strdup("dummy_cmd");
    assert_int_equal(uspl_operate_extract(usp_rx, &output_var), USP_ERR_INVALID_ARGUMENTS);
    operate->command_key = "";
    assert_int_equal(uspl_operate_extract(usp_rx, &output_var), USP_ERR_INVALID_ARGUMENTS);
    operate->command_key = strdup("dummy_cmd");
    operate->n_input_args = 0;
    assert_int_equal(uspl_operate_extract(usp_rx, &output_var), USP_ERR_OK);
    operate->n_input_args = 1;
    operate->input_args = NULL;
    assert_int_equal(uspl_operate_extract(usp_rx, &output_var), USP_ERR_OK);
    operate->input_args = calloc(1, sizeof(void*));
    assert_int_equal(uspl_operate_extract(usp_rx, &output_var), USP_ERR_INVALID_ARGUMENTS);

    free(operate->input_args);
    operate->input_args = NULL;
    operate->n_input_args = 0;
    free(operate->command_key);
    operate->command_key = NULL;
    free(operate->command);
    operate->command = NULL;
    uspl_rx_delete(&usp_rx);
    amxc_var_clean(&output_var);
}

void test_uspl_operate_resp_new(UNUSED void** state) {
    amxc_var_t* reply_data = NULL;
    amxc_var_t* args_var = NULL;
    amxc_var_t* failure_var = NULL;
    amxc_llist_t resp_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    Usp__OperateResp__OperationResult* oper_result = NULL;
    Usp__OperateResp__OperationResult__OutputArgs__OutputArgsEntry* out_args_entry = NULL;

    amxc_llist_init(&resp_list);

    // Request object path variant
    amxc_var_new(&reply_data);
    amxc_var_set_type(reply_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, reply_data, "executed_command", "path.to.dummy()");
    amxc_var_add_key(uint32_t, reply_data, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OBJ_PATH);
    amxc_var_add_key(cstring_t, reply_data, "req_obj_path", "Device.LocalAgent.Request.1.");
    amxc_llist_append(&resp_list, &reply_data->lit);

    // Requested output arguments variant
    amxc_var_new(&reply_data);
    amxc_var_set_type(reply_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, reply_data, "executed_command", "path.to.dummy()");
    amxc_var_add_key(uint32_t, reply_data, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OUTPUT_ARGS);

    args_var = amxc_var_add_key(amxc_htable_t, reply_data, "output_args", NULL);
    amxc_var_add_key(cstring_t, args_var, "out1", "text");
    amxc_var_add_key(bool, args_var, "out2", true);
    amxc_var_add_key(uint32_t, args_var, "out3", 456);
    amxc_llist_append(&resp_list, &reply_data->lit);

    // Command failure variant
    amxc_var_new(&reply_data);
    amxc_var_set_type(reply_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, reply_data, "executed_command", "path.to.dummy()");
    amxc_var_add_key(uint32_t, reply_data, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_CMD_FAILURE);
    failure_var = amxc_var_add_key(amxc_htable_t, reply_data, "cmd_failure", NULL);

    amxc_var_add_key(cstring_t, failure_var, "err_msg", "test error message");
    amxc_var_add_key(uint32_t, failure_var, "err_code", USP_ERR_GENERAL_FAILURE);
    amxc_llist_append(&resp_list, &reply_data->lit);

    // Create response
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_operate_resp_new(usp_tx, &resp_list, "456"), 0);
    assert_string_equal(usp_tx->msg_id, "456");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__OPERATE_RESP);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(usp_rx->msg->header->msg_type, USP__HEADER__MSG_TYPE__OPERATE_RESP);
    assert_int_equal(usp_rx->msg->body->msg_body_case, USP__BODY__MSG_BODY_RESPONSE);
    assert_int_equal(usp_rx->msg->body->response->resp_type_case, USP__RESPONSE__RESP_TYPE_OPERATE_RESP);

    assert_int_equal(usp_rx->msg->body->response->operate_resp->n_operation_results, 3);
    assert_non_null(usp_rx->msg->body->response->operate_resp->operation_results);

    // Verify request object path command
    oper_result = usp_rx->msg->body->response->operate_resp->operation_results[0];
    assert_string_equal(oper_result->executed_command, "path.to.dummy()");
    assert_int_equal(oper_result->operation_resp_case, USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OBJ_PATH);
    assert_string_equal(oper_result->req_obj_path, "Device.LocalAgent.Request.1.");

    // Verify requested output args command
    oper_result = usp_rx->msg->body->response->operate_resp->operation_results[1];
    assert_string_equal(oper_result->executed_command, "path.to.dummy()");
    assert_int_equal(oper_result->operation_resp_case, USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OUTPUT_ARGS);
    assert_non_null(oper_result->req_output_args);
    assert_int_equal(oper_result->req_output_args->n_output_args, 3);
    assert_non_null(oper_result->req_output_args->output_args);

    out_args_entry = oper_result->req_output_args->output_args[0];
    assert_string_equal(out_args_entry->key, "out1");
    assert_string_equal(out_args_entry->value, "text");

    out_args_entry = oper_result->req_output_args->output_args[1];
    assert_string_equal(out_args_entry->key, "out2");
    assert_string_equal(out_args_entry->value, "true");

    out_args_entry = oper_result->req_output_args->output_args[2];
    assert_string_equal(out_args_entry->key, "out3");
    assert_string_equal(out_args_entry->value, "456");

    // Verify command failure command
    oper_result = usp_rx->msg->body->response->operate_resp->operation_results[2];
    assert_string_equal(oper_result->executed_command, "path.to.dummy()");
    assert_int_equal(oper_result->operation_resp_case, USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_CMD_FAILURE);
    assert_non_null(oper_result->cmd_failure);
    assert_int_equal(oper_result->cmd_failure->err_code, USP_ERR_GENERAL_FAILURE);
    assert_string_equal(oper_result->cmd_failure->err_msg, "test error message");

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, variant_list_it_free);
}

void test_uspl_operate_resp_new_no_out_args(UNUSED void** state) {
    amxc_var_t* reply_data = NULL;
    amxc_llist_t resp_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;

    amxc_llist_init(&resp_list);

    // Requested output arguments variant
    amxc_var_new(&reply_data);
    amxc_var_set_type(reply_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, reply_data, "executed_command", "path.to.dummy()");
    amxc_var_add_key(uint32_t, reply_data, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OUTPUT_ARGS);
    amxc_llist_append(&resp_list, &reply_data->lit);

    // Create response
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_operate_resp_new(usp_tx, &resp_list, "456"), 0);
    assert_string_equal(usp_tx->msg_id, "456");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__OPERATE_RESP);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, variant_list_it_free);
}

void test_uspl_operate_resp_invalid(UNUSED void** state) {
    amxc_var_t* reply_data = NULL;
    amxc_llist_t resp_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;

    amxc_llist_init(&resp_list);

    // Request object path variant
    amxc_var_new(&reply_data);
    amxc_var_set_type(reply_data, AMXC_VAR_ID_HTABLE);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    // Invalid input
    assert_int_equal(uspl_operate_resp_new(NULL, &resp_list, "456"), -1);
    assert_int_equal(uspl_operate_resp_new(usp_tx, NULL, "456"), -1);
    assert_int_equal(uspl_operate_resp_new(usp_tx, &resp_list, NULL), -1);
    assert_int_equal(uspl_operate_resp_new(usp_tx, &resp_list, "456"), 0);

    // Invalid req_obj_path
    amxc_llist_append(&resp_list, &reply_data->lit);
    assert_int_equal(uspl_operate_resp_new(usp_tx, &resp_list, "456"), -1);
    amxc_var_add_key(cstring_t, reply_data, "executed_command", "path.to.dummy()");
    assert_int_equal(uspl_operate_resp_new(usp_tx, &resp_list, "456"), -1);
    amxc_var_add_key(uint32_t, reply_data, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OBJ_PATH);
    assert_int_equal(uspl_operate_resp_new(usp_tx, &resp_list, "456"), -1);
    amxc_var_add_key(cstring_t, reply_data, "req_obj_path", "Device.LocalAgent.Request.1.");

    // Invalid cmd failure
    amxc_var_new(&reply_data);
    amxc_var_set_type(reply_data, AMXC_VAR_ID_HTABLE);
    amxc_llist_append(&resp_list, &reply_data->lit);
    amxc_var_add_key(cstring_t, reply_data, "executed_command", "path.to.dummy()");
    amxc_var_add_key(uint32_t, reply_data, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_CMD_FAILURE);
    assert_int_equal(uspl_operate_resp_new(usp_tx, &resp_list, "456"), -1);
    amxc_var_add_key(amxc_htable_t, reply_data, "cmd_failure", NULL);
    assert_int_equal(uspl_operate_resp_new(usp_tx, &resp_list, "456"), -1);

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, variant_list_it_free);
}

void test_uspl_operate_resp_extract(UNUSED void** state) {
    amxc_var_t* reply_data = NULL;
    amxc_var_t* args_var = NULL;
    amxc_var_t* out_table = NULL;
    amxc_var_t* out_list = NULL;
    amxc_var_t* failure_var = NULL;
    amxc_var_t* extract_var = NULL;
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;

    amxc_llist_init(&resp_list);

    // Request object path variant
    amxc_var_new(&reply_data);
    amxc_var_set_type(reply_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, reply_data, "executed_command", "path.to.dummy()");
    amxc_var_add_key(uint32_t, reply_data, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OBJ_PATH);
    amxc_var_add_key(cstring_t, reply_data, "req_obj_path", "Device.LocalAgent.Request.1.");
    amxc_llist_append(&resp_list, &reply_data->lit);

    // Requested output arguments variant
    amxc_var_new(&reply_data);
    amxc_var_set_type(reply_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, reply_data, "executed_command", "path.to.dummy()");
    amxc_var_add_key(uint32_t, reply_data, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OUTPUT_ARGS);

    args_var = amxc_var_add_key(amxc_htable_t, reply_data, "output_args", NULL);
    amxc_var_add_key(cstring_t, args_var, "out1", "text");
    amxc_var_add_key(bool, args_var, "out2", true);
    out_table = amxc_var_add_key(amxc_htable_t, args_var, "out-table", NULL);
    amxc_var_add_key(uint32_t, out_table, "out3", 456);
    out_list = amxc_var_add_key(amxc_llist_t, args_var, "out-list", NULL);
    amxc_var_add(cstring_t, out_list, "list-text");
    amxc_llist_append(&resp_list, &reply_data->lit);

    // Command failure variant
    amxc_var_new(&reply_data);
    amxc_var_set_type(reply_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, reply_data, "executed_command", "path.to.dummy()");
    amxc_var_add_key(uint32_t, reply_data, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_CMD_FAILURE);
    failure_var = amxc_var_add_key(amxc_htable_t, reply_data, "cmd_failure", NULL);

    amxc_var_add_key(cstring_t, failure_var, "err_msg", "test error message");
    amxc_var_add_key(uint32_t, failure_var, "err_code", USP_ERR_GENERAL_FAILURE);
    amxc_llist_append(&resp_list, &reply_data->lit);

    // Create response
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_operate_resp_new(usp_tx, &resp_list, "456"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Extract USP message into variant
    amxc_llist_init(&extract_list);
    assert_int_equal(uspl_operate_resp_extract(usp_rx, &extract_list), 0);

    extract_var = amxc_var_from_llist_it(amxc_llist_get_at(&extract_list, 0));
    amxc_var_dump(extract_var, STDOUT_FILENO);
    assert_true(test_verify_data(extract_var, "executed_command", "path.to.dummy()"));
    assert_true(test_verify_data(extract_var, "operation_resp_case", "2"));
    assert_true(test_verify_data(extract_var, "req_obj_path", "Device.LocalAgent.Request.1."));

    extract_var = amxc_var_from_llist_it(amxc_llist_get_at(&extract_list, 1));
    amxc_var_dump(extract_var, STDOUT_FILENO);
    assert_true(test_verify_data(extract_var, "executed_command", "path.to.dummy()"));
    assert_true(test_verify_data(extract_var, "operation_resp_case", "3"));
    args_var = GET_ARG(extract_var, "output_args");
    assert_non_null(args_var);
    assert_true(test_verify_data(args_var, "out1", "text"));
    assert_true(test_verify_data(args_var, "out2", "true"));
    assert_true(test_verify_data(args_var, "out-list.0", "list-text"));
    assert_true(test_verify_data(args_var, "out-table.out3", "456"));

    extract_var = amxc_var_from_llist_it(amxc_llist_get_at(&extract_list, 2));
    amxc_var_dump(extract_var, STDOUT_FILENO);
    assert_true(test_verify_data(extract_var, "executed_command", "path.to.dummy()"));
    assert_true(test_verify_data(extract_var, "operation_resp_case", "4"));
    failure_var = GET_ARG(extract_var, "cmd_failure");
    assert_non_null(failure_var);
    assert_true(test_verify_data(failure_var, "err_code", "7000"));
    assert_true(test_verify_data(failure_var, "err_msg", "test error message"));

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&extract_list, variant_list_it_free);
    amxc_llist_clean(&resp_list, variant_list_it_free);
}

void test_uspl_operate_resp_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_llist_t extract_list;
    Usp__OperateResp* oper_resp = NULL;
    Usp__OperateResp__OperationResult* oper_result = NULL;

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_operate_resp_extract(NULL, &extract_list), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_operate_resp_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_operate_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_operate_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_operate_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_operate_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    assert_int_equal(uspl_operate_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response = malloc(sizeof(Usp__Response));
    usp__response__init(usp_rx->msg->body->response);
    assert_int_equal(uspl_operate_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    usp_rx->msg->body->response->resp_type_case = USP__RESPONSE__RESP_TYPE_OPERATE_RESP;
    assert_int_equal(uspl_operate_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response->operate_resp = malloc(sizeof(Usp__OperateResp));
    usp__operate_resp__init(usp_rx->msg->body->response->operate_resp);

    oper_resp = usp_rx->msg->body->response->operate_resp;
    oper_resp->n_operation_results = 0;
    assert_int_equal(uspl_operate_resp_extract(usp_rx, &extract_list), USP_ERR_INVALID_ARGUMENTS);
    oper_resp->n_operation_results = 1;
    oper_resp->operation_results = NULL;
    assert_int_equal(uspl_operate_resp_extract(usp_rx, &extract_list), USP_ERR_INVALID_ARGUMENTS);

    oper_resp->operation_results = calloc(1, sizeof(void*));
    oper_result = malloc(sizeof(Usp__OperateResp__OperationResult));
    usp__operate_resp__operation_result__init(oper_result);
    oper_resp->operation_results[0] = oper_result;

    oper_result->executed_command = strdup("dummy");
    oper_result->operation_resp_case = USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP__NOT_SET;
    assert_int_equal(uspl_operate_resp_extract(usp_rx, &extract_list), USP_ERR_INVALID_ARGUMENTS);

    oper_result->operation_resp_case = USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OBJ_PATH;
    oper_result->req_obj_path = strdup("dummy");

    uspl_rx_delete(&usp_rx);
}

// Test that we can send an ambiorix return variant with a htable at index 0 and output args
// (another htable) at index 1
void test_uspl_operate_resp_composite_output(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t* ret_0 = NULL;
    amxc_var_t* ret_1 = NULL;
    amxc_var_t* attributes = NULL;
    amxc_var_t* reply_data = NULL;
    amxc_var_t* args_var = NULL;
    amxc_var_t* arg = NULL;
    amxc_var_t* output = NULL;
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;

    amxc_llist_init(&extract_list);
    amxc_llist_init(&resp_list);
    amxc_var_init(&ret);
    amxc_var_set_type(&ret, AMXC_VAR_ID_LIST);

    // Add default Greeter._describe return variant at index 0
    ret_0 = amxc_var_add(amxc_htable_t, &ret, NULL);
    amxc_var_add_key(cstring_t, ret_0, "name", "Greeter");
    amxc_var_add_key(cstring_t, ret_0, "object", "Greeter.");
    amxc_var_add_key(cstring_t, ret_0, "path", "Greeter.");
    amxc_var_add_key(cstring_t, ret_0, "type_name", "singleton");
    amxc_var_add_key(uint32_t, ret_0, "type_id", 1);
    attributes = amxc_var_add_key(amxc_htable_t, ret_0, "attributes", NULL);
    amxc_var_add_key(bool, attributes, "locked", false);
    amxc_var_add_key(bool, attributes, "persistent", true);
    amxc_var_add_key(bool, attributes, "private", false);
    amxc_var_add_key(bool, attributes, "protected", false);
    amxc_var_add_key(bool, attributes, "read-only", false);

    // Add extra output arguments at index 1
    ret_1 = amxc_var_add(amxc_htable_t, &ret, NULL);
    amxc_var_add_key(uint32_t, ret_1, "err_code", USP_ERR_OK);
    amxc_var_add_key(cstring_t, ret_1, "err_msg", "Success");

    // Requested output arguments variant
    amxc_var_new(&reply_data);
    amxc_var_set_type(reply_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, reply_data, "executed_command", "Greeter._describe()");
    amxc_var_add_key(uint32_t, reply_data, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OUTPUT_ARGS);

    args_var = amxc_var_add_key(amxc_htable_t, reply_data, "output_args", NULL);
    amxc_var_move(args_var, ret_0);
    arg = amxc_var_add_key(amxc_htable_t, args_var, "_retval", NULL);
    amxc_var_move(arg, ret_1);
    amxc_var_dump(reply_data, STDOUT_FILENO);
    amxc_llist_append(&resp_list, &reply_data->lit);

    // Create response
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_operate_resp_new(usp_tx, &resp_list, "456"), 0);
    assert_string_equal(usp_tx->msg_id, "456");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__OPERATE_RESP);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    uspl_operate_resp_extract(usp_rx, &extract_list);

    output = amxc_var_from_llist_it(amxc_llist_get_first(&extract_list));
    amxc_var_dump(output, STDOUT_FILENO);

    amxc_var_clean(&ret);
    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    amxc_llist_clean(&extract_list, variant_list_it_free);
}
