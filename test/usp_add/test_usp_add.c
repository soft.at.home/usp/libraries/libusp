/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdio.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_function.h>

#include <amxo/amxo.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_add.h"
#include "usp/uspl_msghandler.h"
#include "uspl_private.h"
#include "test_usp_add.h"

static amxo_parser_t parser;
static const char* odl_defs = "test_dm.odl";

static amxd_dm_t dm;

static void clean_list(amxc_llist_it_t* it) {
    amxc_var_t* data = amxc_llist_it_get_data(it, amxc_var_t, lit);
    amxc_var_delete(&data);
}

static void build_add_var(amxc_var_t* add_var) {
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;

    amxc_var_set_type(add_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, add_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, add_var, "requests", NULL);

    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", "LocalAgent.");
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "EndpointID");
    amxc_var_add_key(cstring_t, param, "value", "test");
    amxc_var_add_key(bool, param, "required", false);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "UpTime");
    amxc_var_add_key(uint32_t, param, "value", 6000);
    amxc_var_add_key(bool, param, "required", true);

    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", "LocalAgent.MTP.1.");
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "Enable");
    amxc_var_add_key(bool, param, "value", true);
    amxc_var_add_key(bool, param, "required", false);
}

static bool test_verify_data(const amxc_var_t* data, const char* field, const char* value) {
    bool rv = false;
    char* field_value = NULL;
    amxc_var_t* field_data = GETP_ARG(data, field);

    printf("Verify event data: check field [%s] contains [%s]\n", field, value);
    fflush(stdout);
    assert_non_null(field_data);

    field_value = amxc_var_dyncast(cstring_t, field_data);
    assert_non_null(field_data);

    rv = (strcmp(field_value, value) == 0);

    free(field_value);
    return rv;
}

int test_dm_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), 0);

    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    assert_int_equal(amxo_parser_invoke_entry_points(&parser, &dm, AMXO_START), 0);

    return 0;
}

int test_dm_teardown(UNUSED void** state) {
    assert_int_equal(amxo_parser_invoke_entry_points(&parser, &dm, AMXO_STOP), 0);

    amxo_parser_clean(&parser);

    amxo_resolver_import_close_all();

    amxd_dm_clean(&dm);

    return 0;
}

void test_uspl_add_new(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t add_var;

    Usp__Add* add = NULL;
    Usp__Add__CreateObject* create_obj = NULL;
    Usp__Add__CreateParamSetting* param_setting = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&add_var);

    build_add_var(&add_var);

    amxc_var_dump(&add_var, STDOUT_FILENO);

    assert_int_equal(uspl_add_new(usp_tx, &add_var), 0);
    assert_string_equal(usp_tx->msg_id, "0");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__ADD);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Add message
    add = usp_rx->msg->body->request->add;
    assert_false(add->allow_partial);
    assert_int_equal(add->n_create_objs, 2);

    create_obj = add->create_objs[0];
    assert_string_equal(create_obj->obj_path, "LocalAgent.");
    assert_int_equal(create_obj->n_param_settings, 2);

    param_setting = create_obj->param_settings[0];
    assert_string_equal(param_setting->param, "EndpointID");
    assert_string_equal(param_setting->value, "test");
    assert_false(param_setting->required);

    param_setting = create_obj->param_settings[1];
    assert_string_equal(param_setting->param, "UpTime");
    assert_string_equal(param_setting->value, "6000");
    assert_true(param_setting->required);

    create_obj = add->create_objs[1];
    assert_string_equal(create_obj->obj_path, "LocalAgent.MTP.1.");
    assert_int_equal(create_obj->n_param_settings, 1);

    param_setting = create_obj->param_settings[0];
    assert_string_equal(param_setting->param, "Enable");
    assert_string_equal(param_setting->value, "true");
    assert_false(param_setting->required);

    amxc_var_clean(&add_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_add_new_invalid(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    amxc_var_t add_var;
    amxc_var_t* partial = NULL;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    amxc_var_t* dummy = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&add_var);

    assert_int_equal(uspl_add_new(NULL, &add_var), -1);
    assert_int_equal(uspl_add_new(usp_tx, NULL), -1);

    amxc_var_set_type(&add_var, AMXC_VAR_ID_HTABLE);
    partial = amxc_var_add_key(bool, &add_var, "allow_partial", true);
    assert_int_equal(uspl_add_new(usp_tx, &add_var), -1);

    amxc_var_set(bool, partial, false);
    assert_int_equal(uspl_add_new(usp_tx, &add_var), -1);

    requests = amxc_var_add_key(amxc_llist_t, &add_var, "requests", NULL);
    assert_int_equal(uspl_add_new(usp_tx, &add_var), -1);

    request = amxc_var_add(amxc_htable_t, requests, NULL);
    assert_int_equal(uspl_add_new(usp_tx, &add_var), -1);
    amxc_var_add_key(cstring_t, request, "object_path", "LocalAgent.");
    assert_int_equal(uspl_add_new(usp_tx, &add_var), 0);

    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);
    dummy = amxc_var_add(cstring_t, params, "dummy");
    amxc_var_dump(&add_var, STDOUT_FILENO);
    assert_int_equal(uspl_add_new(usp_tx, &add_var), -1);
    amxc_llist_it_clean(&dummy->lit, clean_list);
    amxc_var_dump(&add_var, STDOUT_FILENO);
    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "EndpointID");
    amxc_var_dump(&add_var, STDOUT_FILENO);
    assert_int_equal(uspl_add_new(usp_tx, &add_var), -1);

    amxc_var_clean(&add_var);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_add_extract(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t add_var;
    amxc_var_t output_var;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&add_var);
    build_add_var(&add_var);

    assert_int_equal(uspl_add_new(usp_tx, &add_var), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    amxc_var_init(&output_var);
    assert_int_equal(uspl_add_extract(usp_rx, &output_var), 0);

    amxc_var_clean(&add_var);
    amxc_var_clean(&output_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_add_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t output_var;
    amxc_var_init(&output_var);

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_add_extract(NULL, &output_var), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_add_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_add_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_add_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_add_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_add_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    assert_int_equal(uspl_add_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_add_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request = malloc(sizeof(Usp__Request));
    usp__request__init(usp_rx->msg->body->request);
    assert_int_equal(uspl_add_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request->req_type_case = USP__REQUEST__REQ_TYPE_SET;
    assert_int_equal(uspl_add_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request->req_type_case = USP__REQUEST__REQ_TYPE_ADD;
    assert_int_equal(uspl_add_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    usp_rx->msg->body->request->add = malloc(sizeof(Usp__Add));
    usp__add__init(usp_rx->msg->body->request->add);
    usp_rx->msg->body->request->add->allow_partial = true;
    assert_int_equal(uspl_add_extract(usp_rx, &output_var), USP_ERR_OK);

    amxc_var_clean(&output_var);
    uspl_rx_delete(&usp_rx);
}

void test_uspl_add_resp(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t retval;
    amxc_var_t* first_ret = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* result = NULL;
    amxd_object_t* obj = NULL;
    char* req_path = "test_root.child.[Alias == \"One\"].box.";
    amxc_llist_t resp_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    Usp__AddResp* add_resp = NULL;
    Usp__AddResp__CreatedObjectResult__OperationStatus* status = NULL;
    Usp__AddResp__CreatedObjectResult__OperationStatus__OperationSuccess* success = NULL;
    Usp__AddResp__CreatedObjectResult__OperationStatus__OperationSuccess__UniqueKeysEntry* entry = NULL;

    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_var_new(&first_ret);

    obj = amxd_dm_findf(&dm, "test_root.");
    assert_non_null(obj);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, &args, "rel_path", "child.[Alias == \"One\"].box.");
    amxc_var_add_key(cstring_t, params, "Alias", "pizza");
    amxc_var_add_key(uint32_t, params, "Dummy", 0);
    amxc_var_add_key(uint32_t, params, "length", 3);
    amxc_var_add_key(bool, params, "liar", true);
    assert_int_equal(amxd_object_invoke_function(obj, "_add", &args, &retval), 0);
    amxc_var_dump(&retval, STDOUT_FILENO);
    amxc_var_move(first_ret, GETI_ARG(&retval, 0));
    result = amxc_var_add_key(amxc_htable_t, first_ret, "result", NULL);
    amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_OK);
    amxc_var_add_key(cstring_t, result, "requested_path", req_path);

    amxc_llist_init(&resp_list);
    amxc_llist_append(&resp_list, &first_ret->lit);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_add_resp_new(usp_tx, &resp_list, "456"), 0);
    assert_string_equal(usp_tx->msg_id, "456");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__ADD_RESP);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(usp_rx->msg->header->msg_type, USP__HEADER__MSG_TYPE__ADD_RESP);
    assert_int_equal(usp_rx->msg->body->msg_body_case, USP__BODY__MSG_BODY_RESPONSE);
    assert_int_equal(usp_rx->msg->body->response->resp_type_case, USP__RESPONSE__RESP_TYPE_ADD_RESP);

    add_resp = usp_rx->msg->body->response->add_resp;
    assert_int_equal(add_resp->n_created_obj_results, 1);
    assert_string_equal(add_resp->created_obj_results[0]->requested_path, req_path);

    status = add_resp->created_obj_results[0]->oper_status;
    assert_int_equal(status->oper_status_case, USP__ADD_RESP__CREATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS);

    success = status->oper_success;
    assert_string_equal(success->instantiated_path, "test_root.child.1.box.1.");
    assert_int_equal(success->n_param_errs, 0);
    assert_int_equal(success->n_unique_keys, 2);

    entry = success->unique_keys[0];
    assert_string_equal(entry->key, "Dummy");
    assert_string_equal(entry->value, "0");

    entry = success->unique_keys[1];
    assert_string_equal(entry->key, "Alias");
    assert_string_equal(entry->value, "pizza");

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_uspl_add_resp_invalid(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    amxc_llist_t resp_list;
    amxc_var_t reply_data;
    amxc_var_t* result = NULL;
    amxc_var_t* err_code = NULL;

    amxc_llist_init(&resp_list);
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    assert_int_equal(uspl_add_resp_new(NULL, &resp_list, "456"), -1);
    assert_int_equal(uspl_add_resp_new(usp_tx, NULL, "456"), -1);
    assert_int_equal(uspl_add_resp_new(usp_tx, &resp_list, NULL), -1);

    amxc_var_init(&reply_data);
    amxc_var_set_type(&reply_data, AMXC_VAR_ID_HTABLE);
    amxc_llist_append(&resp_list, &reply_data.lit);
    assert_int_equal(uspl_add_resp_new(usp_tx, &resp_list, "456"), -1);
    result = amxc_var_add_key(amxc_htable_t, &reply_data, "result", NULL);
    assert_int_equal(uspl_add_resp_new(usp_tx, &resp_list, "456"), -1);
    amxc_var_add_key(cstring_t, result, "requested_path", "test");
    assert_int_equal(uspl_add_resp_new(usp_tx, &resp_list, "456"), -1);
    err_code = amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_GENERAL_FAILURE);
    assert_int_equal(uspl_add_resp_new(usp_tx, &resp_list, "456"), 0);
    uspl_tx_delete(&usp_tx);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    amxc_var_set(uint32_t, err_code, USP_ERR_OK);
    assert_int_equal(uspl_add_resp_new(usp_tx, &resp_list, "456"), -1);

    amxc_var_add_key(cstring_t, &reply_data, "path", "path.to.obj");
    assert_int_equal(uspl_add_resp_new(usp_tx, &resp_list, "456"), -1);
    amxc_var_add_key(amxc_htable_t, &reply_data, "parameters", NULL);
    assert_int_equal(uspl_add_resp_new(usp_tx, &resp_list, "456"), 0);

    uspl_tx_delete(&usp_tx);
    amxc_var_clean(&reply_data);
    amxc_llist_clean(&resp_list, NULL);
}

void test_uspl_add_resp_extract(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t retval;
    amxc_var_t* params = NULL;
    amxc_var_t* result = NULL;
    amxc_var_t* copy_var = NULL;
    amxc_var_t* res = NULL;
    amxd_object_t* obj = NULL;
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    char** req_paths = calloc(2, sizeof(char*));
    req_paths[0] = "test_root.child.[Alias == \"One\"].box.";
    req_paths[1] = "test_root_2.child.1.box.";

    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_llist_init(&resp_list);

    // Add object with Alias
    amxc_var_new(&copy_var);
    obj = amxd_dm_findf(&dm, "test_root.");
    assert_non_null(obj);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, &args, "rel_path", "child.1.box.");
    amxc_var_add_key(cstring_t, params, "Alias", "second");
    amxc_var_add_key(uint32_t, params, "Dummy", 1);
    amxc_var_add_key(uint32_t, params, "length", 3);
    amxc_var_add_key(bool, params, "liar", true);
    assert_int_equal(amxd_object_invoke_function(obj, "_add", &args, &retval), 0);
    result = amxc_var_add_key(amxc_htable_t, &retval, "result", NULL);
    amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_OK);
    amxc_var_add_key(cstring_t, result, "requested_path", req_paths[0]);
    amxc_var_dump(&retval, STDOUT_FILENO);
    amxc_var_copy(copy_var, &retval);
    assert_int_equal(amxc_llist_append(&resp_list, &copy_var->lit), 0);

    // Add object without Alias
    amxc_var_new(&copy_var);
    obj = amxd_dm_findf(&dm, "test_root_2.");
    assert_non_null(obj);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, &args, "rel_path", "child.1.box.");
    amxc_var_add_key(cstring_t, params, "foo", "bar");
    amxc_var_add_key(uint32_t, params, "length", 3);
    amxc_var_add_key(bool, params, "liar", true);
    assert_int_equal(amxd_object_invoke_function(obj, "_add", &args, &retval), 0);
    result = amxc_var_add_key(amxc_htable_t, &retval, "result", NULL);
    amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_OK);
    amxc_var_add_key(cstring_t, result, "requested_path", req_paths[1]);
    amxc_var_dump(&retval, STDOUT_FILENO);
    amxc_var_copy(copy_var, &retval);
    assert_int_equal(amxc_llist_append(&resp_list, &copy_var->lit), 0);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_add_resp_new(usp_tx, &resp_list, "456"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    amxc_llist_init(&extract_list);
    assert_int_equal(uspl_add_resp_extract(usp_rx, &extract_list), 0);

    res = amxc_var_from_llist_it(amxc_llist_get_last(&extract_list));
    amxc_var_dump(res, STDOUT_FILENO);
    assert_true(test_verify_data(res, "index", "1"));
    assert_true(test_verify_data(res, "name", "1"));
    assert_true(test_verify_data(res, "object", "test_root_2.child.1.box.1."));
    assert_true(test_verify_data(res, "path", "test_root_2.child.1.box.1."));
    params = GET_ARG(res, "parameters");
    assert_non_null(params);
    assert_true(test_verify_data(params, "foo", "bar"));
    result = GET_ARG(res, "result");
    assert_non_null(result);
    assert_true(test_verify_data(result, "err_code", "0"));
    assert_true(test_verify_data(result, "requested_path", "test_root_2.child.1.box."));

    res = amxc_var_from_llist_it(amxc_llist_get_first(&extract_list));
    amxc_var_dump(res, STDOUT_FILENO);
    assert_true(test_verify_data(res, "index", "2"));
    assert_true(test_verify_data(res, "name", "second"));
    //assert_true(test_verify_data(res, "object", "test_root.child.cpe-child-1.box.second."));          // FAILURE!
    assert_true(test_verify_data(res, "path", "test_root.child.1.box.2."));
    params = GET_ARG(res, "parameters");
    assert_non_null(params);
    assert_true(test_verify_data(params, "Alias", "second"));
    assert_true(test_verify_data(params, "Dummy", "1"));
    assert_int_equal(amxc_var_type_of(GET_ARG(params, "Dummy")), AMXC_VAR_ID_UINT32);
    result = GET_ARG(res, "result");
    assert_non_null(result);
    assert_true(test_verify_data(result, "err_code", "0"));
    assert_true(test_verify_data(result, "requested_path", "test_root.child.[Alias == \"One\"].box."));

    free(req_paths);
    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&extract_list, clean_list);
    amxc_llist_clean(&resp_list, clean_list);
    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_uspl_add_resp_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_llist_t extract_list;

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_add_resp_extract(NULL, &extract_list), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_add_resp_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_add_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_add_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_add_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_add_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_add_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    assert_int_equal(uspl_add_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response = malloc(sizeof(Usp__Response));
    usp__response__init(usp_rx->msg->body->response);
    assert_int_equal(uspl_add_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response->resp_type_case = USP__RESPONSE__RESP_TYPE_GET_RESP;
    assert_int_equal(uspl_add_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response->resp_type_case = USP__RESPONSE__RESP_TYPE_ADD_RESP;
    assert_int_equal(uspl_add_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    usp_rx->msg->body->response->add_resp = malloc(sizeof(Usp__AddResp));
    usp__add_resp__init(usp_rx->msg->body->response->add_resp);
    usp_rx->msg->body->response->add_resp->n_created_obj_results = 0;
    assert_int_equal(uspl_add_resp_extract(usp_rx, &extract_list), USP_ERR_INVALID_ARGUMENTS);
    usp_rx->msg->body->response->add_resp->n_created_obj_results = 1;
    usp_rx->msg->body->response->add_resp->created_obj_results = NULL;
    assert_int_equal(uspl_add_resp_extract(usp_rx, &extract_list), USP_ERR_INVALID_ARGUMENTS);

    uspl_rx_delete(&usp_rx);
}

void test_uspl_add_resp_partial(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret_1;
    amxc_var_t ret_2;
    amxc_var_t* params = NULL;
    amxc_var_t* result = NULL;
    amxd_object_t* obj = NULL;
    char* req_path = "test_root.child.";
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;

    amxc_llist_init(&extract_list);
    amxc_llist_init(&resp_list);
    amxc_var_init(&args);
    amxc_var_init(&ret_1);
    amxc_var_init(&ret_2);

    obj = amxd_dm_findf(&dm, req_path);
    assert_non_null(obj);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, params, "Alias", "unique");
    assert_int_equal(amxd_object_invoke_function(obj, "_add", &args, &ret_1), 0);
    amxc_var_dump(&ret_1, STDOUT_FILENO);
    result = amxc_var_add_key(amxc_htable_t, &ret_1, "result", NULL);
    amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_OK);
    amxc_var_add_key(cstring_t, result, "requested_path", req_path);
    amxc_llist_append(&resp_list, &ret_1.lit);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, params, "Alias", "unique");
    assert_int_not_equal(amxd_object_invoke_function(obj, "_add", &args, &ret_2), 0);
    amxc_var_dump(&ret_2, STDOUT_FILENO);
    amxc_var_set_type(&ret_2, AMXC_VAR_ID_HTABLE);
    result = amxc_var_add_key(amxc_htable_t, &ret_2, "result", NULL);
    amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_UNIQUE_KEY_CONFLICT);
    amxc_var_add_key(cstring_t, result, "requested_path", req_path);
    amxc_llist_append(&resp_list, &ret_2.lit);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_add_resp_new(usp_tx, &resp_list, "456"), 0);
    assert_string_equal(usp_tx->msg_id, "456");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__ADD_RESP);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(uspl_add_resp_extract(usp_rx, &extract_list), 0);
    assert_int_equal(amxc_llist_size(&extract_list), 2);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&extract_list));
    amxc_var_dump(result, STDOUT_FILENO);
    assert_int_equal(GETP_UINT32(result, "result.err_code"), 0);

    result = amxc_var_from_llist_it(amxc_llist_get_last(&extract_list));
    amxc_var_dump(result, STDOUT_FILENO);
    assert_int_equal(GETP_UINT32(result, "result.err_code"), USP_ERR_UNIQUE_KEY_CONFLICT);

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, NULL);
    amxc_llist_clean(&extract_list, variant_list_it_free);
    amxc_var_clean(&args);
    amxc_var_clean(&ret_1);
    amxc_var_clean(&ret_2);
}
