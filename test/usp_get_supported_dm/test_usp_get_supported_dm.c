/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdio.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_function.h>
#include <amxo/amxo.h>
#include <amxb/amxb_types.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_get_supported_dm.h"
#include "usp/uspl_msghandler.h"
#include "uspl_private.h"
#include "test_usp_get_supported_dm.h"

static amxo_parser_t parser;
static const char* odl_defs = "test_dm.odl";

static amxd_dm_t dm;

static void free_resp_list(amxc_llist_it_t* it) {
    amxc_var_t* data = amxc_llist_it_get_data(it, amxc_var_t, lit);
    amxc_var_delete(&data);
}

int test_dm_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), 0);

    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    assert_int_equal(amxo_parser_invoke_entry_points(&parser, &dm, AMXO_START), 0);

    return 0;
}

int test_dm_teardown(UNUSED void** state) {
    assert_int_equal(amxo_parser_invoke_entry_points(&parser, &dm, AMXO_STOP), 0);

    amxo_parser_clean(&parser);

    amxo_resolver_import_close_all();

    amxd_dm_clean(&dm);

    return 0;
}

void test_uspl_get_supported_dm_new(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    char* path = "LocalAgent.";
    amxc_var_t request;
    amxc_var_t* flags;
    amxc_var_t* paths;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    flags = amxc_var_add_key(amxc_htable_t, &request, "flags", NULL);
    amxc_var_add_key(bool, flags, "first_level_only", true);
    amxc_var_add_key(bool, flags, "return_events", true);
    paths = amxc_var_add_key(amxc_llist_t, &request, "paths", NULL);
    amxc_var_add(cstring_t, paths, path);

    assert_int_equal(uspl_get_supported_dm_new(NULL, NULL), -1);
    assert_int_equal(uspl_get_supported_dm_new(usp_tx, NULL), -1);
    assert_int_equal(uspl_get_supported_dm_new(usp_tx, &request), 0);
    assert_string_equal(usp_tx->msg_id, "0");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(usp_rx->msg->body->request->get_supported_dm->n_obj_paths, 1);
    assert_string_equal(usp_rx->msg->body->request->get_supported_dm->obj_paths[0], path);
    assert_true(usp_rx->msg->body->request->get_supported_dm->first_level_only);
    assert_false(usp_rx->msg->body->request->get_supported_dm->return_commands);
    assert_true(usp_rx->msg->body->request->get_supported_dm->return_events);
    assert_false(usp_rx->msg->body->request->get_supported_dm->return_params);

    amxc_var_clean(&request);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_get_supported_dm_extract(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    char* path = "LocalAgent.";
    const char* path_res = NULL;
    amxc_var_t request;
    amxc_var_t result;
    amxc_var_t* flags;
    amxc_var_t* paths;
    amxc_var_t* path_var;
    const amxc_llist_t* paths_list = NULL;

    // First test
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    flags = amxc_var_add_key(amxc_htable_t, &request, "flags", NULL);
    amxc_var_add_key(bool, flags, "return_events", true);
    amxc_var_add_key(bool, flags, "return_params", true);
    amxc_var_add_key(bool, flags, "return_commands", true);
    paths = amxc_var_add_key(amxc_llist_t, &request, "paths", NULL);
    amxc_var_add(cstring_t, paths, path);

    assert_int_equal(uspl_get_supported_dm_new(usp_tx, &request), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Extract data from protbuf and validate if everything is as expected
    amxc_var_init(&result);
    assert_int_equal(uspl_get_supported_dm_extract(usp_rx, &result), 0);

    paths = GET_ARG(&result, "paths");
    assert_non_null(paths);
    paths_list = amxc_var_constcast(amxc_llist_t, paths);
    path_var = amxc_var_from_llist_it(amxc_llist_get_first(paths_list));
    path_res = amxc_var_constcast(cstring_t, path_var);
    assert_string_equal(path_res, path);

    flags = GET_ARG(&result, "flags");
    assert_non_null(flags);
    assert_false(GET_BOOL(flags, "first_level_only"));
    assert_true(GET_BOOL(flags, "return_events"));
    assert_true(GET_BOOL(flags, "return_params"));
    assert_true(GET_BOOL(flags, "return_commands"));

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);

    // Second test
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    flags = amxc_var_add_key(amxc_htable_t, &request, "flags", NULL);
    amxc_var_add_key(bool, flags, "first_level_only", true);
    paths = amxc_var_add_key(amxc_llist_t, &request, "paths", NULL);
    amxc_var_add(cstring_t, paths, path);

    assert_int_equal(uspl_get_supported_dm_new(usp_tx, &request), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Extract data from protbuf and validate if everything is as expected
    assert_int_equal(uspl_get_supported_dm_extract(usp_rx, &result), 0);

    paths = GET_ARG(&result, "paths");
    assert_non_null(paths);
    paths_list = amxc_var_constcast(amxc_llist_t, paths);
    path_var = amxc_var_from_llist_it(amxc_llist_get_first(paths_list));
    path_res = amxc_var_constcast(cstring_t, path_var);
    assert_string_equal(path_res, path);

    flags = GET_ARG(&result, "flags");
    assert_non_null(flags);
    assert_true(GET_BOOL(flags, "first_level_only"));
    assert_false(GET_BOOL(flags, "return_events"));
    assert_false(GET_BOOL(flags, "return_params"));
    assert_false(GET_BOOL(flags, "return_commands"));

    amxc_var_clean(&result);
    amxc_var_clean(&request);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_get_supported_dm_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t result;

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_get_supported_dm_extract(NULL, NULL), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_get_supported_dm_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_get_supported_dm_extract(usp_rx, &result), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_get_supported_dm_extract(usp_rx, &result), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_get_supported_dm_extract(usp_rx, &result), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_get_supported_dm_extract(usp_rx, &result), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    assert_int_equal(uspl_get_supported_dm_extract(usp_rx, &result), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_get_supported_dm_extract(usp_rx, &result), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request = malloc(sizeof(Usp__Request));
    usp__request__init(usp_rx->msg->body->request);
    assert_int_equal(uspl_get_supported_dm_extract(usp_rx, &result), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request->req_type_case = USP__REQUEST__REQ_TYPE_GET;
    assert_int_equal(uspl_get_supported_dm_extract(usp_rx, &result), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request->req_type_case = USP__REQUEST__REQ_TYPE_GET_SUPPORTED_DM;
    assert_int_equal(uspl_get_supported_dm_extract(usp_rx, &result), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    usp_rx->msg->body->response->get_supported_dm_resp = malloc(sizeof(Usp__GetSupportedDM));
    usp__get_supported_dm__init(usp_rx->msg->body->request->get_supported_dm);
    assert_int_equal(uspl_get_supported_dm_extract(usp_rx, &result), USP_ERR_INVALID_ARGUMENTS);
    usp_rx->msg->body->response->get_supported_dm_resp->n_req_obj_results = 1;
    assert_int_equal(uspl_get_supported_dm_extract(usp_rx, &result), USP_ERR_INVALID_ARGUMENTS);

    uspl_rx_delete(&usp_rx);
}

void test_uspl_get_supported_dm_resp_new(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxd_object_t* object = NULL;
    amxc_var_t retval;
    amxc_var_t args;
    amxc_var_t* resolved = NULL;
    amxc_var_t* result_var = NULL;
    amxc_llist_t resp_list;
    char** req_paths = calloc(2, sizeof(char*));
    req_paths[0] = "LocalAgent.";
    req_paths[1] = "MQTT.";
    Usp__GetSupportedDMResp* gs_resp = NULL;
    Usp__GetSupportedDMResp__RequestedObjectResult* ror = NULL;
    Usp__GetSupportedDMResp__SupportedObjectResult* sor = NULL;

    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_llist_init(&resp_list);

    // First path
    object = amxd_dm_findf(&dm, req_paths[0]);
    assert_non_null(object);

    amxc_var_new(&resolved);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "functions", true);
    amxc_var_add_key(bool, &args, "parameters", true);
    amxc_var_add_key(bool, &args, "events", true);
    amxc_var_add_key(bool, &args, "first_level_only", false);
    amxc_var_add_key(bool, &args, "access", AMXB_PUBLIC);
    assert_int_equal(amxd_object_invoke_function(object, "_get_supported", &args, &retval), 0);
    amxc_var_dump(&retval, STDOUT_FILENO);
    amxc_var_copy(resolved, &retval);

    // Add requested path and error information
    result_var = amxc_var_add_key(amxc_htable_t, resolved, "result", NULL);
    amxc_var_add_key(uint32_t, result_var, "err_code", 0);
    amxc_var_add_key(cstring_t, result_var, "requested_path", req_paths[0]);
    amxc_llist_append(&resp_list, &resolved->lit);

    // Second path
    object = amxd_dm_findf(&dm, req_paths[1]);
    assert_non_null(object);

    amxc_var_new(&resolved);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "functions", false);
    amxc_var_add_key(bool, &args, "parameters", true);
    amxc_var_add_key(bool, &args, "events", false);
    amxc_var_add_key(bool, &args, "first_level_only", true);
    amxc_var_add_key(bool, &args, "access", AMXB_PUBLIC);
    assert_int_equal(amxd_object_invoke_function(object, "_get_supported", &args, &retval), 0);
    amxc_var_dump(&retval, STDOUT_FILENO);
    amxc_var_copy(resolved, &retval);

    // Add requested path and error information
    result_var = amxc_var_add_key(amxc_htable_t, resolved, "result", NULL);
    amxc_var_add_key(uint32_t, result_var, "err_code", 0);
    amxc_var_add_key(cstring_t, result_var, "requested_path", req_paths[1]);
    amxc_llist_append(&resp_list, &resolved->lit);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_get_supported_dm_resp_new(usp_tx, &resp_list, "123"), 0);
    assert_string_equal(usp_tx->msg_id, "123");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM_RESP);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(usp_rx->msg->header->msg_type, USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM_RESP);
    assert_int_equal(usp_rx->msg->body->msg_body_case, USP__BODY__MSG_BODY_RESPONSE);
    assert_int_equal(usp_rx->msg->body->response->resp_type_case, USP__RESPONSE__RESP_TYPE_GET_SUPPORTED_DM_RESP);
    gs_resp = usp_rx->msg->body->response->get_supported_dm_resp;

    assert_int_equal(gs_resp->n_req_obj_results, 2);
    ror = gs_resp->req_obj_results[0];
    assert_string_equal(ror->data_model_inst_uri, "urn:broadband-forum-org:tr-181-2-16-0");
    assert_int_equal(ror->err_code, USP_ERR_OK);
    assert_string_equal(ror->err_msg, "");
    assert_string_equal(ror->req_obj_path, req_paths[0]);
    assert_int_equal(ror->n_supported_objs, 2);

    sor = ror->supported_objs[0];
    assert_string_equal(sor->supported_obj_path, "LocalAgent.MTP.{i}.");
    assert_true(sor->is_multi_instance);
    assert_int_equal(sor->access, USP__GET_SUPPORTED_DMRESP__OBJ_ACCESS_TYPE__OBJ_ADD_DELETE);
    assert_int_equal(sor->n_supported_commands, 0);
    assert_int_equal(sor->n_supported_events, 0);
    assert_int_equal(sor->n_supported_params, 2);
    for(int i = 0; i < 2; i++) {
        if(strcmp(sor->supported_params[i]->param_name, "Alias") == 0) {
            assert_int_equal(sor->supported_params[i]->access, USP__GET_SUPPORTED_DMRESP__PARAM_ACCESS_TYPE__PARAM_READ_WRITE);
            assert_int_equal(sor->supported_params[i]->value_change, USP__GET_SUPPORTED_DMRESP__VALUE_CHANGE_TYPE__VALUE_CHANGE_ALLOWED);
            assert_int_equal(sor->supported_params[i]->value_type, USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_STRING);
        } else if(strcmp(sor->supported_params[i]->param_name, "Enable") == 0) {
            assert_int_equal(sor->supported_params[i]->access, USP__GET_SUPPORTED_DMRESP__PARAM_ACCESS_TYPE__PARAM_READ_WRITE);
            assert_int_equal(sor->supported_params[i]->value_change, USP__GET_SUPPORTED_DMRESP__VALUE_CHANGE_TYPE__VALUE_CHANGE_ALLOWED);
            assert_int_equal(sor->supported_params[i]->value_type, USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_BOOLEAN);
        } else {
            assert(0);
        }
    }

    sor = ror->supported_objs[1];
    assert_string_equal(sor->supported_obj_path, "LocalAgent.");
    assert_false(sor->is_multi_instance);
    assert_int_equal(sor->access, USP__GET_SUPPORTED_DMRESP__OBJ_ACCESS_TYPE__OBJ_READ_ONLY);
    assert_int_equal(sor->n_supported_commands, 2);
    //assert_int_equal(sor->n_supported_events, 2); // events are not yet obtained from the dm
    assert_int_equal(sor->n_supported_params, 2); // one private param is not detected

    for(int i = 0; i < 2; i++) {
        if(strcmp(sor->supported_commands[i]->command_name, "AddCertificate()") == 0) {
            assert_int_equal(sor->supported_commands[i]->n_input_arg_names, 2);
            assert_int_equal(sor->supported_commands[i]->n_output_arg_names, 0);
            assert_int_equal(sor->supported_commands[i]->command_type, USP__GET_SUPPORTED_DMRESP__CMD_TYPE__CMD_SYNC);
        } else if(strcmp(sor->supported_commands[i]->command_name, "DummyFunction()") == 0) {
            assert_int_equal(sor->supported_commands[i]->n_input_arg_names, 1);
            assert_int_equal(sor->supported_commands[i]->n_output_arg_names, 1);
            assert_int_equal(sor->supported_commands[i]->command_type, USP__GET_SUPPORTED_DMRESP__CMD_TYPE__CMD_ASYNC);
        } else {
            assert(0);
        }
    }

    assert_int_equal(sor->n_supported_events, 3);

    // Second request path
    ror = gs_resp->req_obj_results[1];
    assert_int_equal(ror->err_code, USP_ERR_OK);
    assert_string_equal(ror->err_msg, "");
    assert_string_equal(ror->req_obj_path, req_paths[1]);
    assert_int_equal(ror->n_supported_objs, 2);

    sor = ror->supported_objs[0];
    assert_string_equal(sor->supported_obj_path, "MQTT.");
    assert_false(sor->is_multi_instance);
    assert_int_equal(sor->access, USP__GET_SUPPORTED_DMRESP__OBJ_ACCESS_TYPE__OBJ_READ_ONLY);
    assert_int_equal(sor->n_supported_commands, 0);
    assert_int_equal(sor->n_supported_events, 0);
    assert_int_equal(sor->n_supported_params, 1);

    assert_string_equal(sor->supported_params[0]->param_name, "BrokerNumberOfEntries");
    assert_int_equal(sor->supported_params[0]->access, USP__GET_SUPPORTED_DMRESP__PARAM_ACCESS_TYPE__PARAM_READ_WRITE);
    assert_int_equal(sor->supported_params[0]->value_change, USP__GET_SUPPORTED_DMRESP__VALUE_CHANGE_TYPE__VALUE_CHANGE_ALLOWED);
    assert_int_equal(sor->supported_params[0]->value_type, USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_UNSIGNED_INT);

    sor = ror->supported_objs[1];
    assert_string_equal(sor->supported_obj_path, "MQTT.Client.{i}.");
    assert_true(sor->is_multi_instance);
    assert_int_equal(sor->access, USP__GET_SUPPORTED_DMRESP__OBJ_ACCESS_TYPE__OBJ_ADD_DELETE);
    assert_int_equal(sor->n_supported_commands, 0);
    assert_int_equal(sor->n_supported_events, 0);
    assert_int_equal(sor->n_supported_params, 0);

    free(req_paths);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    amxc_llist_clean(&resp_list, free_resp_list);
    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_uspl_get_supported_dm_resp_new_invalid(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_llist_t resp_list;
    amxc_var_t args;
    amxc_var_t* result_var = NULL;
    Usp__GetSupportedDMResp* gs_resp = NULL;
    Usp__GetSupportedDMResp__RequestedObjectResult* ror = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    amxc_llist_init(&resp_list);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    result_var = amxc_var_add_key(amxc_htable_t, &args, "result", NULL);
    amxc_var_add_key(uint32_t, result_var, "err_code", USP_ERR_INVALID_PATH);
    amxc_var_add_key(cstring_t, result_var, "requested_path", "foo");
    amxc_llist_append(&resp_list, &args.lit);

    assert_int_equal(uspl_get_supported_dm_resp_new(NULL, &resp_list, "123"), -1);
    assert_int_equal(uspl_get_supported_dm_resp_new(usp_tx, NULL, "123"), -1);
    assert_int_equal(uspl_get_supported_dm_resp_new(usp_tx, &resp_list, NULL), -1);

    assert_int_equal(uspl_get_supported_dm_resp_new(usp_tx, &resp_list, "123"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(usp_rx->msg->header->msg_type, USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM_RESP);
    assert_int_equal(usp_rx->msg->body->msg_body_case, USP__BODY__MSG_BODY_RESPONSE);
    assert_int_equal(usp_rx->msg->body->response->resp_type_case, USP__RESPONSE__RESP_TYPE_GET_SUPPORTED_DM_RESP);
    gs_resp = usp_rx->msg->body->response->get_supported_dm_resp;

    assert_int_equal(gs_resp->n_req_obj_results, 1);
    ror = gs_resp->req_obj_results[0];
    assert_int_equal(ror->err_code, USP_ERR_INVALID_PATH);
    assert_string_equal(ror->err_msg, "Invalid path");
    assert_int_equal(ror->n_supported_objs, 0);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    amxc_var_clean(&args);
    amxc_llist_clean(&resp_list, NULL);
}

void test_uspl_get_supported_dm_resp_extract(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxd_object_t* object = NULL;
    amxc_var_t retval;
    amxc_var_t args;
    amxc_var_t* resolved = NULL;
    amxc_var_t* entry = NULL;
    amxc_var_t* result_var = NULL;
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;
    amxc_llist_it_t* obj_it = NULL;
    char** req_paths = calloc(2, sizeof(char*));
    req_paths[0] = "LocalAgent.";
    amxc_var_t* obj_var = NULL;
    const amxc_llist_t* cmd_list = NULL;
    const amxc_llist_t* param_list = NULL;
    const amxc_htable_t* event_table = NULL;

    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_llist_init(&resp_list);

    object = amxd_dm_findf(&dm, req_paths[0]);
    assert_non_null(object);

    amxc_var_new(&resolved);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "functions", true);
    amxc_var_add_key(bool, &args, "parameters", true);
    amxc_var_add_key(bool, &args, "events", true);
    amxc_var_add_key(bool, &args, "first_level_only", false);
    amxc_var_add_key(bool, &args, "access", AMXB_PUBLIC);
    assert_int_equal(amxd_object_invoke_function(object, "_get_supported", &args, &retval), 0);
    amxc_var_copy(resolved, &retval);

    // Add requested path and error information
    result_var = amxc_var_add_key(amxc_htable_t, resolved, "result", NULL);
    amxc_var_add_key(uint32_t, result_var, "err_code", 0);
    amxc_var_add_key(cstring_t, result_var, "requested_path", req_paths[0]);
    amxc_llist_append(&resp_list, &resolved->lit);
    amxc_var_dump(resolved, STDOUT_FILENO);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_get_supported_dm_resp_new(usp_tx, &resp_list, "123"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    amxc_llist_init(&extract_list);
    uspl_get_supported_dm_resp_extract(usp_rx, &extract_list);

    obj_it = amxc_llist_get_first(&extract_list);
    assert_non_null(obj_it);
    entry = amxc_var_from_llist_it(obj_it);
    assert_non_null(entry);
    amxc_var_dump(entry, STDOUT_FILENO);

    // Verify variant fields
    assert_string_equal(GETP_CHAR(entry, "result.requested_path"), "LocalAgent.");

    obj_var = GET_ARG(entry, "LocalAgent.");
    assert_non_null(obj_var);
    assert_int_equal(GET_UINT32(obj_var, "access"), USP__GET_SUPPORTED_DMRESP__OBJ_ACCESS_TYPE__OBJ_READ_ONLY);
    assert_false(GET_BOOL(obj_var, "is_multi_instance"));

    cmd_list = amxc_var_constcast(amxc_llist_t, GET_ARG(obj_var, "supported_commands"));
    amxc_llist_iterate(it, cmd_list) {
        amxc_var_t* cmd = amxc_var_from_llist_it(it);
        const char* cmd_name = GET_CHAR(cmd, "command_name");
        if(strcmp(cmd_name, "AddCertificate()") == 0) {
            const amxc_llist_t* in_list = amxc_var_constcast(amxc_llist_t, GET_ARG(cmd, "input_arg_names"));
            const amxc_llist_t* out_list = amxc_var_constcast(amxc_llist_t, GET_ARG(cmd, "output_arg_names"));
            assert_int_equal(amxc_llist_size(in_list), 2);
            assert_int_equal(amxc_llist_size(out_list), 0);
            assert_int_equal(GET_UINT32(cmd, "command_type"), USP__GET_SUPPORTED_DMRESP__CMD_TYPE__CMD_SYNC);
        } else if(strcmp(cmd_name, "DummyFunction()") == 0) {
            const amxc_llist_t* in_list = amxc_var_constcast(amxc_llist_t, GET_ARG(cmd, "input_arg_names"));
            const amxc_llist_t* out_list = amxc_var_constcast(amxc_llist_t, GET_ARG(cmd, "output_arg_names"));
            assert_int_equal(amxc_llist_size(in_list), 1);
            assert_int_equal(amxc_llist_size(out_list), 1);
            assert_int_equal(GET_UINT32(cmd, "command_type"), USP__GET_SUPPORTED_DMRESP__CMD_TYPE__CMD_ASYNC);
        } else {
            assert(0);
        }
    }

    event_table = amxc_var_constcast(amxc_htable_t, GET_ARG(obj_var, "supported_events"));
    assert_int_equal(amxc_htable_size(event_table), 3);
    amxc_htable_for_each(hit, event_table) {
        amxc_var_t* event = amxc_var_from_htable_it(hit);
        const amxc_llist_t* arg_list = amxc_var_constcast(amxc_llist_t, event);
        const char* event_name = amxc_var_key(event);
        if(strcmp(event_name, "TransferComplete") == 0) {
            assert_int_equal(amxc_llist_size(arg_list), 10);
        } else {
            assert_int_equal(amxc_llist_size(arg_list), 0);
        }
    }

    param_list = amxc_var_constcast(amxc_llist_t, GET_ARG(obj_var, "supported_params"));
    amxc_llist_iterate(it, param_list) {
        amxc_var_t* param = amxc_var_from_llist_it(it);
        const char* param_name = GET_CHAR(param, "param_name");
        if(strcmp(param_name, "EndpointID") == 0) {
            assert_int_equal(GET_UINT32(param, "access"), 1);
            assert_int_equal(GET_UINT32(param, "type"), AMXC_VAR_ID_CSTRING);
            assert_int_equal(GET_UINT32(param, "value_change"), 1);
        } else if(strcmp(param_name, "SoftwareVersion") == 0) {
            assert_int_equal(GET_UINT32(param, "access"), 0);
            assert_int_equal(GET_UINT32(param, "type"), AMXC_VAR_ID_CSTRING);
            assert_int_equal(GET_UINT32(param, "value_change"), 1);
        } else {
            assert(0);
        }
    }

    obj_var = GET_ARG(entry, "LocalAgent.MTP.{i}.");
    assert_non_null(obj_var);
    assert_int_equal(GET_UINT32(obj_var, "access"), USP__GET_SUPPORTED_DMRESP__OBJ_ACCESS_TYPE__OBJ_ADD_DELETE);
    assert_true(GET_BOOL(obj_var, "is_multi_instance"));

    free(req_paths);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    amxc_llist_clean(&resp_list, free_resp_list);
    amxc_llist_clean(&extract_list, free_resp_list);
    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_uspl_get_supported_dm_resp_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_llist_t extract_list;

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_get_supported_dm_resp_extract(NULL, &extract_list), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_get_supported_dm_resp_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_get_supported_dm_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_get_supported_dm_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_get_supported_dm_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_get_supported_dm_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_get_supported_dm_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    assert_int_equal(uspl_get_supported_dm_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response = malloc(sizeof(Usp__Response));
    usp__response__init(usp_rx->msg->body->response);
    assert_int_equal(uspl_get_supported_dm_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response->resp_type_case = USP__RESPONSE__RESP_TYPE_GET_RESP;
    assert_int_equal(uspl_get_supported_dm_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response->resp_type_case = USP__RESPONSE__RESP_TYPE_GET_SUPPORTED_DM_RESP;
    assert_int_equal(uspl_get_supported_dm_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    usp_rx->msg->body->response->get_supported_dm_resp = malloc(sizeof(Usp__GetSupportedDMResp));
    usp__get_supported_dmresp__init(usp_rx->msg->body->response->get_supported_dm_resp);
    usp_rx->msg->body->response->get_supported_dm_resp->req_obj_results = NULL;
    usp_rx->msg->body->response->get_supported_dm_resp->n_req_obj_results = 99;
    assert_int_equal(uspl_get_supported_dm_resp_extract(usp_rx, &extract_list), USP_ERR_INVALID_ARGUMENTS);

    uspl_rx_delete(&usp_rx);
}