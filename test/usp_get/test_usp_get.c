/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_function.h>

#include <amxo/amxo.h>

#include "uspl_private.h"
#include "test_usp_get.h"
#include "usp/uspl_get.h"
#include "usp/usp_err_codes.h"

static amxo_parser_t parser;
static const char* odl_defs = "test_dm.odl";
static amxd_dm_t dm;

static bool test_verify_data(const amxc_var_t* data, const char* field, const char* value) {
    bool rv = false;
    char* field_value = NULL;
    amxc_var_t* field_data = GETP_ARG(data, field);

    printf("Verify event data: check field [%s] contains [%s]\n", field, value);
    fflush(stdout);
    assert_non_null(field_data);

    field_value = amxc_var_dyncast(cstring_t, field_data);
    assert_non_null(field_data);

    rv = (strcmp(field_value, value) == 0);

    free(field_value);
    return rv;
}

void test_uspl_get_new(UNUSED void** state) {
    uspl_tx_t* usp = NULL;
    amxc_var_t request;
    amxc_var_t* paths = NULL;
    uint32_t max_depth = 10;

    assert_int_equal(uspl_tx_new(&usp, "lpointid", "rpointid"), 0);

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    paths = amxc_var_add_key(amxc_llist_t, &request, "paths", NULL);
    amxc_var_add(cstring_t, paths, "foo");
    amxc_var_add(cstring_t, paths, "bar");
    amxc_var_add_key(uint32_t, &request, "max_depth", max_depth);

    assert_int_equal(uspl_get_new(NULL, NULL), -1);
    assert_int_equal(uspl_get_new(usp, NULL), -1);
    assert_int_equal(uspl_get_new(NULL, &request), -1);
    assert_int_equal(uspl_get_new(usp, &request), 0);
    assert_string_equal(usp->msg_id, "0");
    assert_int_equal(usp->msg_type, USP__HEADER__MSG_TYPE__GET);

    uspl_rx_t* usp_rx = uspl_msghandler_unpack_protobuf(usp->pbuf, usp->pbuf_len);
    assert_int_equal(usp_rx->msg->body->request->get->n_param_paths, 2);
    assert_string_equal(usp_rx->msg->body->request->get->param_paths[0], "foo");
    assert_string_equal(usp_rx->msg->body->request->get->param_paths[1], "bar");
    assert_int_equal(usp_rx->msg->body->request->get->max_depth, max_depth);

    amxc_var_clean(&request);
    uspl_tx_delete(&usp);
    uspl_rx_delete(&usp_rx);
}

void test_uspl_get_extract(UNUSED void** state) {
    uspl_tx_t* usp = NULL;
    amxc_var_t request;
    amxc_var_t result;
    amxc_var_t* paths = NULL;
    const amxc_llist_t* res_list = NULL;
    const char* path1 = NULL;
    const char* path2 = NULL;
    uint32_t max_depth = 10;

    assert_int_equal(uspl_tx_new(&usp, "lpointid", "rpointid"), 0);

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    paths = amxc_var_add_key(amxc_llist_t, &request, "paths", NULL);
    amxc_var_add(cstring_t, paths, "foo");
    amxc_var_add(cstring_t, paths, "bar");
    amxc_var_add_key(uint32_t, &request, "max_depth", max_depth);

    assert_int_equal(uspl_get_new(usp, &request), 0);

    uspl_rx_t* usp_rx = uspl_msghandler_unpack_protobuf(usp->pbuf, usp->pbuf_len);

    amxc_var_init(&result);
    assert_int_equal(uspl_get_extract(usp_rx, &result), 0);
    res_list = amxc_var_constcast(amxc_llist_t, GET_ARG(&result, "paths"));

    amxc_llist_it_t* it1 = amxc_llist_get_first(res_list);
    path1 = amxc_var_constcast(cstring_t, amxc_var_from_llist_it(it1));
    amxc_llist_it_t* it2 = amxc_llist_it_get_next(it1);
    path2 = amxc_var_constcast(cstring_t, amxc_var_from_llist_it(it2));
    assert_string_equal(path1, "foo");
    assert_string_equal(path2, "bar");
    assert_int_equal(GET_UINT32(&result, "max_depth"), max_depth);

    amxc_var_clean(&result);
    amxc_var_clean(&request);
    uspl_tx_delete(&usp);
    uspl_rx_delete(&usp_rx);
}

void test_uspl_get_resp_new(UNUSED void** state) {
    amxd_object_t* root = NULL;
    assert_int_equal(amxd_dm_init(&dm), 0);
    assert_int_equal(amxo_parser_init(&parser), 0);
    root = amxd_dm_get_root(&dm);
    assert_non_null(root);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root), 0);

    uspl_tx_t* usp = NULL;
    int num_path_expr = 5;
    char** path_exprs = (char**) malloc(num_path_expr * sizeof(char*));
    char* path0 = "test_root.child.[child_param_1 == \"red\"].";
    char* path1 = "test_root.child.[child_param_2 == \"white\"].";
    char* path2 = "test_root.child.1.";
    char* path3 = "test_root.";
    char* path4 = "test_root.child.2.child_param_2";
    path_exprs[0] = path0;
    path_exprs[1] = path1;
    path_exprs[2] = path2;
    path_exprs[3] = path3;
    path_exprs[4] = path4;

    amxc_llist_t resp_list;
    amxc_llist_init(&resp_list);
    for(int i = 0; i < num_path_expr; i++) {
        amxc_var_t args;
        amxc_var_t rv;
        amxc_var_t* resolved = NULL;
        amxc_var_init(&args);
        amxc_var_init(&rv);
        amxc_var_new(&resolved);
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, "rel_path", path_exprs[i]);
        amxc_var_add_key(int32_t, &args, "depth", INT32_MAX);
        int retval = amxd_object_invoke_function(root, "_get", &args, &rv);
        assert_int_equal(retval, 0);

        // Add result variant
        amxc_var_t* result = amxc_var_add_key(amxc_htable_t, &rv, "result", NULL);
        amxc_var_add_key(cstring_t, result, "requested_path", path_exprs[i]);
        amxc_var_add_key(int64_t, result, "err_code", retval);
        amxc_var_dump(&rv, STDOUT_FILENO);

        amxc_var_copy(resolved, &rv);
        amxc_llist_append(&resp_list, &resolved->lit);
        amxc_var_clean(&args);
        amxc_var_clean(&rv);
    }

    /* create an error in the third entry of resp_list */

    uspl_tx_new(&usp, "sender", "receiver");

    assert_int_equal(uspl_get_resp_new(NULL, NULL, NULL), -1);
    assert_int_equal(uspl_get_resp_new(usp, NULL, NULL), -1);
    assert_int_equal(uspl_get_resp_new(usp, &resp_list, NULL), -1);
    assert_int_equal(uspl_get_resp_new(usp, &resp_list, "123"), 0);
    assert_string_equal(usp->msg_id, "123");
    assert_int_equal(usp->msg_type, USP__HEADER__MSG_TYPE__GET_RESP);

    uspl_rx_t* usp_rx = uspl_msghandler_unpack_protobuf(usp->pbuf, usp->pbuf_len);
    assert_non_null(usp_rx);
    assert_int_equal(usp_rx->msg->body->response->get_resp->n_req_path_results, num_path_expr);
    /* first requested path */
    Usp__GetResp__RequestedPathResult* req_path = usp_rx->msg->body->response->get_resp->req_path_results[0];
    assert_string_equal(req_path->requested_path, path_exprs[0]);
    assert_int_equal(req_path->n_resolved_path_results, 2);
    assert_string_equal(req_path->resolved_path_results[0]->resolved_path, "test_root.child.1.");
    assert_string_equal(req_path->resolved_path_results[1]->resolved_path, "test_root.child.2.");
    assert_int_equal(req_path->resolved_path_results[0]->n_result_params, 4);
    assert_string_equal(req_path->resolved_path_results[0]->result_params[0]->key, "boolean");
    assert_string_equal(req_path->resolved_path_results[0]->result_params[0]->value, "true");
    assert_string_equal(req_path->resolved_path_results[0]->result_params[1]->key, "child_param_1");
    assert_string_equal(req_path->resolved_path_results[0]->result_params[1]->value, "red");
    assert_string_equal(req_path->resolved_path_results[0]->result_params[2]->key, "child_param_2");
    assert_string_equal(req_path->resolved_path_results[0]->result_params[2]->value, "yellow");
    assert_string_equal(req_path->resolved_path_results[0]->result_params[3]->key, "child_param_3");
    assert_string_equal(req_path->resolved_path_results[0]->result_params[3]->value, "black");
    assert_int_equal(req_path->resolved_path_results[1]->n_result_params, 4);
    assert_string_equal(req_path->resolved_path_results[1]->result_params[0]->key, "boolean");
    assert_string_equal(req_path->resolved_path_results[1]->result_params[0]->value, "false");
    assert_string_equal(req_path->resolved_path_results[1]->result_params[1]->key, "child_param_1");
    assert_string_equal(req_path->resolved_path_results[1]->result_params[1]->value, "red");
    assert_string_equal(req_path->resolved_path_results[1]->result_params[2]->key, "child_param_2");
    assert_string_equal(req_path->resolved_path_results[1]->result_params[2]->value, "white");
    assert_string_equal(req_path->resolved_path_results[1]->result_params[3]->key, "child_param_3");
    assert_string_equal(req_path->resolved_path_results[1]->result_params[3]->value, "red");
    /* second requested path */
    req_path = usp_rx->msg->body->response->get_resp->req_path_results[1];
    assert_string_equal(req_path->requested_path, path_exprs[1]);
    assert_int_equal(req_path->n_resolved_path_results, 1);
    assert_string_equal(req_path->resolved_path_results[0]->resolved_path, "test_root.child.2.");
    assert_int_equal(req_path->resolved_path_results[0]->n_result_params, 4);
    assert_string_equal(req_path->resolved_path_results[0]->result_params[0]->key, "boolean");
    assert_string_equal(req_path->resolved_path_results[0]->result_params[0]->value, "false");
    assert_string_equal(req_path->resolved_path_results[0]->result_params[1]->key, "child_param_1");
    assert_string_equal(req_path->resolved_path_results[0]->result_params[1]->value, "red");
    assert_string_equal(req_path->resolved_path_results[0]->result_params[2]->key, "child_param_2");
    assert_string_equal(req_path->resolved_path_results[0]->result_params[2]->value, "white");
    assert_string_equal(req_path->resolved_path_results[0]->result_params[3]->key, "child_param_3");
    assert_string_equal(req_path->resolved_path_results[0]->result_params[3]->value, "red");
    /* fifth requested path */
    req_path = usp_rx->msg->body->response->get_resp->req_path_results[4];
    assert_string_equal(req_path->requested_path, path_exprs[4]);
    assert_int_equal(req_path->n_resolved_path_results, 1);
    assert_string_equal(req_path->resolved_path_results[0]->resolved_path, "test_root.child.2.");
    assert_int_equal(req_path->resolved_path_results[0]->n_result_params, 1);
    assert_string_equal(req_path->resolved_path_results[0]->result_params[0]->key, "child_param_2");
    assert_string_equal(req_path->resolved_path_results[0]->result_params[0]->value, "white");

    amxo_parser_clean(&parser);
    uspl_tx_delete(&usp);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    amxd_dm_clean(&dm);
    free(path_exprs);
}

void test_uspl_get_resp_extract(UNUSED void** state) {
    amxd_object_t* root = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    int num_path_expr = 1;
    char** path_exprs = (char**) malloc(num_path_expr * sizeof(char*));
    path_exprs[0] = "test_root_2.";
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;
    amxc_var_t* entry = NULL;
    amxc_var_t* result = NULL;
    const char* param_text = NULL;
    amxc_var_t* obj_var = NULL;

    assert_int_equal(amxd_dm_init(&dm), 0);
    assert_int_equal(amxo_parser_init(&parser), 0);
    root = amxd_dm_get_root(&dm);
    assert_non_null(root);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root), 0);

    amxc_llist_init(&resp_list);
    for(int i = 0; i < num_path_expr; i++) {
        amxc_var_t args;
        amxc_var_t rv;
        amxc_var_t* resolved = NULL;
        amxc_var_init(&args);
        amxc_var_init(&rv);
        amxc_var_new(&resolved);
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, "rel_path", path_exprs[i]);
        amxc_var_add_key(int32_t, &args, "depth", INT32_MAX);
        int retval = amxd_object_invoke_function(root, "_get", &args, &rv);
        assert_int_equal(retval, 0);
        amxc_var_dump(&rv, STDOUT_FILENO);

        // Add result variant
        amxc_var_t* result = amxc_var_add_key(amxc_htable_t, &rv, "result", NULL);
        amxc_var_add_key(cstring_t, result, "requested_path", path_exprs[i]);
        amxc_var_add_key(int64_t, result, "err_code", retval);

        amxc_var_copy(resolved, &rv);
        amxc_llist_append(&resp_list, &resolved->lit);
        amxc_var_clean(&args);
        amxc_var_clean(&rv);
    }

    uspl_tx_new(&usp_tx, "sender", "receiver");
    assert_int_equal(uspl_get_resp_new(usp_tx, &resp_list, "123"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);

    amxc_llist_init(&extract_list);
    assert_int_equal(uspl_get_resp_extract(usp_rx, &extract_list), 0);

    amxc_llist_it_t* obj_it = amxc_llist_get_first(&extract_list);
    entry = amxc_var_from_llist_it(obj_it);
    assert_non_null(entry);
    amxc_var_dump(entry, STDOUT_FILENO);

    // Check the resulting parameters in the final variant
    result = GET_ARG(entry, "result");
    assert_non_null(result);
    param_text = amxc_var_constcast(cstring_t, GET_ARG(result, "requested_path"));
    assert_non_null(param_text);
    assert_string_equal(param_text, "test_root_2.");
    obj_var = GET_ARG(entry, "test_root_2.");
    assert_non_null(obj_var);

    assert_string_equal(GET_CHAR(obj_var, "text"), "foo");
    assert_false(GET_BOOL(obj_var, "liar"));
    assert_int_equal(GET_UINT32(obj_var, "length"), 50);
    assert_int_equal(amxc_var_type_of(GET_ARG(obj_var, "length")), AMXC_VAR_ID_UINT32);

    amxo_parser_clean(&parser);
    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    amxc_llist_clean(&extract_list, variant_list_it_free);
    amxd_dm_clean(&dm);
    free(path_exprs);
}

void test_uspl_get_resp_error(UNUSED void** state) {
    amxc_var_t* result = NULL;
    amxc_var_t response;
    amxc_var_init(&response);
    amxc_var_set_type(&response, AMXC_VAR_ID_HTABLE);
    amxc_llist_t resp_list;
    amxc_llist_init(&resp_list);
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    Usp__GetResp__RequestedPathResult* req_path = NULL;

    result = amxc_var_add_key(amxc_htable_t, &response, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", "Does.Not.Exist.");
    amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_INVALID_PATH);
    amxc_var_add_key(cstring_t, result, "err_msg", "Path does not exist");

    amxc_llist_append(&resp_list, &response.lit);

    uspl_tx_new(&usp_tx, "sender", "receiver");
    assert_int_equal(uspl_get_resp_new(usp_tx, &resp_list, "123"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);
    assert_int_equal(usp_rx->msg->body->response->get_resp->n_req_path_results, 1);
    req_path = usp_rx->msg->body->response->get_resp->req_path_results[0];
    assert_string_equal(req_path->requested_path, "Does.Not.Exist.");
    assert_int_equal(req_path->err_code, USP_ERR_INVALID_PATH);
    assert_string_equal(req_path->err_msg, "Path does not exist");
    assert_int_equal(req_path->n_resolved_path_results, 0);
    assert_null(req_path->resolved_path_results);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    amxc_llist_clean(&resp_list, NULL);
    amxc_var_clean(&response);
}

void test_uspl_get_resp_non_existing(UNUSED void** state) {
    amxc_var_t* result = NULL;
    amxc_var_t response;
    amxc_var_init(&response);
    amxc_var_set_type(&response, AMXC_VAR_ID_HTABLE);
    amxc_llist_t resp_list;
    amxc_llist_init(&resp_list);
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    Usp__GetResp* get_resp = NULL;
    Usp__GetResp__RequestedPathResult* req_path_res = NULL;

    result = amxc_var_add_key(amxc_htable_t, &response, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", "Phonebook.Contact.[FirstName=='Johkjgfhn']");
    amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_OK);

    amxc_var_dump(&response, STDOUT_FILENO);

    amxc_llist_append(&resp_list, &response.lit);

    uspl_tx_new(&usp_tx, "sender", "receiver");
    assert_int_equal(uspl_get_resp_new(usp_tx, &resp_list, "123"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);
    get_resp = usp_rx->msg->body->response->get_resp;
    assert_non_null(get_resp);
    assert_int_equal(get_resp->n_req_path_results, 1);
    req_path_res = get_resp->req_path_results[0];
    assert_string_equal(req_path_res->requested_path, "Phonebook.Contact.[FirstName=='Johkjgfhn']");
    assert_int_equal(req_path_res->err_code, USP_ERR_OK);
    assert_int_equal(req_path_res->n_resolved_path_results, 0);
    assert_null(req_path_res->resolved_path_results);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    amxc_llist_clean(&resp_list, NULL);
    amxc_var_clean(&response);
}

void test_uspl_get_resp_search_path(UNUSED void** state) {
    amxd_object_t* root = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    const char* requested_path = "test_root.child.*.";
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;
    amxc_var_t* entry = NULL;
    amxc_var_t* result = NULL;
    amxc_var_t args;
    amxc_var_t rv;
    amxc_var_t* resolved = NULL;

    amxc_llist_init(&resp_list);
    amxc_var_init(&args);
    amxc_var_init(&rv);
    amxc_var_new(&resolved);

    assert_int_equal(amxd_dm_init(&dm), 0);
    assert_int_equal(amxo_parser_init(&parser), 0);
    root = amxd_dm_get_root(&dm);
    assert_non_null(root);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root), 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "rel_path", requested_path);
    amxc_var_add_key(int32_t, &args, "depth", INT32_MAX);
    int retval = amxd_object_invoke_function(root, "_get", &args, &rv);
    assert_int_equal(retval, 0);
    amxc_var_dump(&rv, STDOUT_FILENO);

    // Add result variant
    result = amxc_var_add_key(amxc_htable_t, &rv, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", requested_path);
    amxc_var_add_key(uint32_t, result, "err_code", 0);

    amxc_var_copy(resolved, &rv);
    amxc_llist_append(&resp_list, &resolved->lit);
    amxc_var_clean(&args);
    amxc_var_clean(&rv);

    uspl_tx_new(&usp_tx, "sender", "receiver");
    assert_int_equal(uspl_get_resp_new(usp_tx, &resp_list, "123"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);

    amxc_llist_init(&extract_list);
    assert_int_equal(uspl_get_resp_extract(usp_rx, &extract_list), 0);

    assert_int_equal(amxc_llist_size(&extract_list), 1);
    amxc_llist_it_t* obj_it = amxc_llist_get_first(&extract_list);
    entry = amxc_var_from_llist_it(obj_it);
    assert_non_null(entry);
    amxc_var_dump(entry, STDOUT_FILENO);

    // Check the resulting parameters in the final variant
    assert_true(test_verify_data(entry, "result.err_code", "0"));
    assert_true(test_verify_data(entry, "result.requested_path", requested_path));
    assert_true(test_verify_data(entry, "'test_root.child.1.'.boolean", "true"));
    assert_true(test_verify_data(entry, "'test_root.child.1.'.child_param_1", "red"));
    assert_true(test_verify_data(entry, "'test_root.child.1.'.child_param_2", "yellow"));
    assert_true(test_verify_data(entry, "'test_root.child.1.'.child_param_3", "black"));
    assert_true(test_verify_data(entry, "'test_root.child.2.'.boolean", "false"));
    assert_true(test_verify_data(entry, "'test_root.child.2.'.child_param_1", "red"));
    assert_true(test_verify_data(entry, "'test_root.child.2.'.child_param_2", "white"));
    assert_true(test_verify_data(entry, "'test_root.child.2.'.child_param_3", "red"));

    amxo_parser_clean(&parser);
    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    amxc_llist_clean(&extract_list, variant_list_it_free);
    amxd_dm_clean(&dm);
}

void test_uspl_get_resp_search_path_no_match(UNUSED void** state) {
    amxd_object_t* root = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    const char* requested_path = "test_root.child.[child_param_1 == 'indigo'].";
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;
    amxc_var_t* entry = NULL;
    amxc_var_t* result = NULL;
    amxc_var_t args;
    amxc_var_t rv;
    amxc_var_t* resolved = NULL;

    amxc_llist_init(&resp_list);
    amxc_var_init(&args);
    amxc_var_init(&rv);
    amxc_var_new(&resolved);

    assert_int_equal(amxd_dm_init(&dm), 0);
    assert_int_equal(amxo_parser_init(&parser), 0);
    root = amxd_dm_get_root(&dm);
    assert_non_null(root);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root), 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "rel_path", requested_path);
    amxc_var_add_key(int32_t, &args, "depth", INT32_MAX);
    int retval = amxd_object_invoke_function(root, "_get", &args, &rv);
    assert_int_equal(retval, 0);
    amxc_var_dump(&rv, STDOUT_FILENO);

    // Add result variant
    result = amxc_var_add_key(amxc_htable_t, &rv, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", requested_path);
    amxc_var_add_key(uint32_t, result, "err_code", 0);

    amxc_var_copy(resolved, &rv);
    amxc_llist_append(&resp_list, &resolved->lit);
    amxc_var_clean(&args);
    amxc_var_clean(&rv);

    uspl_tx_new(&usp_tx, "sender", "receiver");
    assert_int_equal(uspl_get_resp_new(usp_tx, &resp_list, "123"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);

    amxc_llist_init(&extract_list);
    assert_int_equal(uspl_get_resp_extract(usp_rx, &extract_list), 0);

    assert_int_equal(amxc_llist_size(&extract_list), 1);
    amxc_llist_it_t* obj_it = amxc_llist_get_first(&extract_list);
    entry = amxc_var_from_llist_it(obj_it);
    assert_non_null(entry);
    amxc_var_dump(entry, STDOUT_FILENO);

    // Check the resulting parameters in the final variant
    assert_true(test_verify_data(entry, "result.err_code", "0"));
    assert_true(test_verify_data(entry, "result.requested_path", requested_path));
    assert_null(GET_ARG(entry, "'test_root.child.1.'"));
    assert_null(GET_ARG(entry, "'test_root.child.2.'"));

    amxo_parser_clean(&parser);
    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    amxc_llist_clean(&extract_list, variant_list_it_free);
    amxd_dm_clean(&dm);
}
