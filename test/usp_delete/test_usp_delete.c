/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdio.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_function.h>
#include <amxd/amxd_path.h>

#include <amxo/amxo.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_error.h"
#include "usp/uspl_delete.h"
#include "usp/uspl_msghandler.h"
#include "uspl_private.h"
#include "test_usp_delete.h"

static amxo_parser_t parser;
static const char* odl_defs = "test_dm.odl";

static amxd_dm_t dm;

static void clean_list(amxc_llist_it_t* it) {
    amxc_var_t* data = amxc_llist_it_get_data(it, amxc_var_t, lit);
    amxc_var_delete(&data);
}

int test_dm_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), 0);

    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    assert_int_equal(amxo_parser_invoke_entry_points(&parser, &dm, AMXO_START), 0);

    return 0;
}

int test_dm_teardown(UNUSED void** state) {
    assert_int_equal(amxo_parser_invoke_entry_points(&parser, &dm, AMXO_STOP), 0);

    amxo_parser_clean(&parser);

    amxo_resolver_import_close_all();

    amxd_dm_clean(&dm);

    return 0;
}

void test_uspl_delete_new(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t delete_var;
    amxc_var_t* requests = NULL;

    Usp__Delete* delete = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&delete_var);
    amxc_var_set_type(&delete_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &delete_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &delete_var, "requests", NULL);
    amxc_var_add(cstring_t, requests, "test_root.child.*.");

    assert_int_equal(uspl_delete_new(usp_tx, &delete_var), 0);
    assert_string_equal(usp_tx->msg_id, "0");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__DELETE);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Delete message
    delete = usp_rx->msg->body->request->delete_;
    assert_non_null(delete);
    assert_false(delete->allow_partial);
    assert_int_equal(delete->n_obj_paths, 1);
    assert_non_null(delete->obj_paths);
    assert_string_equal(delete->obj_paths[0], "test_root.child.*.");

    amxc_var_clean(&delete_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_delete_new_invalid(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    amxc_var_t delete_var;
    amxc_var_t* partial = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&delete_var);

    assert_int_equal(uspl_delete_new(NULL, &delete_var), -1);
    assert_int_equal(uspl_delete_new(usp_tx, NULL), -1);

    amxc_var_set_type(&delete_var, AMXC_VAR_ID_HTABLE);
    amxc_var_set(bool, partial, false);
    assert_int_equal(uspl_delete_new(usp_tx, &delete_var), -1);

    amxc_var_clean(&delete_var);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_delete_extract(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    const char* path = "test_root.child.*.";
    amxc_var_t delete_var;
    amxc_var_t output_var;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&delete_var);
    amxc_var_set_type(&delete_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &delete_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &delete_var, "requests", NULL);
    amxc_var_add(cstring_t, requests, path);

    assert_int_equal(uspl_delete_new(usp_tx, &delete_var), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    amxc_var_init(&output_var);
    assert_int_equal(uspl_delete_extract(usp_rx, &output_var), 0);
    amxc_var_dump(&output_var, STDOUT_FILENO);

    assert_false(GET_BOOL(&output_var, "allow_partial"));
    request = GETI_ARG(GET_ARG(&output_var, "requests"), 0);
    assert_string_equal(amxc_var_constcast(cstring_t, request), path);

    amxc_var_clean(&delete_var);
    amxc_var_clean(&output_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_delete_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t output_var;

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_delete_extract(NULL, &output_var), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_delete_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_delete_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_delete_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_delete_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_delete_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    assert_int_equal(uspl_delete_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_delete_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request = malloc(sizeof(Usp__Request));
    usp__request__init(usp_rx->msg->body->request);
    assert_int_equal(uspl_delete_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request->req_type_case = USP__REQUEST__REQ_TYPE_SET;
    assert_int_equal(uspl_delete_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request->req_type_case = USP__REQUEST__REQ_TYPE_DELETE;
    assert_int_equal(uspl_delete_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    usp_rx->msg->body->request->delete_ = malloc(sizeof(Usp__Delete));
    usp__delete__init(usp_rx->msg->body->request->delete_);
    assert_int_equal(uspl_delete_extract(usp_rx, &output_var), USP_ERR_INVALID_ARGUMENTS);
    usp_rx->msg->body->request->delete_->n_obj_paths = 1;
    usp_rx->msg->body->request->delete_->obj_paths = NULL;
    assert_int_equal(uspl_delete_extract(usp_rx, &output_var), USP_ERR_INVALID_ARGUMENTS);

    usp_rx->msg->body->request->delete_->n_obj_paths = 0;
    uspl_rx_delete(&usp_rx);
}

void test_uspl_delete_resp(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t rv;
    const char* req_path = "test_root.child.[Alias == \"Dummy\"].";
    char* fixed = NULL;
    amxd_path_t path;
    amxd_object_t* template = NULL;
    amxd_object_t* new_inst = NULL;
    amxc_var_t values;
    amxc_var_t reply_data;
    amxc_var_t* result = NULL;
    const amxc_llist_t* rv_list = NULL;
    amxc_llist_t resp_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    Usp__DeleteResp* delete_resp = NULL;
    Usp__DeleteResp__DeletedObjectResult__OperationStatus* status = NULL;
    Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationSuccess* success = NULL;

    // Split path in object path and search path
    amxd_path_init(&path, req_path);
    assert_true(amxd_path_is_search_path(&path));
    fixed = amxd_path_get_fixed_part(&path, true);

    // Add instance
    template = amxd_dm_findf(&dm, fixed);
    assert_non_null(template);

    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Alias", "Dummy");

    assert_non_null(template);
    assert_int_equal(amxd_object_add_instance(&new_inst, template, NULL, 0, &values), 0);

    // Delete recently added instance
    amxc_var_init(&args);
    amxc_var_init(&rv);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "rel_path", amxc_string_get(&path.path, 0));
    assert_int_equal(amxd_object_invoke_function(template, "_del", &args, &rv), 0);
    amxc_var_dump(&rv, STDOUT_FILENO);

    // Fill up variant used to build USP response message
    amxc_var_init(&reply_data);
    amxc_var_set_type(&reply_data, AMXC_VAR_ID_HTABLE);
    result = amxc_var_add_key(amxc_htable_t, &reply_data, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", req_path);
    amxc_var_add_key(uint32_t, result, "err_code", 0);

    rv_list = amxc_var_constcast(amxc_llist_t, &rv);
    amxc_var_add_key(amxc_llist_t, &reply_data, "affected_paths", rv_list);
    amxc_var_add_key(amxc_llist_t, &reply_data, "unaffected_paths", NULL);
    amxc_var_dump(&reply_data, STDOUT_FILENO);

    amxc_llist_init(&resp_list);
    amxc_llist_append(&resp_list, &reply_data.lit);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_delete_resp_new(usp_tx, &resp_list, "456"), 0);
    assert_string_equal(usp_tx->msg_id, "456");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__DELETE_RESP);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(usp_rx->msg->header->msg_type, USP__HEADER__MSG_TYPE__DELETE_RESP);
    assert_int_equal(usp_rx->msg->body->msg_body_case, USP__BODY__MSG_BODY_RESPONSE);
    assert_int_equal(usp_rx->msg->body->response->resp_type_case, USP__RESPONSE__RESP_TYPE_DELETE_RESP);

    delete_resp = usp_rx->msg->body->response->delete_resp;
    assert_int_equal(delete_resp->n_deleted_obj_results, 1);
    assert_string_equal(delete_resp->deleted_obj_results[0]->requested_path, req_path);

    status = delete_resp->deleted_obj_results[0]->oper_status;
    assert_int_equal(status->oper_status_case, USP__DELETE_RESP__DELETED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS);

    success = status->oper_success;
    assert_int_equal(success->n_affected_paths, 2);
    assert_string_equal(success->affected_paths[0], "test_root.child.2.");
    assert_string_equal(success->affected_paths[1], "test_root.child.2.box.");
    assert_int_equal(success->n_unaffected_path_errs, 0);
    assert_null(success->unaffected_path_errs);

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, NULL);
    amxc_var_clean(&reply_data);
    amxc_var_clean(&args);
    amxc_var_clean(&rv);
    amxc_var_clean(&values);
    free(fixed);
    amxd_path_clean(&path);
}

void test_uspl_delete_resp_unaffected(UNUSED void** state) {
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    Usp__DeleteResp* delete_resp = NULL;
    Usp__DeleteResp__DeletedObjectResult__OperationStatus* status = NULL;
    Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationSuccess* success = NULL;
    Usp__DeleteResp__UnaffectedPathError* unaffected_path_err = NULL;
    const char* req_path = "test_root.child.[Alias == \"Dummy\"].";
    amxc_var_t reply_data;
    amxc_var_t unaffected_entry;
    amxc_llist_t unaffected_list;

    // Fill up variant used to build USP response message
    amxc_var_init(&reply_data);
    amxc_var_set_type(&reply_data, AMXC_VAR_ID_HTABLE);
    result = amxc_var_add_key(amxc_htable_t, &reply_data, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", req_path);
    amxc_var_add_key(uint32_t, result, "err_code", 0);

    amxc_var_init(&unaffected_entry);
    amxc_var_set_type(&unaffected_entry, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &unaffected_entry, "path", "foo");
    amxc_var_add_key(uint32_t, &unaffected_entry, "err_code", USP_ERR_OBJECT_NOT_DELETABLE);
    amxc_var_add_key(cstring_t, &unaffected_entry, "err_msg", uspl_error_code_to_str(USP_ERR_OBJECT_NOT_DELETABLE));

    amxc_llist_init(&unaffected_list);
    amxc_llist_append(&unaffected_list, &unaffected_entry.lit);
    amxc_var_add_key(amxc_llist_t, &reply_data, "affected_paths", NULL);
    amxc_var_add_key(amxc_llist_t, &reply_data, "unaffected_paths", &unaffected_list);
    amxc_var_dump(&reply_data, STDOUT_FILENO);

    amxc_llist_init(&resp_list);
    amxc_llist_append(&resp_list, &reply_data.lit);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_delete_resp_new(usp_tx, &resp_list, "456"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(usp_rx->msg->header->msg_type, USP__HEADER__MSG_TYPE__DELETE_RESP);
    assert_int_equal(usp_rx->msg->body->msg_body_case, USP__BODY__MSG_BODY_RESPONSE);
    assert_int_equal(usp_rx->msg->body->response->resp_type_case, USP__RESPONSE__RESP_TYPE_DELETE_RESP);

    delete_resp = usp_rx->msg->body->response->delete_resp;
    assert_int_equal(delete_resp->n_deleted_obj_results, 1);
    assert_string_equal(delete_resp->deleted_obj_results[0]->requested_path, req_path);

    status = delete_resp->deleted_obj_results[0]->oper_status;
    assert_int_equal(status->oper_status_case, USP__DELETE_RESP__DELETED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS);

    success = status->oper_success;
    assert_int_equal(success->n_affected_paths, 0);
    assert_null(success->affected_paths);
    assert_int_equal(success->n_unaffected_path_errs, 1);
    assert_non_null(success->unaffected_path_errs);

    unaffected_path_err = success->unaffected_path_errs[0];
    assert_int_equal(unaffected_path_err->err_code, USP_ERR_OBJECT_NOT_DELETABLE);
    assert_string_equal(unaffected_path_err->err_msg, "Object not deletable");
    assert_string_equal(unaffected_path_err->unaffected_path, "foo");

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&unaffected_list, NULL);
    amxc_llist_clean(&resp_list, NULL);
    amxc_var_clean(&reply_data);
    amxc_var_clean(&unaffected_entry);
}

void test_uspl_delete_resp_oper_failure(UNUSED void** state) {
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    Usp__DeleteResp* delete_resp = NULL;
    Usp__DeleteResp__DeletedObjectResult__OperationStatus* status = NULL;
    Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationFailure* failure = NULL;
    const char* req_path = "test_root.child.[Alias == \"Dummy\"].";
    amxc_var_t reply_data;

    // Fill up variant used to build USP response message
    amxc_var_init(&reply_data);
    amxc_var_set_type(&reply_data, AMXC_VAR_ID_HTABLE);
    result = amxc_var_add_key(amxc_htable_t, &reply_data, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", req_path);
    amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_OBJECT_NOT_DELETABLE);
    amxc_var_add_key(cstring_t, result, "err_msg", uspl_error_code_to_str(USP_ERR_OBJECT_NOT_DELETABLE));

    amxc_var_add_key(amxc_llist_t, &reply_data, "affected_paths", NULL);
    amxc_var_add_key(amxc_llist_t, &reply_data, "unaffected_paths", NULL);
    amxc_var_dump(&reply_data, STDOUT_FILENO);

    amxc_llist_init(&resp_list);
    amxc_llist_append(&resp_list, &reply_data.lit);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_delete_resp_new(usp_tx, &resp_list, "456"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(usp_rx->msg->header->msg_type, USP__HEADER__MSG_TYPE__DELETE_RESP);
    assert_int_equal(usp_rx->msg->body->msg_body_case, USP__BODY__MSG_BODY_RESPONSE);
    assert_int_equal(usp_rx->msg->body->response->resp_type_case, USP__RESPONSE__RESP_TYPE_DELETE_RESP);

    delete_resp = usp_rx->msg->body->response->delete_resp;
    assert_int_equal(delete_resp->n_deleted_obj_results, 1);
    assert_string_equal(delete_resp->deleted_obj_results[0]->requested_path, req_path);

    status = delete_resp->deleted_obj_results[0]->oper_status;
    assert_int_equal(status->oper_status_case, USP__DELETE_RESP__DELETED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE);

    failure = status->oper_failure;
    assert_int_equal(failure->err_code, USP_ERR_OBJECT_NOT_DELETABLE);
    assert_string_equal(failure->err_msg, "Object not deletable");

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, NULL);
    amxc_var_clean(&reply_data);
}

void test_uspl_delete_resp_invalid(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    amxc_llist_t resp_list;
    amxc_var_t reply_data;
    amxc_var_t* result = NULL;
    amxc_var_t* unaffected_paths = NULL;
    amxc_var_t* unaffected_entry = NULL;

    amxc_llist_init(&resp_list);
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    assert_int_equal(uspl_delete_resp_new(NULL, &resp_list, "456"), -1);
    assert_int_equal(uspl_delete_resp_new(usp_tx, NULL, "456"), -1);
    assert_int_equal(uspl_delete_resp_new(usp_tx, &resp_list, NULL), -1);

    amxc_var_init(&reply_data);
    amxc_var_set_type(&reply_data, AMXC_VAR_ID_HTABLE);
    amxc_llist_append(&resp_list, &reply_data.lit);
    assert_int_equal(uspl_delete_resp_new(usp_tx, &resp_list, "456"), -1);

    // Test invalid input for error code == 0
    result = amxc_var_add_key(amxc_htable_t, &reply_data, "result", NULL);
    amxc_var_add_key(uint32_t, result, "err_code", 0);
    assert_int_equal(uspl_delete_resp_new(usp_tx, &resp_list, "456"), -1);
    amxc_var_add_key(cstring_t, result, "requested_path", "foo");
    assert_int_equal(uspl_delete_resp_new(usp_tx, &resp_list, "456"), -1);
    amxc_var_add_key(amxc_llist_t, &reply_data, "affected_paths", NULL);
    assert_int_equal(uspl_delete_resp_new(usp_tx, &resp_list, "456"), -1);
    unaffected_paths = amxc_var_add_key(amxc_llist_t, &reply_data, "unaffected_paths", NULL);
    unaffected_entry = amxc_var_add(amxc_htable_t, unaffected_paths, NULL);
    assert_int_equal(uspl_delete_resp_new(usp_tx, &resp_list, "456"), -1);
    amxc_var_add_key(cstring_t, unaffected_entry, "path", "foo");
    assert_int_equal(uspl_delete_resp_new(usp_tx, &resp_list, "456"), -1);

    // Test invalid input for error code != 0
    amxc_llist_clean(&resp_list, NULL);
    amxc_var_clean(&reply_data);

    amxc_var_init(&reply_data);
    amxc_var_set_type(&reply_data, AMXC_VAR_ID_HTABLE);
    amxc_llist_init(&resp_list);
    amxc_llist_append(&resp_list, &reply_data.lit);

    result = amxc_var_add_key(amxc_htable_t, &reply_data, "result", NULL);
    amxc_var_add_key(uint32_t, result, "err_code", 555);
    assert_int_equal(uspl_delete_resp_new(usp_tx, &resp_list, "456"), -1);
    amxc_var_add_key(cstring_t, result, "requested_path", "foo");
    assert_int_equal(uspl_delete_resp_new(usp_tx, &resp_list, "456"), -1);

    uspl_tx_delete(&usp_tx);
    amxc_var_clean(&reply_data);
    amxc_llist_clean(&resp_list, NULL);
}

void test_uspl_delete_resp_extract(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t rv;
    const char* req_path = "test_root.child.[Alias == \"Dummy\"].";
    char* fixed = NULL;
    amxd_path_t path;
    amxd_object_t* template = NULL;
    amxd_object_t* new_inst = NULL;
    amxc_var_t values;
    amxc_var_t reply_data;
    amxc_var_t* result = NULL;
    const amxc_llist_t* rv_list = NULL;
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;

    // Split path in object path and search path
    amxd_path_init(&path, req_path);
    assert_true(amxd_path_is_search_path(&path));
    fixed = amxd_path_get_fixed_part(&path, true);

    // Add instance
    template = amxd_dm_findf(&dm, fixed);
    assert_non_null(template);

    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Alias", "Dummy");

    assert_non_null(template);
    assert_int_equal(amxd_object_add_instance(&new_inst, template, NULL, 0, &values), 0);

    // Delete recently added instance
    amxc_var_init(&args);
    amxc_var_init(&rv);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "rel_path", amxc_string_get(&path.path, 0));
    assert_int_equal(amxd_object_invoke_function(template, "_del", &args, &rv), 0);
    amxc_var_dump(&rv, STDOUT_FILENO);

    // Fill up variant used to build USP response message
    amxc_var_init(&reply_data);
    amxc_var_set_type(&reply_data, AMXC_VAR_ID_HTABLE);
    result = amxc_var_add_key(amxc_htable_t, &reply_data, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", req_path);
    amxc_var_add_key(uint32_t, result, "err_code", 0);

    rv_list = amxc_var_constcast(amxc_llist_t, &rv);
    amxc_var_add_key(amxc_llist_t, &reply_data, "affected_paths", rv_list);
    amxc_var_add_key(amxc_llist_t, &reply_data, "unaffected_paths", NULL);
    amxc_var_dump(&reply_data, STDOUT_FILENO);

    amxc_llist_init(&resp_list);
    amxc_llist_append(&resp_list, &reply_data.lit);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_delete_resp_new(usp_tx, &resp_list, "456"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    amxc_llist_init(&extract_list);
    assert_int_equal(uspl_delete_resp_extract(usp_rx, &extract_list), 0);

    amxc_llist_iterate(it, &extract_list) {
        amxc_var_t* var = amxc_var_from_llist_it(it);
        amxc_var_dump(var, STDOUT_FILENO);
    }
    // Need to check output variant

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, NULL);
    amxc_llist_clean(&extract_list, clean_list);
    amxc_var_clean(&reply_data);
    amxc_var_clean(&args);
    amxc_var_clean(&rv);
    amxc_var_clean(&values);
    free(fixed);
    amxd_path_clean(&path);
}

void test_uspl_delete_resp_extract_unaffected(UNUSED void** state) {
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    const char* req_path = "test_root.child.[Alias == \"Dummy\"].";
    amxc_var_t reply_data;
    amxc_var_t unaffected_entry;
    amxc_llist_t unaffected_list;

    // Fill up variant used to build USP response message
    amxc_var_init(&reply_data);
    amxc_var_set_type(&reply_data, AMXC_VAR_ID_HTABLE);
    result = amxc_var_add_key(amxc_htable_t, &reply_data, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", req_path);
    amxc_var_add_key(uint32_t, result, "err_code", 0);

    amxc_var_init(&unaffected_entry);
    amxc_var_set_type(&unaffected_entry, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &unaffected_entry, "path", "foo");
    amxc_var_add_key(uint32_t, &unaffected_entry, "err_code", USP_ERR_OBJECT_NOT_DELETABLE);
    amxc_var_add_key(cstring_t, &unaffected_entry, "err_msg", uspl_error_code_to_str(USP_ERR_OBJECT_NOT_DELETABLE));

    amxc_llist_init(&unaffected_list);
    amxc_llist_append(&unaffected_list, &unaffected_entry.lit);
    amxc_var_add_key(amxc_llist_t, &reply_data, "affected_paths", NULL);
    amxc_var_add_key(amxc_llist_t, &reply_data, "unaffected_paths", &unaffected_list);
    amxc_var_dump(&reply_data, STDOUT_FILENO);

    amxc_llist_init(&resp_list);
    amxc_llist_append(&resp_list, &reply_data.lit);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_delete_resp_new(usp_tx, &resp_list, "456"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    amxc_llist_init(&extract_list);
    assert_int_equal(uspl_delete_resp_extract(usp_rx, &extract_list), 0);

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&extract_list, clean_list);
    amxc_llist_clean(&unaffected_list, NULL);
    amxc_llist_clean(&resp_list, NULL);
    amxc_var_clean(&reply_data);
    amxc_var_clean(&unaffected_entry);
}

void test_uspl_delete_resp_extract_oper_failure(UNUSED void** state) {
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    const char* req_path = "test_root.child.[Alias == \"Dummy\"].";
    amxc_var_t reply_data;

    // Fill up variant used to build USP response message
    amxc_var_init(&reply_data);
    amxc_var_set_type(&reply_data, AMXC_VAR_ID_HTABLE);
    result = amxc_var_add_key(amxc_htable_t, &reply_data, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", req_path);
    amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_OBJECT_NOT_DELETABLE);
    amxc_var_add_key(cstring_t, result, "err_msg", uspl_error_code_to_str(USP_ERR_OBJECT_NOT_DELETABLE));

    amxc_var_add_key(amxc_llist_t, &reply_data, "affected_paths", NULL);
    amxc_var_add_key(amxc_llist_t, &reply_data, "unaffected_paths", NULL);
    amxc_var_dump(&reply_data, STDOUT_FILENO);

    amxc_llist_init(&resp_list);
    amxc_llist_append(&resp_list, &reply_data.lit);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_delete_resp_new(usp_tx, &resp_list, "456"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    amxc_llist_init(&extract_list);
    assert_int_equal(uspl_delete_resp_extract(usp_rx, &extract_list), 0);

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&extract_list, clean_list);
    amxc_llist_clean(&resp_list, NULL);
    amxc_var_clean(&reply_data);
}

void test_uspl_delete_resp_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_llist_t extract_list;

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_delete_resp_extract(NULL, &extract_list), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_delete_resp_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_delete_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_delete_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_delete_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_delete_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_delete_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    assert_int_equal(uspl_delete_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response = malloc(sizeof(Usp__Response));
    usp__response__init(usp_rx->msg->body->response);
    assert_int_equal(uspl_delete_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response->resp_type_case = USP__RESPONSE__RESP_TYPE_GET_RESP;
    assert_int_equal(uspl_delete_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    usp_rx->msg->body->response->resp_type_case = USP__RESPONSE__RESP_TYPE_DELETE_RESP;
    assert_int_equal(uspl_delete_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response->delete_resp = malloc(sizeof(Usp__DeleteResp));
    usp__delete_resp__init(usp_rx->msg->body->response->delete_resp);
    usp_rx->msg->body->response->delete_resp->n_deleted_obj_results = 0;
    assert_int_equal(uspl_delete_resp_extract(usp_rx, &extract_list), USP_ERR_INVALID_ARGUMENTS);
    usp_rx->msg->body->response->delete_resp->n_deleted_obj_results = 1;
    usp_rx->msg->body->response->delete_resp->deleted_obj_results = NULL;
    assert_int_equal(uspl_delete_resp_extract(usp_rx, &extract_list), USP_ERR_INVALID_ARGUMENTS);

    // Set invalid oper_status_case
    Usp__DeleteResp* del_resp = usp_rx->msg->body->response->delete_resp;
    Usp__DeleteResp__DeletedObjectResult* deleted_obj_res;
    Usp__DeleteResp__DeletedObjectResult__OperationStatus* oper_status;
    Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationFailure* oper_failure;

    deleted_obj_res = malloc(sizeof(Usp__DeleteResp__DeletedObjectResult));
    usp__delete_resp__deleted_object_result__init(deleted_obj_res);

    oper_status = malloc(sizeof(Usp__DeleteResp__DeletedObjectResult__OperationStatus));
    usp__delete_resp__deleted_object_result__operation_status__init(oper_status);

    oper_failure = malloc(sizeof(Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationFailure));
    usp__delete_resp__deleted_object_result__operation_status__operation_failure__init(oper_failure);

    del_resp->deleted_obj_results = realloc(del_resp->deleted_obj_results, sizeof(void*));
    del_resp->n_deleted_obj_results = 1;
    del_resp->deleted_obj_results[0] = deleted_obj_res;

    deleted_obj_res->requested_path = NULL;
    deleted_obj_res->oper_status = oper_status;

    oper_status->oper_status_case = 999;
    oper_status->oper_failure = oper_failure;

    assert_int_equal(uspl_delete_resp_extract(usp_rx, &extract_list), USP_ERR_INVALID_ARGUMENTS);

    oper_status->oper_status_case = USP__DELETE_RESP__DELETED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE;
    uspl_rx_delete(&usp_rx);
}