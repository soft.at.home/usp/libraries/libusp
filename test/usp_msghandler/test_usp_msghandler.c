/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <setjmp.h>
#include <cmocka.h>

#include "uspl_private.h"
#include "test_usp_msghandler.h"
#include "uspl_msghandler_priv.h"
#include "usp/uspl_msghandler.h"
#include "usp/uspl_get.h"

static Usp__Msg* CreateGet(char* msg_id) {
    Usp__Msg* msg;
    Usp__Header* header;
    Usp__Body* body;
    Usp__Request* request;
    Usp__Get* get;

    // Allocate memory to store the USP message
    msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(msg);

    header = malloc(sizeof(Usp__Header));
    usp__header__init(header);

    body = malloc(sizeof(Usp__Body));
    usp__body__init(body);

    request = malloc(sizeof(Usp__Request));
    usp__request__init(request);

    get = malloc(sizeof(Usp__Get));
    usp__get__init(get);

    // Connect the structures together
    msg->header = header;
    header->msg_id = strdup(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__GET;

    msg->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    body->request = request;
    request->req_type_case = USP__REQUEST__REQ_TYPE_GET;
    request->get = get;
    get->n_param_paths = 0;
    get->param_paths = NULL;

    return msg;
}

void test_uspl_tx_new(UNUSED void** state) {
    uspl_tx_t* ctx = NULL;

    int retval = 0;
    retval = uspl_tx_new(NULL, NULL, NULL);
    assert_int_not_equal(retval, 0);

    retval = uspl_tx_new(&ctx, NULL, NULL);
    assert_int_not_equal(retval, 0);

    retval = uspl_tx_new(&ctx, "foo", NULL);
    assert_int_not_equal(retval, 0);
    retval = uspl_tx_new(&ctx, NULL, "bar");
    assert_int_not_equal(retval, 0);

    retval = uspl_tx_new(&ctx, "foo", "bar");
    assert_int_equal(retval, 0);
    assert_non_null(ctx);
    assert_null(ctx->pbuf);
    assert_int_equal(ctx->pbuf_len, 0);
    assert_string_equal(ctx->lendpoint_id, "foo");
    assert_string_equal(ctx->rendpoint_id, "bar");
    assert_null(ctx->msg_id);
    assert_int_equal(ctx->msg_type, -1);

    uspl_tx_delete(&ctx);
}

void test_uspl_tx_delete(UNUSED void** state) {
    uspl_tx_t* ctx = NULL;

    uspl_tx_delete(NULL);
    uspl_tx_delete(&ctx);
    assert_null(ctx);

    assert_int_equal(uspl_tx_new(&ctx, "foo", "bar"), 0);
    assert_non_null(ctx);

    uspl_tx_delete(&ctx);
    assert_null(ctx);
}

void test_uspl_msghandler_pack_unpack_protobuf(UNUSED void** state) {
    Usp__Msg* msg = NULL;
    int retval = 0;

    uspl_tx_t* record = NULL;
    retval = uspl_tx_new(&record, "foo", "bar");

    char** path_exprs = (char**) malloc(2 * sizeof(char*));
    char* foo = "foo";
    char* bar = "bar";
    path_exprs[0] = foo;
    path_exprs[1] = bar;
    int num_path_expr = 2;
    msg = CreateGet("0");

    msg->body->request->get->param_paths = calloc(num_path_expr, sizeof(char*));
    msg->body->request->get->n_param_paths = num_path_expr;
    for(int i = 0; i < num_path_expr; i++) {
        msg->body->request->get->param_paths[i] = strdup(path_exprs[i]);
    }

    retval = uspl_msghandler_pack_protobuf(NULL, NULL);
    assert_int_not_equal(retval, 0);
    retval = uspl_msghandler_pack_protobuf(msg, NULL);
    assert_int_not_equal(retval, 0);
    retval = uspl_msghandler_pack_protobuf(NULL, record);
    assert_int_not_equal(retval, 0);

    retval = uspl_msghandler_pack_protobuf(msg, record);
    assert_int_equal(retval, 0);

    uspl_rx_t* usp_rx = uspl_msghandler_unpack_protobuf(NULL, 0);
    assert_null(usp_rx);
    usp_rx = uspl_msghandler_unpack_protobuf(record->pbuf, 0);
    assert_null(usp_rx);
    usp_rx = uspl_msghandler_unpack_protobuf(NULL, record->pbuf_len);
    assert_null(usp_rx);

    usp_rx = uspl_msghandler_unpack_protobuf(record->pbuf, record->pbuf_len);
    assert_int_equal(usp_rx->msg->body->request->get->n_param_paths, num_path_expr);
    assert_string_equal(usp_rx->msg->body->request->get->param_paths[0], foo);
    assert_string_equal(usp_rx->msg->body->request->get->param_paths[1], bar);

    uspl_rx_delete(&usp_rx);
    assert_null(usp_rx);

    uspl_tx_delete(&record);
    assert_null(record);

    uspl_msghandler_free_unpacked_message(msg);
    free(path_exprs);
}
