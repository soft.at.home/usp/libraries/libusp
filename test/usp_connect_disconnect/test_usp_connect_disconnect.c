/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc_macros.h>

#include "usp/uspl_connect_disconnect.h"
#include "usp/uspl_error.h"
#include "usp/usp_err_codes.h"

#include "test_usp_connect_disconnect.h"

static const char* from_id = "from";
static const char* to_id = "to";

void test_can_create_mqtt_connect_record(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t request;
    amxc_var_t* mqtt = NULL;
    amxc_var_t* version = NULL;
    const char* topic = "topic";

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &request, "record_type", USP_RECORD__RECORD__RECORD_TYPE_MQTT_CONNECT);
    mqtt = amxc_var_add_key(amxc_htable_t, &request, "mqtt", NULL);
    amxc_var_add_key(cstring_t, mqtt, "subscribed_topic", topic);
    version = amxc_var_add_key(uint32_t, mqtt, "version", USP_RECORD__MQTTCONNECT_RECORD__MQTTVERSION__V3_1_1);

    uspl_tx_new(&usp_tx, from_id, to_id);
    assert_int_equal(uspl_connect_record_new(usp_tx, &request), 0);
    uspl_tx_delete(&usp_tx);

    amxc_var_set(uint32_t, version, USP_RECORD__MQTTCONNECT_RECORD__MQTTVERSION__V5);
    amxc_var_dump(&request, STDOUT_FILENO);
    uspl_tx_new(&usp_tx, from_id, to_id);
    assert_int_equal(uspl_connect_record_new(usp_tx, &request), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(uspl_msghandler_record_type(usp_rx), USP_RECORD__RECORD__RECORD_TYPE_MQTT_CONNECT);
    assert_null(usp_rx->msg);
    assert_int_equal(usp_rx->rec->mqtt_connect->version, USP_RECORD__MQTTCONNECT_RECORD__MQTTVERSION__V5);
    assert_string_equal(usp_rx->rec->mqtt_connect->subscribed_topic, topic);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    amxc_var_clean(&request);
}

void test_cannot_create_invalid_mqtt_connect_record(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    amxc_var_t request;
    amxc_var_t* mqtt = NULL;
    amxc_var_t* version = NULL;

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);

    uspl_tx_new(&usp_tx, from_id, to_id);
    assert_int_not_equal(uspl_connect_record_new(NULL, NULL), 0);
    assert_int_not_equal(uspl_connect_record_new(usp_tx, NULL), 0);
    assert_int_not_equal(uspl_connect_record_new(usp_tx, &request), 0);

    amxc_var_add_key(uint32_t, &request, "record_type", USP_RECORD__RECORD__RECORD_TYPE_MQTT_CONNECT);
    assert_int_not_equal(uspl_connect_record_new(usp_tx, &request), 0);

    mqtt = amxc_var_add_key(amxc_htable_t, &request, "mqtt", NULL);
    assert_int_not_equal(uspl_connect_record_new(usp_tx, &request), 0);
    version = amxc_var_add_key(uint32_t, mqtt, "version", 555);
    assert_int_not_equal(uspl_connect_record_new(usp_tx, &request), 0);
    amxc_var_set(uint32_t, version, USP_RECORD__MQTTCONNECT_RECORD__MQTTVERSION__V5);
    assert_int_not_equal(uspl_connect_record_new(usp_tx, &request), 0);
    amxc_var_add_key(cstring_t, mqtt, "subscribed_topic", "");
    assert_int_not_equal(uspl_connect_record_new(usp_tx, &request), 0);

    uspl_tx_delete(&usp_tx);
    amxc_var_clean(&request);
}

void test_can_create_uds_connect_record(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t request;

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &request, "record_type", USP_RECORD__RECORD__RECORD_TYPE_UDS_CONNECT);

    uspl_tx_new(&usp_tx, from_id, to_id);
    assert_int_equal(uspl_connect_record_new(usp_tx, &request), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(uspl_msghandler_record_type(usp_rx), USP_RECORD__RECORD__RECORD_TYPE_UDS_CONNECT);
    assert_null(usp_rx->msg);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    amxc_var_clean(&request);
}

void test_can_create_disconnect_record(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t request;

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);

    assert_int_not_equal(uspl_disconnect_record_new(NULL, NULL), 0);

    uspl_tx_new(&usp_tx, from_id, to_id);
    assert_int_equal(uspl_disconnect_record_new(usp_tx, &request), 0);
    uspl_tx_delete(&usp_tx);

    amxc_var_add_key(uint32_t, &request, "reason_code", USP_ERR_RECORD_NOT_PARSED);
    amxc_var_add_key(cstring_t, &request, "reason", uspl_error_code_to_str(USP_ERR_RECORD_NOT_PARSED));
    uspl_tx_new(&usp_tx, from_id, to_id);
    assert_int_equal(uspl_disconnect_record_new(usp_tx, &request), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);
    assert_int_equal(uspl_msghandler_record_type(usp_rx), USP_RECORD__RECORD__RECORD_TYPE_DISCONNECT);
    assert_null(usp_rx->msg);

    assert_int_equal(usp_rx->rec->disconnect->reason_code, USP_ERR_RECORD_NOT_PARSED);
    assert_string_equal(usp_rx->rec->disconnect->reason, uspl_error_code_to_str(USP_ERR_RECORD_NOT_PARSED));

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    amxc_var_clean(&request);
}
