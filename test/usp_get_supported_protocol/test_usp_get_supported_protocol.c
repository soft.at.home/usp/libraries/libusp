/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdio.h>

#include <amxc/amxc.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_error.h"
#include "usp/uspl_get_supported_protocol.h"
#include "usp/uspl_msghandler.h"
#include "uspl_private.h"
#include "test_usp_get_supported_protocol.h"

void test_uspl_get_supported_protocol_new(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t request_var;
    const char* versions = "1.0,2.0,3.0";
    Usp__GetSupportedProtocol* gs_proto = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&request_var);
    amxc_var_set_type(&request_var, AMXC_VAR_ID_CSV_STRING);
    amxc_var_set(csv_string_t, &request_var, versions);

    assert_int_equal(uspl_get_supported_protocol_new(usp_tx, &request_var), 0);
    assert_string_equal(usp_tx->msg_id, "0");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP GetSupportedProtocol message
    gs_proto = usp_rx->msg->body->request->get_supported_protocol;
    assert_non_null(gs_proto);
    assert_string_equal(gs_proto->controller_supported_protocol_versions, versions);

    amxc_var_clean(&request_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_get_supported_protocol_new_invalid(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    amxc_var_t request_var;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    assert_int_equal(uspl_get_supported_protocol_new(NULL, &request_var), -1);
    assert_int_equal(uspl_get_supported_protocol_new(usp_tx, NULL), -1);

    amxc_var_init(&request_var);
    amxc_var_set_type(&request_var, AMXC_VAR_ID_CSV_STRING);
    assert_int_equal(uspl_get_supported_protocol_new(usp_tx, &request_var), -1);

    amxc_var_clean(&request_var);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_get_supported_protocol_extract(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t request_var;
    amxc_var_t extract_var;
    const char* versions = "1.0,2.0,3.0";

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&request_var);
    amxc_var_set_type(&request_var, AMXC_VAR_ID_CSV_STRING);
    amxc_var_set(csv_string_t, &request_var, versions);

    assert_int_equal(uspl_get_supported_protocol_new(usp_tx, &request_var), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    amxc_var_init(&extract_var);
    assert_int_equal(uspl_get_supported_protocol_extract(usp_rx, &extract_var), 0);
    amxc_var_dump(&extract_var, STDOUT_FILENO);

    assert_string_equal(amxc_var_constcast(cstring_t, &extract_var), versions);

    amxc_var_clean(&extract_var);
    amxc_var_clean(&request_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_get_supported_protocol_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t output_var;

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_get_supported_protocol_extract(NULL, &output_var), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_get_supported_protocol_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_get_supported_protocol_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_get_supported_protocol_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_get_supported_protocol_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_get_supported_protocol_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_get_supported_protocol_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request = malloc(sizeof(Usp__Request));
    usp__request__init(usp_rx->msg->body->request);
    assert_int_equal(uspl_get_supported_protocol_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request->req_type_case = USP__REQUEST__REQ_TYPE_GET_SUPPORTED_PROTOCOL;
    assert_int_equal(uspl_get_supported_protocol_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request->get_supported_protocol = malloc(sizeof(Usp__GetSupportedProtocol));
    usp__get_supported_protocol__init(usp_rx->msg->body->request->get_supported_protocol);
    usp_rx->msg->body->request->get_supported_protocol->controller_supported_protocol_versions = NULL;
    assert_int_equal(uspl_get_supported_protocol_extract(usp_rx, &output_var), USP_ERR_INVALID_ARGUMENTS);

    uspl_rx_delete(&usp_rx);
}

void test_uspl_get_supported_protocol_resp_new(UNUSED void** state) {
    amxc_var_t reply_data;
    amxc_llist_t resp_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    const char* versions = "1.0,2.0,3.0";

    // Fill up variant used to build USP response message
    amxc_var_init(&reply_data);
    amxc_var_set_type(&reply_data, AMXC_VAR_ID_CSV_STRING);
    amxc_var_set(csv_string_t, &reply_data, versions);

    amxc_llist_init(&resp_list);
    amxc_llist_append(&resp_list, &reply_data.lit);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_get_supported_protocol_resp_new(usp_tx, &resp_list, "456"), 0);
    assert_string_equal(usp_tx->msg_id, "456");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO_RESP);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(usp_rx->msg->header->msg_type, USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO_RESP);
    assert_int_equal(usp_rx->msg->body->msg_body_case, USP__BODY__MSG_BODY_RESPONSE);
    assert_int_equal(usp_rx->msg->body->response->resp_type_case, USP__RESPONSE__RESP_TYPE_GET_SUPPORTED_PROTOCOL_RESP);
    assert_string_equal(usp_rx->msg->body->response->get_supported_protocol_resp->agent_supported_protocol_versions, versions);

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, NULL);
    amxc_var_clean(&reply_data);
}

void test_uspl_get_supported_protocol_resp_new_invalid(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    amxc_llist_t resp_list;
    amxc_var_t reply_data;

    amxc_llist_init(&resp_list);
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    assert_int_equal(uspl_get_supported_protocol_resp_new(NULL, &resp_list, "456"), -1);
    assert_int_equal(uspl_get_supported_protocol_resp_new(usp_tx, NULL, "456"), -1);
    assert_int_equal(uspl_get_supported_protocol_resp_new(usp_tx, &resp_list, NULL), -1);

    assert_int_equal(uspl_get_supported_protocol_resp_new(usp_tx, &resp_list, "456"), -1);

    amxc_var_init(&reply_data);
    amxc_llist_append(&resp_list, &reply_data.lit);
    assert_int_equal(uspl_get_supported_protocol_resp_new(usp_tx, &resp_list, "456"), -1);

    uspl_tx_delete(&usp_tx);
    amxc_llist_clean(&resp_list, NULL);
    amxc_var_clean(&reply_data);
}

void test_uspl_get_supported_protocol_resp_extract(UNUSED void** state) {
    amxc_var_t reply_data;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    const char* versions = "1.0,2.0,3.0";

    // Fill up variant used to build USP response message
    amxc_var_init(&reply_data);
    amxc_var_set_type(&reply_data, AMXC_VAR_ID_CSV_STRING);
    amxc_var_set(csv_string_t, &reply_data, versions);

    amxc_llist_init(&resp_list);
    amxc_llist_append(&resp_list, &reply_data.lit);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_get_supported_protocol_resp_new(usp_tx, &resp_list, "456"), 0);
    assert_string_equal(usp_tx->msg_id, "456");

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Extract variant from response message
    amxc_llist_init(&extract_list);
    assert_int_equal(uspl_get_supported_protocol_resp_extract(usp_rx, &extract_list), 0);

    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    assert_string_equal(amxc_var_constcast(cstring_t, result), versions);

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, NULL);
    amxc_llist_clean(&extract_list, variant_list_it_free);
    amxc_var_clean(&reply_data);
}

void test_uspl_get_supported_protocol_resp_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_llist_t extract_list;

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_get_supported_protocol_resp_extract(NULL, &extract_list), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_get_supported_protocol_resp_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_get_supported_protocol_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_get_supported_protocol_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_get_supported_protocol_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_get_supported_protocol_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    assert_int_equal(uspl_get_supported_protocol_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response = malloc(sizeof(Usp__Response));
    usp__response__init(usp_rx->msg->body->response);
    assert_int_equal(uspl_get_supported_protocol_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response->resp_type_case = USP__RESPONSE__RESP_TYPE_GET_SUPPORTED_PROTOCOL_RESP;
    assert_int_equal(uspl_get_supported_protocol_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    usp_rx->msg->body->response->get_supported_protocol_resp = malloc(sizeof(Usp__GetSupportedProtocolResp));
    usp__get_supported_protocol_resp__init(usp_rx->msg->body->response->get_supported_protocol_resp);
    usp_rx->msg->body->response->get_supported_protocol_resp->agent_supported_protocol_versions = NULL;
    assert_int_equal(uspl_get_supported_protocol_resp_extract(usp_rx, &extract_list), USP_ERR_INVALID_ARGUMENTS);

    uspl_rx_delete(&usp_rx);
}