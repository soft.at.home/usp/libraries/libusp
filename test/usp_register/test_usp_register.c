/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdio.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_function.h>

#include <amxo/amxo.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_register.h"
#include "usp/uspl_msghandler.h"
#include "uspl_private.h"
#include "test_usp_register.h"

static amxo_parser_t parser;

static amxd_dm_t dm;

static bool test_verify_data(const amxc_var_t* data, const char* field, const char* value) {
    bool rv = false;
    char* field_value = NULL;
    amxc_var_t* field_data = GETP_ARG(data, field);

    printf("Verify event data: check field [%s] contains [%s]\n", field, value);
    fflush(stdout);
    assert_non_null(field_data);

    field_value = amxc_var_dyncast(cstring_t, field_data);
    assert_non_null(field_data);

    rv = (strcmp(field_value, value) == 0);

    free(field_value);
    return rv;
}

static void build_register_var(amxc_var_t* reg_var, bool add_keys) {
    amxc_var_t* reg_paths = NULL;
    amxc_var_t* reg_entry = NULL;
    amxc_var_t* keys = NULL;
    amxc_var_t* key_entry = NULL;

    amxc_var_set_type(reg_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, reg_var, "allow_partial", false);
    reg_paths = amxc_var_add_key(amxc_llist_t, reg_var, "reg_paths", NULL);

    reg_entry = amxc_var_add(amxc_htable_t, reg_paths, NULL);
    amxc_var_add_key(cstring_t, reg_entry, "path", "Device.Foo.");

    if(add_keys) {
        keys = amxc_var_add_key(amxc_llist_t, reg_entry, "keys", NULL);
        key_entry = amxc_var_add(amxc_htable_t, keys, NULL);
        amxc_var_add_key(bool, key_entry, "bool_key", true);
        amxc_var_add_key(cstring_t, key_entry, "string_key", "text");

        key_entry = amxc_var_add(amxc_htable_t, keys, NULL);
        amxc_var_add_key(uint32_t, key_entry, "int_key", 123);
    }
}

static void build_multi_register_var(amxc_var_t* reg_var) {
    amxc_var_t* reg_paths = NULL;
    amxc_var_t* reg_entry = NULL;

    amxc_var_set_type(reg_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, reg_var, "allow_partial", false);
    reg_paths = amxc_var_add_key(amxc_llist_t, reg_var, "reg_paths", NULL);

    reg_entry = amxc_var_add(amxc_htable_t, reg_paths, NULL);
    amxc_var_add_key(cstring_t, reg_entry, "path", "Device.Foo.");

    reg_entry = amxc_var_add(amxc_htable_t, reg_paths, NULL);
    amxc_var_add_key(cstring_t, reg_entry, "path", "Device.Foo.");
}

static void build_register_resp_list(amxc_llist_t* resp_list, bool add_params) {
    amxc_var_t* entry = NULL;
    amxc_var_t* oper_success = NULL;
    amxc_var_t* oper_failure = NULL;
    amxc_var_t* registered_params = NULL;

    amxc_var_new(&entry);
    amxc_var_set_type(entry, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, entry, "requested_path", "Device.Foo.");
    amxc_var_add_key(uint32_t, entry, "oper_status_case", USP__REGISTER_RESP__REGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS);

    oper_success = amxc_var_add_key(amxc_htable_t, entry, "oper_success", NULL);
    amxc_var_add_key(cstring_t, oper_success, "registered_path", "Device.Foo.");
    amxc_llist_append(resp_list, &entry->lit);

    amxc_var_new(&entry);
    amxc_var_set_type(entry, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, entry, "requested_path", "Device.Foo.");
    amxc_var_add_key(uint32_t, entry, "oper_status_case", USP__REGISTER_RESP__REGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE);

    oper_failure = amxc_var_add_key(amxc_htable_t, entry, "oper_failure", NULL);
    amxc_var_add_key(uint32_t, oper_failure, "err_code", USP_ERR_INTERNAL_ERROR);
    amxc_var_add_key(cstring_t, oper_failure, "err_msg", "Test error");

    if(add_params) {
        registered_params = amxc_var_add_key(amxc_llist_t, oper_failure, "registered_params", NULL);
        amxc_var_add(cstring_t, registered_params, "bar");
    }
    amxc_llist_append(resp_list, &entry->lit);
}

int test_dm_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), 0);

    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    return 0;
}

int test_dm_teardown(UNUSED void** state) {
    amxo_parser_clean(&parser);

    amxo_resolver_import_close_all();

    amxd_dm_clean(&dm);

    return 0;
}

void test_uspl_register_new(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t request;

    Usp__Register* reg = NULL;
    Usp__Register__RegistrationPath* reg_path = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&request);

    build_register_var(&request, false);

    amxc_var_dump(&request, STDOUT_FILENO);

    assert_int_equal(uspl_register_new(usp_tx, &request), 0);
    assert_string_equal(usp_tx->msg_id, "0");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__REGISTER);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Register message
    reg = usp_rx->msg->body->request->register_;
    assert_false(reg->allow_partial);
    assert_int_equal(reg->n_reg_paths, 1);

    reg_path = reg->reg_paths[0];
    assert_string_equal(reg_path->path, "Device.Foo.");

    amxc_var_clean(&request);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_register_new_no_keys(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t request;

    Usp__Register* reg = NULL;
    Usp__Register__RegistrationPath* reg_path = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&request);

    build_register_var(&request, false);

    amxc_var_dump(&request, STDOUT_FILENO);

    assert_int_equal(uspl_register_new(usp_tx, &request), 0);
    assert_string_equal(usp_tx->msg_id, "1");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__REGISTER);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Register message
    reg = usp_rx->msg->body->request->register_;
    assert_false(reg->allow_partial);
    assert_int_equal(reg->n_reg_paths, 1);

    reg_path = reg->reg_paths[0];
    assert_string_equal(reg_path->path, "Device.Foo.");

    amxc_var_clean(&request);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_register_new_invalid(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    amxc_var_t reg_var;
    amxc_var_t* reg_paths = NULL;
    amxc_var_t* reg_path = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&reg_var);

    assert_int_equal(uspl_register_new(NULL, &reg_var), -1);
    assert_int_equal(uspl_register_new(usp_tx, NULL), -1);
    assert_int_equal(uspl_register_new(usp_tx, &reg_var), -1);

    amxc_var_set_type(&reg_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &reg_var, "uri", "test");
    assert_int_equal(uspl_register_new(usp_tx, &reg_var), -1);

    reg_paths = amxc_var_add_key(amxc_llist_t, &reg_var, "reg_paths", NULL);
    assert_int_equal(uspl_register_new(usp_tx, &reg_var), -1);

    reg_path = amxc_var_add(amxc_htable_t, reg_paths, NULL);
    assert_int_equal(uspl_register_new(usp_tx, &reg_var), -1);
    amxc_var_add_key(cstring_t, reg_path, "path", "");
    assert_int_equal(uspl_register_new(usp_tx, &reg_var), -1);

    amxc_var_clean(&reg_var);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_register_extract(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t reg_var;
    amxc_var_t output_var;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&reg_var);
    build_register_var(&reg_var, false);

    assert_int_equal(uspl_register_new(usp_tx, &reg_var), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    amxc_var_init(&output_var);
    assert_int_equal(uspl_register_extract(usp_rx, &output_var), 0);
    amxc_var_dump(&output_var, STDOUT_FILENO);

    assert_true(test_verify_data(&output_var, "allow_partial", "false"));
    assert_true(test_verify_data(&output_var, "reg_paths.0.path", "Device.Foo."));

    amxc_var_clean(&reg_var);
    amxc_var_clean(&output_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_register_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t output_var;
    amxc_var_init(&output_var);

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_register_extract(NULL, &output_var), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_register_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_register_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_register_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_register_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_register_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_register_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request = malloc(sizeof(Usp__Request));
    usp__request__init(usp_rx->msg->body->request);
    assert_int_equal(uspl_register_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request->req_type_case = USP__REQUEST__REQ_TYPE_REGISTER;
    assert_int_equal(uspl_register_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    amxc_var_clean(&output_var);
    uspl_rx_delete(&usp_rx);
}

void test_uspl_register_resp(UNUSED void** state) {
    amxc_llist_t resp_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    Usp__RegisterResp* reg_resp = NULL;
    Usp__RegisterResp__RegisteredPathResult* reg_path_res = NULL;
    Usp__RegisterResp__RegisteredPathResult__OperationStatus__OperationSuccess* oper_success = NULL;
    Usp__RegisterResp__RegisteredPathResult__OperationStatus__OperationFailure* oper_failure = NULL;

    amxc_llist_init(&resp_list);
    build_register_resp_list(&resp_list, false);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_register_resp_new(usp_tx, &resp_list, "456"), 0);
    assert_string_equal(usp_tx->msg_id, "456");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__REGISTER_RESP);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(usp_rx->msg->header->msg_type, USP__HEADER__MSG_TYPE__REGISTER_RESP);
    assert_int_equal(usp_rx->msg->body->msg_body_case, USP__BODY__MSG_BODY_RESPONSE);
    assert_int_equal(usp_rx->msg->body->response->resp_type_case, USP__RESPONSE__RESP_TYPE_REGISTER_RESP);

    reg_resp = usp_rx->msg->body->response->register_resp;
    assert_int_equal(reg_resp->n_registered_path_results, 2);

    reg_path_res = reg_resp->registered_path_results[0];
    assert_string_equal(reg_path_res->requested_path, "Device.Foo.");
    assert_int_equal(reg_path_res->oper_status->oper_status_case, USP__REGISTER_RESP__REGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS);

    oper_success = reg_path_res->oper_status->oper_success;
    assert_string_equal(oper_success->registered_path, "Device.Foo.");

    reg_path_res = reg_resp->registered_path_results[1];
    assert_string_equal(reg_path_res->requested_path, "Device.Foo.");
    assert_int_equal(reg_path_res->oper_status->oper_status_case, USP__REGISTER_RESP__REGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE);

    oper_failure = reg_path_res->oper_status->oper_failure;
    assert_int_equal(oper_failure->err_code, USP_ERR_INTERNAL_ERROR);
    assert_string_equal(oper_failure->err_msg, "Test error");

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, variant_list_it_free);
}

void test_uspl_register_resp_no_params(UNUSED void** state) {
    amxc_llist_t resp_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    Usp__RegisterResp* reg_resp = NULL;
    Usp__RegisterResp__RegisteredPathResult* reg_path_res = NULL;
    Usp__RegisterResp__RegisteredPathResult__OperationStatus__OperationFailure* oper_failure = NULL;

    amxc_llist_init(&resp_list);
    build_register_resp_list(&resp_list, false);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_register_resp_new(usp_tx, &resp_list, "456"), 0);
    assert_string_equal(usp_tx->msg_id, "456");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__REGISTER_RESP);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(usp_rx->msg->header->msg_type, USP__HEADER__MSG_TYPE__REGISTER_RESP);
    assert_int_equal(usp_rx->msg->body->msg_body_case, USP__BODY__MSG_BODY_RESPONSE);
    assert_int_equal(usp_rx->msg->body->response->resp_type_case, USP__RESPONSE__RESP_TYPE_REGISTER_RESP);

    reg_resp = usp_rx->msg->body->response->register_resp;
    assert_int_equal(reg_resp->n_registered_path_results, 2);

    reg_path_res = reg_resp->registered_path_results[1];
    assert_string_equal(reg_path_res->requested_path, "Device.Foo.");
    assert_int_equal(reg_path_res->oper_status->oper_status_case, USP__REGISTER_RESP__REGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE);

    oper_failure = reg_path_res->oper_status->oper_failure;
    assert_int_equal(oper_failure->err_code, USP_ERR_INTERNAL_ERROR);
    assert_string_equal(oper_failure->err_msg, "Test error");

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, variant_list_it_free);
}

void test_uspl_register_resp_invalid(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    amxc_llist_t resp_list;
    amxc_var_t reply_data;
    amxc_var_t* requested_path = NULL;
    amxc_var_t* oper_status_case = NULL;
    amxc_var_t* oper_success = NULL;

    amxc_llist_init(&resp_list);
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    assert_int_equal(uspl_register_resp_new(NULL, &resp_list, "456"), -1);
    assert_int_equal(uspl_register_resp_new(usp_tx, NULL, "456"), -1);
    assert_int_equal(uspl_register_resp_new(usp_tx, &resp_list, NULL), -1);

    amxc_var_init(&reply_data);
    amxc_var_set_type(&reply_data, AMXC_VAR_ID_HTABLE);
    amxc_llist_append(&resp_list, &reply_data.lit);
    assert_int_equal(uspl_register_resp_new(usp_tx, &resp_list, "456"), -1);

    requested_path = amxc_var_add_key(cstring_t, &reply_data, "requested_path", "");
    assert_int_equal(uspl_register_resp_new(usp_tx, &resp_list, "456"), -1);

    amxc_var_set(cstring_t, requested_path, "Device.Foo.");
    assert_int_equal(uspl_register_resp_new(usp_tx, &resp_list, "456"), -1);

    oper_status_case = amxc_var_add_key(uint32_t, &reply_data, "oper_status_case", USP__REGISTER_RESP__REGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS);
    assert_int_equal(uspl_register_resp_new(usp_tx, &resp_list, "456"), -1);

    oper_success = amxc_var_add_key(amxc_htable_t, &reply_data, "oper_success", NULL);
    amxc_var_add_key(cstring_t, oper_success, "registered_path", "");
    assert_int_equal(uspl_register_resp_new(usp_tx, &resp_list, "456"), -1);

    amxc_var_set(uint32_t, oper_status_case, USP__REGISTER_RESP__REGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE);
    assert_int_equal(uspl_register_resp_new(usp_tx, &resp_list, "456"), -1);

    amxc_var_add_key(amxc_htable_t, &reply_data, "oper_failure", NULL);
    assert_int_equal(uspl_register_resp_new(usp_tx, &resp_list, "456"), -1);

    uspl_tx_delete(&usp_tx);
    amxc_var_clean(&reply_data);
    amxc_llist_clean(&resp_list, NULL);
}

void test_uspl_register_resp_extract(UNUSED void** state) {
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t* first = NULL;
    amxc_var_t* last = NULL;

    amxc_llist_init(&extract_list);
    amxc_llist_init(&resp_list);
    build_register_resp_list(&resp_list, false);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_register_resp_new(usp_tx, &resp_list, "456"), 0);
    assert_string_equal(usp_tx->msg_id, "456");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__REGISTER_RESP);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(uspl_register_resp_extract(usp_rx, &extract_list), 0);

    first = amxc_var_from_llist_it(amxc_llist_get_first(&extract_list));
    assert_non_null(first);
    amxc_var_dump(first, STDOUT_FILENO);

    assert_true(test_verify_data(first, "requested_path", "Device.Foo."));
    assert_true(test_verify_data(first, "oper_status_case", "2"));
    assert_true(test_verify_data(first, "oper_success.registered_path", "Device.Foo."));

    last = amxc_var_from_llist_it(amxc_llist_get_last(&extract_list));
    assert_non_null(last);
    amxc_var_dump(last, STDOUT_FILENO);

    assert_true(test_verify_data(last, "requested_path", "Device.Foo."));
    assert_true(test_verify_data(last, "oper_status_case", "1"));
    assert_true(test_verify_data(last, "oper_failure.err_code", "7003"));
    assert_true(test_verify_data(last, "oper_failure.err_msg", "Test error"));

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    amxc_llist_clean(&extract_list, variant_list_it_free);
}

void test_uspl_register_resp_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_llist_t extract_list;
    Usp__RegisterResp* reg_resp = NULL;

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_register_resp_extract(NULL, &extract_list), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_register_resp_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_register_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_register_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_register_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_register_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    assert_int_equal(uspl_register_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response = malloc(sizeof(Usp__Response));
    usp__response__init(usp_rx->msg->body->response);
    assert_int_equal(uspl_register_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response->resp_type_case = USP__RESPONSE__RESP_TYPE_REGISTER_RESP;
    assert_int_equal(uspl_register_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    usp_rx->msg->body->response->register_resp = malloc(sizeof(Usp__RegisterResp));
    usp__register_resp__init(usp_rx->msg->body->response->register_resp);

    reg_resp = usp_rx->msg->body->response->register_resp;
    reg_resp->registered_path_results = NULL;
    assert_int_equal(uspl_register_resp_extract(usp_rx, &extract_list), USP_ERR_INVALID_ARGUMENTS);
    reg_resp->n_registered_path_results = 1;
    assert_int_equal(uspl_register_resp_extract(usp_rx, &extract_list), USP_ERR_INVALID_ARGUMENTS);
    reg_resp->n_registered_path_results = 0;

    uspl_rx_delete(&usp_rx);
}

void test_uspl_register_multiple_paths(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t reg_var;
    amxc_var_t output_var;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&reg_var);
    build_multi_register_var(&reg_var);

    assert_int_equal(uspl_register_new(usp_tx, &reg_var), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    amxc_var_init(&output_var);
    assert_int_equal(uspl_register_extract(usp_rx, &output_var), 0);
    amxc_var_dump(&output_var, STDOUT_FILENO);

    assert_true(test_verify_data(&output_var, "allow_partial", "false"));
    assert_true(test_verify_data(&output_var, "reg_paths.0.path", "Device.Foo."));
    assert_true(test_verify_data(&output_var, "reg_paths.1.path", "Device.Foo."));

    amxc_var_clean(&reg_var);
    amxc_var_clean(&output_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}
