/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_function.h>

#include <amxo/amxo.h>

#include "uspl_private.h"
#include "test_usp_get_instances.h"
#include "usp/uspl_get_instances.h"
#include "usp/usp_err_codes.h"

static bool test_verify_data(const amxc_var_t* data, const char* field, const char* value) {
    bool rv = false;
    char* field_value = NULL;
    amxc_var_t* field_data = GETP_ARG(data, field);

    printf("Verify event data: check field [%s] contains [%s]\n", field, value);
    fflush(stdout);
    assert_non_null(field_data);

    field_value = amxc_var_dyncast(cstring_t, field_data);
    assert_non_null(field_data);

    rv = (strcmp(field_value, value) == 0);

    free(field_value);
    return rv;
}

static void build_get_instances(amxc_var_t* request) {
    amxc_var_t* paths = NULL;

    amxc_var_set_type(request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, request, "first_level_only", true);
    paths = amxc_var_add_key(amxc_llist_t, request, "obj_paths", NULL);
    amxc_var_add(cstring_t, paths, "foo");
    amxc_var_add(cstring_t, paths, "bar");
}

static void build_get_instances_resp(amxc_llist_t* resp_list) {
    amxc_var_t* get_inst_result = NULL;
    amxc_var_t* curr_insts = NULL;
    amxc_var_t* inst_entry = NULL;
    amxc_var_t* unique_keys = NULL;

    // Add result without instances
    amxc_var_new(&get_inst_result);
    amxc_var_set_type(get_inst_result, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, get_inst_result, "requested_path", "Path.To.NoInstObj.");
    amxc_var_add_key(uint32_t, get_inst_result, "err_code", USP_ERR_OK);
    amxc_llist_append(resp_list, &get_inst_result->lit);

    // Add result without keys
    amxc_var_new(&get_inst_result);
    amxc_var_set_type(get_inst_result, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, get_inst_result, "requested_path", "Path.To.NoKeyObj.");
    amxc_var_add_key(uint32_t, get_inst_result, "err_code", USP_ERR_OK);
    amxc_llist_append(resp_list, &get_inst_result->lit);

    curr_insts = amxc_var_add_key(amxc_llist_t, get_inst_result, "curr_insts", NULL);
    inst_entry = amxc_var_add(amxc_htable_t, curr_insts, NULL);
    amxc_var_add_key(cstring_t, inst_entry, "inst_path", "Path.To.NoKeyObj.1.");

    // Add result with keys
    amxc_var_new(&get_inst_result);
    amxc_var_set_type(get_inst_result, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, get_inst_result, "requested_path", "Path.To.KeyObj.");
    amxc_var_add_key(uint32_t, get_inst_result, "err_code", USP_ERR_OK);

    curr_insts = amxc_var_add_key(amxc_llist_t, get_inst_result, "curr_insts", NULL);
    inst_entry = amxc_var_add(amxc_htable_t, curr_insts, NULL);
    amxc_var_add_key(cstring_t, inst_entry, "inst_path", "Path.To.KeyObj.1.");

    unique_keys = amxc_var_add_key(amxc_htable_t, inst_entry, "unique_keys", NULL);
    amxc_var_add_key(cstring_t, unique_keys, "Alias", "foo");
    amxc_var_add_key(uint32_t, unique_keys, "ID", 666);
    amxc_llist_append(resp_list, &get_inst_result->lit);

    // Add failure result
    amxc_var_new(&get_inst_result);
    amxc_var_set_type(get_inst_result, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, get_inst_result, "requested_path", "Path.To.NonExisting.");
    amxc_var_add_key(uint32_t, get_inst_result, "err_code", USP_ERR_INVALID_PATH);
    amxc_var_add_key(cstring_t, get_inst_result, "err_msg", "Path not found");
    amxc_llist_append(resp_list, &get_inst_result->lit);
}

void test_uspl_get_instances_new(UNUSED void** state) {
    uspl_tx_t* usp = NULL;
    amxc_var_t request;
    Usp__GetInstances* get_inst = NULL;

    amxc_var_init(&request);
    build_get_instances(&request);

    assert_int_equal(uspl_tx_new(&usp, "one", "two"), 0);

    assert_int_equal(uspl_get_instances_new(NULL, NULL), -1);
    assert_int_equal(uspl_get_instances_new(usp, NULL), -1);
    assert_int_equal(uspl_get_instances_new(NULL, &request), -1);
    assert_int_equal(uspl_get_instances_new(usp, &request), 0);
    assert_string_equal(usp->msg_id, "0");
    assert_int_equal(usp->msg_type, USP__HEADER__MSG_TYPE__GET_INSTANCES);

    uspl_rx_t* usp_rx = uspl_msghandler_unpack_protobuf(usp->pbuf, usp->pbuf_len);
    assert_non_null(usp_rx);

    get_inst = usp_rx->msg->body->request->get_instances;
    assert_non_null(get_inst);
    assert_true(get_inst->first_level_only);
    assert_int_equal(get_inst->n_obj_paths, 2);
    assert_string_equal(get_inst->obj_paths[0], "foo");
    assert_string_equal(get_inst->obj_paths[1], "bar");

    amxc_var_clean(&request);
    uspl_tx_delete(&usp);
    uspl_rx_delete(&usp_rx);
}

void test_uspl_get_instances_extract(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t request;
    amxc_var_t result;
    amxc_var_t* paths = NULL;

    amxc_var_init(&request);
    amxc_var_init(&result);
    build_get_instances(&request);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    assert_int_equal(uspl_get_instances_new(usp_tx, &request), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);

    assert_int_equal(uspl_get_instances_extract(usp_rx, &result), 0);

    assert_true(GET_BOOL(&result, "first_level_only"));
    paths = GET_ARG(&result, "obj_paths");

    assert_string_equal(GETI_CHAR(paths, 0), "foo");
    assert_string_equal(GETI_CHAR(paths, 1), "bar");

    amxc_var_clean(&result);
    amxc_var_clean(&request);
    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
}

void test_uspl_get_instances_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t output_var;
    amxc_var_init(&output_var);

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_get_instances_extract(NULL, &output_var), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_get_instances_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_get_instances_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_get_instances_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_get_instances_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_get_instances_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    assert_int_equal(uspl_get_instances_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_get_instances_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request = malloc(sizeof(Usp__Request));
    usp__request__init(usp_rx->msg->body->request);
    assert_int_equal(uspl_get_instances_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request->req_type_case = USP__REQUEST__REQ_TYPE_GET_INSTANCES;
    assert_int_equal(uspl_get_instances_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    usp_rx->msg->body->request->get_instances = malloc(sizeof(Usp__GetInstances));
    usp__get_instances__init(usp_rx->msg->body->request->get_instances);
    assert_int_equal(uspl_get_instances_extract(usp_rx, &output_var), USP_ERR_INVALID_ARGUMENTS);
    usp_rx->msg->body->request->get_instances->n_obj_paths = 1;
    assert_int_equal(uspl_get_instances_extract(usp_rx, &output_var), USP_ERR_INVALID_ARGUMENTS);
    usp_rx->msg->body->request->get_instances->n_obj_paths = 0;

    amxc_var_clean(&output_var);
    uspl_rx_delete(&usp_rx);
}

void test_uspl_get_instances_resp_new(UNUSED void** state) {
    amxc_llist_t resp_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    Usp__GetInstancesResp* gi_resp = NULL;
    Usp__GetInstancesResp__RequestedPathResult* req_path_res = NULL;
    Usp__GetInstancesResp__CurrInstance* curr_inst = NULL;
    Usp__GetInstancesResp__CurrInstance__UniqueKeysEntry* unique_key = NULL;

    amxc_llist_init(&resp_list);

    build_get_instances_resp(&resp_list);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_get_instances_resp_new(usp_tx, &resp_list, "456"), 0);
    assert_string_equal(usp_tx->msg_id, "456");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__GET_INSTANCES_RESP);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(usp_rx->msg->header->msg_type, USP__HEADER__MSG_TYPE__GET_INSTANCES_RESP);
    assert_int_equal(usp_rx->msg->body->msg_body_case, USP__BODY__MSG_BODY_RESPONSE);
    assert_int_equal(usp_rx->msg->body->response->resp_type_case, USP__RESPONSE__RESP_TYPE_GET_INSTANCES_RESP);

    gi_resp = usp_rx->msg->body->response->get_instances_resp;
    assert_int_equal(gi_resp->n_req_path_results, 4);

    req_path_res = gi_resp->req_path_results[0];
    assert_string_equal(req_path_res->requested_path, "Path.To.NoInstObj.");
    assert_int_equal(req_path_res->err_code, USP_ERR_OK);
    assert_string_equal(req_path_res->err_msg, "");
    assert_int_equal(req_path_res->n_curr_insts, 0);
    assert_null(req_path_res->curr_insts);

    req_path_res = gi_resp->req_path_results[1];
    assert_string_equal(req_path_res->requested_path, "Path.To.NoKeyObj.");
    assert_int_equal(req_path_res->err_code, USP_ERR_OK);
    assert_string_equal(req_path_res->err_msg, "");
    assert_int_equal(req_path_res->n_curr_insts, 1);
    assert_non_null(req_path_res->curr_insts);
    curr_inst = req_path_res->curr_insts[0];
    assert_string_equal(curr_inst->instantiated_obj_path, "Path.To.NoKeyObj.1.");
    assert_int_equal(curr_inst->n_unique_keys, 0);
    assert_null(curr_inst->unique_keys);

    req_path_res = gi_resp->req_path_results[2];
    assert_string_equal(req_path_res->requested_path, "Path.To.KeyObj.");
    assert_int_equal(req_path_res->err_code, USP_ERR_OK);
    assert_string_equal(req_path_res->err_msg, "");
    assert_int_equal(req_path_res->n_curr_insts, 1);
    assert_non_null(req_path_res->curr_insts);
    curr_inst = req_path_res->curr_insts[0];
    assert_string_equal(curr_inst->instantiated_obj_path, "Path.To.KeyObj.1.");
    assert_int_equal(curr_inst->n_unique_keys, 2);
    assert_non_null(curr_inst->unique_keys);
    unique_key = curr_inst->unique_keys[0];
    assert_string_equal(unique_key->key, "ID");
    assert_string_equal(unique_key->value, "666");
    unique_key = curr_inst->unique_keys[1];
    assert_string_equal(unique_key->key, "Alias");
    assert_string_equal(unique_key->value, "foo");

    req_path_res = gi_resp->req_path_results[3];
    assert_string_equal(req_path_res->requested_path, "Path.To.NonExisting.");
    assert_int_equal(req_path_res->err_code, USP_ERR_INVALID_PATH);
    assert_string_equal(req_path_res->err_msg, "Path not found");
    assert_int_equal(req_path_res->n_curr_insts, 0);
    assert_null(req_path_res->curr_insts);

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, variant_list_it_free);
}

void test_uspl_get_instances_resp_new_invalid(UNUSED void** state) {
    amxc_var_t request;
    amxc_llist_t resp_list;
    uspl_tx_t usp_tx;

    amxc_var_init(&request);
    amxc_llist_init(&resp_list);
    amxc_llist_append(&resp_list, &request.lit);

    assert_int_equal(uspl_get_instances_resp_new(NULL, NULL, NULL), -1);
    assert_int_equal(uspl_get_instances_resp_new(&usp_tx, NULL, NULL), -1);
    assert_int_equal(uspl_get_instances_resp_new(&usp_tx, &resp_list, NULL), -1);
    assert_int_equal(uspl_get_instances_resp_new(&usp_tx, &resp_list, "123"), -1);

    amxc_llist_clean(&resp_list, NULL);
    amxc_var_clean(&request);
}

void test_uspl_get_instances_resp_extract(UNUSED void** state) {
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_llist_it_t* lit = NULL;
    amxc_var_t* result = NULL;

    amxc_llist_init(&resp_list);
    amxc_llist_init(&extract_list);

    build_get_instances_resp(&resp_list);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_get_instances_resp_new(usp_tx, &resp_list, "456"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(uspl_get_instances_resp_extract(usp_rx, &extract_list), 0);

    lit = amxc_llist_get_first(&extract_list);
    assert_non_null(lit);
    result = amxc_var_from_llist_it(lit);
    amxc_var_dump(result, STDOUT_FILENO);

    assert_true(test_verify_data(result, "requested_path", "Path.To.NoInstObj."));
    assert_true(test_verify_data(result, "err_code", "0"));
    assert_true(test_verify_data(result, "err_msg", ""));

    lit = amxc_llist_it_get_next(lit);
    assert_non_null(lit);
    result = amxc_var_from_llist_it(lit);
    amxc_var_dump(result, STDOUT_FILENO);

    assert_true(test_verify_data(result, "requested_path", "Path.To.NoKeyObj."));
    assert_true(test_verify_data(result, "err_code", "0"));
    assert_true(test_verify_data(result, "err_msg", ""));
    assert_true(test_verify_data(result, "curr_insts.0.inst_path", "Path.To.NoKeyObj.1."));

    lit = amxc_llist_it_get_next(lit);
    assert_non_null(lit);
    result = amxc_var_from_llist_it(lit);
    amxc_var_dump(result, STDOUT_FILENO);

    assert_true(test_verify_data(result, "requested_path", "Path.To.KeyObj."));
    assert_true(test_verify_data(result, "err_code", "0"));
    assert_true(test_verify_data(result, "err_msg", ""));
    assert_true(test_verify_data(result, "curr_insts.0.inst_path", "Path.To.KeyObj.1."));
    assert_true(test_verify_data(result, "curr_insts.0.unique_keys.0", "666"));
    assert_true(test_verify_data(result, "curr_insts.0.unique_keys.1", "foo"));

    lit = amxc_llist_it_get_next(lit);
    assert_non_null(lit);
    result = amxc_var_from_llist_it(lit);
    amxc_var_dump(result, STDOUT_FILENO);

    assert_true(test_verify_data(result, "requested_path", "Path.To.NonExisting."));
    assert_true(test_verify_data(result, "err_code", "7026"));
    assert_true(test_verify_data(result, "err_msg", "Path not found"));

    lit = amxc_llist_it_get_next(lit);
    assert_null(lit);

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    amxc_llist_clean(&extract_list, variant_list_it_free);
}

void test_uspl_get_instances_resp_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_llist_t extract_list;

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_get_instances_resp_extract(NULL, &extract_list), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_get_instances_resp_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_get_instances_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_get_instances_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_get_instances_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_get_instances_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_get_instances_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    assert_int_equal(uspl_get_instances_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response = malloc(sizeof(Usp__Response));
    usp__response__init(usp_rx->msg->body->response);
    assert_int_equal(uspl_get_instances_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response->resp_type_case = USP__RESPONSE__RESP_TYPE_GET_INSTANCES_RESP;
    assert_int_equal(uspl_get_instances_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    usp_rx->msg->body->response->get_instances_resp = malloc(sizeof(Usp__GetInstancesResp));
    usp__get_instances_resp__init(usp_rx->msg->body->response->get_instances_resp);
    assert_int_equal(uspl_get_instances_resp_extract(usp_rx, &extract_list), USP_ERR_INVALID_ARGUMENTS);
    usp_rx->msg->body->response->get_instances_resp->n_req_path_results = 1;
    assert_int_equal(uspl_get_instances_resp_extract(usp_rx, &extract_list), USP_ERR_INVALID_ARGUMENTS);
    usp_rx->msg->body->response->get_instances_resp->n_req_path_results = 0;

    uspl_rx_delete(&usp_rx);
}

void test_uspl_get_instances_status_translation(UNUSED void** state) {
    assert_int_equal(uspl_get_instances_amxd_status_to_usp_err(amxd_status_ok),
                     USP_ERR_OK);
    assert_int_equal(uspl_get_instances_amxd_status_to_usp_err(amxd_status_not_supported),
                     USP_ERR_INVALID_PATH);
    assert_int_equal(uspl_get_instances_amxd_status_to_usp_err(amxd_status_not_a_template),
                     USP_ERR_NOT_A_TABLE);
    assert_int_equal(uspl_get_instances_amxd_status_to_usp_err(amxd_status_permission_denied),
                     USP_ERR_OBJECT_DOES_NOT_EXIST);
    assert_int_equal(uspl_get_instances_amxd_status_to_usp_err(amxd_status_not_instantiated),
                     USP_ERR_OBJECT_DOES_NOT_EXIST);
    // default / anything else
    assert_int_equal(uspl_get_instances_amxd_status_to_usp_err(amxd_status_unknown_error),
                     USP_ERR_OBJECT_DOES_NOT_EXIST);
}
