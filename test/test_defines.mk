MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include ../mocks)

HEADERS = $(wildcard $(INCDIR)/usp/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c) \
          $(wildcard $(SRCDIR)/bbf/*.c)

MOCK_SRC_DIR = ../mocks
MOCK_SOURCES = $(wildcard $(MOCK_SRC_DIR)/*.c)

WRAP_FUNC=-Wl,--wrap=

CFLAGS += -Werror -Wall -Wextra -Wno-attributes \
          --std=gnu99 -g3 -Wmissing-declarations \
          $(addprefix -I ,$(INCDIR)) \
          -fprofile-arcs -ftest-coverage \
          -fkeep-inline-functions -fkeep-static-functions \
          $(shell pkg-config --cflags cmocka) -fPIC \

LDFLAGS += -fprofile-arcs -ftest-coverage \
           -fkeep-inline-functions -fkeep-static-functions \
           $(shell pkg-config --libs cmocka) \
           -lamxc -lamxj -lamxd -lamxp -lamxo -lamxb -lsahtrace -luspprotobuf

LDFLAGS += -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))
