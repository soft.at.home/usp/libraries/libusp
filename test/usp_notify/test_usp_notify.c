/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdio.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_error.h"
#include "usp/uspl_notify.h"
#include "usp/uspl_msghandler.h"
#include "uspl_private.h"
#include "test_usp_notify.h"

static void clean_list(amxc_llist_it_t* it) {
    amxc_var_t* data = amxc_llist_it_get_data(it, amxc_var_t, lit);
    amxc_var_delete(&data);
}

void test_uspl_notify_new_event(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t notify_var;
    amxc_var_t* event_var = NULL;
    amxc_var_t* table = NULL;
    amxc_var_t* list = NULL;

    Usp__Notify* notify = NULL;
    Usp__Notify__Event__ParamsEntry* params_entry = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&notify_var);
    amxc_var_set_type(&notify_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &notify_var, "subscription_id", "subs-1");
    amxc_var_add_key(bool, &notify_var, "send_resp", true);
    amxc_var_add_key(uint32_t, &notify_var, "notification_case", USP__NOTIFY__NOTIFICATION_EVENT);
    event_var = amxc_var_add_key(amxc_htable_t, &notify_var, "event", NULL);

    amxc_var_add_key(cstring_t, event_var, "object", "Device.");
    amxc_var_add_key(cstring_t, event_var, "path", "Device.");
    amxc_var_add_key(cstring_t, event_var, "event_name", "EventName");
    amxc_var_add_key(cstring_t, event_var, "str_param", "text");
    table = amxc_var_add_key(amxc_htable_t, event_var, "table", NULL);
    amxc_var_add_key(cstring_t, table, "table_var_1", "value");
    amxc_var_add_key(uint32_t, table, "table_var_2", 786);
    list = amxc_var_add_key(amxc_llist_t, event_var, "list", NULL);
    amxc_var_add(uint32_t, list, 0);
    amxc_var_add(uint32_t, list, 1);

    amxc_var_dump(&notify_var, STDOUT_FILENO);

    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), 0);
    assert_string_equal(usp_tx->msg_id, "0");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__NOTIFY);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Notify message
    notify = usp_rx->msg->body->request->notify;
    assert_non_null(notify);
    assert_int_equal(notify->notification_case, USP__NOTIFY__NOTIFICATION_EVENT);
    assert_true(notify->send_resp);
    assert_string_equal(notify->subscription_id, "subs-1");
    assert_non_null(notify->event);

    assert_string_equal(notify->event->event_name, "EventName");
    assert_string_equal(notify->event->obj_path, "Device.");
    assert_int_equal(notify->event->n_params, 4);

    params_entry = notify->event->params[0];
    assert_string_equal(params_entry->key, "str_param");
    assert_string_equal(params_entry->value, "text");

    params_entry = notify->event->params[1];
    assert_string_equal(params_entry->key, "table");
    assert_string_equal(params_entry->value, "{\"table_var_1\":\"value\",\"table_var_2\":786}");

    params_entry = notify->event->params[2];
    assert_string_equal(params_entry->key, "object");
    assert_string_equal(params_entry->value, "Device.");

    params_entry = notify->event->params[3];
    assert_string_equal(params_entry->key, "list");
    assert_string_equal(params_entry->value, "[0,1]");

    // Test extraction
    amxc_var_t result;
    amxc_var_t* res_event = NULL;
    amxc_var_t* res_list = NULL;
    amxc_var_t* res_table = NULL;
    amxc_var_init(&result);
    assert_int_equal(uspl_notify_extract(usp_rx, &result), 0);
    amxc_var_dump(&result, STDOUT_FILENO);

    assert_true(GET_BOOL(&result, "send_resp"));
    assert_string_equal(GET_CHAR(&result, "subscription_id"), "subs-1");
    assert_int_equal(GET_UINT32(&result, "notification_case"), USP__NOTIFY__NOTIFICATION_EVENT);

    res_event = GET_ARG(&result, "event");
    assert_non_null(res_event);
    assert_string_equal(GET_CHAR(res_event, "event_name"), "EventName");
    res_list = GET_ARG(res_event, "list");
    assert_non_null(res_list);
    assert_string_equal(GET_CHAR(res_event, "object"), "Device.");
    assert_string_equal(GET_CHAR(res_event, "path"), "Device.");
    assert_string_equal(GET_CHAR(res_event, "str_param"), "text");
    res_table = GET_ARG(res_event, "table");
    assert_non_null(res_table);

    amxc_var_clean(&result);
    amxc_var_clean(&notify_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_notify_new_vc(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t notify_var;
    amxc_var_t* vc_var = NULL;

    Usp__Notify* notify = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&notify_var);
    amxc_var_set_type(&notify_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &notify_var, "subscription_id", "subs-1");
    amxc_var_add_key(bool, &notify_var, "send_resp", true);
    amxc_var_add_key(uint32_t, &notify_var, "notification_case", USP__NOTIFY__NOTIFICATION_VALUE_CHANGE);
    vc_var = amxc_var_add_key(amxc_htable_t, &notify_var, "value_change", NULL);

    amxc_var_add_key(cstring_t, vc_var, "param_path", "path.foo");
    amxc_var_add_key(cstring_t, vc_var, "param_value", "bar");

    amxc_var_dump(&notify_var, STDOUT_FILENO);

    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Notify message
    notify = usp_rx->msg->body->request->notify;
    assert_non_null(notify);
    assert_int_equal(notify->notification_case, USP__NOTIFY__NOTIFICATION_VALUE_CHANGE);
    assert_true(notify->send_resp);
    assert_string_equal(notify->subscription_id, "subs-1");
    assert_non_null(notify->value_change);

    assert_string_equal(notify->value_change->param_path, "path.foo");
    assert_string_equal(notify->value_change->param_value, "bar");

    // Test extraction
    amxc_var_t result;
    amxc_var_t* res_vc = NULL;
    amxc_var_init(&result);
    assert_int_equal(uspl_notify_extract(usp_rx, &result), 0);
    amxc_var_dump(&result, STDOUT_FILENO);

    assert_true(GET_BOOL(&result, "send_resp"));
    assert_string_equal(GET_CHAR(&result, "subscription_id"), "subs-1");
    assert_int_equal(GET_UINT32(&result, "notification_case"), USP__NOTIFY__NOTIFICATION_VALUE_CHANGE);

    res_vc = GET_ARG(&result, "value_change");
    assert_non_null(res_vc);
    assert_string_equal(GET_CHAR(res_vc, "param_path"), "path.foo");
    assert_string_equal(GET_CHAR(res_vc, "param_value"), "bar");

    amxc_var_clean(&result);
    amxc_var_clean(&notify_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_notify_new_obj_creation(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t notify_var;
    amxc_var_t* sub_var = NULL;
    amxc_var_t* keys = NULL;

    Usp__Notify* notify = NULL;
    Usp__Notify__ObjectCreation__UniqueKeysEntry* unique_key = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&notify_var);
    amxc_var_set_type(&notify_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &notify_var, "subscription_id", "subs-1");
    amxc_var_add_key(bool, &notify_var, "send_resp", true);
    amxc_var_add_key(uint32_t, &notify_var, "notification_case", USP__NOTIFY__NOTIFICATION_OBJ_CREATION);
    sub_var = amxc_var_add_key(amxc_htable_t, &notify_var, "obj_creation", NULL);

    amxc_var_add_key(cstring_t, sub_var, "path", "path.foo");
    keys = amxc_var_add_key(amxc_htable_t, sub_var, "keys", NULL);
    amxc_var_add_key(bool, keys, "dummy_bool", true);
    amxc_var_add_key(uint64_t, keys, "dummy_int", 85);
    amxc_var_add_key(cstring_t, keys, "dummy_string", "bar");

    amxc_var_dump(&notify_var, STDOUT_FILENO);

    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Notify message
    notify = usp_rx->msg->body->request->notify;
    assert_non_null(notify);
    assert_int_equal(notify->notification_case, USP__NOTIFY__NOTIFICATION_OBJ_CREATION);
    assert_true(notify->send_resp);
    assert_string_equal(notify->subscription_id, "subs-1");
    assert_non_null(notify->obj_creation);

    assert_string_equal(notify->obj_creation->obj_path, "path.foo");
    assert_int_equal(notify->obj_creation->n_unique_keys, 3);

    unique_key = notify->obj_creation->unique_keys[0];
    assert_string_equal(unique_key->key, "dummy_string");
    assert_string_equal(unique_key->value, "bar");

    unique_key = notify->obj_creation->unique_keys[1];
    assert_string_equal(unique_key->key, "dummy_int");
    assert_string_equal(unique_key->value, "85");

    unique_key = notify->obj_creation->unique_keys[2];
    assert_string_equal(unique_key->key, "dummy_bool");
    assert_string_equal(unique_key->value, "true");

    // Test extraction
    amxc_var_t result;
    amxc_var_t* res_obj_creation = NULL;
    amxc_var_t* res_keys = NULL;
    amxc_var_init(&result);
    assert_int_equal(uspl_notify_extract(usp_rx, &result), 0);
    amxc_var_dump(&result, STDOUT_FILENO);

    assert_true(GET_BOOL(&result, "send_resp"));
    assert_string_equal(GET_CHAR(&result, "subscription_id"), "subs-1");
    assert_int_equal(GET_UINT32(&result, "notification_case"), USP__NOTIFY__NOTIFICATION_OBJ_CREATION);

    res_obj_creation = GET_ARG(&result, "obj_creation");
    assert_non_null(res_obj_creation);
    assert_string_equal(GET_CHAR(res_obj_creation, "path"), "path.foo");
    res_keys = GET_ARG(res_obj_creation, "keys");
    assert_non_null(res_keys);
    assert_true(GET_BOOL(res_keys, "dummy_bool"));

    assert_int_equal(amxc_var_dyncast(int64_t, GET_ARG(res_keys, "dummy_int")), 85);
    assert_string_equal(GET_CHAR(res_keys, "dummy_string"), "bar");

    amxc_var_clean(&result);
    amxc_var_clean(&notify_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_notify_new_obj_deletion(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t notify_var;
    amxc_var_t* sub_var = NULL;
    const char* instance_path = "path.foo.1.";

    Usp__Notify* notify = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&notify_var);
    amxc_var_set_type(&notify_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &notify_var, "subscription_id", "subs-1");
    amxc_var_add_key(bool, &notify_var, "send_resp", true);
    amxc_var_add_key(uint32_t, &notify_var, "notification_case", USP__NOTIFY__NOTIFICATION_OBJ_DELETION);
    sub_var = amxc_var_add_key(amxc_htable_t, &notify_var, "obj_deletion", NULL);

    amxc_var_add_key(cstring_t, sub_var, "path", instance_path);

    amxc_var_dump(&notify_var, STDOUT_FILENO);

    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Notify message
    notify = usp_rx->msg->body->request->notify;
    assert_non_null(notify);
    assert_int_equal(notify->notification_case, USP__NOTIFY__NOTIFICATION_OBJ_DELETION);
    assert_true(notify->send_resp);
    assert_string_equal(notify->subscription_id, "subs-1");
    assert_non_null(notify->obj_deletion);

    assert_string_equal(notify->obj_deletion->obj_path, instance_path);

    // Test extraction
    amxc_var_t result;
    amxc_var_t* res_obj_del;
    amxc_var_init(&result);
    assert_int_equal(uspl_notify_extract(usp_rx, &result), 0);
    amxc_var_dump(&result, STDOUT_FILENO);

    assert_true(GET_BOOL(&result, "send_resp"));
    assert_string_equal(GET_CHAR(&result, "subscription_id"), "subs-1");
    assert_int_equal(GET_UINT32(&result, "notification_case"), USP__NOTIFY__NOTIFICATION_OBJ_DELETION);

    res_obj_del = GET_ARG(&result, "obj_deletion");
    assert_non_null(res_obj_del);
    assert_string_equal(GET_CHAR(res_obj_del, "path"), instance_path);

    amxc_var_clean(&result);
    amxc_var_clean(&notify_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_notify_new_oper_complete_success(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t notify_var;
    amxc_var_t* sub_var = NULL;
    amxc_var_t* out_args_var = NULL;

    Usp__Notify* notify = NULL;
    Usp__Notify__OperationComplete__OutputArgs* req_out_args = NULL;
    Usp__Notify__OperationComplete__OutputArgs__OutputArgsEntry* entry = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&notify_var);
    amxc_var_set_type(&notify_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &notify_var, "subscription_id", "subs-1");
    amxc_var_add_key(bool, &notify_var, "send_resp", true);
    amxc_var_add_key(uint32_t, &notify_var, "notification_case", USP__NOTIFY__NOTIFICATION_OPER_COMPLETE);
    sub_var = amxc_var_add_key(amxc_htable_t, &notify_var, "oper_complete", NULL);

    amxc_var_add_key(cstring_t, sub_var, "path", "path.foo");
    amxc_var_add_key(cstring_t, sub_var, "cmd_name", "Reboot");
    amxc_var_add_key(cstring_t, sub_var, "cmd_key", "Key");
    amxc_var_add_key(uint32_t, sub_var, "operation_case", USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_REQ_OUTPUT_ARGS);
    out_args_var = amxc_var_add_key(amxc_htable_t, sub_var, "output_args", NULL);
    amxc_var_add_key(cstring_t, out_args_var, "foo", "bar");
    amxc_var_add_key(int32_t, out_args_var, "number", -75);

    amxc_var_dump(&notify_var, STDOUT_FILENO);

    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Notify message
    notify = usp_rx->msg->body->request->notify;
    assert_non_null(notify);
    assert_int_equal(notify->notification_case, USP__NOTIFY__NOTIFICATION_OPER_COMPLETE);
    assert_true(notify->send_resp);
    assert_string_equal(notify->subscription_id, "subs-1");
    assert_non_null(notify->oper_complete);

    assert_string_equal(notify->oper_complete->obj_path, "path.foo");
    assert_string_equal(notify->oper_complete->command_name, "Reboot");
    assert_string_equal(notify->oper_complete->command_key, "Key");
    assert_int_equal(notify->oper_complete->operation_resp_case, USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_REQ_OUTPUT_ARGS);

    req_out_args = notify->oper_complete->req_output_args;
    assert_int_equal(req_out_args->n_output_args, 2);

    entry = notify->oper_complete->req_output_args->output_args[0];
    assert_string_equal(entry->key, "number");
    assert_string_equal(entry->value, "-75");

    // Test extraction
    amxc_var_t result;
    amxc_var_t* res_oper = NULL;
    amxc_var_t* res_out_args = NULL;
    amxc_var_init(&result);
    assert_int_equal(uspl_notify_extract(usp_rx, &result), 0);
    amxc_var_dump(&result, STDOUT_FILENO);

    assert_true(GET_BOOL(&result, "send_resp"));
    assert_string_equal(GET_CHAR(&result, "subscription_id"), "subs-1");
    assert_int_equal(GET_UINT32(&result, "notification_case"), USP__NOTIFY__NOTIFICATION_OPER_COMPLETE);

    res_oper = GET_ARG(&result, "oper_complete");
    assert_non_null(res_oper);
    assert_string_equal(GET_CHAR(res_oper, "cmd_key"), "Key");
    assert_string_equal(GET_CHAR(res_oper, "cmd_name"), "Reboot");
    assert_string_equal(GET_CHAR(res_oper, "path"), "path.foo");
    assert_int_equal(GET_UINT32(res_oper, "operation_case"), USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_REQ_OUTPUT_ARGS);

    res_out_args = GET_ARG(res_oper, "output_args");
    assert_non_null(res_out_args);
    assert_string_equal(GET_CHAR(res_out_args, "foo"), "bar");
    assert_int_equal(amxc_var_dyncast(int64_t, GET_ARG(res_out_args, "number")), -75);

    amxc_var_clean(&result);
    amxc_var_clean(&notify_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_notify_new_oper_complete_success_no_out_args(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t notify_var;
    amxc_var_t* sub_var = NULL;
    const amxc_htable_t* out_args = NULL;

    Usp__Notify* notify = NULL;
    Usp__Notify__OperationComplete__OutputArgs* req_out_args = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&notify_var);
    amxc_var_set_type(&notify_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &notify_var, "subscription_id", "subs-1");
    amxc_var_add_key(bool, &notify_var, "send_resp", true);
    amxc_var_add_key(uint32_t, &notify_var, "notification_case", USP__NOTIFY__NOTIFICATION_OPER_COMPLETE);
    sub_var = amxc_var_add_key(amxc_htable_t, &notify_var, "oper_complete", NULL);

    amxc_var_add_key(cstring_t, sub_var, "path", "path.foo");
    amxc_var_add_key(cstring_t, sub_var, "cmd_name", "Reboot");
    amxc_var_add_key(cstring_t, sub_var, "cmd_key", "Key");
    amxc_var_add_key(uint32_t, sub_var, "operation_case", USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_REQ_OUTPUT_ARGS);

    amxc_var_dump(&notify_var, STDOUT_FILENO);

    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Notify message
    notify = usp_rx->msg->body->request->notify;
    assert_non_null(notify);
    assert_int_equal(notify->notification_case, USP__NOTIFY__NOTIFICATION_OPER_COMPLETE);
    assert_true(notify->send_resp);
    assert_string_equal(notify->subscription_id, "subs-1");
    assert_non_null(notify->oper_complete);

    assert_string_equal(notify->oper_complete->obj_path, "path.foo");
    assert_string_equal(notify->oper_complete->command_name, "Reboot");
    assert_string_equal(notify->oper_complete->command_key, "Key");
    assert_int_equal(notify->oper_complete->operation_resp_case, USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_REQ_OUTPUT_ARGS);

    req_out_args = notify->oper_complete->req_output_args;
    assert_non_null(req_out_args);
    assert_int_equal(req_out_args->n_output_args, 0);

    // Test extraction
    amxc_var_t result;
    amxc_var_t* res_oper = NULL;
    amxc_var_t* res_out_args = NULL;
    amxc_var_init(&result);
    assert_int_equal(uspl_notify_extract(usp_rx, &result), 0);
    amxc_var_dump(&result, STDOUT_FILENO);

    assert_true(GET_BOOL(&result, "send_resp"));
    assert_string_equal(GET_CHAR(&result, "subscription_id"), "subs-1");
    assert_int_equal(GET_UINT32(&result, "notification_case"), USP__NOTIFY__NOTIFICATION_OPER_COMPLETE);

    res_oper = GET_ARG(&result, "oper_complete");
    assert_non_null(res_oper);
    assert_string_equal(GET_CHAR(res_oper, "cmd_key"), "Key");
    assert_string_equal(GET_CHAR(res_oper, "cmd_name"), "Reboot");
    assert_string_equal(GET_CHAR(res_oper, "path"), "path.foo");
    assert_int_equal(GET_UINT32(res_oper, "operation_case"), USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_REQ_OUTPUT_ARGS);

    res_out_args = GET_ARG(res_oper, "output_args");
    assert_non_null(res_out_args);
    out_args = amxc_var_constcast(amxc_htable_t, res_out_args);
    assert_non_null(out_args);
    assert_true(amxc_htable_is_empty(out_args));

    amxc_var_clean(&result);
    amxc_var_clean(&notify_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_notify_new_oper_complete_failure(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t notify_var;
    amxc_var_t* sub_var = NULL;
    amxc_var_t* cmd_fail_var = NULL;

    Usp__Notify* notify = NULL;
    Usp__Notify__OperationComplete__CommandFailure* cmd_failure = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&notify_var);
    amxc_var_set_type(&notify_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &notify_var, "subscription_id", "subs-1");
    amxc_var_add_key(bool, &notify_var, "send_resp", true);
    amxc_var_add_key(uint32_t, &notify_var, "notification_case", USP__NOTIFY__NOTIFICATION_OPER_COMPLETE);
    sub_var = amxc_var_add_key(amxc_htable_t, &notify_var, "oper_complete", NULL);

    amxc_var_add_key(cstring_t, sub_var, "path", "path.foo");
    amxc_var_add_key(cstring_t, sub_var, "cmd_name", "Reboot");
    amxc_var_add_key(cstring_t, sub_var, "cmd_key", "Key");
    amxc_var_add_key(uint32_t, sub_var, "operation_case", USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_CMD_FAILURE);
    cmd_fail_var = amxc_var_add_key(amxc_htable_t, sub_var, "cmd_failure", NULL);
    amxc_var_add_key(uint32_t, cmd_fail_var, "err_code", 7007);
    amxc_var_add_key(cstring_t, cmd_fail_var, "err_msg", "Dummy error");

    amxc_var_dump(&notify_var, STDOUT_FILENO);

    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Notify message
    notify = usp_rx->msg->body->request->notify;
    assert_non_null(notify);
    assert_int_equal(notify->notification_case, USP__NOTIFY__NOTIFICATION_OPER_COMPLETE);
    assert_true(notify->send_resp);
    assert_string_equal(notify->subscription_id, "subs-1");
    assert_non_null(notify->oper_complete);

    assert_string_equal(notify->oper_complete->obj_path, "path.foo");
    assert_string_equal(notify->oper_complete->command_name, "Reboot");
    assert_string_equal(notify->oper_complete->command_key, "Key");
    assert_int_equal(notify->oper_complete->operation_resp_case, USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_CMD_FAILURE);

    cmd_failure = notify->oper_complete->cmd_failure;
    assert_int_equal(cmd_failure->err_code, 7007);
    assert_string_equal(cmd_failure->err_msg, "Dummy error");

    // Test extraction
    amxc_var_t result;
    amxc_var_t* res_oper = NULL;
    amxc_var_t* res_fail = NULL;
    amxc_var_init(&result);
    assert_int_equal(uspl_notify_extract(usp_rx, &result), 0);
    amxc_var_dump(&result, STDOUT_FILENO);

    assert_true(GET_BOOL(&result, "send_resp"));
    assert_string_equal(GET_CHAR(&result, "subscription_id"), "subs-1");
    assert_int_equal(GET_UINT32(&result, "notification_case"), USP__NOTIFY__NOTIFICATION_OPER_COMPLETE);

    res_oper = GET_ARG(&result, "oper_complete");
    assert_non_null(res_oper);
    assert_string_equal(GET_CHAR(res_oper, "cmd_key"), "Key");
    assert_string_equal(GET_CHAR(res_oper, "cmd_name"), "Reboot");
    assert_string_equal(GET_CHAR(res_oper, "path"), "path.foo");
    assert_int_equal(GET_UINT32(res_oper, "operation_case"), USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_CMD_FAILURE);

    res_fail = GET_ARG(res_oper, "cmd_failure");
    assert_non_null(res_fail);
    assert_string_equal(GET_CHAR(res_fail, "err_msg"), "Dummy error");
    assert_int_equal(GET_UINT32(res_fail, "err_code"), 7007);

    amxc_var_clean(&result);
    amxc_var_clean(&notify_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_notify_new_on_board_req(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t notify_var;
    amxc_var_t* sub_var = NULL;

    Usp__Notify* notify = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&notify_var);
    amxc_var_set_type(&notify_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &notify_var, "subscription_id", "subs-1");
    amxc_var_add_key(bool, &notify_var, "send_resp", true);
    amxc_var_add_key(uint32_t, &notify_var, "notification_case", USP__NOTIFY__NOTIFICATION_ON_BOARD_REQ);
    sub_var = amxc_var_add_key(amxc_htable_t, &notify_var, "on_board_req", NULL);

    amxc_var_add_key(cstring_t, sub_var, "oui", "12:34:56");
    amxc_var_add_key(cstring_t, sub_var, "product_class", "Test");
    amxc_var_add_key(cstring_t, sub_var, "serial_number", "666");
    amxc_var_add_key(cstring_t, sub_var, "agent_supported_protocol_version", "3.0");

    amxc_var_dump(&notify_var, STDOUT_FILENO);

    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Notify message
    notify = usp_rx->msg->body->request->notify;
    assert_non_null(notify);
    assert_int_equal(notify->notification_case, USP__NOTIFY__NOTIFICATION_ON_BOARD_REQ);
    assert_true(notify->send_resp);
    assert_string_equal(notify->subscription_id, "subs-1");
    assert_non_null(notify->on_board_req);

    assert_string_equal(notify->on_board_req->oui, "12:34:56");
    assert_string_equal(notify->on_board_req->product_class, "Test");
    assert_string_equal(notify->on_board_req->serial_number, "666");
    assert_string_equal(notify->on_board_req->agent_supported_protocol_versions, "3.0");

    // Test extraction
    amxc_var_t result;
    amxc_var_t* res_on_board = NULL;
    amxc_var_init(&result);
    assert_int_equal(uspl_notify_extract(usp_rx, &result), 0);
    amxc_var_dump(&result, STDOUT_FILENO);

    assert_true(GET_BOOL(&result, "send_resp"));
    assert_string_equal(GET_CHAR(&result, "subscription_id"), "subs-1");
    assert_int_equal(GET_UINT32(&result, "notification_case"), USP__NOTIFY__NOTIFICATION_ON_BOARD_REQ);

    res_on_board = GET_ARG(&result, "on_board_req");
    assert_non_null(res_on_board);
    assert_string_equal(GET_CHAR(res_on_board, "agent_supported_protocol_version"), "3.0");
    assert_string_equal(GET_CHAR(res_on_board, "oui"), "12:34:56");
    assert_string_equal(GET_CHAR(res_on_board, "product_class"), "Test");
    assert_string_equal(GET_CHAR(res_on_board, "serial_number"), "666");

    amxc_var_clean(&result);
    amxc_var_clean(&notify_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

/*
   {
    eobject = "Greeter.",
    notification = "dm:object-changed",
    object = "Greeter.",
    parameters = {
        State = {
            from = "Start",
            to = "Running"
        }
    },
    path = "Greeter."
   }
 */
void test_uspl_notify_new_amx_notification(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    Usp__Notify* notify = NULL;
    amxc_var_t notify_var;
    amxc_var_t extracted_var;
    amxc_var_t* sub_var = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    int result = 0;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&notify_var);
    amxc_var_set_type(&notify_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &notify_var, "subscription_id", "subs-1");
    amxc_var_add_key(bool, &notify_var, "send_resp", true);
    amxc_var_add_key(uint32_t, &notify_var, "notification_case", USP__NOTIFY__NOTIFICATION_AMX_NOTIFICATION);
    sub_var = amxc_var_add_key(amxc_htable_t, &notify_var, "amx_notification", NULL);

    amxc_var_add_key(cstring_t, sub_var, "eobject", "Greeter.");
    amxc_var_add_key(cstring_t, sub_var, "notification", "dm:object-changed");
    amxc_var_add_key(cstring_t, sub_var, "object", "Greeter.");
    amxc_var_add_key(cstring_t, sub_var, "path", "Greeter.");
    params = amxc_var_add_key(amxc_htable_t, sub_var, "parameters", NULL);
    param = amxc_var_add_key(amxc_htable_t, params, "State", NULL);
    amxc_var_add_key(cstring_t, param, "from", "Start");
    amxc_var_add_key(cstring_t, param, "to", "Running");

    amxc_var_dump(&notify_var, STDOUT_FILENO);

    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Notify message
    notify = usp_rx->msg->body->request->notify;
    assert_non_null(notify);
    assert_int_equal(notify->notification_case, USP__NOTIFY__NOTIFICATION_AMX_NOTIFICATION);
    assert_true(notify->send_resp);
    assert_string_equal(notify->subscription_id, "subs-1");
    assert_non_null(notify->amx_notification);

    assert_int_equal(notify->amx_notification->n_params, 5);
    assert_string_equal(notify->amx_notification->params[0]->key, "object");
    assert_string_equal(notify->amx_notification->params[0]->value, "Greeter.");
    assert_string_equal(notify->amx_notification->params[1]->key, "notification");
    assert_string_equal(notify->amx_notification->params[1]->value, "dm:object-changed");
    assert_string_equal(notify->amx_notification->params[2]->key, "eobject");
    assert_string_equal(notify->amx_notification->params[2]->value, "Greeter.");
    assert_string_equal(notify->amx_notification->params[3]->key, "parameters");
    assert_string_equal(notify->amx_notification->params[3]->value, "{\"State\":{\"from\":\"Start\",\"to\":\"Running\"}}");
    assert_string_equal(notify->amx_notification->params[4]->key, "path");
    assert_string_equal(notify->amx_notification->params[4]->value, "Greeter.");

    // Test extraction
    amxc_var_init(&extracted_var);
    assert_int_equal(uspl_notify_extract(usp_rx, &extracted_var), 0);

    assert_int_equal(amxc_var_compare(&notify_var, &extracted_var, &result), 0);
    assert_int_equal(result, 0);

    amxc_var_clean(&extracted_var);
    amxc_var_clean(&notify_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_notify_new_invalid(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t notify_var;
    amxc_var_t* notify_case = NULL;
    amxc_var_t* oper_case = NULL;
    amxc_var_t* sub_var = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    assert_int_equal(uspl_notify_new(NULL, &notify_var), -1);
    assert_int_equal(uspl_notify_new(usp_tx, NULL), -1);

    amxc_var_init(&notify_var);
    amxc_var_set_type(&notify_var, AMXC_VAR_ID_HTABLE);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);

    amxc_var_add_key(cstring_t, &notify_var, "subscription_id", "subs-1");
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);

    amxc_var_add_key(bool, &notify_var, "send_resp", true);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);

    notify_case = amxc_var_add_key(uint32_t, &notify_var, "notification_case", 9999);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);

    amxc_var_set(uint32_t, notify_case, USP__NOTIFY__NOTIFICATION_EVENT);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);

    amxc_var_set(uint32_t, notify_case, USP__NOTIFY__NOTIFICATION_VALUE_CHANGE);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);

    sub_var = amxc_var_add_key(amxc_htable_t, &notify_var, "value_change", NULL);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);

    amxc_var_add_key(cstring_t, sub_var, "param_path", "path.foo");
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);

    amxc_var_set(uint32_t, notify_case, USP__NOTIFY__NOTIFICATION_OBJ_CREATION);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);

    sub_var = amxc_var_add_key(amxc_htable_t, &notify_var, "obj_creation", NULL);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);
    amxc_var_add_key(cstring_t, sub_var, "path", "path.foo");
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), 0);

    // Delete and recreate struct
    uspl_tx_delete(&usp_tx);
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_set(uint32_t, notify_case, USP__NOTIFY__NOTIFICATION_OBJ_DELETION);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);

    sub_var = amxc_var_add_key(amxc_htable_t, &notify_var, "obj_deletion", NULL);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);

    amxc_var_set(uint32_t, notify_case, USP__NOTIFY__NOTIFICATION_OPER_COMPLETE);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);

    sub_var = amxc_var_add_key(amxc_htable_t, &notify_var, "oper_complete", NULL);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);
    amxc_var_add_key(cstring_t, sub_var, "path", "path.foo");
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);
    amxc_var_add_key(cstring_t, sub_var, "cmd_name", "cmd_name");
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);
    amxc_var_add_key(cstring_t, sub_var, "cmd_key", "cmd_key");
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);

    oper_case = amxc_var_add_key(uint32_t, sub_var, "operation_case", USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_REQ_OUTPUT_ARGS);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), 0); // notification without output_args is allowed

    amxc_var_set(uint32_t, oper_case, USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_CMD_FAILURE);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);
    amxc_var_add_key(amxc_htable_t, sub_var, "cmd_failure", NULL);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);

    amxc_var_set(uint32_t, notify_case, USP__NOTIFY__NOTIFICATION_ON_BOARD_REQ);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);

    sub_var = amxc_var_add_key(amxc_htable_t, &notify_var, "on_board_req", NULL);
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);
    amxc_var_add_key(cstring_t, sub_var, "oui", "dummy");
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);
    amxc_var_add_key(cstring_t, sub_var, "product_class", "dummy");
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);
    amxc_var_add_key(cstring_t, sub_var, "serial_number", "dummy");
    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), -1);

    amxc_var_clean(&notify_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_notify_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t output_var;

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_notify_extract(NULL, &output_var), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_notify_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_notify_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_notify_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_notify_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_notify_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    assert_int_equal(uspl_notify_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_notify_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request = malloc(sizeof(Usp__Request));
    usp__request__init(usp_rx->msg->body->request);
    assert_int_equal(uspl_notify_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request->req_type_case = USP__REQUEST__REQ_TYPE_SET;
    assert_int_equal(uspl_notify_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request->req_type_case = USP__REQUEST__REQ_TYPE_NOTIFY;
    assert_int_equal(uspl_notify_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    uspl_rx_delete(&usp_rx);
}

void test_uspl_notify_resp_new(UNUSED void** state) {
    amxc_var_t reply_data;
    amxc_llist_t resp_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;

    // Fill up variant used to build USP response message
    amxc_var_init(&reply_data);
    amxc_var_set_type(&reply_data, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, &reply_data, "dummy_id");

    amxc_llist_init(&resp_list);
    amxc_llist_append(&resp_list, &reply_data.lit);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_notify_resp_new(usp_tx, &resp_list, "456"), 0);
    assert_string_equal(usp_tx->msg_id, "456");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__NOTIFY_RESP);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(usp_rx->msg->header->msg_type, USP__HEADER__MSG_TYPE__NOTIFY_RESP);
    assert_int_equal(usp_rx->msg->body->msg_body_case, USP__BODY__MSG_BODY_RESPONSE);
    assert_int_equal(usp_rx->msg->body->response->resp_type_case, USP__RESPONSE__RESP_TYPE_NOTIFY_RESP);
    assert_string_equal(usp_rx->msg->body->response->notify_resp->subscription_id, "dummy_id");

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, NULL);
    amxc_var_clean(&reply_data);
}

void test_uspl_notify_resp_new_invalid(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    amxc_llist_t resp_list;
    amxc_var_t reply_data;

    amxc_llist_init(&resp_list);
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    assert_int_equal(uspl_notify_resp_new(NULL, &resp_list, "456"), -1);
    assert_int_equal(uspl_notify_resp_new(usp_tx, NULL, "456"), -1);
    assert_int_equal(uspl_notify_resp_new(usp_tx, &resp_list, NULL), -1);

    assert_int_equal(uspl_notify_resp_new(usp_tx, &resp_list, "456"), -1);

    amxc_var_init(&reply_data);
    amxc_llist_append(&resp_list, &reply_data.lit);
    assert_int_equal(uspl_notify_resp_new(usp_tx, &resp_list, "456"), -1);

    uspl_tx_delete(&usp_tx);
    amxc_llist_clean(&resp_list, NULL);
    amxc_var_clean(&reply_data);
}

void test_uspl_notify_resp_extract(UNUSED void** state) {
    amxc_var_t reply_data;
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;

    // Fill up variant used to build USP response message
    amxc_var_init(&reply_data);
    amxc_var_set_type(&reply_data, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, &reply_data, "dummy_id");

    amxc_llist_init(&resp_list);
    amxc_llist_append(&resp_list, &reply_data.lit);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_notify_resp_new(usp_tx, &resp_list, "456"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Extract variant from response message
    amxc_llist_init(&extract_list);
    assert_int_equal(uspl_notify_resp_extract(usp_rx, &extract_list), 0);

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, NULL);
    amxc_llist_clean(&extract_list, clean_list);
    amxc_var_clean(&reply_data);
}

void test_uspl_notify_resp_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_llist_t extract_list;

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_notify_resp_extract(NULL, &extract_list), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_notify_resp_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_notify_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_notify_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_notify_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_notify_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_notify_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    assert_int_equal(uspl_notify_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response = malloc(sizeof(Usp__Response));
    usp__response__init(usp_rx->msg->body->response);
    assert_int_equal(uspl_notify_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response->resp_type_case = USP__RESPONSE__RESP_TYPE_GET_RESP;
    assert_int_equal(uspl_notify_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    usp_rx->msg->body->response->resp_type_case = USP__RESPONSE__RESP_TYPE_NOTIFY_RESP;
    assert_int_equal(uspl_notify_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response->notify_resp = malloc(sizeof(Usp__NotifyResp));
    usp__notify_resp__init(usp_rx->msg->body->response->notify_resp);
    usp_rx->msg->body->response->notify_resp->subscription_id = NULL;
    assert_int_equal(uspl_notify_resp_extract(usp_rx, &extract_list), USP_ERR_INVALID_ARGUMENTS);

    uspl_rx_delete(&usp_rx);
}

void test_uspl_notify_oper_complete_install_du(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t notify_var;
    amxc_var_t* sub_var = NULL;
    const amxc_htable_t* out_args = NULL;

    Usp__Notify* notify = NULL;
    Usp__Notify__OperationComplete__OutputArgs* req_out_args = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "proto::Agent-OpenWrt", "controller"), 0);

    amxc_var_init(&notify_var);
    amxc_var_set_type(&notify_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &notify_var, "subscription_id", "install-operationcomplete");
    amxc_var_add_key(bool, &notify_var, "send_resp", true);
    amxc_var_add_key(uint32_t, &notify_var, "notification_case", USP__NOTIFY__NOTIFICATION_OPER_COMPLETE);
    sub_var = amxc_var_add_key(amxc_htable_t, &notify_var, "oper_complete", NULL);

    amxc_var_add_key(cstring_t, sub_var, "path", "Device.SoftwareModules.");
    amxc_var_add_key(cstring_t, sub_var, "cmd_name", "InstallDU");
    amxc_var_add_key(cstring_t, sub_var, "cmd_key", "ck18");
    amxc_var_add_key(uint32_t, sub_var, "operation_case", USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_REQ_OUTPUT_ARGS);

    amxc_var_dump(&notify_var, STDOUT_FILENO);

    assert_int_equal(uspl_notify_new(usp_tx, &notify_var), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Notify message
    notify = usp_rx->msg->body->request->notify;
    assert_non_null(notify);
    assert_int_equal(notify->notification_case, USP__NOTIFY__NOTIFICATION_OPER_COMPLETE);
    assert_true(notify->send_resp);
    assert_string_equal(notify->subscription_id, "install-operationcomplete");
    assert_non_null(notify->oper_complete);

    assert_string_equal(notify->oper_complete->obj_path, "Device.SoftwareModules.");
    assert_string_equal(notify->oper_complete->command_name, "InstallDU");
    assert_string_equal(notify->oper_complete->command_key, "ck18");
    assert_int_equal(notify->oper_complete->operation_resp_case, USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_REQ_OUTPUT_ARGS);

    req_out_args = notify->oper_complete->req_output_args;
    assert_non_null(req_out_args);
    assert_int_equal(req_out_args->n_output_args, 0);

    // Test extraction
    amxc_var_t result;
    amxc_var_t* res_oper = NULL;
    amxc_var_t* res_out_args = NULL;
    amxc_var_init(&result);
    assert_int_equal(uspl_notify_extract(usp_rx, &result), 0);
    amxc_var_dump(&result, STDOUT_FILENO);

    assert_true(GET_BOOL(&result, "send_resp"));
    assert_string_equal(GET_CHAR(&result, "subscription_id"), "install-operationcomplete");
    assert_int_equal(GET_UINT32(&result, "notification_case"), USP__NOTIFY__NOTIFICATION_OPER_COMPLETE);

    res_oper = GET_ARG(&result, "oper_complete");
    assert_non_null(res_oper);
    assert_string_equal(GET_CHAR(res_oper, "cmd_key"), "ck18");
    assert_string_equal(GET_CHAR(res_oper, "cmd_name"), "InstallDU");
    assert_string_equal(GET_CHAR(res_oper, "path"), "Device.SoftwareModules.");
    assert_int_equal(GET_UINT32(res_oper, "operation_case"), USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_REQ_OUTPUT_ARGS);

    res_out_args = GET_ARG(res_oper, "output_args");
    assert_non_null(res_out_args);
    out_args = amxc_var_constcast(amxc_htable_t, res_out_args);
    assert_non_null(out_args);
    assert_true(amxc_htable_is_empty(out_args));

    amxc_var_clean(&result);
    amxc_var_clean(&notify_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_read_raw_proto(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    const char* pbuf_str = "0A03312E30120A636F6E74726F6C6C65721A1470726F746F3A3A4167656E742D4F70656E5772743A5912570A060A0232351003124D0A4B42490A19696E7374616C6C2D6F7065726174696F6E636F6D706C6574653A2C0A174465766963652E536F6674776172654D6F64756C65732E1209496E7374616C6C44551A04636B31382200";
    uint32_t len = strlen(pbuf_str);
    unsigned char* pbuf = calloc(1, len);
    amxc_var_t result;

    amxc_var_init(&result);

    uint8_t* ptr = (uint8_t*) pbuf_str;
    for(uint32_t i = 0; i < len / 2; i++) {
        char* first_char = strndup((char*) ptr, 1);
        char* second_char = strndup((char*) ptr + 1, 1);
        int first_byte = (int) strtol(first_char, NULL, 16);
        int second_byte = (int) strtol(second_char, NULL, 16);
        pbuf[i] = second_byte + (first_byte << 4);
        ptr = ptr + 2;
        free(first_char);
        free(second_char);
    }

    usp_rx = uspl_msghandler_unpack_protobuf(pbuf, len / 2);
    assert_non_null(usp_rx);

    assert_int_equal(uspl_notify_extract(usp_rx, &result), 0);
    assert_int_equal(usp_rx->msg->body->request->notify->notification_case, USP__NOTIFY__NOTIFICATION_OPER_COMPLETE);
    assert_int_equal(usp_rx->msg->body->request->notify->oper_complete->operation_resp_case, USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_REQ_OUTPUT_ARGS);

    free(pbuf);
    amxc_var_clean(&result);
    uspl_rx_delete(&usp_rx);
}
