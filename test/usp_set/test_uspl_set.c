/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_function.h>

#include <amxo/amxo.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_msghandler.h"
#include "usp/uspl_set.h"
#include "uspl_private.h"

#include "test_uspl_set.h"

static amxo_parser_t parser;
static const char* odl_defs = "test_dm.odl";
static amxd_dm_t dm;

static bool test_verify_data(const amxc_var_t* data, const char* field, const char* value) {
    bool rv = false;
    char* field_value = NULL;
    amxc_var_t* field_data = GETP_ARG(data, field);

    printf("Verify event data: check field [%s] contains [%s]\n", field, value);
    fflush(stdout);
    assert_non_null(field_data);

    field_value = amxc_var_dyncast(cstring_t, field_data);
    assert_non_null(field_data);

    rv = (strcmp(field_value, value) == 0);

    free(field_value);
    return rv;
}

static void build_set_var(amxc_var_t* set_var) {
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;

    amxc_var_set_type(set_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, set_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, set_var, "requests", NULL);

    request = amxc_var_add(amxc_htable_t, requests, NULL);
    amxc_var_add_key(cstring_t, request, "object_path", "test_root.child.1.");
    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "string_param");
    amxc_var_add_key(cstring_t, param, "value", "new_string");
    amxc_var_add_key(bool, param, "required", false);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "int_param");
    amxc_var_add_key(uint32_t, param, "value", 6000);
    amxc_var_add_key(bool, param, "required", true);

    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "bool_param");
    amxc_var_add_key(bool, param, "value", true);
    amxc_var_add_key(bool, param, "required", true);
}

static void build_set_resp_list(amxc_llist_t* resp_list) {
    amxc_var_t* set_success = NULL;
    amxc_var_t* success = NULL;
    amxc_var_t* updated_inst_results = NULL;
    amxc_var_t* res = NULL;
    amxc_var_t* updated_params = NULL;
    amxc_var_t* param_errs = NULL;
    amxc_var_t* err = NULL;
    amxc_var_t* set_failure = NULL;
    amxc_var_t* failure = NULL;
    amxc_var_t* updated_inst_failures = NULL;

    amxc_var_new(&set_success);
    amxc_var_set_type(set_success, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, set_success, "requested_path", "test_root.child.*.");
    amxc_var_add_key(uint32_t, set_success, "oper_status", USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS);

    success = amxc_var_add_key(amxc_htable_t, set_success, "success", NULL);
    updated_inst_results = amxc_var_add_key(amxc_llist_t, success, "updated_inst_results", NULL);

    res = amxc_var_add(amxc_htable_t, updated_inst_results, NULL);
    amxc_var_add_key(cstring_t, res, "affected_path", "test_root.child.1.");
    updated_params = amxc_var_add_key(amxc_htable_t, res, "updated_params", NULL);
    amxc_var_add_key(cstring_t, updated_params, "string_param", "foo");
    amxc_var_add_key(uint32_t, updated_params, "int_param", 122);
    amxc_var_add_key(bool, updated_params, "bool_param", true);
    param_errs = amxc_var_add_key(amxc_llist_t, res, "param_errs", NULL);
    err = amxc_var_add(amxc_htable_t, param_errs, NULL);
    amxc_var_add_key(cstring_t, err, "param", "InvalidParam");
    amxc_var_add_key(uint32_t, err, "err_code", USP_ERR_INVALID_PATH);
    amxc_var_add_key(cstring_t, err, "err_msg", "Invalid parameter path");

    res = amxc_var_add(amxc_htable_t, updated_inst_results, NULL);
    amxc_var_add_key(cstring_t, res, "affected_path", "test_root.child.2.");
    param_errs = amxc_var_add_key(amxc_llist_t, res, "param_errs", NULL);
    err = amxc_var_add(amxc_htable_t, param_errs, NULL);
    amxc_var_add_key(cstring_t, err, "param", "string_param");
    amxc_var_add_key(uint32_t, err, "err_code", USP_ERR_PERMISSION_DENIED);
    amxc_var_add_key(cstring_t, err, "err_msg", "No permission");
    err = amxc_var_add(amxc_htable_t, param_errs, NULL);
    amxc_var_add_key(cstring_t, err, "param", "InvalidParam");
    amxc_var_add_key(uint32_t, err, "err_code", USP_ERR_INVALID_PATH);
    amxc_var_add_key(cstring_t, err, "err_msg", "Invalid parameter path");

    amxc_llist_append(resp_list, &set_success->lit);

    amxc_var_new(&set_failure);
    amxc_var_set_type(set_failure, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, set_failure, "requested_path", "test_root.child_2.*.");
    amxc_var_add_key(uint32_t, set_failure, "oper_status", USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE);

    failure = amxc_var_add_key(amxc_htable_t, set_failure, "failure", NULL);
    amxc_var_add_key(uint32_t, failure, "err_code", USP_ERR_REQUIRED_PARAM_FAILED);
    amxc_var_add_key(cstring_t, failure, "err_msg", "Required param failed to be set");
    updated_inst_failures = amxc_var_add_key(amxc_llist_t, failure, "updated_inst_failures", NULL);

    res = amxc_var_add(amxc_htable_t, updated_inst_failures, NULL);
    amxc_var_add_key(cstring_t, res, "affected_path", "test_root.child_2.1.");
    param_errs = amxc_var_add_key(amxc_llist_t, res, "param_errs", NULL);
    err = amxc_var_add(amxc_htable_t, param_errs, NULL);
    amxc_var_add_key(cstring_t, err, "param", "child_param_1");
    amxc_var_add_key(uint32_t, err, "err_code", USP_ERR_PERMISSION_DENIED);
    amxc_var_add_key(cstring_t, err, "err_msg", "No permission");
    err = amxc_var_add(amxc_htable_t, param_errs, NULL);
    amxc_var_add_key(cstring_t, err, "param", "child_param_2");
    amxc_var_add_key(uint32_t, err, "err_code", USP_ERR_UNSUPPORTED_PARAM);
    amxc_var_add_key(cstring_t, err, "err_msg", "Parameter not supported");

    amxc_llist_append(resp_list, &set_failure->lit);
}

int test_dm_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), 0);

    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    assert_int_equal(amxo_parser_invoke_entry_points(&parser, &dm, AMXO_START), 0);

    return 0;
}

int test_dm_teardown(UNUSED void** state) {
    assert_int_equal(amxo_parser_invoke_entry_points(&parser, &dm, AMXO_STOP), 0);

    amxo_parser_clean(&parser);

    amxo_resolver_import_close_all();

    amxd_dm_clean(&dm);

    return 0;
}

void test_uspl_set_new(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t set_var;

    Usp__Set* set = NULL;
    Usp__Set__UpdateObject* update_obj = NULL;
    Usp__Set__UpdateParamSetting* param_setting = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&set_var);

    build_set_var(&set_var);

    amxc_var_dump(&set_var, STDOUT_FILENO);

    assert_int_equal(uspl_set_new(usp_tx, &set_var), 0);
    assert_string_equal(usp_tx->msg_id, "0");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__SET);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Set message
    set = usp_rx->msg->body->request->set;
    assert_false(set->allow_partial);
    assert_int_equal(set->n_update_objs, 1);

    update_obj = set->update_objs[0];
    assert_string_equal(update_obj->obj_path, "test_root.child.1.");
    assert_int_equal(update_obj->n_param_settings, 3);

    param_setting = update_obj->param_settings[0];
    assert_string_equal(param_setting->param, "string_param");
    assert_string_equal(param_setting->value, "new_string");
    assert_false(param_setting->required);

    param_setting = update_obj->param_settings[1];
    assert_string_equal(param_setting->param, "int_param");
    assert_string_equal(param_setting->value, "6000");
    assert_true(param_setting->required);

    param_setting = update_obj->param_settings[2];
    assert_string_equal(param_setting->param, "bool_param");
    assert_string_equal(param_setting->value, "true");
    assert_true(param_setting->required);

    amxc_var_clean(&set_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_set_new_invalid(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    amxc_var_t set_var;
    amxc_var_t* requests = NULL;
    amxc_var_t* request = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* param = NULL;
    amxc_var_t* dummy = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&set_var);

    assert_int_equal(uspl_set_new(NULL, &set_var), -1);
    assert_int_equal(uspl_set_new(usp_tx, NULL), -1);

    amxc_var_set_type(&set_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &set_var, "allow_partial", true);
    assert_int_equal(uspl_set_new(usp_tx, &set_var), -1);

    requests = amxc_var_add_key(amxc_llist_t, &set_var, "requests", NULL);
    assert_int_equal(uspl_set_new(usp_tx, &set_var), -1);

    request = amxc_var_add(amxc_htable_t, requests, NULL);
    assert_int_equal(uspl_set_new(usp_tx, &set_var), -1);
    amxc_var_add_key(cstring_t, request, "object_path", "test_root.child.1.");
    assert_int_equal(uspl_set_new(usp_tx, &set_var), -1);

    params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);
    dummy = amxc_var_add(cstring_t, params, "dummy");
    amxc_var_dump(&set_var, STDOUT_FILENO);
    assert_int_equal(uspl_set_new(usp_tx, &set_var), -1);
    amxc_llist_it_clean(&dummy->lit, variant_list_it_free);
    param = amxc_var_add(amxc_htable_t, params, NULL);
    amxc_var_add_key(cstring_t, param, "param", "str_param");
    assert_int_equal(uspl_set_new(usp_tx, &set_var), -1);

    amxc_var_clean(&set_var);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_set_extract(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t set_var;
    amxc_var_t output_var;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&set_var);
    build_set_var(&set_var);

    assert_int_equal(uspl_set_new(usp_tx, &set_var), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    amxc_var_init(&output_var);
    assert_int_equal(uspl_set_extract(usp_rx, &output_var), 0);
    amxc_var_dump(&output_var, STDOUT_FILENO); // TODO verify fields

    amxc_var_clean(&set_var);
    amxc_var_clean(&output_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_set_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t output_var;
    amxc_var_init(&output_var);

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_set_extract(NULL, &output_var), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_set_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_set_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_set_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_set_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_set_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_set_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request = malloc(sizeof(Usp__Request));
    usp__request__init(usp_rx->msg->body->request);
    assert_int_equal(uspl_set_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request->req_type_case = USP__REQUEST__REQ_TYPE_SET;
    assert_int_equal(uspl_set_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    usp_rx->msg->body->request->set = malloc(sizeof(Usp__Set));
    usp__set__init(usp_rx->msg->body->request->set);
    usp_rx->msg->body->request->set->allow_partial = true;
    assert_int_equal(uspl_set_extract(usp_rx, &output_var), USP_ERR_OK);

    amxc_var_clean(&output_var);
    uspl_rx_delete(&usp_rx);
}

void test_uspl_set_resp_new(UNUSED void** state) {
    amxc_llist_t resp_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    Usp__SetResp* set_resp = NULL;
    Usp__SetResp__UpdatedObjectResult* updated_obj_result = NULL;
    Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationSuccess* success = NULL;
    Usp__SetResp__UpdatedInstanceResult* updated_inst_result = NULL;
    Usp__SetResp__UpdatedInstanceResult__UpdatedParamsEntry* params_entry = NULL;
    Usp__SetResp__ParameterError* param_error = NULL;
    Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationFailure* failure = NULL;
    Usp__SetResp__UpdatedInstanceFailure* updated_inst_failure = NULL;

    amxc_llist_init(&resp_list);

    build_set_resp_list(&resp_list);
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "123"), USP_ERR_OK);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    set_resp = usp_rx->msg->body->response->set_resp;
    assert_non_null(set_resp);
    assert_int_equal(set_resp->n_updated_obj_results, 2);

    // 1st requested path
    updated_obj_result = set_resp->updated_obj_results[0];
    assert_non_null(updated_obj_result);
    assert_string_equal(updated_obj_result->requested_path, "test_root.child.*.");
    assert_int_equal(updated_obj_result->oper_status->oper_status_case, USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS);

    success = updated_obj_result->oper_status->oper_success;
    assert_non_null(success);
    assert_int_equal(success->n_updated_inst_results, 2);

    // 1st successful object
    updated_inst_result = success->updated_inst_results[0];
    assert_non_null(updated_inst_result);
    assert_string_equal(updated_inst_result->affected_path, "test_root.child.1.");
    assert_int_equal(updated_inst_result->n_updated_params, 3);
    assert_int_equal(updated_inst_result->n_param_errs, 1);

    params_entry = updated_inst_result->updated_params[0];
    assert_non_null(params_entry);
    assert_string_equal(params_entry->key, "int_param");
    assert_string_equal(params_entry->value, "122");
    params_entry = updated_inst_result->updated_params[1];
    assert_non_null(params_entry);
    assert_string_equal(params_entry->key, "bool_param");
    assert_string_equal(params_entry->value, "true");
    params_entry = updated_inst_result->updated_params[2];
    assert_non_null(params_entry);
    assert_string_equal(params_entry->key, "string_param");
    assert_string_equal(params_entry->value, "foo");

    param_error = updated_inst_result->param_errs[0];
    assert_non_null(param_error);
    assert_string_equal(param_error->param, "InvalidParam");
    assert_string_equal(param_error->err_msg, "Invalid parameter path");
    assert_int_equal(param_error->err_code, USP_ERR_INVALID_PATH);

    // 2nd successful object
    updated_inst_result = success->updated_inst_results[1];
    assert_non_null(updated_inst_result);
    assert_string_equal(updated_inst_result->affected_path, "test_root.child.2.");
    assert_int_equal(updated_inst_result->n_updated_params, 0);
    assert_int_equal(updated_inst_result->n_param_errs, 2);

    param_error = updated_inst_result->param_errs[0];
    assert_non_null(param_error);
    assert_string_equal(param_error->param, "string_param");
    assert_string_equal(param_error->err_msg, "No permission");
    assert_int_equal(param_error->err_code, USP_ERR_PERMISSION_DENIED);
    param_error = updated_inst_result->param_errs[1];
    assert_non_null(param_error);
    assert_string_equal(param_error->param, "InvalidParam");
    assert_string_equal(param_error->err_msg, "Invalid parameter path");
    assert_int_equal(param_error->err_code, USP_ERR_INVALID_PATH);

    // 2nd requested path
    updated_obj_result = set_resp->updated_obj_results[1];
    assert_non_null(updated_obj_result);
    assert_string_equal(updated_obj_result->requested_path, "test_root.child_2.*.");
    assert_int_equal(updated_obj_result->oper_status->oper_status_case, USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE);

    failure = updated_obj_result->oper_status->oper_failure;
    assert_non_null(failure);
    assert_int_equal(failure->n_updated_inst_failures, 1);
    assert_int_equal(failure->err_code, USP_ERR_REQUIRED_PARAM_FAILED);
    assert_string_equal(failure->err_msg, "Required param failed to be set");

    updated_inst_failure = failure->updated_inst_failures[0];
    assert_non_null(updated_inst_failure);
    assert_string_equal(updated_inst_failure->affected_path, "test_root.child_2.1.");
    assert_int_equal(updated_inst_failure->n_param_errs, 2);

    param_error = updated_inst_failure->param_errs[0];
    assert_non_null(param_error);
    assert_string_equal(param_error->param, "child_param_1");
    assert_string_equal(param_error->err_msg, "No permission");
    assert_int_equal(param_error->err_code, USP_ERR_PERMISSION_DENIED);
    param_error = updated_inst_failure->param_errs[1];
    assert_non_null(param_error);
    assert_string_equal(param_error->param, "child_param_2");
    assert_string_equal(param_error->err_msg, "Parameter not supported");
    assert_int_equal(param_error->err_code, USP_ERR_UNSUPPORTED_PARAM);

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_set_resp_new_invalid(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    amxc_llist_t resp_list;
    amxc_var_t set_success;
    amxc_var_t set_failure;
    amxc_var_t* requested_path = NULL;
    amxc_var_t* success = NULL;
    amxc_var_t* updated_inst_results = NULL;
    amxc_var_t* res = NULL;
    amxc_var_t* failure = NULL;
    amxc_var_t* updated_inst_failures = NULL;
    amxc_var_t* err_msg = NULL;
    amxc_var_t* err_code = NULL;
    amxc_var_t* affected_path = NULL;

    amxc_llist_init(&resp_list);
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    assert_int_equal(uspl_set_resp_new(NULL, &resp_list, "456"), -1);
    assert_int_equal(uspl_set_resp_new(usp_tx, NULL, "456"), -1);
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, NULL), -1);

    // Check invalid success
    amxc_var_init(&set_success);
    amxc_var_set_type(&set_success, AMXC_VAR_ID_HTABLE);
    amxc_llist_append(&resp_list, &set_success.lit);
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), -1);

    requested_path = amxc_var_add_key(cstring_t, &set_success, "requested_path", "");
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), -1);

    amxc_var_set(cstring_t, requested_path, "test_root.child.*.");
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), -1);

    amxc_var_add_key(uint32_t, &set_success, "oper_status", USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS);
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), -1);

    success = amxc_var_add_key(amxc_htable_t, &set_success, "success", NULL);
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), -1);

    updated_inst_results = amxc_var_add_key(amxc_llist_t, success, "updated_inst_results", NULL);
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), -1);

    res = amxc_var_add(amxc_htable_t, updated_inst_results, NULL);
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), -1);

    amxc_var_add_key(cstring_t, res, "affected_path", "test_root.child.1.");
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), 0);
    uspl_tx_delete(&usp_tx);

    // Check invalid failure
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    amxc_var_init(&set_failure);
    amxc_var_set_type(&set_failure, AMXC_VAR_ID_HTABLE);
    amxc_llist_append(&resp_list, &set_failure.lit);

    amxc_var_add_key(cstring_t, &set_failure, "requested_path", "test_root.child.*.");
    amxc_var_add_key(uint32_t, &set_failure, "oper_status", USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE);
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), -1);

    failure = amxc_var_add_key(amxc_htable_t, &set_failure, "failure", NULL);
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), -1);

    updated_inst_failures = amxc_var_add_key(amxc_llist_t, failure, "updated_inst_failures", NULL);
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), -1);

    err_msg = amxc_var_add_key(cstring_t, failure, "err_msg", "");
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), -1);
    amxc_var_set(cstring_t, err_msg, "Required param failed to be set");
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), -1);

    err_code = amxc_var_add_key(uint32_t, failure, "err_code", USP_ERR_OK);
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), -1);
    amxc_var_set(uint32_t, err_code, USP_ERR_REQUIRED_PARAM_FAILED);

    res = amxc_var_add(amxc_htable_t, updated_inst_failures, NULL);
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), -1);

    affected_path = amxc_var_add_key(cstring_t, res, "affected_path", "");
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), -1);
    amxc_var_set(cstring_t, affected_path, "Path");
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "456"), 0);

    uspl_tx_delete(&usp_tx);
    amxc_var_clean(&set_failure);
    amxc_var_clean(&set_success);
    amxc_llist_clean(&resp_list, NULL);
}

void test_uspl_set_resp_extract(UNUSED void** state) {
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_llist_it_t* first_it = NULL;
    amxc_llist_it_t* second_it = NULL;
    amxc_var_t* first = NULL;
    amxc_var_t* second = NULL;
    amxc_var_t* check = NULL;

    amxc_llist_init(&resp_list);
    amxc_llist_init(&extract_list);

    build_set_resp_list(&resp_list);
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_set_resp_new(usp_tx, &resp_list, "123"), USP_ERR_OK);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(uspl_set_resp_extract(usp_rx, &extract_list), 0);

    first_it = amxc_llist_get_first(&extract_list);
    first = amxc_var_from_llist_it(first_it);
    amxc_var_dump(first, STDOUT_FILENO);

    assert_true(test_verify_data(first, "oper_status", "2"));
    assert_true(test_verify_data(first, "requested_path", "test_root.child.*."));
    assert_true(test_verify_data(first, "success.updated_inst_results.0.affected_path", "test_root.child.1."));
    assert_true(test_verify_data(first, "success.updated_inst_results.0.param_errs.0.err_code", "7026"));
    assert_true(test_verify_data(first, "success.updated_inst_results.0.param_errs.0.err_msg", "Invalid parameter path"));
    assert_true(test_verify_data(first, "success.updated_inst_results.0.param_errs.0.param", "InvalidParam"));
    assert_true(test_verify_data(first, "success.updated_inst_results.0.updated_params.bool_param", "true"));
    assert_true(test_verify_data(first, "success.updated_inst_results.0.updated_params.int_param", "122"));
    assert_true(test_verify_data(first, "success.updated_inst_results.0.updated_params.string_param", "foo"));

    // Check types
    check = GETP_ARG(first, "success.updated_inst_results.0.updated_params.bool_param");
    assert_int_equal(amxc_var_type_of(check), AMXC_VAR_ID_BOOL);
    check = GETP_ARG(first, "success.updated_inst_results.0.updated_params.int_param");
    assert_int_equal(amxc_var_type_of(check), AMXC_VAR_ID_UINT32);
    check = GETP_ARG(first, "success.updated_inst_results.0.updated_params.string_param");
    assert_int_equal(amxc_var_type_of(check), AMXC_VAR_ID_CSTRING);

    assert_true(test_verify_data(first, "success.updated_inst_results.1.affected_path", "test_root.child.2."));
    assert_true(test_verify_data(first, "success.updated_inst_results.1.param_errs.0.err_code", "7006"));
    assert_true(test_verify_data(first, "success.updated_inst_results.1.param_errs.0.err_msg", "No permission"));
    assert_true(test_verify_data(first, "success.updated_inst_results.1.param_errs.0.param", "string_param"));
    assert_true(test_verify_data(first, "success.updated_inst_results.1.param_errs.1.err_code", "7026"));
    assert_true(test_verify_data(first, "success.updated_inst_results.1.param_errs.1.err_msg", "Invalid parameter path"));
    assert_true(test_verify_data(first, "success.updated_inst_results.1.param_errs.1.param", "InvalidParam"));

    second_it = amxc_llist_it_get_next(first_it);
    second = amxc_var_from_llist_it(second_it);
    amxc_var_dump(second, STDOUT_FILENO);

    assert_true(test_verify_data(second, "failure.err_code", "7021"));
    assert_true(test_verify_data(second, "failure.err_msg", "Required param failed to be set"));
    assert_true(test_verify_data(second, "failure.updated_inst_failures.0.affected_path", "test_root.child_2.1."));
    assert_true(test_verify_data(second, "failure.updated_inst_failures.0.param_errs.0.err_code", "7006"));
    assert_true(test_verify_data(second, "failure.updated_inst_failures.0.param_errs.0.err_msg", "No permission"));
    assert_true(test_verify_data(second, "failure.updated_inst_failures.0.param_errs.0.param", "child_param_1"));
    assert_true(test_verify_data(second, "failure.updated_inst_failures.0.param_errs.1.err_code", "7010"));
    assert_true(test_verify_data(second, "failure.updated_inst_failures.0.param_errs.1.err_msg", "Parameter not supported"));
    assert_true(test_verify_data(second, "failure.updated_inst_failures.0.param_errs.1.param", "child_param_2"));

    amxc_llist_clean(&extract_list, variant_list_it_free);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_set_resp_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_llist_t extract_list;

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_set_resp_extract(NULL, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    assert_int_equal(uspl_set_resp_extract(usp_rx, NULL), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    assert_int_equal(uspl_set_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_set_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_set_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_set_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_set_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    assert_int_equal(uspl_set_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response = malloc(sizeof(Usp__Response));
    usp__response__init(usp_rx->msg->body->response);
    assert_int_equal(uspl_set_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response->resp_type_case = USP__RESPONSE__RESP_TYPE_GET_RESP;
    assert_int_equal(uspl_set_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response->resp_type_case = USP__RESPONSE__RESP_TYPE_SET_RESP;
    assert_int_equal(uspl_set_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    usp_rx->msg->body->response->set_resp = malloc(sizeof(Usp__SetResp));
    usp__set_resp__init(usp_rx->msg->body->response->set_resp);
    usp_rx->msg->body->response->set_resp->n_updated_obj_results = 0;
    assert_int_equal(uspl_set_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response->set_resp->n_updated_obj_results = 1;
    usp_rx->msg->body->response->set_resp->updated_obj_results = NULL;
    assert_int_equal(uspl_set_resp_extract(usp_rx, &extract_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    uspl_rx_delete(&usp_rx);
}
