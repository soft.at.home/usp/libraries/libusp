/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdio.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_function.h>

#include <amxo/amxo.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_deregister.h"
#include "usp/uspl_msghandler.h"
#include "uspl_private.h"
#include "test_usp_deregister.h"

static amxo_parser_t parser;

static amxd_dm_t dm;

static bool test_verify_data(const amxc_var_t* data, const char* field, const char* value) {
    bool rv = false;
    char* field_value = NULL;
    amxc_var_t* field_data = GETP_ARG(data, field);

    printf("Verify event data: check field [%s] contains [%s]\n", field, value);
    fflush(stdout);
    assert_non_null(field_data);

    field_value = amxc_var_dyncast(cstring_t, field_data);
    assert_non_null(field_data);

    rv = (strcmp(field_value, value) == 0);

    free(field_value);
    return rv;
}

static void build_deregister_var(amxc_var_t* reg_var) {
    amxc_var_set_type(reg_var, AMXC_VAR_ID_LIST);
    amxc_var_add(cstring_t, reg_var, "Phonebook.");
    amxc_var_add(cstring_t, reg_var, "Greeter.");
}

static void build_deregister_resp_var(amxc_llist_t* resp_list) {
    amxc_var_t* dereg = NULL;
    amxc_var_t* success = NULL;
    amxc_var_t* dereg_path = NULL;
    amxc_var_t* failure = NULL;

    amxc_var_new(&dereg);
    amxc_var_set_type(dereg, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, dereg, "requested_path", "Phonebook.");
    amxc_var_add_key(uint32_t, dereg, "oper_status_case", USP__DEREGISTER_RESP__DEREGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS);
    success = amxc_var_add_key(amxc_htable_t, dereg, "oper_success", NULL);
    dereg_path = amxc_var_add_key(amxc_llist_t, success, "deregistered_path", NULL);
    amxc_var_add(cstring_t, dereg_path, "Phonebook.");
    amxc_llist_append(resp_list, &dereg->lit);

    amxc_var_new(&dereg);
    amxc_var_set_type(dereg, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, dereg, "requested_path", "Greeter.");
    amxc_var_add_key(uint32_t, dereg, "oper_status_case", USP__DEREGISTER_RESP__DEREGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE);
    failure = amxc_var_add_key(amxc_htable_t, dereg, "oper_failure", NULL);
    amxc_var_add_key(cstring_t, failure, "err_msg", "Registration failed");
    amxc_var_add_key(uint32_t, failure, "err_code", USP_ERR_DEREGISTER_FAILURE);
    amxc_llist_append(resp_list, &dereg->lit);
}

int test_dm_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), 0);

    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    return 0;
}

int test_dm_teardown(UNUSED void** state) {
    amxo_parser_clean(&parser);

    amxo_resolver_import_close_all();

    amxd_dm_clean(&dm);

    return 0;
}

void test_uspl_deregister_new(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t request;

    Usp__Deregister* dereg = NULL;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&request);

    build_deregister_var(&request);

    amxc_var_dump(&request, STDOUT_FILENO);

    assert_int_equal(uspl_deregister_new(usp_tx, &request), 0);
    assert_string_equal(usp_tx->msg_id, "0");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__DEREGISTER);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    // Verify output of USP Register message
    dereg = usp_rx->msg->body->request->deregister;
    assert_int_equal(dereg->n_paths, 2);
    assert_string_equal(dereg->paths[0], "Phonebook.");
    assert_string_equal(dereg->paths[1], "Greeter.");

    amxc_var_clean(&request);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_deregister_new_invalid(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    amxc_var_t reg_var;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&reg_var);

    assert_int_equal(uspl_deregister_new(NULL, &reg_var), -1);
    assert_int_equal(uspl_deregister_new(usp_tx, NULL), -1);
    assert_int_equal(uspl_deregister_new(usp_tx, &reg_var), -1);

    amxc_var_clean(&reg_var);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_deregister_extract(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t request;
    amxc_var_t output_var;

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    amxc_var_init(&request);
    build_deregister_var(&request);

    assert_int_equal(uspl_deregister_new(usp_tx, &request), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    amxc_var_init(&output_var);
    assert_int_equal(uspl_deregister_extract(usp_rx, &output_var), 0);
    amxc_var_dump(&output_var, STDOUT_FILENO);

    assert_true(test_verify_data(&output_var, "0", "Phonebook."));
    assert_true(test_verify_data(&output_var, "1", "Greeter."));

    amxc_var_clean(&request);
    amxc_var_clean(&output_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_deregister_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t output_var;
    amxc_var_init(&output_var);

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_deregister_extract(NULL, &output_var), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_deregister_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_deregister_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_deregister_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_deregister_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_deregister_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_deregister_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request = malloc(sizeof(Usp__Request));
    usp__request__init(usp_rx->msg->body->request);
    assert_int_equal(uspl_deregister_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->request->req_type_case = USP__REQUEST__REQ_TYPE_DEREGISTER;
    assert_int_equal(uspl_deregister_extract(usp_rx, &output_var), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    amxc_var_clean(&output_var);
    uspl_rx_delete(&usp_rx);
}

void test_uspl_deregister_resp(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    Usp__DeregisterResp* dereg_resp = NULL;
    Usp__DeregisterResp__DeregisteredPathResult* dereg_path_res = NULL;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    build_deregister_resp_var(&resp_list);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_deregister_resp_new(usp_tx, &resp_list, "456"), 0);
    assert_string_equal(usp_tx->msg_id, "456");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__DEREGISTER_RESP);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(usp_rx->msg->header->msg_type, USP__HEADER__MSG_TYPE__DEREGISTER_RESP);
    assert_int_equal(usp_rx->msg->body->msg_body_case, USP__BODY__MSG_BODY_RESPONSE);
    assert_int_equal(usp_rx->msg->body->response->resp_type_case, USP__RESPONSE__RESP_TYPE_DEREGISTER_RESP);

    dereg_resp = usp_rx->msg->body->response->deregister_resp;
    assert_int_equal(dereg_resp->n_deregistered_path_results, 2);

    dereg_path_res = dereg_resp->deregistered_path_results[0];
    assert_non_null(dereg_path_res);
    assert_string_equal(dereg_path_res->requested_path, "Phonebook.");
    assert_int_equal(dereg_path_res->oper_status->oper_status_case, USP__DEREGISTER_RESP__DEREGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS);
    assert_int_equal(dereg_path_res->oper_status->oper_success->n_deregistered_path, 1);
    assert_string_equal(dereg_path_res->oper_status->oper_success->deregistered_path[0], "Phonebook.");

    dereg_path_res = dereg_resp->deregistered_path_results[1];
    assert_non_null(dereg_path_res);
    assert_string_equal(dereg_path_res->requested_path, "Greeter.");
    assert_int_equal(dereg_path_res->oper_status->oper_status_case, USP__DEREGISTER_RESP__DEREGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE);
    assert_string_equal(dereg_path_res->oper_status->oper_failure->err_msg, "Registration failed");
    assert_int_equal(dereg_path_res->oper_status->oper_failure->err_code, USP_ERR_DEREGISTER_FAILURE);

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, variant_list_it_free);
}

void test_uspl_deregister_resp_invalid(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);

    assert_int_equal(uspl_deregister_resp_new(NULL, &resp_list, "456"), -1);
    assert_int_equal(uspl_deregister_resp_new(usp_tx, NULL, "456"), -1);
    assert_int_equal(uspl_deregister_resp_new(usp_tx, &resp_list, NULL), -1);

    uspl_tx_delete(&usp_tx);
    amxc_llist_clean(&resp_list, NULL);
}

void test_uspl_deregister_resp_extract(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t* data = NULL;
    amxc_llist_t resp_list;
    amxc_llist_t extract_list;

    amxc_llist_init(&resp_list);
    amxc_llist_init(&extract_list);
    build_deregister_resp_var(&resp_list);

    assert_int_equal(uspl_tx_new(&usp_tx, "one", "two"), 0);
    assert_int_equal(uspl_deregister_resp_new(usp_tx, &resp_list, "456"), 0);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(uspl_deregister_resp_extract(usp_rx, &extract_list), 0);
    data = amxc_var_from_llist_it(amxc_llist_get_first(&extract_list));
    amxc_var_dump(data, STDOUT_FILENO);
    assert_true(test_verify_data(data, "requested_path", "Phonebook."));
    assert_true(test_verify_data(data, "oper_success.deregistered_path.0", "Phonebook."));
    data = amxc_var_from_llist_it(amxc_llist_get_last(&extract_list));
    amxc_var_dump(data, STDOUT_FILENO);
    assert_true(test_verify_data(data, "requested_path", "Greeter."));
    assert_true(test_verify_data(data, "oper_failure.err_code", "7030"));
    assert_true(test_verify_data(data, "oper_failure.err_msg", "Registration failed"));

    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_rx);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    amxc_llist_clean(&extract_list, variant_list_it_free);
}

void test_uspl_deregister_resp_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_llist_t resp_list;
    Usp__DeregisterResp* dereg_resp = NULL;

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_deregister_resp_extract(NULL, &resp_list), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_deregister_resp_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_deregister_resp_extract(usp_rx, &resp_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_deregister_resp_extract(usp_rx, &resp_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_deregister_resp_extract(usp_rx, &resp_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_deregister_resp_extract(usp_rx, &resp_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    assert_int_equal(uspl_deregister_resp_extract(usp_rx, &resp_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response = malloc(sizeof(Usp__Response));
    usp__response__init(usp_rx->msg->body->response);
    assert_int_equal(uspl_deregister_resp_extract(usp_rx, &resp_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->response->resp_type_case = USP__RESPONSE__RESP_TYPE_DEREGISTER_RESP;
    assert_int_equal(uspl_deregister_resp_extract(usp_rx, &resp_list), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    usp_rx->msg->body->response->deregister_resp = malloc(sizeof(Usp__DeregisterResp));
    usp__deregister_resp__init(usp_rx->msg->body->response->deregister_resp);

    dereg_resp = usp_rx->msg->body->response->deregister_resp;
    dereg_resp->deregistered_path_results = NULL;
    assert_int_equal(uspl_deregister_resp_extract(usp_rx, &resp_list), USP_ERR_INVALID_ARGUMENTS);
    dereg_resp->n_deregistered_path_results = 1;
    assert_int_equal(uspl_deregister_resp_extract(usp_rx, &resp_list), USP_ERR_INVALID_ARGUMENTS);
    dereg_resp->n_deregistered_path_results = 0;

    uspl_rx_delete(&usp_rx);
}
