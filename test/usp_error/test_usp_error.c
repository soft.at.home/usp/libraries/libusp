/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>

#include "uspl_private.h"
#include "test_usp_error.h"
#include "usp/usp_err_codes.h"
#include "usp/uspl_error.h"

static void build_error_var(amxc_var_t* error_var) {
    amxc_var_t* param_errs = NULL;
    amxc_var_t* param_err = NULL;

    amxc_var_set_type(error_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, error_var, "err_code", USP_ERR_INTERNAL_ERROR);
    amxc_var_add_key(cstring_t, error_var, "err_msg", "test");
    param_errs = amxc_var_add_key(amxc_llist_t, error_var, "param_errs", NULL);
    param_err = amxc_var_add(amxc_htable_t, param_errs, NULL);
    amxc_var_add_key(cstring_t, param_err, "param_path", "Device.Foo.Param");
    amxc_var_add_key(cstring_t, param_err, "err_msg", "Param err");
    amxc_var_add_key(uint32_t, param_err, "err_code", USP_ERR_INVALID_VALUE);
}

void test_uspl_error_resp_new(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    Usp__Error* error = NULL;
    amxc_var_t error_var;

    amxc_var_init(&error_var);
    build_error_var(&error_var);

    assert_int_equal(uspl_tx_new(&usp_tx, "lpointid", "rpointid"), 0);

    assert_int_equal(uspl_error_resp_new(NULL, &error_var, "123"), -1);
    assert_int_equal(uspl_error_resp_new(usp_tx, NULL, "123"), -1);
    assert_int_equal(uspl_error_resp_new(usp_tx, &error_var, NULL), -1);

    assert_int_equal(uspl_error_resp_new(usp_tx, &error_var, "123"), 0);
    assert_string_equal(usp_tx->msg_id, "123");
    assert_int_equal(usp_tx->msg_type, USP__HEADER__MSG_TYPE__ERROR);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(usp_rx->msg->body->msg_body_case, USP__BODY__MSG_BODY_ERROR);
    assert_int_equal(usp_rx->msg->header->msg_type, USP__HEADER__MSG_TYPE__ERROR);

    error = usp_rx->msg->body->error;
    assert_int_equal(error->err_code, USP_ERR_INTERNAL_ERROR);
    assert_string_equal(error->err_msg, "test");
    assert_int_equal(error->n_param_errs, 1);
    assert_non_null(error->param_errs);
    assert_string_equal(error->param_errs[0]->param_path, "Device.Foo.Param");
    assert_string_equal(error->param_errs[0]->err_msg, "Param err");
    assert_int_equal(error->param_errs[0]->err_code, USP_ERR_INVALID_VALUE);

    amxc_var_clean(&error_var);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_error_resp_extract(UNUSED void** state) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t error_var;
    amxc_var_t values;

    amxc_var_init(&error_var);
    build_error_var(&error_var);

    assert_int_equal(uspl_tx_new(&usp_tx, "lpointid", "rpointid"), 0);
    assert_int_equal(uspl_error_resp_new(usp_tx, &error_var, "123"), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    amxc_var_init(&values);
    assert_int_equal(uspl_error_resp_extract(usp_rx, &values), 0);
    amxc_var_dump(&values, STDOUT_FILENO);

    assert_int_equal(GET_UINT32(&values, "err_code"), USP_ERR_INTERNAL_ERROR);
    assert_string_equal(GET_CHAR(&values, "err_msg"), "test");

    amxc_var_clean(&error_var);
    amxc_var_clean(&values);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
}

void test_uspl_error_extract_invalid(UNUSED void** state) {
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t values;

    usp_rx = calloc(1, sizeof(uspl_rx_t));

    assert_int_equal(uspl_error_resp_extract(NULL, &values), USP_ERR_INTERNAL_ERROR);
    assert_int_equal(uspl_error_resp_extract(usp_rx, NULL), USP_ERR_INTERNAL_ERROR);

    assert_int_equal(uspl_error_resp_extract(usp_rx, &values), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg = malloc(sizeof(Usp__Msg));
    usp__msg__init(usp_rx->msg);
    assert_int_equal(uspl_error_resp_extract(usp_rx, &values), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->header = malloc(sizeof(Usp__Header));
    usp__header__init(usp_rx->msg->header);
    assert_int_equal(uspl_error_resp_extract(usp_rx, &values), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body = malloc(sizeof(Usp__Body));
    usp__body__init(usp_rx->msg->body);
    assert_int_equal(uspl_error_resp_extract(usp_rx, &values), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    assert_int_equal(uspl_error_resp_extract(usp_rx, &values), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->msg_body_case = USP__BODY__MSG_BODY_ERROR;
    assert_int_equal(uspl_error_resp_extract(usp_rx, &values), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->error = malloc(sizeof(Usp__Error));
    usp__error__init(usp_rx->msg->body->error);
    assert_int_equal(uspl_error_resp_extract(usp_rx, &values), USP_ERR_MESSAGE_NOT_UNDERSTOOD);
    usp_rx->msg->body->error->err_msg = NULL;
    assert_int_equal(uspl_error_resp_extract(usp_rx, &values), USP_ERR_MESSAGE_NOT_UNDERSTOOD);

    uspl_rx_delete(&usp_rx);
}
