/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__USPL_DERIGISTER_H__)
#define __USPL_DERIGISTER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>

#include "usp/uspl_msghandler.h"

/**
   @file
   @brief
   USP Deregister message API
 */

/**
   @ingroup lib_usp
   @defgroup uspl_deregister Deregister message
 */

/**
   @ingroup uspl_deregister
   @brief
   Create a protobuf message to send a deregister request

   The request is created based on an input variant, which contains a list of paths that need to be
   deregistered:
   ```
   [
     <string>,
     ...
   ]
   ```

   @note The memory will be freed by calling @ref uspl_tx_delete
   @note The created protobuf will have USP__HEADER__MSG_TYPE__DERIGISTER as header type.

   @param usp_tx pointer to an allocated uspl_tx_t struct that will contain the resulting protobuf
   @param request variant containing the request as listed above

   @return 0 in case of success, -1 in case of error
 */
int uspl_deregister_new(uspl_tx_t* usp_tx, const amxc_var_t* request);

/**
   @ingroup uspl_deregister
   @brief
   Extract the deregistration information from the USP Deregister message

   @param usp_rx pointer to parsed USP message structure. This is always freed by the caller (not this function)
   @param result variant containing the request with the same form as the input in @ref uspl_deregister_new

   @return 0 in case of success, USP specific error code in case of error
 */
int uspl_deregister_extract(uspl_rx_t* usp_rx, amxc_var_t* result);

/**
   @ingroup uspl_deregister
   @brief
   Create a protobuf message to reply to a Deregister request

   The Deregister response is created based on a linked list of variants that looks like this:
   ```
   [
    {
      requested_path = <string>,
      oper_status_case = <uint32_t>,

      oper_failure (optional) = {
        err_code = <uint32_t>,
        err_msg = <string>,
      }

      oper_success (optional) = {
        deregistered_path = [
          <string>,
          ...
        ]
      }
    },
    ...
   ]
   ```

   @note The memory will be freed by calling @ref uspl_tx_delete
   @note The created protobuf will have USP__HEADER__MSG_TYPE__DERIGISTER_RESP as header type.

   @param usp_tx pointer to an allocated uspl_tx_t struct that will contain the resulting protobuf
   @param resp_list list of variants that contain the data model responses to the requests
   @param msg_id message id that corresponds to the request

   @return 0 in case of success, -1 in case of error
 */
int uspl_deregister_resp_new(uspl_tx_t* usp_tx, amxc_llist_t* resp_list, const char* msg_id);

/**
   @ingroup uspl_deregister
   @brief
   Extract the results from the deregister response protobuf

   @param usp_rx pointer to an allocated uspl_tx_t struct containing the unpacked protobuf
   @param resp_list empty list of variants that will be filled up with a response for each
          deregistered path

   @return 0 in case of success, -1 in case of error
 */
int uspl_deregister_resp_extract(uspl_rx_t* usp_rx, amxc_llist_t* resp_list);

#ifdef __cplusplus
}
#endif

#endif // __USPL_DERIGISTER_H_
