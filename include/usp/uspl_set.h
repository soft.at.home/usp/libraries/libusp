/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPL_SET_H__)
#define __USPL_SET_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>

#include "usp/uspl_msghandler.h"

/**
   @file
   @brief
   USP Set message API
 */

/**
   @ingroup lib_usp
   @defgroup uspl_set Set message
 */

/**
   @ingroup uspl_set
   @brief
   Create a uspl_tx_t struct for transmitting a Set request.

   The request is created based on a variant, which looks like this:
   ```
    {
      allow_partial = <bool>,
      requests = [
        {
          object_path = <string>,
          parameters = [
            {
              param = <string>,
              value = <any>,
              required = <bool>
            },
            ...
          ]
        },
        ...
      ]
    }
   ```

   @note The memory will be freed by calling @ref uspl_tx_delete
   @note The created protobuf will have USP__HEADER__MSG_TYPE__SET as header type.

   @param usp_tx initialized structure to hold the created protobuf
   @param request variant containing the request as listed above

   @return 0 in case of success, -1 in case of error.
 */
int uspl_set_new(uspl_tx_t* usp_tx, const amxc_var_t* request);

/**
   @ingroup uspl_set
   @brief
   Extract requested path expressions from USP Set message into a variant with the same style described in @ref uspl_set_new

   @param usp_rx pointer to parsed USP message structure. This is always freed by the caller (not this function)
   @param result variant containing the request with the same form as the input in @ref uspl_set_new

   @return 0 in case of success, USP specific error code in case of error.
 */
int uspl_set_extract(uspl_rx_t* usp_rx, amxc_var_t* result);

/**
   @ingroup uspl_set
   @brief
   Dynamically allocates memory for a set response protobuf and attaches the protobuf to the passed
   uspl_tx_t struct.

   The Set response is created based on a linked list of variants that looks like this:
   ```
    [
      {
        requested_path = <string>,
        oper_status = <uint32_t>,

        success (optional) = {
          updated_inst_results = [
            {
              affected_path = <string>,
              updated_params = {
                <param> = <value>,
                ...
              },
              param_errs = [
                {
                  param = <string>,
                  err_code = <uint32_t>,
                  err_msg = <string>
                },
                ...
              ]
            },
            ...
          ]
        }
        failure (optional) = {
          err_code = <uint32_t>,
          err_msg = <string>,
          updated_inst_failures = [
            {
              affected_path = <string>,
              param_errs = [
                {
                  param = <string>,
                  err_code = <uint32_t>,
                  err_msg = <string>
                },
                ...
              }
            },
            ...
          ]
        }
      }
    ]
   ```

   There will be one variant in the list per requested_path in the original Set request. The
   operation for each requested path can either result in success or a failure, which will be
   indicated by the oper_status field.

   In case of success, there will be one or more affected_paths, depending on whether the
   requested_path is a search path. For each affected path, there can be updated_params or
   param_errs. Each parameter error can have its own reason for failing, which will be explained in
   the err_code and err_msg field. The parameters listed here failed to be set, but did not result
   in an operation failure, because they were not marked as required.

   In case one of the required parameters fails to be set and allow_partial is set to true, there
   will be an operation failure. This failure will have an err_code and err_msg to indicate why it
   failed. Because the requested_path could have been a search path, there can be multiple
   updated_inst_failures. Each of these failures contains an affected path and a list of param_errs.
   The parameter errors can again contain different failure reasons depending on the parameter.

   If one of the required parameters fails to be set and allow_partial is set to false, then an
   Error message will be returned instead of a Set Response.

   @note The memory will be freed by calling @ref uspl_tx_delete
   @note All error codes must be USP specific error codes
   @note The value of oper_status will either be USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS or
         USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE
   @note The created protobuf will have USP__HEADER__MSG_TYPE__SET_RESP as header type.

   @param usp_tx pointer to usp protobuf record to be constructed
   @param resp_list llist containing the response
   @param msg_id msg id

   @return 0 in case of success, USP specific error code in case of error
 */
int uspl_set_resp_new(uspl_tx_t* usp_tx, amxc_llist_t* resp_list, const char* msg_id);

/**
   @ingroup uspl_set
   @brief
   Extract requested path expressions from USP SetResp message into a llist with the same style
   as described in @ref uspl_set_resp_new

   @param usp_rx pointer to parsed USP message structure. This is always freed by the caller (not this function)
   @param resp_list llist which will contain the response messages

   @return  0 in case of success, -1 in case of error
 */
int uspl_set_resp_extract(uspl_rx_t* usp_rx, amxc_llist_t* resp_list);

#ifdef __cplusplus
}
#endif

#endif // __USPL_SET_H__
