/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__USPL_GET_H__)
#define __USPL_GET_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>

#include "usp/uspl_msghandler.h"

/**
   @file
   @brief
   USP Get message API
 */

/**
   @ingroup lib_usp
   @defgroup uspl_get Get message
 */

/**
   @ingroup uspl_get
   @brief
   Create a protobuf message to send a get request

   The request is created based on a variant, which looks like this:
   ```
   {
     max_depth = <uint32_t>,
     paths = [
      <string>,
      ...
     ]
   }
   ```
   The max_depth field limits the maximum depth of each result to the depth specified by this value.
   A value of 0 indicates maximum depth.

   The paths field contains a set of Object Paths, Object Instance Paths, Parameter Paths, or
   Search Paths to Objects and Parameters in an Agent’s Instantiated Data Model.

   @note The memory will be freed by calling @ref uspl_tx_delete
   @note The created protobuf will have USP__HEADER__MSG_TYPE__GET as header type.

   @param usp_tx pointer to an allocated uspl_tx_t struct that will contain the resulting protobuf
   @param request variant containing the request as listed above

   @return 0 in case of success, -1 in case of error
 */
int uspl_get_new(uspl_tx_t* usp_tx, const amxc_var_t* request);

/**
   @ingroup uspl_get
   @brief
   Extract the requested path expressions from the USP Get message

   @param usp_rx pointer to parsed USP message structure. This is always freed by the caller (not this function)
   @param result variant containing the request with the same form as the input in @ref uspl_get_new

   @return 0 in case of success, USP specific error code in case of error
 */
int uspl_get_extract(uspl_rx_t* usp_rx, amxc_var_t* result);

/**
   @ingroup uspl_get
   @brief
   Create a protobuf message to reply to a Get request

   The Get response is created based on a linked list of variants that looks like this:
   ```
   [
    {
      result = {
          err_code = <uint32_t>       // Assume zero error code
          requested_path = <string>,
      },
      <resolved_path> = {
          <param> = <value>,
          <param> = <value>
      },
      <resolved_path> = {
          <param> = <value>,
          <param> = <value>
      }
    },
    {
      result = {
          err_code = <uint32_t>       // Non-zero error code has no resolved_paths
          err_msg = <string>
          request_path = <string>,
      }
    }
   ]
   ```

   @note The memory will be freed by calling @ref uspl_tx_delete
   @note The passed err_code in the result section must be a USP specific error code
   @note The created protobuf will have USP__HEADER__MSG_TYPE__GET_RESP as header type.

   @param usp_tx pointer to an allocated uspl_tx_t struct that will contain the resulting protobuf
   @param resp_list list of variants that contain the data model responses to the requests
   @param msg_id message id that corresponds to the request

   @return 0 in case of success, -1 in case of error
 */
int uspl_get_resp_new(uspl_tx_t* usp_tx, amxc_llist_t* resp_list, const char* msg_id);

/**
   @ingroup uspl_get
   @brief
   Extract the responses to the requested paths from the received USP message

   @param usp_rx pointer to an allocated uspl_tx_t struct that contains the unpacked protobuf
   @param resp_list empty list of variants that will be filled up with a response for each
          requested path

   @return 0 in case of success, -1 in case of error
 */
int uspl_get_resp_extract(uspl_rx_t* usp_rx, amxc_llist_t* resp_list);

#ifdef __cplusplus
}
#endif

#endif // __USPL_GET_H_
