/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPL_NOTIFY_H__)
#define __USPL_NOTIFY_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>

#include "usp/uspl_msghandler.h"

/**
   @file
   @brief
   USP Notify message API
 */

/**
   @ingroup lib_usp
   @defgroup uspl_notify Notify message
 */

/**
   @ingroup uspl_notify
   @brief
   Dynamically allocates memory for a Notify request protobuf and attaches the
   protobuf to the passed uspl_tx_t struct.

   The input should be a hash table that looks like this
   ```
    {
        subscription_id = <string>
        send_resp = <bool>
        notification_case = <uint32_t>

        event (optional) = {
            path = <string>
            event_name = <string>
            <param_key> = <param_value> (optional),
            ...
        }

        value_change (optional) {
            param_path = <string>
            param_value = <string>
        }

        obj_creation (optional) {
            keys = {
                <key> = <value>,
                ...
            },
            path = <string>
        }

        obj_deletion(optional) {
            path = <string>
        }

        oper_complete (optional) {
            path = <string>
            cmd_name = <string>
            cmd_key = <string>
            operation_case = <uint32_t>
            output_args (optional) = {
                <key> = <value>,
                ...
            }
            cmd_failure (optional) = {
                err_code = <uint32_t>
                err_msg = <string>
            }
        }

        on_board_req (optional) {
            oui = <string>
            product_class = <string>
            serial_number = <string>
            agent_supported_protocol_version = <string>
        }

        amx_notification {
            <key> = <value>,
            ...
        }
    }
   ```

   @note The memory will be freed by calling @ref uspl_tx_delete
   @note The created protobuf will have USP__HEADER__MSG_TYPE__NOTIFY as header type.
   @note Notifications of type amx_notification can contain composite key value pairs. These will
         be JSON encoded before they are added to the USP Notify

   @param usp_tx initialized structure to hold the created protobuf
   @param request variant of type htable containing the request

   @return 0 in case of success, -1 in case of error
 */
int uspl_notify_new(uspl_tx_t* usp_tx, const amxc_var_t* request);

/**
   @ingroup uspl_notify
   @brief
   Extract the Notify request from a USP message and return it as a variant.

   @param usp_rx pointer to uspl_rx_t struct that contains the unpacked protobuf
   @param result initialized variant that will be filled up with the request information

   @return 0 in case of success, USP specific error code in case of error.
 */
int uspl_notify_extract(uspl_rx_t* usp_rx, amxc_var_t* result);

/**
   @ingroup uspl_notify
   @brief

   Dynamically allocates memory for a Notify response protobuf
   and attaches the protobuf to the passed uspl_tx_t struct.

   The Notify response is created based on a linked list of variants that looks like this:
   ```
    [
        <subscription_id>
    ]
   ```

   @note The function prototype is chosen identical to the prototype of the other USP message
         creation functions. However it is not possible to send multiple notify messages in a
         single USP record. Therefore only the first entry in the passed list will be used to
         create the NotifyResp message.
   @note The memory will be freed by calling @ref uspl_tx_delete
   @note The created protobuf will have USP__HEADER__MSG_TYPE__NOTIFY_RESP as header type.

   @param usp_tx pointer to an allocated uspl_tx_t struct that will contain the resulting protobuf
   @param resp_list single element list that contains the subscription_id
   @param msg_id message id that corresponds to the request

   @return 0 in case of success, -1 in case of error
 */
int uspl_notify_resp_new(uspl_tx_t* usp_tx, amxc_llist_t* resp_list, const char* msg_id);

/**
   @ingroup uspl_notify
   @brief

   Extract the response to the Notify request from the received USP message.

   @note
   The argument resp_list will be dynamically filled up with variants. Use amxc_llist_clean
   with a function that calls amxc_var_delete on each of the variants in the list to properly free
   the allocated memory.

   @param usp_rx pointer to an allocated uspl_tx_t struct that contains the unpacked protobuf
   @param resp_list empty list of variants that will be filled up with a single response

   @return 0 in case of success, USP specific error code in case of error.
 */
int uspl_notify_resp_extract(uspl_rx_t* usp_rx, amxc_llist_t* resp_list);

#ifdef __cplusplus
}
#endif

#endif // __USPL_NOTIFY_H__
