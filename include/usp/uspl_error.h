/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__USPL_ERROR_H__)
#define __USPL_ERROR_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>

#include "usp/uspl_msghandler.h"

/**
   @file
   @brief
   USP Error message API
 */

/**
   @ingroup lib_usp
   @defgroup uspl_error Error message
 */

/**
   @ingroup uspl_error
   @brief
   Creates a USP error message in response to an incoming request and attaches
   the protobuf to the passed uspl_tx_t struct.

   The Error message is built based on an input variant with the following structure:
   ```
    {
      err_code = <uint32_t>
      err_msg = <string>
      param_errs (optional) = [
        {
          err_code = <uint32_t>
          err_msg = <string>
          param_path = <string>
        },
        ...
      ]
    }
   ```
   @note The param_errs field in the error variant is optional
   @note Memory is allocated for this protobuf and will be freed by calling @ref uspl_tx_delete
   @note The created protobuf will have USP__HEADER__MSG_TYPE__ERROR as header type.

   @param usp_tx initialized structure to hold the created protobuf
   @param error variant containing the error information
   @param msg_id message id that corresponds to the request

   @return  0 in case of success, -1 in case of error
 */
int uspl_error_resp_new(uspl_tx_t* usp_tx, amxc_var_t* error, const char* msg_id);

/**
   @ingroup uspl_error
   @brief
   Extract the error information from a USP error message. The information will be returned as a
   variant that looks like this:


   @param usp_rx pointer to an allocated uspl_tx_t struct that contains the unpacked protobuf
   @param values variant that will contain the requested error information

   @return  0 in case of success, -1 in case of error
 */
int uspl_error_resp_extract(uspl_rx_t* usp_rx, amxc_var_t* values);

/**
   @ingroup uspl_error
   @brief
   Converts a USP error number into a textual error message. Memory will be
   allocated dynamically for this message and must be freed later on.

   @param err error number to convert to a textual representation

   @return pointer to the error message string
 */
const char* uspl_error_code_to_str(int err);

/**
   @ingroup uspl_error
   @brief
   Converts an amxd_status_t to a corresponding USP error code

   @param status status to convert to USP error code

   @return USP specific error code
 */
int uspl_amxd_status_to_usp_error(amxd_status_t status);

/**
   @ingroup uspl_error
   @brief
   Converts an amxd_status_t to a corresponding USP param error for the operation Set

   @param status status to convert to USP error code

   @return USP specific param error
 */
int uspl_set_amxd_status_to_usp_param_error(amxd_status_t status);

/**
   @ingroup uspl_error
   @brief
   Tries to convert a USP error code to its corresponding amxd_status_t

   When the input doesn't match with an existing USP error code, returns the
   same value as the input value, because the input could already have been
   an amxd_status_t.

   @param err_code USP error code to convert to status

   @return amxd_status_t
 */
amxd_status_t uspl_usp_error_to_amxd_status(int err_code);

#ifdef __cplusplus
}
#endif

#endif // __USPL_ERROR_H__
