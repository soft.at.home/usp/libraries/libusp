/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__USPL_GET_SUPPORTED_DM_H__)
#define __USPL_GET_SUPPORTED_DM_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>

#include "usp/uspl_msghandler.h"

/**
   @file
   @brief
   USP Get Supported DM message API
 */

/**
   @ingroup lib_usp
   @defgroup uspl_get_supported_dm Get Supported DM message
 */

/**
   @ingroup uspl_get_supported_dm
   @brief
   Dynamically allocates memory for a get supported datamodel request protobuf
   and attaches the protobuf to the passed uspl_tx_t struct.

   The request is created based on a variant, which looks like this:
   ```
    {
      flags = {
        first_level_only = <bool>
        return_commands = <bool>
        return_events = <bool>
        return_params = <bool>
      }
      paths = [
        <requested_path>,
        <requested_path>,
        ...
      ],
    }
   ```

   @note The memory will be freed by calling @ref uspl_tx_delete
   @note The created protobuf will have USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM as header type.

   @param usp_tx initialized structure to hold the created protobuf
   @param request variant containing the request as listed above

   @return  0 in case of success, -1 in case of error
 */
int uspl_get_supported_dm_new(uspl_tx_t* usp_tx, const amxc_var_t* request);

/**
   @ingroup uspl_get_supported_dm
   @brief
   Extract the requested path expressions from the USP Get Supported DM request

   @param usp_rx pointer to uspl_rx_t struct that contains the unpacked protobuf
   @param result variant containing the request with the same form as the input in @ref uspl_get_supported_dm_new

   @return 0 in case of success, USP specific error code in case of error
 */
int uspl_get_supported_dm_extract(uspl_rx_t* usp_rx, amxc_var_t* result);

/**
   @ingroup uspl_get_supported_dm
   @brief
   Dynamically allocates memory for a get supported datamodel response protobuf
   and attaches the protobuf to the passed uspl_tx_t struct.

   The GetSupportedDM response is created based on a linked list of variants that looks like this:
   ```
   [
    {
      result = {
        err_code = <uint32_t>
        requested_path = <string>,
      }
      <object_path> = {
        access = <int>  // only for multi instance objects
        is_multi_instance = <bool>
        supported_commands = [
            {
                command_name = <string>
                input_arg_names = [
                  <string>,
                  ...
                ],
                output_arg_names = [
                  <string>,
                  ...
                ],
            },
            ...
        ],
        supported_params = [
            {
                access = <int>
                param_name = <string>,
            },
            ...
        ],
      }
    },
    ...
   ]
   ```

   @note The memory will be freed by calling @ref uspl_tx_delete
   @note The passed err_code in the result section must be a USP specific error code
   @note The created protobuf will have USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM_RESP as header type.

   @param usp_tx pointer to an allocated uspl_tx_t struct that will contain the resulting protobuf
   @param resp_list list of variants that contain the data model responses to the requests
   @param msg_id message id that corresponds to the request

   @return 0 in case of success, -1 in case of error
 */
int uspl_get_supported_dm_resp_new(uspl_tx_t* usp_tx, amxc_llist_t* resp_list, const char* msg_id);

/**
   @ingroup uspl_get_supported_dm
   @brief
   Extract the responses to the get supported dm request from the received USP message

   @param usp_rx pointer to an allocated uspl_tx_t struct that contains the unpacked protobuf
   @param resp_list empty list of variants that will be filled up with a response for each
          requested data model path

   @return 0 in case of success, -1 in case of error
 */
int uspl_get_supported_dm_resp_extract(uspl_rx_t* usp_rx, amxc_llist_t* resp_list);


#ifdef __cplusplus
}
#endif

#endif // __USPL_GET_SUPPORTED_DM_H__
