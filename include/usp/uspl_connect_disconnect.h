/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__USPL_CONNECT_DISCONNECT_H__)
#define __USPL_CONNECT_DISCONNECT_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>

#include "usp/uspl_msghandler.h"

/**
   @file
   @brief
   USP Connect and Disconnect Records
 */

/**
   @ingroup lib_usp
   @defgroup uspl_connect Connect and Disconnect Records
 */


/**
   @ingroup uspl_connect
   @brief
   Creates a new USP Connect record for one of the MTPs.

   The record is created based on a variant, which looks like this:
   ```
   {
     record_type = <uint32_t>,

     mqtt (optional) = {
        version = <uint32_t>
        subscribed_topic = <string>
     }

     stomp (optional) = {
        version = <string>
     }
   }
   ```

   The field record_type must be one of the following:
   - USP_RECORD__RECORD__RECORD_TYPE_MQTT_CONNECT
   - USP_RECORD__RECORD__RECORD_TYPE_UDS_CONNECT
   - USP_RECORD__RECORD__RECORD_TYPE_STOMP_CONNECT
   - USP_RECORD__RECORD__RECORD_TYPE_WEBSOCKET_CONNECT

   In case the MTP is MQTT, the version must be one of the following:
   - USP_RECORD__MQTTCONNECT_RECORD__MQTTVERSION__V5
   - USP_RECORD__MQTTCONNECT_RECORD__MQTTVERSION__V3_1_1

   All other fields are optional and only one of them will be present depending on the record_type.
   Note that there will only be an MTP specific field if the Record actually contains data for that
   MTP. The Connect Records for UDS and WebSockets will for example be empty.
   The contents of the MTP specific fields should be straightforward as they correspond to the USP
   Record fields as described in TR-369.

   @param usp_tx pointer to an allocated uspl_tx_t struct that will contain the resulting protobuf
   @param request variant containing the request as listed above
   @return 0 in case of success, -1 in case of error
 */
int uspl_connect_record_new(uspl_tx_t* usp_tx, const amxc_var_t* request);

/**
   @ingroup uspl_connect
   @brief
   Creates a new USP Disconnect record.

   The record is created based on a variant, which looks like this:
   ```
   {
     reason (optional) = <string>,
     reason_code (optional) = <uint32_t>
   }
   ```

   Note that both the reason and reason_code are optional. The reason code should be a USP Record
   Error code.

   @param usp_tx pointer to an allocated uspl_tx_t struct that will contain the resulting protobuf
   @param request variant containing the request as listed above
   @return 0 in case of success, -1 in case of error
 */
int uspl_disconnect_record_new(uspl_tx_t* usp_tx, const amxc_var_t* request);

#ifdef __cplusplus
}
#endif

#endif // __USPL_CONNECT_DISCONNECT_H__

