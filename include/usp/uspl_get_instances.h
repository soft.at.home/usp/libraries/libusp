/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPL_GET_INSTANCES_H__)
#define __USPL_GET_INSTANCES_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxd/amxd_types.h>

#include "usp/uspl_msghandler.h"

/**
   @file
   @brief
   USP GetInstances message API
 */

/**
   @ingroup lib_usp
   @defgroup uspl_get_instances GetInstances message
 */

/**
   @ingroup uspl_get_instances
   @brief
   Create a protobuf message to send a get_instances request

   The request is created based on a variant with a list of requested paths and a boolean
   first_level_only:
   ```
   {
       obj_paths = [ <string>, ... ]
       first_level_only = <bool>
   }
   ```

   @note The memory will be freed by calling @ref uspl_tx_delete
   @note The created protobuf will have USP__HEADER__MSG_TYPE__GET_INSTANCES as header type.

   @param usp_tx pointer to an allocated uspl_tx_t struct that will contain the resulting protobuf
   @param request variant containing the request as listed above

   @return 0 in case of success, -1 in case of error
 */
int uspl_get_instances_new(uspl_tx_t* usp_tx, const amxc_var_t* request);

/**
   @ingroup uspl_get_instances
   @brief
   Extract the USP GetInstances message into a variant

   @param usp_rx pointer to parsed USP message structure. This is always freed by the caller (not this function)
   @param result variant containing the request with the same form as the input in @ref uspl_get_instances_new

   @return 0 in case of success, USP specific error code in case of error
 */
int uspl_get_instances_extract(uspl_rx_t* usp_rx, amxc_var_t* result);

/**
   @ingroup uspl_get_instances
   @brief
   Create a protobuf message to reply to a GetInstances request

   The GetInstances response is created based on a linked list of variants that looks like this:
   ```
   [
    {
      requested_path = <string>
      err_code = <uint32_t>
      err_msg = <string>
      curr_insts = [
        {
          inst_path = <string>
          unique_keys = {
            <key> = <value>
          }
        },
        ...
      ]
    },
    ...
   ]
   ```

   @note The memory will be freed by calling @ref uspl_tx_delete
   @note The passed err_code must be a USP specific error code
   @note The created protobuf will have USP__HEADER__MSG_TYPE__GET_INSTANCES_RESP as header type.

   @param usp_tx pointer to an allocated uspl_tx_t struct that will contain the resulting protobuf
   @param resp_list list of variants that contain the data model responses to the requests
   @param msg_id message id that corresponds to the request

   @return 0 in case of success, -1 in case of error
 */
int uspl_get_instances_resp_new(uspl_tx_t* usp_tx, amxc_llist_t* resp_list, const char* msg_id);

/**
   @ingroup uspl_get_instances
   @brief
   Extract the GetInstances response into a list of variants as described in
   @ref uspl_get_instances_resp_new

   @param usp_rx pointer to an allocated uspl_tx_t struct that contains the unpacked protobuf
   @param resp_list empty list of variants that will be filled up with a response for each
          requested path

   @return 0 in case of success, -1 in case of error
 */
int uspl_get_instances_resp_extract(uspl_rx_t* usp_rx, amxc_llist_t* resp_list);

/**
   @brief
   Translates the amxd_status_t to an appropriate USP error code.

   amxd_status_not_supported -> USP_ERR_INVALID_PATH (7026)
   amxd_status_not_a_template -> USP_ERR_NOT_A_TABLE (7018)
   amxd_status_permission_denied -> USP_ERR_OBJ_DOES_NOT_EXIST (7016) (see R-GIN.0)
   amxd_status_not_instantiated -> USP_ERR_OBJ_DOES_NOT_EXIST (7016)
   default -> USP_ERR_OBJ_DOES_NOT_EXIST (7016)

   @param status the status code from the get instances operation
   @return the converted USP error code
 */
int uspl_get_instances_amxd_status_to_usp_err(amxd_status_t status);

#ifdef __cplusplus
}
#endif

#endif // __USPL_GET_INSTANCES_H__
