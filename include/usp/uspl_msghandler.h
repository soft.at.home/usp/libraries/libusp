/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__USPL_MSGHANDLER_H__)
#define __USPL_MSGHANDLER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <uspprotobuf/usp-msg-1-3.pb-c.h>
#include <uspprotobuf/usp-record-1-3.pb-c.h>

/**
   @file
   @brief
   USP message handler API
 */

/**
   @ingroup lib_usp
   @defgroup uspl_msghandler Message handler
 */

/**
   @ingroup uspl_msghandler
   @brief
   A USP transmit context object.

   This object can be used to create a protobuf for transmission.
   Use @ref uspl_tx_new to initialize the object with a local and remote endpoint ID. Use one
   of the available uspl_<operation>_new functions to create the desired protobuf based on the
   information in this structure and some additional message specific parameters.

   The msg_type will be -1 upon initialization, but will be filled up with one of the following
   types when the protobuf has been filled up:
   ```
    USP__HEADER__MSG_TYPE__ERROR = 0,
    USP__HEADER__MSG_TYPE__GET = 1,
    USP__HEADER__MSG_TYPE__GET_RESP = 2,
    USP__HEADER__MSG_TYPE__NOTIFY = 3,
    USP__HEADER__MSG_TYPE__SET = 4,
    USP__HEADER__MSG_TYPE__SET_RESP = 5,
    USP__HEADER__MSG_TYPE__OPERATE = 6,
    USP__HEADER__MSG_TYPE__OPERATE_RESP = 7,
    USP__HEADER__MSG_TYPE__ADD = 8,
    USP__HEADER__MSG_TYPE__ADD_RESP = 9,
    USP__HEADER__MSG_TYPE__DELETE = 10,
    USP__HEADER__MSG_TYPE__DELETE_RESP = 11,
    USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM = 12,
    USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM_RESP = 13,
    USP__HEADER__MSG_TYPE__GET_INSTANCES = 14,
    USP__HEADER__MSG_TYPE__GET_INSTANCES_RESP = 15,
    USP__HEADER__MSG_TYPE__NOTIFY_RESP = 16,
    USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO = 17,
    USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO_RESP = 18
   ```

   Use @ref uspl_tx_delete to delete the object
 */
typedef struct _uspl_tx {
    unsigned char* pbuf;  /* protobuf buffer */
    size_t pbuf_len;      /* protobuf length */
    char* lendpoint_id;   /* local endpointid */
    char* rendpoint_id;   /* remote endpointid */
    char* msg_id;         /* USP message id */
    int32_t msg_type;     /* USP message type of the embedded protobuf */
} uspl_tx_t;

/**
   @ingroup uspl_msghandler
   @brief
   A USP receive context object.

   This object is returned when calling @ref uspl_msghandler_unpack_protobuf to unpack a protobuf
   message into a USP record and message.
 */
typedef struct _uspl_rx {
    UspRecord__Record* rec;
    Usp__Msg* msg;
} uspl_rx_t;

/**
   @ingroup uspl_msghandler
   @brief
   Create a new uspl_tx_t struct for transmission of a USP message.

   Allocates memory for a uspl_tx_t structure. This memory must be freed with @ref uspl_tx_delete

   @param ctx a pointer to a pointer for the new uspl_tx_t struct
   @param lendpointid a string for the local endpoint id (sender)
   @param rendpointid a string for the remote endpoint id (receiver)

   @return 0 in case of success, -1 in case of error.
 */
int uspl_tx_new(uspl_tx_t** ctx, const char* lendpointid, const char* rendpointid);

/**
   @ingroup uspl_msghandler
   @brief
   Deletes a uspl_tx_t struct.

   @param ctx a pointer to a pointer to the existing uspl_tx_t struct
 */
void uspl_tx_delete(uspl_tx_t** ctx);

/**
   @ingroup uspl_msghandler
   @brief
   Deletes a uspl_rx_t struct.

   @param ctx a pointer to a pointer to the existing uspl_rx_t struct
 */
void uspl_rx_delete(uspl_rx_t** ctx);

/**
   @ingroup uspl_msghandler
   @brief
   Unpack a received USP protobuf message into a uspl_rx_t struct.

   @param pbuf a binary protobuf message
   @param pbuf_len the length of the protobuf message

   @note Use @ref uspl_msghandler_msg_type to determine the type of the unpacked protobuf.

   @return a filled up uspl_rx_t struct in case of success, NULL in case of error
 */
uspl_rx_t* uspl_msghandler_unpack_protobuf(unsigned char* pbuf, int pbuf_len);

/**
   @ingroup uspl_msghandler
   @brief
   Get the message id from a received USP message.

   @param usp a uspl_rx_t struct containing the received USP message.

   @return a string containing the message id
 */
char* uspl_msghandler_msg_id(uspl_rx_t* usp);

/**
   @ingroup uspl_msghandler
   @brief
   Get the from_id from a received USP message.

   @param usp a uspl_rx_t struct containing the received USP message.

   @return a string containing the from_id
 */
char* uspl_msghandler_from_id(uspl_rx_t* usp);

/**
   @ingroup uspl_msghandler
   @brief
   Get the to_id from a received USP message.

   @param usp a uspl_rx_t struct containing the received USP message.

   @return a string containing the to_id
 */
char* uspl_msghandler_to_id(uspl_rx_t* usp);

/**
   @ingroup uspl_msghandler
   @brief
   Get the message type from a received USP message.

   @note
   The message type can be one of the following
   ```
    USP__HEADER__MSG_TYPE__ERROR = 0,
    USP__HEADER__MSG_TYPE__GET = 1,
    USP__HEADER__MSG_TYPE__GET_RESP = 2,
    USP__HEADER__MSG_TYPE__NOTIFY = 3,
    USP__HEADER__MSG_TYPE__SET = 4,
    USP__HEADER__MSG_TYPE__SET_RESP = 5,
    USP__HEADER__MSG_TYPE__OPERATE = 6,
    USP__HEADER__MSG_TYPE__OPERATE_RESP = 7,
    USP__HEADER__MSG_TYPE__ADD = 8,
    USP__HEADER__MSG_TYPE__ADD_RESP = 9,
    USP__HEADER__MSG_TYPE__DELETE = 10,
    USP__HEADER__MSG_TYPE__DELETE_RESP = 11,
    USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM = 12,
    USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM_RESP = 13,
    USP__HEADER__MSG_TYPE__GET_INSTANCES = 14,
    USP__HEADER__MSG_TYPE__GET_INSTANCES_RESP = 15,
    USP__HEADER__MSG_TYPE__NOTIFY_RESP = 16,
    USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO = 17,
    USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO_RESP = 18
   ```

   @param usp a uspl_rx_t struct containing the received USP message.

   @return an integer representing the message type
 */
int uspl_msghandler_msg_type(uspl_rx_t* usp);

/**
   @ingroup uspl_msghandler
   @brief
   Get the record type from a received USP record.

   @note
   The record type can be one of the following
   ```
    USP_RECORD__RECORD__RECORD_TYPE_NO_SESSION_CONTEXT = 7,
    USP_RECORD__RECORD__RECORD_TYPE_SESSION_CONTEXT = 8,
    USP_RECORD__RECORD__RECORD_TYPE_WEBSOCKET_CONNECT = 9,
    USP_RECORD__RECORD__RECORD_TYPE_MQTT_CONNECT = 10,
    USP_RECORD__RECORD__RECORD_TYPE_STOMP_CONNECT = 11,
    USP_RECORD__RECORD__RECORD_TYPE_DISCONNECT = 12,
    USP_RECORD__RECORD__RECORD_TYPE_UDS_CONNECT = 13
   ```

   @param usp a uspl_rx_t struct containing the received USP record.

   @return an integer representing the record type
 */
int uspl_msghandler_record_type(uspl_rx_t* usp);

#ifdef __cplusplus
}
#endif

#endif // __USPL_MSGHANDLER_H__
