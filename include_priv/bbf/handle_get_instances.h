/*
 *
 * Copyright (C) 2019-2023, Broadband Forum
 * Copyright (C) 2016-2023  CommScope, Inc
 * Copyright (C) 2020,  BT PLC
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#if !defined(__HANDLE_GET_INSTANCES_H__)
#define __HANDLE_GET_INSTANCES_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "uspl_private.h"

/**
   @brief
   Dynamically creates an GetInstancesResponse object

   @note The object should be deleted using usp__msg__free_unpacked()

   @param msg_id string containing the message id of the request, which initiated this response

   @return Pointer to a GetInstances Response object
 */
Usp__Msg* CreateGetInstancesResp(const char* msg_id);

/**
   @brief
   Dynamically adds a RequestedPathResult object to a GetInstancesResponse object

   @param gi_resp pointer to GetInstancesResponse object of USP response message
   @param requested_path path identifying the set of object instances that are to be addressed
   @param err USP error for this object. If no error, then USP_ERR_OK
   @param err_msg pointer to error message. If no err, then an empty string.

   @return Pointer to a RequestedPathResult object
 */
Usp__GetInstancesResp__RequestedPathResult*
AddGetInstances_RequestedPathResult(Usp__GetInstancesResp* gi_resp, const char* requested_path, uint32_t err, const char* err_msg);

#ifdef __cplusplus
}
#endif

#endif // __HANDLE_GET_INSTANCES_H__
