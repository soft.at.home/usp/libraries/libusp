/*
 *
 * Copyright (C) 2019-2023, Broadband Forum
 * Copyright (C) 2016-2023  CommScope, Inc
 * Copyright (C) 2020,  BT PLC
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#if !defined(__ERROR_RESP_H__)
#define __ERROR_RESP_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "uspl_private.h"

/**
   @brief
   Dynamically creates an Error object

   @note The object is created without any param_errors
   @note The object should be deleted using usp__msg__free_unpacked()

   @param msg_id string containing the message id of the get request, which initiated this response
   @param err_code error code
   @param err_msg pointer to string containing reason for the error

   @return Pointer to a GetResponse object
 */
Usp__Msg* ERROR_RESP_Create(const char* msg_id, int err_code, const char* err_msg);

/**
   @brief
   Adds a ParamError entry to the Error object

   @param resp pointer to Error object
   @param path path name of the object causing the error
   @param err_code numeric code indicating cause of error
   @param err_msg string containing additional information about the error

   @return Pointer to dynamically allocated param_error (within the Error object)
 */
Usp__Error__ParamError*
ERROR_RESP_AddParamError(Usp__Msg* resp, const char* path, int err_code, const char* err_msg);

#ifdef __cplusplus
}
#endif

#endif // __ERROR_RESP_H__
