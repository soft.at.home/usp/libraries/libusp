/*
 *
 * Copyright (C) 2019-2023, Broadband Forum
 * Copyright (C) 2016-2023  CommScope, Inc
 * Copyright (C) 2020,  BT PLC
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#if !defined(__HANDLE_SET_H__)
#define __HANDLE_SET_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "uspl_private.h"

/**
   @brief
   Dynamically creates an GetResponse object

   @note The object is created without any updated_obj_results
   @note The object should be deleted using usp__msg__free_unpacked()

   @param msg_id string containing the message id of the set request, which initiated this response

   @return Pointer to a SetResponse object
 */
Usp__Msg* CreateSetResp(const char* msg_id);

/**
   @brief
   Dynamically adds an operation success object to the SetResponse object

   @param   resp - pointer to GetResponse object
   @param   path - requested path

   @return  Pointer to dynamically allocated operation success object
 */
Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationSuccess*
AddSetResp_OperSuccess(Usp__SetResp* set_resp, const char* path);

/**
   @brief
   Dynamically adds an updated instance result entry to an OperationSuccess object

   @param   oper_success - pointer to operation success object to add this entry to
   @param   path - path to object which was updated

   @return  Pointer to dynamically allocated updated instance result entry
 */
Usp__SetResp__UpdatedInstanceResult*
AddOperSuccess_UpdatedInstRes(Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationSuccess* oper_success,
                              const char* affected_path);

/**
   @brief
   Dynamically adds a param map entry to an updated instance result object

   @param   updated_inst_result - pointer to updated instance result object to add this entry to
   @param   key - name of the parameter which was updated successfully
   @param   value - value of the parameter which was updated

   @return  Pointer to dynamically allocated updated instance result entry
 */
Usp__SetResp__UpdatedInstanceResult__UpdatedParamsEntry*
AddUpdatedInstRes_ParamsEntry(Usp__SetResp__UpdatedInstanceResult* updated_inst_result,
                              const char* key,
                              char* value);

/**
   @brief
   Dynamically adds a param err entry to an updated instance result object

   @param   updated_inst_result - pointer to updated instance result object to add this entry to
   @param   path - name of parameter which failed to update
   @param   err_code - error code representing the cause of the failure to update
   @param   err_msg - string representing the cause of the error

   @return  Pointer to dynamically allocated parameter_error entry
 */
Usp__SetResp__ParameterError*
AddUpdatedInstRes_ParamErr(Usp__SetResp__UpdatedInstanceResult* updated_inst_result,
                           const char* path,
                           int err_code,
                           const char* err_msg);

/**
   @brief
   Dynamically adds an operation failure object to the SetResponse object

   @param   resp - pointer to GetResponse object
   @param   path - requested path of object which failed to update
   @param   err_code - numeric code indicating reason object failed to be set
   @param   err_msg - error message indicating reason object failed to be set

   @return  Pointer to dynamically allocated operation failure object
 */
Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationFailure*
AddSetResp_OperFailure(Usp__SetResp* set_resp, const char* path, int err_code, const char* err_msg);

/**
   @brief
   Dynamically adds an updated instance failure entry to an OperationFailure object

   @param   oper_failure - pointer to operation failure object to add this entry to
   @param   path - path to object which failed to be updated

   @return  Pointer to dynamically allocated updated instance result entry
 */
Usp__SetResp__UpdatedInstanceFailure*
AddOperFailure_UpdatedInstFailure(Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationFailure* oper_failure,
                                  const char* affected_path);

/**
   @brief
   Dynamically adds a param err entry to an updated instance failure object

   @param   updated_inst_failure - pointer to updated instance failure object to add this entry to
   @param   path - name of parameter which failed to update
   @param   err_code - error code representing the cause of the failure to update
   @param   err_msg - string representing the cause of the error

   @return  Pointer to dynamically allocated parameter_error entry
 */
Usp__SetResp__ParameterError*
AddUpdatedInstFailure_ParamErr(Usp__SetResp__UpdatedInstanceFailure* updated_inst_failure,
                               const char* path,
                               int err_code,
                               const char* err_msg);

#ifdef __cplusplus
}
#endif

#endif // __HANDLE_SET_H__
