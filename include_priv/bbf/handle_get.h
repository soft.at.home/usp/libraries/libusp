/*
 *
 * Copyright (C) 2019-2023, Broadband Forum
 * Copyright (C) 2016-2023  CommScope, Inc
 * Copyright (C) 2020,  BT PLC
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#if !defined(__HANDLE_GET_H__)
#define __HANDLE_GET_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "uspl_private.h"

/**
   @brief
   Dynamically creates a GetResponse object

   @note The object is created without any requested_path_results
   @note The object should be deleted using usp__msg__free_unpacked()

   @param msg_id string containing the message id of the get request, which initiated this response

   @return Pointer to a GetResponse object
 */
Usp__Msg* CreateGetResp(const char* msg_id);

/**
   @brief
   Dynamically adds a requested path result to the GetResponse object

   @note The object is created without any entries in the result_params

   @param resp pointer to GetResponse object
   @param requested_path string containing one of the path expresssions from the Get request
   @param err_code numeric code indicating reason the get failed
   @param err_msg error message indicating reason the get failed

   @return  Pointer to dynamically allocated requested path result
 */
Usp__GetResp__RequestedPathResult*
AddGetResp_ReqPathRes(Usp__Msg* resp, const char* requested_path, int err_code, const char* err_msg);

/**
   @brief
   Dynamically adds a resolved_path_result object to a requested_path_result object

   @param req_path_result pointer to requested_path_result to add this entry to
   @param obj_path data model path of the object to add to the map

   @return Pointer to dynamically allocated resolved_path_result
 */
Usp__GetResp__ResolvedPathResult* AddReqPathRes_ResolvedPathResult(Usp__GetResp__RequestedPathResult* req_path_result, const char* obj_path);

/**
   @brief
   Dynamically adds a result_params entry to a resolved_path_result object

   @param resolved_path_res pointer to resolved_oath_result to add this entry to
   @param param_name name of the parameter (not including object path) of the parameter to add to the map
   @param value value of the parameter

   @return Pointer to dynamically allocated result_params
 */
Usp__GetResp__ResolvedPathResult__ResultParamsEntry*
AddResolvedPathRes_ParamsEntry(Usp__GetResp__ResolvedPathResult* resolved_path_res, const char* param_name, const char* value);

#ifdef __cplusplus
}
#endif

#endif // __HANDLE_GET_H__
