/*
 *
 * Copyright (C) 2019-2023, Broadband Forum
 * Copyright (C) 2016-2023  CommScope, Inc
 * Copyright (C) 2020,  BT PLC
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#if !defined(__HANDLE_ADD_H__)
#define __HANDLE_ADD_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "uspl_private.h"

/**
   @brief
   Dynamically creates an AddResponse object

   @note The object is created without any created_obj_results
   @note The object should be deleted using usp__msg__free_unpacked()

   @param msg_id string containing the message id of the add request, which initiated this response

   @return Pointer to an AddResponse object
 */
Usp__Msg* CreateAddResp(const char* msg_id);

/**
   @brief
   Dynamically adds an operation success object to the AddResponse object

   @note The object is created without any param_err or unique_keys entries

   @param add_resp pointer to AddResponse object
   @param req_path requested path
   @param path instantiated path

   @return Pointer to dynamically allocated operation success object
 */
Usp__AddResp__CreatedObjectResult__OperationStatus__OperationSuccess*
AddResp_OperSuccess(Usp__AddResp* add_resp, const char* req_path, const char* path);

/**
   @brief
   Dynamically adds an operation failure object to the AddResponse object

   @param resp pointer to AddResponse object
   @param path requested path of object which failed to create
   @param err_code numeric code indicating reason object failed to be created
   @param err_msg error message indicating reason object failed to be created

   @return  Pointer to dynamically allocated operation failure object
 */
Usp__AddResp__CreatedObjectResult__OperationStatus__OperationFailure*
AddResp_OperFailure(Usp__AddResp* add_resp, const char* path, int err_code, const char* err_msg);

#ifdef __cplusplus
}
#endif

#endif // __HANDLE_ADD_H__

