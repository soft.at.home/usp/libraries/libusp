/*
 *
 * Copyright (C) 2019-2023, Broadband Forum
 * Copyright (C) 2016-2023  CommScope, Inc
 * Copyright (C) 2020,  BT PLC
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#if !defined(__HANDLE_DELETE_H__)
#define __HANDLE_DELETE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "uspl_private.h"

/**
   @brief
   Dynamically creates a DeleteResponse object

   @note The object is created without any deleted_obj_results
   @note The object should be deleted using usp__msg__free_unpacked()

   @param msg_id string containing the message id of the delete request, which initiated this response

   @return  Pointer to a DeleteResponse object
 */
Usp__Msg* CreateDeleteResp(const char* msg_id);

/**
   @brief
   Dynamically adds a deleted object result success object to the DeleteResponse object

   @param del_resp pointer to DeleteResponse object
   @param path requested path of object to delete

   @return  Pointer to dynamically allocated deleted object result success object
 */
Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationSuccess*
AddDeleteResp_OperSuccess(Usp__DeleteResp* del_resp, const char* path);

/**
   @brief
   Dynamically adds an affected path to the DeleteResponse OperSuccess object

   @param oper_success pointer to DeleteResponse OperSuccess object
   @param path path of the object resolved from the search path

   @return  None
 */
void AddOperSuccess_AffectedPath(Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationSuccess* oper_success,
                                 const char* path);

/**
   @brief
   Dynamically adds an unaffected path error to the DeleteResponse OperSuccess object

   @param oper_success pointer to DeleteResponse OperSuccess object
   @param path path of the object resolved from the search path
   @param err_code numeric code indicating reason object failed to be deleted
   @param err_msg error message indicating reason object failed to be deleted

   @return None
 */
void AddOperSuccess_UnaffectedPathError(Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationSuccess* oper_success,
                                        const char* path, uint32_t err_code, const char* err_msg);

/**
   @brief
   Dynamically adds a deleted object result failure object to the DeleteResponse object

   @param del_resp pointer to DeleteResponse object
   @param path requested search path of object(s) that failed to delete
   @param err_code numeric code indicating reason object failed to be deleted
   @param err_msg error message indicating reason object failed to be deleted

   @return Pointer to dynamically allocated deleted object result failure object
 */
Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationFailure*
AddDeleteResp_OperFailure(Usp__DeleteResp* del_resp, const char* path, uint32_t err_code, const char* err_msg);

#ifdef __cplusplus
}
#endif

#endif // __HANDLE_DELETE_H__
