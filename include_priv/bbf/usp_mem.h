/*
 *
 * Copyright (C) 2019-2023, Broadband Forum
 * Copyright (C) 2016-2023  CommScope, Inc
 * Copyright (C) 2020,  BT PLC
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file usp_mem.h
 *
 * Header file for functions which wrap memory allocation on USP Agent
 *
 */
#if !defined(__USP_MEM_H__)
#define __USP_MEM_H__


#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------------
// Helper macros, so that the code does not have to provide (__FUNCTION__, __LINE__) to the underlying function
#define USP_MALLOC(x)               USP_MEM_Malloc(__FUNCTION__, __LINE__, x)
#define USP_FREE(x)                 USP_MEM_Free(__FUNCTION__, __LINE__, x)
#define USP_SAFE_FREE(x)            if(x != NULL) { USP_MEM_Free(__FUNCTION__, __LINE__, x); x = NULL; }
#define USP_REALLOC(x, y)           USP_MEM_Realloc(__FUNCTION__, __LINE__, x, y)
#define USP_STRDUP(x)               USP_MEM_Strdup(__FUNCTION__, __LINE__, x)

//------------------------------------------------------------------------------------
// Functions wrapping memory allocation
void* USP_MEM_Malloc(const char* func, int line, int size);
void USP_MEM_Free(const char* func, int line, void* ptr);
void* USP_MEM_Realloc(const char* func, int line, void* ptr, int size);
void* USP_MEM_Strdup(const char* func, int line, const char* ptr);

// Pointer to structure containing the protocol buffer allocator function
extern void* pbuf_allocator;

#ifdef __cplusplus
}
#endif

#endif // __USP_MEM_H__
