# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v2.7.3 - 2024-09-30(05:52:57 +0000)

### Other

- CI: Disable squashing of open source commits
- [AMX] Disable commit squashing

## Release v2.7.2 - 2024-07-17(06:48:54 +0000)

### New

- [USP] libusp library improvements

## Release v2.7.1 - 2024-07-04(06:34:08 +0000)

### Other

- [TR181-Device]Bidirectional communication support between UBUS and IMTP
- Key parameters without read-only in definition are write-once and must be reported as read-write in gsdm

## Release v2.7.0 - 2024-06-06(18:43:30 +0000)

### New

- Add generic function to try to convert USP error to amxd status

## Release v2.6.8 - 2024-04-30(13:39:13 +0000)

### Other

- Update dependencies in .gitlab-ci.yml

## Release v2.6.7 - 2024-02-28(12:18:50 +0000)

### Other

- Rename BBF license to OBUSPA license

## Release v2.6.6 - 2024-02-28(08:28:28 +0000)

### Other

- [USP] Fix licensing issues for opensourcing USP libs
- Move component to prpl-foundation gitlab

## Release v2.6.5 - 2023-11-21(10:42:41 +0000)

### Fixes

- [USP] Add extra NULL pointer checks

## Release v2.6.4 - 2023-11-16(12:53:54 +0000)

### Fixes

- [USP] Auto cast results from a set and add response

## Release v2.6.3 - 2023-11-16(12:01:27 +0000)

### Changes

- [USP] Make set response less strict

## Release v2.6.2 - 2023-11-14(14:21:25 +0000)

### Fixes

- [USP] Auto cast results from a get response

### Other

- Don't rename LICENSE.BSD to LICENSE in oss
- Update dependencies in .gitlab-ci.yml

## Release v2.6.1 - 2023-09-26(08:31:14 +0000)

### Fixes

- [USP] deregistered_path can be repeated

## Release v2.6.0 - 2023-09-25(15:38:28 +0000)

### New

- [USP] Use latest protobuf schema

## Release v2.5.2 - 2023-09-25(12:05:12 +0000)

### Other

- Fix license headers in files

## Release v2.5.1 - 2023-09-06(07:29:39 +0000)

### Other

- Update dependencies in .gitlab-ci.yml

## Release v2.5.0 - 2023-07-28(08:37:27 +0000)

### New

- [PRPL][USP][Onboarding]MTP Connector usp messages missing

## Release v2.4.0 - 2023-07-20(07:43:44 +0000)

### New

- [USP] GSDM should return whether commands are (a)sync

## Release v2.3.2 - 2023-07-11(06:47:14 +0000)

### Other

- Update dependencies in .gitlab-ci.yml

## Release v2.3.1 - 2023-07-04(07:35:27 +0000)

### Fixes

- [USP] Update log message for allow_partial=false

## Release v2.3.0 - 2023-06-12(09:39:52 +0000)

### New

- It must be possible to build partially failed add responses

## Release v2.2.5 - 2023-06-08(17:13:54 +0000)

### Fixes

- [USP] allow_partial=false must be rejected

## Release v2.2.4 - 2023-05-26(07:23:19 +0000)

### Changes

- [USP] Add specific error codes for get instances

## Release v2.2.3 - 2023-05-22(14:03:40 +0000)

### Fixes

- [USP] Input arguments must be JSON encoded

## Release v2.2.2 - 2023-05-11(12:15:13 +0000)

## Release v2.2.1 - 2023-05-04(11:09:41 +0000)

### Fixes

- [USP][AMX] GSDM needs a ValueChangeType

## Release v2.2.0 - 2023-05-03(11:47:38 +0000)

### New

- gsdm missing arguments for commands and events

## Release v2.1.1 - 2023-05-03(10:20:23 +0000)

### Other

- Update dependencies in .gitlab-ci.yml

## Release v2.1.0 - 2023-04-26(08:12:22 +0000)

### New

- [USP] Add NotifType AmxNotification for ambiorix events

## Release v2.0.1 - 2023-03-21(14:07:53 +0000)

### Fixes

- [USP] Remove var dumps from libusp

## Release v2.0.0 - 2023-01-11(12:20:07 +0000)

### Breaking

- [KPN][USP] max_depth has no effect on the Get Message

## Release v1.1.2 - 2023-01-09(09:11:49 +0000)

### Fixes

- [USP] Get requests with valid search expressions must return successful

## Release v1.1.1 - 2022-11-25(08:14:28 +0000)

### Fixes

- [USP] GSDM response cannot be extracted properly

## Release v1.1.0 - 2022-11-21(16:18:24 +0000)

### New

- Extend error code conversion

## Release v1.0.3 - 2022-11-10(11:38:42 +0000)

### Fixes

- [USP] Confusion around USP version

## Release v1.0.2 - 2022-10-20(06:24:58 +0000)

### Fixes

- [USP] Fix unit tests of libusp after changes to libamxd

## Release v1.0.1 - 2022-10-19(13:59:07 +0000)

### Other

- Update dependencies in .gitlab-ci.yml

## Release v1.0.0 - 2022-09-05(13:34:34 +0000)

### New

- [USP] Upstep version of libuspprotobuf

## Release v0.8.2 - 2022-08-25(14:06:20 +0000)

### Changes

- Register message does not need to contain URI

## Release v0.8.1 - 2022-07-14(12:33:52 +0000)

### Fixes

- [USP] LIBDIR should be STAGING_LIBDIR to avoid conflicts

## Release v0.8.0 - 2022-07-13(09:52:56 +0000)

### New

- [USP] Add latest USP error codes

## Release v0.7.3 - 2022-07-07(14:11:43 +0000)

### Changes

- [USP] It must be possible to set string parameters starting with a plus

## Release v0.7.2 - 2022-05-19(12:47:48 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.7.1 - 2022-05-05(15:03:18 +0000)

### Fixes

- Update header file

## Release v0.7.0 - 2022-05-03(07:25:11 +0000)

### New

- [USP Agent] Implement GetInstances message

## Release v0.6.0 - 2022-04-27(11:57:51 +0000)

### New

- Separate BBF and SAH code

## Release v0.5.2 - 2022-04-05(06:01:15 +0000)

### Fixes

- [USP] Event parameters can contain empty strings

## Release v0.5.1 - 2022-03-30(12:32:37 +0000)

### Fixes

- [USP] Operate response can contain zero output args

## Release v0.5.0 - 2022-02-23(09:34:04 +0000)

### New

- [USP] It must be possible to build deregister messages

### Fixes

- [USP] Cannot build register message with multiple paths

## Release v0.4.1 - 2022-02-14(13:40:54 +0000)

### Fixes

- Cannot create USP Error messages with parameter errors

### Other

- [USPAgent] The USP Agent must be opensourced to gitlab.com/soft.at.home

## Release v0.4.0 - 2022-01-20(09:11:20 +0000)

### New

- [USP] Implement a discovery service

## Release v0.3.0 - 2022-01-14(08:50:11 +0000)

### New

- Handle allow partial set

## Release v0.2.12 - 2021-12-15(12:13:16 +0000)

### Other

- [USP] libusp is spamming a useless log message

## Release v0.2.11 - 2021-12-13(12:57:25 +0000)

### Changes

- Extend unit tests for OperationComplete notifications

## Release v0.2.10 - 2021-12-09(10:48:36 +0000)

### Fixes

- OperationComplete notification may contain no output_args

## Release v0.2.9 - 2021-11-25(11:43:54 +0000)

### Fixes

- [USP] Version check should be less strict

## Release v0.2.8 - 2021-10-07(07:01:36 +0000)

### Fixes

- [USP] allow_partial should be ignored

## Release v0.2.7 - 2021-08-23(10:17:16 +0000)

### Changes

- Replace snprintf functions with amxc_string_t functions

## Release v0.2.6 - 2021-08-10(09:08:10 +0000)

### Fixes

- Compilation error on WNC

## Release v0.2.5 - 2021-08-02(08:22:38 +0000)

## Release v0.2.4 - 2021-07-19(13:20:54 +0000)

### Changes

- Missing msg type in uspl_tx_t

## Release v0.2.3 - 2021-07-12(12:19:18 +0000)

## Release v0.2.2 - 2021-06-28(15:22:40 +0000)

### Fixes

- [USP] Get response does not return error codes

## Release v0.2.1 - 2021-06-25(06:49:06 +0000)

### Fixes

- [tr181 plugins][makefile] Dangerous clean target for all tr181 components

### Other

- Issue: amx/usp/libraries/libusp#14 Update unit tests for GSDM

## Release v0.2.0 - 2021-04-26(12:41:38 +0000)

### Fixes

- Segmentation fault in uspl_set_resp_new when `request_path` is missing

## Release v0.1.10 - 2021-04-26(08:13:07 +0000)

### Added

- Implement GetSupportedProtocol request and response messages

### Changes

- Getting msg id from send USP message
- Unit tests must be updated to use prefixed internal RPCs

## Release 0.1.9 - 2021-03-18(19:58:26 +0000)

### Changed

- Missing iterator increment when adding unique keys to an add response
- Update documentation

## Release 0.1.8 - 2021-03-16(15:03:10 +0000)

### Changed

- When extracting a USP Add Response protobuf, the requested_path should be added to the result variant

## Release 0.1.7 - 2021-03-10(15:55:29 +0000)

### Changed

- Operate response arguments are converted to cstring_t instead of jstring_t

## Release 0.1.6 - 2021-03-09(14:47:23 +0000)

### Changed

- Update error codes to type uint32_t
- Incorrect operate message type

## Release 0.1.5 - 2021-03-05(09:55:40 +0000)

### Added

- Implement USP operate request and response
- Migrate to new licenses format (baf)

### Changed

- Select latest version of libuspprotobuf

## Release 0.1.4 - 2021-02-26(15:30:34 +0000)

### Changed

- The USP Add response should be more similar to the other operation responses

## Release 0.1.3 - 2021-02-02(11:58:50 +0000)

### Added

- Implement notify messages

### Changed

- It must be allowed to add an object without setting parameter values

## Release 0.1.2 - 2021-01-20(09:25:14 +0000)

### Changed

- Use latest version of `libamxc`

## Release 0.1.1 - 2021-01-14(09:33:52 +0000)

### Added

- Implement and test Delete request and response

## Release 0.1.0 - 2021-01-07(12:37:43 +0000)

### Changed

- Error checks should use the appropriate `when_null`, `when_true`, `when_failed`,... macros
- Documentation should be written in the correct format
- Static functions must be declared static
- Function names should have the correct prefix
- The public APIs need to be more in line
- Both the BBF/Commscope license and the SoftAtHome license are now present in the source files
- `uspl_int_vector`, `uspl_kv_vector` and `uspl_str_vector` are replaced by variants
- Enable cross compilations

## Release 0.0.10 - 2020-12-15(14:02:39 +0000)

### Added

- Hotfix: incorrect .so name
- Implement Add request and response

## Release 0.0.9 - 2020-12-14(14:46:46 +0000)

### Added

- Add BAF

## Release 0.0.8 - 2020-12-08(11:48:57 +0000)

### Added

- Implement set request and response

## Release 0.0.7 - 2020-12-04(11:00:27 +0000)

### Added

- Add function to extract error message.
- Implement and test uspl_get_resp_extract
- Implement get supported dm request and response

## Release 0.0.6 - 2020-12-01(07:54:38 +0000)

### Changed

- Use sofa pipeline

## Release 0.0.5 - 2020-11-26(12:18:03 +0000)

### Changed

- Replace amxc_htable_for_each with amxc_htable_iterate to prevent shadowing of variables
- Use more of the obuspa functions instead of our own implementation
- Clean up get functions

## Release 0.0.4 - 2020-11-24(16:57:15 +0000)

### Added

- Add tests for get function

### Changed

- correct SAH_TRACEZ statements

## Release 0.0.3 - 2020-11-23(14:03:53 +0000)

### Added

- Add function for creating error messages

## Release 0.0.2 - 2020-11-19(15:51:54 +0000)

### Added

- Can create get message and get response message.
- Can extract paths from get message.
- Added unit tests for msghandler code.

## Release 0.0.1 - 2020-11-19(14:51:54 +0000)

### Added

- Initial release
