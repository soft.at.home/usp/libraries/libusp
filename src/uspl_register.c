/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <debug/sahtrace.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_register.h"
#include "usp/uspl_msghandler.h"

#include "uspl_msghandler_priv.h"
#include "uspl_private.h"

#include "bbf/usp_mem.h"

static Usp__Msg* CreateRegister(const char* msg_id) {
    Usp__Msg* msg = NULL;
    Usp__Header* header = NULL;
    Usp__Body* body = NULL;
    Usp__Request* request = NULL;
    Usp__Register* reg = NULL;

    // Allocate memory to store the USP message
    msg = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(msg);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    request = USP_MALLOC(sizeof(Usp__Request));
    usp__request__init(request);

    reg = USP_MALLOC(sizeof(Usp__Register));
    usp__register__init(reg);

    // Connect the structures together
    msg->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__REGISTER;

    msg->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    body->request = request;
    request->req_type_case = USP__REQUEST__REQ_TYPE_REGISTER;
    request->register_ = reg;
    reg->allow_partial = false;
    reg->n_reg_paths = 0;
    reg->reg_paths = NULL;

    return msg;
}

static Usp__Msg* CreateRegisterResp(const char* msg_id) {
    Usp__Msg* resp;
    Usp__Header* header;
    Usp__Body* body;
    Usp__Response* response;
    Usp__RegisterResp* reg_resp;

    // Allocate and initialise memory to store the parts of the USP message
    resp = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(resp);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    response = USP_MALLOC(sizeof(Usp__Response));
    usp__response__init(response);

    reg_resp = USP_MALLOC(sizeof(Usp__RegisterResp));
    usp__register_resp__init(reg_resp);

    // Connect the structures together
    resp->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__REGISTER_RESP;

    resp->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    body->response = response;
    response->resp_type_case = USP__RESPONSE__RESP_TYPE_REGISTER_RESP;
    response->register_resp = reg_resp;

    reg_resp->n_registered_path_results = 0;
    reg_resp->registered_path_results = NULL;

    return resp;
}

static Usp__Register__RegistrationPath* uspl_register_create_reg_path(Usp__Register* reg,
                                                                      const char* path) {
    Usp__Register__RegistrationPath* reg_path = NULL;
    int new_num = 0;    // new number of entries in reg_paths array

    // Allocate memory to store the create_object entry
    reg_path = USP_MALLOC(sizeof(Usp__Register__RegistrationPath));
    usp__register__registration_path__init(reg_path);

    // Increase the size of the array
    new_num = reg->n_reg_paths + 1;
    reg->reg_paths = USP_REALLOC(reg->reg_paths, new_num * sizeof(void*));
    reg->n_reg_paths = new_num;
    reg->reg_paths[new_num - 1] = reg_path;

    // Fill up the reg_path entry
    reg_path->path = USP_STRDUP(path);

    return reg_path;
}

static int uspl_register_populate_req(Usp__Register* reg, amxc_var_t* reg_paths) {
    int retval = -1;
    const amxc_llist_t* reg_list = amxc_var_constcast(amxc_llist_t, reg_paths);

    when_true(amxc_llist_is_empty(reg_list), exit);

    amxc_var_for_each(entry, reg_paths) {
        const char* path = GET_CHAR(entry, "path");

        when_str_empty(path, exit);

        uspl_register_create_reg_path(reg, path);
    }

    retval = 0;
exit:
    return retval;
}

static void uspl_reg_populate_result(Usp__Register* reg, amxc_var_t* result) {
    amxc_var_t* reg_paths = NULL;

    amxc_var_set_type(result, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, result, "allow_partial", reg->allow_partial);
    reg_paths = amxc_var_add_key(amxc_llist_t, result, "reg_paths", NULL);

    for(size_t i = 0; i < reg->n_reg_paths; i++) {
        Usp__Register__RegistrationPath* reg_path = reg->reg_paths[i];
        amxc_var_t* reg_path_entry = amxc_var_add(amxc_htable_t, reg_paths, NULL);
        amxc_var_add_key(cstring_t, reg_path_entry, "path", reg_path->path);
    }
}

static Usp__RegisterResp__RegisteredPathResult*
uspl_register_populate_resp_generic(Usp__RegisterResp* reg_resp,
                                    const char* req_path) {
    Usp__RegisterResp__RegisteredPathResult* reg_path_result = NULL;
    int new_num = -1;    // new number of entries in the created object result array

    // Allocate memory to store the created object result
    reg_path_result = USP_MALLOC(sizeof(Usp__RegisterResp__RegisteredPathResult));
    usp__register_resp__registered_path_result__init(reg_path_result);

    new_num = reg_resp->n_registered_path_results + 1;
    reg_resp->registered_path_results = USP_REALLOC(reg_resp->registered_path_results, new_num * sizeof(void*));
    reg_resp->n_registered_path_results = new_num;
    reg_resp->registered_path_results[new_num - 1] = reg_path_result;

    reg_path_result->requested_path = USP_STRDUP(req_path);
    reg_path_result->oper_status = NULL;

    return reg_path_result;
}

static int uspl_register_populate_resp_success(Usp__RegisterResp__RegisteredPathResult* reg_path_result,
                                               amxc_var_t* reply_data) {
    Usp__RegisterResp__RegisteredPathResult__OperationStatus* oper_status = NULL;
    Usp__RegisterResp__RegisteredPathResult__OperationStatus__OperationSuccess* oper_success = NULL;
    const char* reg_path = GETP_CHAR(reply_data, "oper_success.registered_path");
    int retval = -1;

    when_str_empty(reg_path, exit);

    oper_status = USP_MALLOC(sizeof(Usp__RegisterResp__RegisteredPathResult__OperationStatus));
    usp__register_resp__registered_path_result__operation_status__init(oper_status);

    oper_success = USP_MALLOC(sizeof(Usp__RegisterResp__RegisteredPathResult__OperationStatus__OperationSuccess));
    usp__register_resp__registered_path_result__operation_status__operation_success__init(oper_success);

    reg_path_result->oper_status = oper_status;
    oper_status->oper_status_case = USP__REGISTER_RESP__REGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS;
    oper_status->oper_success = oper_success;

    oper_success->registered_path = USP_STRDUP(reg_path);

    retval = 0;
exit:
    return retval;
}

static int uspl_register_populate_resp_failure(Usp__RegisterResp__RegisteredPathResult* reg_path_result,
                                               amxc_var_t* reply_data) {
    Usp__RegisterResp__RegisteredPathResult__OperationStatus* oper_status = NULL;
    Usp__RegisterResp__RegisteredPathResult__OperationStatus__OperationFailure* oper_failure = NULL;
    amxc_var_t* failure_args = GET_ARG(reply_data, "oper_failure");
    uint32_t err_code = GET_UINT32(failure_args, "err_code");
    const char* err_msg = GET_CHAR(failure_args, "err_msg");
    int retval = -1;

    when_null(failure_args, exit);
    when_null(err_msg, exit);

    oper_status = USP_MALLOC(sizeof(Usp__RegisterResp__RegisteredPathResult__OperationStatus));
    usp__register_resp__registered_path_result__operation_status__init(oper_status);

    oper_failure = USP_MALLOC(sizeof(Usp__RegisterResp__RegisteredPathResult__OperationStatus__OperationFailure));
    usp__register_resp__registered_path_result__operation_status__operation_failure__init(oper_failure);

    reg_path_result->oper_status = oper_status;
    oper_status->oper_status_case = USP__REGISTER_RESP__REGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE;
    oper_status->oper_failure = oper_failure;

    oper_failure->err_code = err_code;
    oper_failure->err_msg = USP_STRDUP(err_msg);

    retval = 0;
exit:
    return retval;
}

static int uspl_register_populate_resp(Usp__RegisterResp* reg_resp, amxc_var_t* reply_data) {
    int retval = -1;
    uint32_t oper_status_case = USP__REGISTER_RESP__REGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS__NOT_SET;
    const char* req_path = NULL;
    Usp__RegisterResp__RegisteredPathResult* reg_path_result = NULL;

    req_path = GET_CHAR(reply_data, "requested_path");
    when_str_empty(req_path, exit);

    reg_path_result = uspl_register_populate_resp_generic(reg_resp, req_path);

    oper_status_case = GET_UINT32(reply_data, "oper_status_case");
    if(oper_status_case == USP__REGISTER_RESP__REGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS) {
        retval = uspl_register_populate_resp_success(reg_path_result, reply_data);
    } else if(oper_status_case == USP__REGISTER_RESP__REGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE) {
        retval = uspl_register_populate_resp_failure(reg_path_result, reply_data);
    } else {
        goto exit;
    }

exit:
    return retval;
}

static void uspl_register_resp_append_result_success(Usp__RegisterResp__RegisteredPathResult__OperationStatus__OperationSuccess* oper_success,
                                                     amxc_var_t* entry) {
    amxc_var_t* success = amxc_var_add_key(amxc_htable_t, entry, "oper_success", NULL);
    amxc_var_add_key(cstring_t, success, "registered_path", oper_success->registered_path);
}

static void uspl_register_resp_append_result_failure(Usp__RegisterResp__RegisteredPathResult__OperationStatus__OperationFailure* oper_failure,
                                                     amxc_var_t* entry) {
    amxc_var_t* failure = amxc_var_add_key(amxc_htable_t, entry, "oper_failure", NULL);

    amxc_var_add_key(cstring_t, failure, "err_msg", oper_failure->err_msg);
    amxc_var_add_key(uint32_t, failure, "err_code", oper_failure->err_code);

    return;
}

static int uspl_register_resp_append_result(Usp__RegisterResp__RegisteredPathResult* result,
                                            amxc_llist_t* resp_list) {
    int retval = -1;
    amxc_var_t* entry = NULL;
    uint32_t status = result->oper_status->oper_status_case;

    amxc_var_new(&entry);
    amxc_var_set_type(entry, AMXC_VAR_ID_HTABLE);
    amxc_llist_append(resp_list, &entry->lit);

    amxc_var_add_key(cstring_t, entry, "requested_path", result->requested_path);
    amxc_var_add_key(uint32_t, entry, "oper_status_case", status);

    if(status == USP__REGISTER_RESP__REGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS) {
        uspl_register_resp_append_result_success(result->oper_status->oper_success, entry);
    } else if(status == USP__REGISTER_RESP__REGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE) {
        uspl_register_resp_append_result_failure(result->oper_status->oper_failure, entry);
    } else {
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}

/***************** Start of public functions *******************/

int uspl_register_new(uspl_tx_t* usp_tx, const amxc_var_t* request) {
    int retval = -1;
    Usp__Msg* msg = NULL;
    amxc_string_t msg_id;
    amxc_var_t* reg_paths = NULL;

    amxc_string_init(&msg_id, 0);

    when_null(usp_tx, exit);
    when_null(request, exit);

    amxc_string_setf(&msg_id, "%d", uspl_msghandler_next_msg_id());

    msg = CreateRegister(amxc_string_get(&msg_id, 0));
    when_null(msg, exit);

    msg->body->request->register_->allow_partial = GET_BOOL(request, "allow_partial");
    reg_paths = GET_ARG(request, "reg_paths");
    when_null(reg_paths, exit);

    retval = uspl_register_populate_req(msg->body->request->register_, reg_paths);
    when_failed(retval, exit);

    retval = uspl_msghandler_pack_protobuf(msg, usp_tx);
    usp_tx->msg_id = USP_STRDUP(msg->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__REGISTER;

exit:
    amxc_string_clean(&msg_id);
    uspl_msghandler_free_unpacked_message(msg);

    return retval;
}

int uspl_register_extract(uspl_rx_t* usp_rx, amxc_var_t* result) {
    int retval = USP_ERR_INTERNAL_ERROR;
    Usp__Register* reg = NULL;

    when_null(usp_rx, exit);
    when_null(result, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_REQUEST, exit);
    when_null(usp_rx->msg->body->request, exit);
    when_true(usp_rx->msg->body->request->req_type_case != USP__REQUEST__REQ_TYPE_REGISTER, exit);
    when_null(usp_rx->msg->body->request->register_, exit);

    reg = usp_rx->msg->body->request->register_;

    uspl_reg_populate_result(reg, result);

    retval = USP_ERR_OK;
exit:
    return retval;
}

int uspl_register_resp_new(uspl_tx_t* usp_tx, amxc_llist_t* resp_list, const char* msg_id) {
    int retval = -1;
    Usp__Msg* resp = NULL;
    Usp__RegisterResp* reg_resp = NULL;

    when_null(usp_tx, exit);
    when_null(resp_list, exit);
    when_null(msg_id, exit);

    resp = CreateRegisterResp(msg_id);
    reg_resp = resp->body->response->register_resp;

    // Iterate over all responses for different registered paths
    amxc_llist_for_each(it, resp_list) {
        amxc_var_t* reply_data = amxc_llist_it_get_data(it, amxc_var_t, lit);
        retval = uspl_register_populate_resp(reg_resp, reply_data);
        when_failed(retval, exit);
    }

    retval = uspl_msghandler_pack_protobuf(resp, usp_tx);
    usp_tx->msg_id = USP_STRDUP(resp->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__REGISTER_RESP;

exit:
    uspl_msghandler_free_unpacked_message(resp);

    return retval;
}

int uspl_register_resp_extract(uspl_rx_t* usp_rx, amxc_llist_t* resp_list) {
    int retval = USP_ERR_INTERNAL_ERROR;
    Usp__RegisterResp* resp = NULL;

    when_null(usp_rx, exit);
    when_null(resp_list, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_RESPONSE, exit);
    when_null(usp_rx->msg->body->response, exit);
    when_true(usp_rx->msg->body->response->resp_type_case != USP__RESPONSE__RESP_TYPE_REGISTER_RESP, exit);
    when_null(usp_rx->msg->body->response->register_resp, exit);

    resp = usp_rx->msg->body->response->register_resp;
    // Exit if there are no results in the response
    retval = USP_ERR_INVALID_ARGUMENTS;
    when_true(resp->n_registered_path_results == 0, exit);
    when_true(resp->registered_path_results == NULL, exit);

    for(size_t i = 0; i < resp->n_registered_path_results; i++) {
        retval = uspl_register_resp_append_result(resp->registered_path_results[i], resp_list);
        when_true_status(retval != 0, exit, retval = USP_ERR_INTERNAL_ERROR);
    }

    retval = USP_ERR_OK;
exit:
    return retval;
}
