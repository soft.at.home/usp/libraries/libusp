/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
   @file uspl_delete.c
   @brief
   Create and extract Delete requests and responses
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>

#include <debug/sahtrace.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_error.h"
#include "usp/uspl_delete.h"
#include "usp/uspl_msghandler.h"

#include "uspl_msghandler_priv.h"

#include "bbf/usp_mem.h"
#include "bbf/handle_delete.h"

/**
   @brief
   Dynamically creates a DeleteResponse object

   @note The object should be deleted using usp__msg__free_unpacked()

   @param msg_id string containing the message id of the delete request

   @return  Pointer to a Delete object
 */
static Usp__Msg* CreateDelete(const char* msg_id) {
    Usp__Msg* msg;
    Usp__Header* header;
    Usp__Body* body;
    Usp__Request* request;
    Usp__Delete* del;

    // Allocate and initialise memory to store the parts of the USP message
    msg = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(msg);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    request = USP_MALLOC(sizeof(Usp__Request));
    usp__request__init(request);

    del = USP_MALLOC(sizeof(Usp__Delete));
    usp__delete__init(del);

    // Connect the structures together
    msg->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__DELETE;

    msg->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    body->request = request;
    request->req_type_case = USP__REQUEST__REQ_TYPE_DELETE;
    request->delete_ = del;

    del->allow_partial = false;
    del->n_obj_paths = 0;
    del->obj_paths = NULL;

    return msg;
}

/**
   @brief
   Populate the DeleteResp object with the information in the response variant in case of a
   successful operation

   @param del_resp the DeleteResp object that is filled in
   @param reply_data the variant containing the resulting information from the delete

   @return 0 in case of success, -1 in case of error
 */
static int uspl_delete_populate_resp_success(Usp__DeleteResp* del_resp, amxc_var_t* reply_data) {
    int retval = -1;
    Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationSuccess* oper_success = NULL;
    const amxc_llist_t* affected_paths = NULL;
    const amxc_llist_t* unaffected_paths = NULL;
    const char* requested_path = NULL;

    requested_path = GET_CHAR(GET_ARG(reply_data, "result"), "requested_path");
    when_null(requested_path, exit);

    oper_success = AddDeleteResp_OperSuccess(del_resp, requested_path);

    affected_paths = amxc_var_constcast(amxc_llist_t, GET_ARG(reply_data, "affected_paths"));
    when_null(affected_paths, exit);

    amxc_llist_iterate(it, affected_paths) {
        const char* path = amxc_var_constcast(cstring_t, amxc_var_from_llist_it(it));
        AddOperSuccess_AffectedPath(oper_success, path);
    }

    unaffected_paths = amxc_var_constcast(amxc_llist_t, GET_ARG(reply_data, "unaffected_paths"));
    when_null(unaffected_paths, exit);

    amxc_llist_iterate(it, unaffected_paths) {
        amxc_var_t* var = amxc_var_from_llist_it(it);
        const char* path = GET_CHAR(var, "path");
        const char* err_msg = GET_CHAR(var, "err_msg");
        uint32_t err_code = GET_UINT32(var, "err_code");

        when_null(path, exit);
        when_null(err_msg, exit);
        AddOperSuccess_UnaffectedPathError(oper_success, path, err_code, err_msg);
    }

    retval = 0;
exit:
    return retval;
}

/**
   @brief
   Populate the DeleteResp object with the information in the delete response variant.
   This function determines whether the delete operation was successful and calls the correct
   functions to fill up the DeleteResp object in case of (partial) success

   @param del_resp the DeleteResp object that is filled in
   @param reply_data the variant containing the resulting information from the delete

   @return 0 in case of success, -1 in case of error
 */
static int uspl_delete_populate_resp(Usp__DeleteResp* del_resp, amxc_var_t* reply_data) {
    int retval = -1;
    int err_code = -1;
    amxc_var_t* result = NULL;
    amxc_var_t* err_code_var = NULL;

    result = GET_ARG(reply_data, "result");
    when_null(result, exit);

    err_code_var = GET_ARG(result, "err_code");
    when_null(err_code_var, exit);

    err_code = amxc_var_dyncast(uint32_t, err_code_var);
    if(err_code == 0) {
        retval = uspl_delete_populate_resp_success(del_resp, reply_data);
        when_failed(retval, exit);
    } else {
        const char* requested_path = GET_CHAR(result, "requested_path");
        const char* err_msg = GET_CHAR(result, "err_msg");
        when_null(requested_path, exit);
        when_null(err_msg, exit);
        AddDeleteResp_OperFailure(del_resp, requested_path, err_code, err_msg);
    }

    retval = 0;
exit:
    return retval;
}

/**
   @brief
   Append the results of a successful Delete operation to the list of responses

   @param success an operation success result
   @param resp_list an empty list that will be filled up with variants of the responses

   @returns 0 in case of success, -1 in case of error
 */
static void uspl_delete_resp_append_result_success(Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationSuccess* success,
                                                   amxc_llist_t* resp_list,
                                                   const char* requested_path) {
    amxc_var_t* entry = NULL;
    amxc_var_t* result = NULL;
    amxc_var_t* affected_paths = NULL;
    amxc_var_t* unaffected_paths = NULL;

    amxc_var_new(&entry);
    amxc_var_set_type(entry, AMXC_VAR_ID_HTABLE);
    result = amxc_var_add_key(amxc_htable_t, entry, "result", NULL);
    affected_paths = amxc_var_add_key(amxc_llist_t, entry, "affected_paths", NULL);
    unaffected_paths = amxc_var_add_key(amxc_llist_t, entry, "unaffected_paths", NULL);

    amxc_var_add_key(cstring_t, result, "requested_path", requested_path);
    amxc_var_add_key(uint32_t, result, "err_code", 0);

    for(size_t i = 0; i < success->n_affected_paths; i++) {
        amxc_var_add(cstring_t, affected_paths, success->affected_paths[i]);
    }

    for(size_t i = 0; i < success->n_unaffected_path_errs; i++) {
        amxc_var_t* path_err = amxc_var_add(amxc_htable_t, unaffected_paths, NULL);
        amxc_var_add_key(cstring_t, path_err, "path", success->unaffected_path_errs[i]->unaffected_path);
        amxc_var_add_key(uint32_t, path_err, "err_code", success->unaffected_path_errs[i]->err_code);
        amxc_var_add_key(cstring_t, path_err, "err_msg", success->unaffected_path_errs[i]->err_msg);
    }

    amxc_llist_append(resp_list, &entry->lit);
}

/**
   @brief
   Append the results of a partially failed Delete operation to the list of responses

   @param failure an operation failure result
   @param resp_list an empty list that will be filled up with variants of the responses

   @returns 0 in case of success, -1 in case of error
 */
static void uspl_delete_resp_append_result_failure(Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationFailure* failure,
                                                   amxc_llist_t* resp_list,
                                                   const char* requested_path) {
    amxc_var_t* entry = NULL;
    amxc_var_t* result = NULL;

    amxc_var_new(&entry);
    amxc_var_set_type(entry, AMXC_VAR_ID_HTABLE);
    result = amxc_var_add_key(amxc_htable_t, entry, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", requested_path);
    amxc_var_add_key(cstring_t, result, "err_msg", failure->err_msg);
    amxc_var_add_key(uint32_t, result, "err_code", failure->err_code);

    amxc_llist_append(resp_list, &entry->lit);
}

/**
   @brief

   @param result a DeletedObjectResult in response to a Delete for a single requested path
   @param resp_list an empty list that will be filled up with variants of the responses

   @returns 0 in case of success, -1 in case of error
 */
static int uspl_delete_resp_append_result(Usp__DeleteResp__DeletedObjectResult* result,
                                          amxc_llist_t* resp_list) {
    int retval = 0;

    if(result->oper_status->oper_status_case == USP__DELETE_RESP__DELETED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS) {
        Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationSuccess* success = NULL;
        success = result->oper_status->oper_success;
        uspl_delete_resp_append_result_success(success, resp_list, result->requested_path);
    } else if(result->oper_status->oper_status_case == USP__DELETE_RESP__DELETED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE) {
        Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationFailure* failure = NULL;
        failure = result->oper_status->oper_failure;
        uspl_delete_resp_append_result_failure(failure, resp_list, result->requested_path);
        SAH_TRACEZ_WARNING(uspl_traceme, "allow_partial not supported, so partial failure not allowed");
        goto exit;
    } else {
        SAH_TRACEZ_ERROR(uspl_traceme, "Unknown operation response status");
        retval = USP_ERR_INVALID_ARGUMENTS;
        goto exit;
    }

exit:
    return retval;
}

/*********************** Start of public functions ****************************/

int uspl_delete_new(uspl_tx_t* usp_tx, const amxc_var_t* request) {
    int retval = -1;
    int i = 0;
    Usp__Msg* msg = NULL;
    amxc_var_t* requests = NULL;
    const amxc_llist_t* req_list = NULL;
    size_t num_paths = 0;
    amxc_string_t msg_id;

    amxc_string_init(&msg_id, 0);

    when_null(usp_tx, exit);
    when_null(request, exit);

    amxc_string_setf(&msg_id, "%d", uspl_msghandler_next_msg_id());

    msg = CreateDelete(amxc_string_get(&msg_id, 0));

    msg->body->request->delete_->allow_partial = GET_BOOL(request, "allow_partial");

    requests = GET_ARG(request, "requests");
    when_null(requests, exit);

    req_list = amxc_var_constcast(amxc_llist_t, requests);
    num_paths = amxc_llist_size(req_list);
    msg->body->request->delete_->obj_paths = calloc(num_paths, sizeof(char*));
    msg->body->request->delete_->n_obj_paths = num_paths;

    amxc_llist_iterate(it, req_list) {
        amxc_var_t* var = amxc_var_from_llist_it(it);
        const char* path = amxc_var_constcast(cstring_t, var);
        msg->body->request->delete_->obj_paths[i] = USP_STRDUP(path);
        i++;
    }

    retval = uspl_msghandler_pack_protobuf(msg, usp_tx);
    usp_tx->msg_id = USP_STRDUP(msg->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__DELETE;

exit:
    amxc_string_clean(&msg_id);
    uspl_msghandler_free_unpacked_message(msg);

    return retval;
}

int uspl_delete_extract(uspl_rx_t* usp_rx, amxc_var_t* result) {
    int retval = USP_ERR_INTERNAL_ERROR;
    Usp__Delete* del = NULL;
    amxc_var_t* requests = NULL;

    when_null(usp_rx, exit);
    when_null(result, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_REQUEST, exit);
    when_null(usp_rx->msg->body->request, exit);
    when_true(usp_rx->msg->body->request->req_type_case != USP__REQUEST__REQ_TYPE_DELETE, exit);
    when_null(usp_rx->msg->body->request->delete_, exit);

    // Exit if there is nothing to delete
    del = usp_rx->msg->body->request->delete_;
    retval = USP_ERR_INVALID_ARGUMENTS;
    when_true(del->n_obj_paths < 1, exit);
    when_null(del->obj_paths, exit);

    amxc_var_set_type(result, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, result, "allow_partial", del->allow_partial);
    requests = amxc_var_add_key(amxc_llist_t, result, "requests", NULL);

    for(size_t i = 0; i < del->n_obj_paths; i++) {
        amxc_var_add(cstring_t, requests, del->obj_paths[i]);
    }

    retval = USP_ERR_OK;
exit:
    return retval;
}

int uspl_delete_resp_new(uspl_tx_t* usp_tx, amxc_llist_t* resp_list, const char* msg_id) {
    int retval = -1;
    Usp__Msg* resp = NULL;
    Usp__DeleteResp* delete_resp = NULL;

    when_null(usp_tx, exit);
    when_null(resp_list, exit);
    when_null(msg_id, exit);

    resp = CreateDeleteResp(msg_id);
    delete_resp = resp->body->response->delete_resp;

    // Iterate over all replies to different Delete requests
    amxc_llist_for_each(it, resp_list) {
        amxc_var_t* reply_data = amxc_llist_it_get_data(it, amxc_var_t, lit);
        retval = uspl_delete_populate_resp(delete_resp, reply_data);
        when_failed(retval, exit);
    }

    retval = uspl_msghandler_pack_protobuf(resp, usp_tx);
    usp_tx->msg_id = USP_STRDUP(resp->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__DELETE_RESP;

exit:
    uspl_msghandler_free_unpacked_message(resp);

    return retval;
}

int uspl_delete_resp_extract(uspl_rx_t* usp_rx, amxc_llist_t* resp_list) {
    int retval = USP_ERR_INTERNAL_ERROR;
    Usp__DeleteResp* resp = NULL;

    when_null(usp_rx, exit);
    when_null(resp_list, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_RESPONSE, exit);
    when_null(usp_rx->msg->body->response, exit);
    when_true(usp_rx->msg->body->response->resp_type_case != USP__RESPONSE__RESP_TYPE_DELETE_RESP, exit);
    when_null(usp_rx->msg->body->response->delete_resp, exit);

    resp = usp_rx->msg->body->response->delete_resp;
    // Exit if there are no results in the response
    retval = USP_ERR_INVALID_ARGUMENTS;
    when_true(resp->n_deleted_obj_results == 0, exit);
    when_true(resp->deleted_obj_results == NULL, exit);

    for(size_t i = 0; i < resp->n_deleted_obj_results; i++) {
        retval = uspl_delete_resp_append_result(resp->deleted_obj_results[i], resp_list);
        when_failed(retval, exit);
    }

    retval = USP_ERR_OK;
exit:
    return retval;
}