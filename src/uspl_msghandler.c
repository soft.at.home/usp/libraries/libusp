/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
   @file uspl_msghandler.c
   @brief
   Functions for handling messages
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "usp/uspl_msghandler.h"

#include "bbf/usp_mem.h"
#include "bbf/proto_trace.h"

#include "uspl_msghandler_priv.h"
#include "uspl_private.h"

static void* uspl_msghandler_protobuf_alloc(void* allocator_data,
                                            size_t size);

static void uspl_msghandler_protobuf_free(void* allocator_data,
                                          void* pointer);

static bool uspl_msghandler_validate_record(UspRecord__Record* rec);

static bool uspl_msghandler_validate_msg(Usp__Msg* usp);

static UspRecord__Record* uspl_msghandler_unpack_binary_record(unsigned char* pbuf,
                                                               int pbuf_len);

static Usp__Msg* uspl_msghandler_unpack_binary_msg(unsigned char* pbuf,
                                                   int pbuf_len);

static ProtobufCAllocator msghandler_protobuf_allocator =
{
    uspl_msghandler_protobuf_alloc,
    uspl_msghandler_protobuf_free,
    NULL   // Opaque pointer passed to above 2 functions. Currently unused by those functions.
};

/* Message id that corresponds to protobuf */
static uint32_t msg_id = 0;

static void* uspl_msghandler_protobuf_alloc(UNUSED void* allocator_data, size_t size) {
    void* ptr;

    ptr = malloc(size);

    return ptr;
}

static void uspl_msghandler_protobuf_free(UNUSED void* allocator_data,
                                          void* pointer) {
    free(pointer);
}

static bool uspl_msghandler_validate_no_session_ctx(UspRecord__Record* rec) {
    bool valid = true;
    UspRecord__NoSessionContextRecord* ctx = rec->no_session_context;

    if((ctx == NULL) || (ctx->payload.data == NULL) || (ctx->payload.len == 0)) {
        SAH_TRACEZ_WARNING(uspl_traceme, "Ignoring USP record as it does not contain a payload");
        valid = false;
    }

    return valid;
}

static bool uspl_msghandler_validate_mqtt_connect(UspRecord__Record* rec) {
    bool valid = false;
    UspRecord__MQTTConnectRecord* mqtt_connect = rec->mqtt_connect;

    if(mqtt_connect == NULL) {
        SAH_TRACEZ_WARNING(uspl_traceme, "Ignoring MQTTConnectRecord with missing information");
        goto exit;
    }
    if((mqtt_connect->version != USP_RECORD__MQTTCONNECT_RECORD__MQTTVERSION__V3_1_1) &&
       (mqtt_connect->version != USP_RECORD__MQTTCONNECT_RECORD__MQTTVERSION__V5)) {
        SAH_TRACEZ_WARNING(uspl_traceme, "Ignoring MQTTConnectRecord with invalid version");
        goto exit;
    }
    if((mqtt_connect->subscribed_topic == NULL) || (*(mqtt_connect->subscribed_topic) == 0)) {
        SAH_TRACEZ_WARNING(uspl_traceme, "Ignoring MQTTConnectRecord without subscribed_topic");
        goto exit;
    }

    valid = true;
exit:
    return valid;
}

static bool uspl_msghandler_validate_record(UspRecord__Record* rec) {
    bool valid = false;

    if(rec->to_id == NULL) {
        SAH_TRACEZ_WARNING(uspl_traceme, "Ignoring USP record without destination EndpointID");
        goto exit;
    }
    if(rec->from_id == NULL) {
        SAH_TRACEZ_WARNING(uspl_traceme, "Ignoring USP record without source EndpointID");
        goto exit;
    }
    /* we do no support encryption */
    if(rec->payload_security != USP_RECORD__RECORD__PAYLOAD_SECURITY__PLAINTEXT) {
        SAH_TRACEZ_WARNING(uspl_traceme, "Ignoring USP record USP record as it contains an encrypted payload ");
        goto exit;
    }
    if((rec->mac_signature.len != 0) || (rec->mac_signature.data != NULL)) {
        SAH_TRACEZ_WARNING(uspl_traceme, "Ignoring USP record with integrity check");
        goto exit;
    }
    if((rec->sender_cert.len != 0) || (rec->sender_cert.data != NULL)) {
        SAH_TRACEZ_WARNING(uspl_traceme, "Ignoring USP record as checking of sender certificate is not supported");
        goto exit;
    }
    if(rec->record_type_case == USP_RECORD__RECORD__RECORD_TYPE_NO_SESSION_CONTEXT) {
        SAH_TRACEZ_INFO(uspl_traceme, "Validating USP record of type NoSessionContext");
        valid = uspl_msghandler_validate_no_session_ctx(rec);
        when_false(valid, exit);
    } else if(rec->record_type_case == USP_RECORD__RECORD__RECORD_TYPE_MQTT_CONNECT) {
        SAH_TRACEZ_INFO(uspl_traceme, "Validating USP record of type MQTTConnect");
        valid = uspl_msghandler_validate_mqtt_connect(rec);
        when_false(valid, exit);
    } else if(rec->record_type_case == USP_RECORD__RECORD__RECORD_TYPE_UDS_CONNECT) {
        SAH_TRACEZ_INFO(uspl_traceme, "Validating USP record of type UDSConnect");
    } else if(rec->record_type_case == USP_RECORD__RECORD__RECORD_TYPE_DISCONNECT) {
        SAH_TRACEZ_INFO(uspl_traceme, "Validating USP record of type Disconnect");
    } else {
        SAH_TRACEZ_WARNING(uspl_traceme, "Ignoring USP record with invalid record_type_case");
        goto exit;
    }

    valid = true;
exit:
    return valid;
}

static bool uspl_msghandler_validate_msg(UNUSED Usp__Msg* usp) {
    if(usp->header == NULL) {
        SAH_TRACEZ_WARNING(uspl_traceme, "ignoring malformed message");
        return false;
    }
    return true;
}

/* we handle only nosession context records, so no segmentation
   handling need and we can just return a valid usp record or NULL */
static UspRecord__Record* uspl_msghandler_unpack_binary_record(unsigned char* pbuf,
                                                               int pbuf_len) {
    SAH_TRACEZ_INFO(uspl_traceme, "enter uspl_msghandler_unpack_binary_record");
    bool res = false;
    UspRecord__Record* rec = NULL;
    rec = usp_record__record__unpack((void*) &msghandler_protobuf_allocator,
                                     pbuf_len,
                                     pbuf);
    if(rec == NULL) {
        SAH_TRACEZ_WARNING(uspl_traceme, "usp_record__record__unpack failed");
        return NULL;
    }

    PrintProtobufCMessageRecursive(&rec->base, 0);

    res = uspl_msghandler_validate_record(rec);
    if(!res) {
        SAH_TRACEZ_WARNING(uspl_traceme, "usprecord validatation failed");
        usp_record__record__free_unpacked(rec,
                                          (void*) &msghandler_protobuf_allocator);
        rec = NULL;
    }
    return rec;
}

static Usp__Msg* uspl_msghandler_unpack_binary_msg(unsigned char* pbuf,
                                                   int pbuf_len) {
    SAH_TRACEZ_INFO(uspl_traceme, "enter uspl_msghandler_unpack_binary_msg");
    bool res = false;
    Usp__Msg* usp = NULL;

    // Exit if unable to unpack the USP message
    usp = usp__msg__unpack((void*) &msghandler_protobuf_allocator,
                           pbuf_len,
                           pbuf);
    if(usp == NULL) {
        SAH_TRACEZ_WARNING(uspl_traceme, "usp__msg__unpack failed");
        return NULL;
    }

    PrintProtobufCMessageRecursive(&usp->base, 0);
    res = uspl_msghandler_validate_msg(usp);
    if(!res) {
        SAH_TRACEZ_WARNING(uspl_traceme, "usprecord validation failed");
        usp__msg__free_unpacked(usp,
                                (void*) &msghandler_protobuf_allocator);
        usp = NULL;
    }
    return usp;
}

/*********************** Start of private functions ****************************/

uint32_t PRIVATE uspl_msghandler_next_msg_id(void) {
    return msg_id++;
}

void PRIVATE uspl_msghandler_free_unpacked_record(UspRecord__Record* rec) {
    usp_record__record__free_unpacked(rec, (void*) &msghandler_protobuf_allocator);
}

void PRIVATE uspl_msghandler_free_unpacked_message(Usp__Msg* msg) {
    usp__msg__free_unpacked(msg, (void*) &msghandler_protobuf_allocator);
}

void PRIVATE uspl_msghandler_record_set_common_fields(UspRecord__Record* rec, uspl_tx_t* usp_tx) {
    rec->version = USP_STRDUP(USPL_VERSION);
    rec->to_id = USP_STRDUP(usp_tx->rendpoint_id);
    rec->from_id = USP_STRDUP(usp_tx->lendpoint_id);
    rec->payload_security = USP_RECORD__RECORD__PAYLOAD_SECURITY__PLAINTEXT;
    rec->mac_signature.data = NULL;
    rec->mac_signature.len = 0;
    rec->sender_cert.data = NULL;
    rec->sender_cert.len = 0;
    rec->record_type_case = USP_RECORD__RECORD__RECORD_TYPE__NOT_SET;
}

int PRIVATE uspl_msghandler_record_pack_common(uspl_tx_t* usp_tx, UspRecord__Record* rec) {
    int retval = -1;
    size_t size = 0;

    PrintProtobufCMessageRecursive(&rec->base, 0);
    usp_tx->pbuf_len = usp_record__record__get_packed_size(rec);
    usp_tx->pbuf = calloc(1, usp_tx->pbuf_len);
    when_null(usp_tx->pbuf, exit);

    size = usp_record__record__pack(rec, usp_tx->pbuf);
    SAH_TRACEZ_INFO(uspl_traceme,
                    "record size: %zu recordptr %p",
                    size,
                    (void*) usp_tx->pbuf);
    if(size != usp_tx->pbuf_len) {
        SAH_TRACEZ_WARNING(uspl_traceme,
                           "#unpacked record check failed size: %zu len: %zu",
                           size,
                           usp_tx->pbuf_len);
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}

/*********************** Start of public functions ****************************/

int uspl_tx_new(uspl_tx_t** ctx, const char* lendpointid, const char* rendpointid) {
    int retval = -1;

    when_null(ctx, exit);
    when_null(lendpointid, exit);
    when_null(rendpointid, exit);

    (*ctx) = calloc(1, sizeof(uspl_tx_t));
    when_null(*ctx, exit);
    (*ctx)->pbuf = NULL;
    (*ctx)->pbuf_len = 0;
    (*ctx)->lendpoint_id = USP_STRDUP(lendpointid);
    (*ctx)->rendpoint_id = USP_STRDUP(rendpointid);
    (*ctx)->msg_id = NULL;
    (*ctx)->msg_type = -1;

    retval = 0;
exit:
    return retval;
}

void uspl_tx_delete(uspl_tx_t** ctx) {
    when_null(ctx, exit);
    when_null(*ctx, exit);

    free((*ctx)->pbuf);
    free((*ctx)->lendpoint_id);
    free((*ctx)->rendpoint_id);
    free((*ctx)->msg_id);
    free(*ctx);
    *ctx = NULL;

exit:
    return;
}

void uspl_rx_delete(uspl_rx_t** ctx) {
    when_null(ctx, exit);
    when_null(*ctx, exit);

    uspl_msghandler_free_unpacked_record((*ctx)->rec);
    uspl_msghandler_free_unpacked_message((*ctx)->msg);
    free(*ctx);
    *ctx = NULL;
exit:
    return;
}

int uspl_msghandler_pack_protobuf(Usp__Msg* msg, uspl_tx_t* usp) {
    int retval = -1;
    unsigned char* pbuf = NULL;
    size_t pbuf_len = 0;
    size_t size = 0;
    UspRecord__Record* rec = NULL;
    UspRecord__NoSessionContextRecord* no_session_ctx = NULL;

    when_null(msg, exit);
    when_null(usp, exit);

    PrintProtobufCMessageRecursive(&msg->base, 0);

    /* pack usp message */
    pbuf_len = usp__msg__get_packed_size(msg);
    pbuf = calloc(1, pbuf_len);
    size = usp__msg__pack(msg, pbuf);
    SAH_TRACEZ_INFO(uspl_traceme, "size: %zu", size);
    if(size != pbuf_len) {
        SAH_TRACEZ_WARNING(uspl_traceme, "error during packing ");
        goto exit;
    }

    /* fill the usp record structure */
    rec = USP_MALLOC(sizeof(UspRecord__Record));
    usp_record__record__init(rec);
    uspl_msghandler_record_set_common_fields(rec, usp);
    rec->record_type_case = USP_RECORD__RECORD__RECORD_TYPE_NO_SESSION_CONTEXT;

    no_session_ctx = USP_MALLOC(sizeof(UspRecord__NoSessionContextRecord));
    usp_record__no_session_context_record__init(no_session_ctx);
    no_session_ctx->payload.data = pbuf;
    no_session_ctx->payload.len = pbuf_len;
    rec->no_session_context = no_session_ctx;

    retval = uspl_msghandler_record_pack_common(usp, rec);
    when_failed(retval, exit);

    retval = 0;
exit:
    uspl_msghandler_free_unpacked_record(rec);

    return retval;
}

static int uspl_msghandler_unpack_no_session_ctx(uspl_rx_t** usp_rx) {
    int retval = -1;

    (*usp_rx)->msg = uspl_msghandler_unpack_binary_msg((*usp_rx)->rec->no_session_context->payload.data,
                                                       (*usp_rx)->rec->no_session_context->payload.len);
    if((*usp_rx)->msg == NULL) {
        uspl_msghandler_free_unpacked_message((*usp_rx)->msg);
        free(*usp_rx);
        *usp_rx = NULL;
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}

uspl_rx_t* uspl_msghandler_unpack_protobuf(unsigned char* pbuf, int pbuf_len) {
    uspl_rx_t* usp_rx = NULL;

    when_null(pbuf, exit);

    usp_rx = calloc(1, sizeof(uspl_rx_t));
    when_null(usp_rx, exit);

    usp_rx->rec = uspl_msghandler_unpack_binary_record(pbuf, pbuf_len);
    if(usp_rx->rec == NULL) {
        free(usp_rx);
        usp_rx = NULL;
        goto exit;
    }

    // In case of no_session_context or session_context, we need to unpack the USP msg as well
    if(usp_rx->rec->record_type_case == USP_RECORD__RECORD__RECORD_TYPE_NO_SESSION_CONTEXT) {
        uspl_msghandler_unpack_no_session_ctx(&usp_rx);
    }

exit:
    return usp_rx;
}

char* uspl_msghandler_msg_id(uspl_rx_t* usp) {
    char* retval = NULL;

    when_null(usp, exit);
    when_null(usp->msg, exit);
    when_null(usp->msg->header, exit);

    retval = usp->msg->header->msg_id;
exit:
    return retval;
}

char* uspl_msghandler_from_id(uspl_rx_t* usp) {
    char* retval = NULL;

    when_null(usp, exit);
    when_null(usp->rec, exit);

    retval = usp->rec->from_id;
exit:
    return retval;
}

char* uspl_msghandler_to_id(uspl_rx_t* usp) {
    char* retval = NULL;

    when_null(usp, exit);
    when_null(usp->rec, exit);

    retval = usp->rec->to_id;
exit:
    return retval;
}

int uspl_msghandler_msg_type(uspl_rx_t* usp) {
    int retval = 0;

    when_null(usp, exit);
    when_null(usp->msg, exit);
    when_null(usp->msg->header, exit);

    retval = usp->msg->header->msg_type;
exit:
    return retval;
}

int uspl_msghandler_record_type(uspl_rx_t* usp) {
    int retval = 0;

    when_null(usp, exit);
    when_null(usp->rec, exit);

    retval = usp->rec->record_type_case;
exit:
    return retval;
}
