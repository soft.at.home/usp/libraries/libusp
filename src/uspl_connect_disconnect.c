/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>

#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>

#include "uspl_private.h"
#include "uspl_msghandler_priv.h"
#include "usp/uspl_connect_disconnect.h"

#include "bbf/usp_mem.h"
#include "bbf/proto_trace.h"

static int uspl_connect_pack_record_mqtt(uspl_tx_t* usp_tx,
                                         UspRecord__Record* rec,
                                         const amxc_var_t* request) {
    int retval = -1;
    UspRecord__MQTTConnectRecord* mqtt_rec = NULL;
    uint32_t version = 0;
    const char* topic = NULL;
    amxc_var_t* mqtt = GET_ARG(request, "mqtt");

    mqtt_rec = USP_MALLOC(sizeof(UspRecord__MQTTConnectRecord));
    usp_record__mqttconnect_record__init(mqtt_rec);
    rec->record_type_case = USP_RECORD__RECORD__RECORD_TYPE_MQTT_CONNECT;
    rec->mqtt_connect = mqtt_rec;

    when_null(mqtt, exit);
    version = GET_UINT32(mqtt, "version");
    when_true(version > USP_RECORD__MQTTCONNECT_RECORD__MQTTVERSION__V5, exit);
    topic = GET_CHAR(mqtt, "subscribed_topic");
    when_str_empty(topic, exit);

    mqtt_rec->version = version;
    mqtt_rec->subscribed_topic = USP_STRDUP(topic);

    retval = uspl_msghandler_record_pack_common(usp_tx, rec);

exit:
    return retval;
}

static int uspl_connect_pack_record_uds(uspl_tx_t* usp_tx,
                                        UspRecord__Record* rec) {
    int retval = -1;
    UspRecord__UDSConnectRecord* uds_rec = NULL;

    uds_rec = USP_MALLOC(sizeof(UspRecord__UDSConnectRecord));
    usp_record__udsconnect_record__init(uds_rec);

    rec->record_type_case = USP_RECORD__RECORD__RECORD_TYPE_UDS_CONNECT;
    rec->uds_connect = uds_rec;

    retval = uspl_msghandler_record_pack_common(usp_tx, rec);

    return retval;
}

int uspl_connect_record_new(uspl_tx_t* usp_tx, const amxc_var_t* request) {
    int retval = -1;
    uint32_t record_type = 0;
    UspRecord__Record* rec = NULL;

    when_null(usp_tx, exit);
    when_null(request, exit);

    rec = USP_MALLOC(sizeof(UspRecord__Record));
    usp_record__record__init(rec);
    uspl_msghandler_record_set_common_fields(rec, usp_tx);

    record_type = GET_UINT32(request, "record_type");
    switch(record_type) {
    case USP_RECORD__RECORD__RECORD_TYPE_MQTT_CONNECT:
        retval = uspl_connect_pack_record_mqtt(usp_tx, rec, request);
        when_failed(retval, exit);
        break;
    case USP_RECORD__RECORD__RECORD_TYPE_UDS_CONNECT:
        retval = uspl_connect_pack_record_uds(usp_tx, rec);
        when_failed(retval, exit);
        break;
    case USP_RECORD__RECORD__RECORD_TYPE_STOMP_CONNECT:
    case USP_RECORD__RECORD__RECORD_TYPE_WEBSOCKET_CONNECT:
    case USP_RECORD__RECORD__RECORD_TYPE_DISCONNECT:
    default:
        goto exit;
        break;
    }

    retval = 0;
exit:
    uspl_msghandler_free_unpacked_record(rec);

    return retval;
}

int uspl_disconnect_record_new(uspl_tx_t* usp_tx, const amxc_var_t* request) {
    int retval = -1;
    UspRecord__Record* rec = NULL;
    UspRecord__DisconnectRecord* disc_rec = NULL;
    amxc_var_t* reason = GET_ARG(request, "reason");
    amxc_var_t* reason_code = GET_ARG(request, "reason_code");

    when_null(usp_tx, exit);

    rec = USP_MALLOC(sizeof(UspRecord__Record));
    usp_record__record__init(rec);
    uspl_msghandler_record_set_common_fields(rec, usp_tx);

    rec->record_type_case = USP_RECORD__RECORD__RECORD_TYPE_DISCONNECT;
    disc_rec = USP_MALLOC(sizeof(UspRecord__DisconnectRecord));
    usp_record__disconnect_record__init(disc_rec);
    rec->disconnect = disc_rec;

    if(reason != NULL) {
        disc_rec->reason = amxc_var_dyncast(cstring_t, reason);
    }
    if(reason_code != NULL) {
        disc_rec->reason_code = amxc_var_dyncast(uint32_t, reason_code);
    }

    retval = uspl_msghandler_record_pack_common(usp_tx, rec);
exit:
    uspl_msghandler_free_unpacked_record(rec);

    return retval;
}
