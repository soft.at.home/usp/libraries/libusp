/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
   @file uspl_get.c
   @brief
   Create or extract a GetRequest or a GetResponse message
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>

#include <debug/sahtrace.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_error.h"
#include "usp/uspl_get.h"
#include "usp/uspl_msghandler.h"

#include "uspl_msghandler_priv.h"
#include "uspl_private.h"
#include "uspl_textutils.h"

#include "bbf/usp_mem.h"
#include "bbf/handle_get.h"

/**
   @brief
   Dynamically creates a Get request object

   @note The object should be deleted using usp__msg__free_unpacked()

   @param msg_id string containing the message id of the request

   @return Pointer to a Get request object
 */
static Usp__Msg* CreateGet(const char* msg_id) {
    Usp__Msg* msg;
    Usp__Header* header;
    Usp__Body* body;
    Usp__Request* request;
    Usp__Get* get;

    // Allocate memory to store the USP message
    msg = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(msg);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    request = USP_MALLOC(sizeof(Usp__Request));
    usp__request__init(request);

    get = USP_MALLOC(sizeof(Usp__Get));
    usp__get__init(get);

    // Connect the structures together
    msg->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__GET;

    msg->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    body->request = request;
    request->req_type_case = USP__REQUEST__REQ_TYPE_GET;
    request->get = get;
    get->n_param_paths = 0;
    get->param_paths = NULL;

    return msg;
}

static void uspl_get_resp_add_resolved_params(Usp__GetResp__ResolvedPathResult* resolved_result,
                                              amxc_var_t* resolved_var) {
    const amxc_htable_t* params_table = amxc_var_constcast(amxc_htable_t, resolved_var);
    amxc_htable_iterate(it, params_table) {
        const char* param_key = amxc_htable_it_get_key(it);
        amxc_var_t* param_var = amxc_var_from_htable_it(it);
        amxc_var_cast(param_var, AMXC_VAR_ID_CSTRING);
        AddResolvedPathRes_ParamsEntry(resolved_result, param_key,
                                       amxc_var_constcast(cstring_t, param_var));
    }
}
static void uspl_get_resp_add_resolved_path(Usp__GetResp__RequestedPathResult* req_path_result,
                                            amxc_var_t* reply_data) {
    const amxc_htable_t* resolved_table = amxc_var_constcast(amxc_htable_t, reply_data);
    amxc_htable_iterate(it, resolved_table) {
        Usp__GetResp__ResolvedPathResult* resolved_result = NULL;
        amxc_var_t* resolved_var = amxc_var_from_htable_it(it);
        const char* resolved_path = amxc_htable_it_get_key(it);
        if(strcmp(resolved_path, "result") == 0) {
            continue;
        }

        resolved_result = AddReqPathRes_ResolvedPathResult(req_path_result, resolved_path);

        uspl_get_resp_add_resolved_params(resolved_result, resolved_var);
    }
}

static int uspl_get_resp_process_response(Usp__Msg* resp, amxc_var_t* reply_data) {
    int retval = -1;
    amxc_var_t* result = NULL;
    amxc_var_t* err_var = NULL;
    uint32_t err_code = 999;
    const char* req_path = NULL;
    Usp__GetResp__RequestedPathResult* req_path_result = NULL;

    result = GET_ARG(reply_data, "result");
    when_null(result, exit);
    req_path = GET_CHAR(result, "requested_path");
    when_null(req_path, exit);

    err_var = GET_ARG(result, "err_code");
    when_null(err_var, exit);
    err_code = amxc_var_dyncast(uint32_t, err_var);
    if(err_code != USP_ERR_OK) {
        const char* err_msg = GET_CHAR(result, "err_msg");
        AddGetResp_ReqPathRes(resp, req_path, err_code, err_msg);
        retval = 0;
        goto exit;
    }

    req_path_result = AddGetResp_ReqPathRes(resp, req_path, USP_ERR_OK, NULL);
    uspl_get_resp_add_resolved_path(req_path_result, reply_data);

    retval = 0;
exit:
    return retval;
}

/**
   @brief

   @param result the result of a get response for a single requested path
   @param get_resp_list an empty list that will be filled up with variants of the responses

   @returns
 */
static void uspl_get_resp_append_result(Usp__GetResp__RequestedPathResult* result,
                                        amxc_llist_t* get_resp_list) {
    amxc_var_t* entry = NULL;
    amxc_var_t* result_var = NULL;

    amxc_var_new(&entry);
    amxc_var_set_type(entry, AMXC_VAR_ID_HTABLE);

    result_var = amxc_var_add_key(amxc_htable_t, entry, "result", NULL);
    amxc_var_add_key(cstring_t, result_var, "requested_path", result->requested_path);
    amxc_var_add_key(uint32_t, result_var, "err_code", result->err_code);


    if(result->err_code != 0) {
        amxc_var_add_key(cstring_t, result_var, "err_msg", result->err_msg);
        amxc_llist_append(get_resp_list, &entry->lit);
        goto exit;
    }

    for(size_t i = 0; i < result->n_resolved_path_results; i++) {
        Usp__GetResp__ResolvedPathResult* resolved = result->resolved_path_results[i];
        amxc_var_t* param_table = NULL;
        param_table = amxc_var_add_key(amxc_htable_t, entry, resolved->resolved_path, NULL);
        for(size_t j = 0; j < resolved->n_result_params; j++) {
            Usp__GetResp__ResolvedPathResult__ResultParamsEntry* param = resolved->result_params[j];
            amxc_var_t* amx_param = amxc_var_add_key(cstring_t, param_table, param->key, param->value);
            amxc_var_cast(amx_param, AMXC_VAR_ID_ANY);
        }
    }
    amxc_llist_append(get_resp_list, &entry->lit);

exit:
    return;
}


/***************** Start of public functions *******************/


int uspl_get_new(uspl_tx_t* usp_tx, const amxc_var_t* request) {
    int retval = -1;
    int nr_of_req = -1;
    int i = 0;
    Usp__Msg* msg = NULL;
    Usp__Get* get = NULL;
    const amxc_llist_t* paths = NULL;
    amxc_string_t msg_id;

    amxc_string_init(&msg_id, 0);

    when_null(usp_tx, exit);
    when_null(request, exit);

    amxc_string_setf(&msg_id, "%d", uspl_msghandler_next_msg_id());

    msg = CreateGet(amxc_string_get(&msg_id, 0));
    when_null(msg, exit);

    paths = amxc_var_constcast(amxc_llist_t, GET_ARG(request, "paths"));
    nr_of_req = amxc_llist_size(paths);
    get = msg->body->request->get;
    get->max_depth = GET_UINT32(request, "max_depth");
    get->param_paths = calloc(nr_of_req, sizeof(char*));
    when_null(get->param_paths, exit);
    get->n_param_paths = nr_of_req;

    amxc_llist_iterate(it, paths) {
        amxc_var_t* var = amxc_var_from_llist_it(it);
        const char* path = amxc_var_constcast(cstring_t, var);
        get->param_paths[i] = USP_STRDUP(path);
        i++;
    }

    retval = uspl_msghandler_pack_protobuf(msg, usp_tx);
    usp_tx->msg_id = USP_STRDUP(msg->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__GET;

exit:
    amxc_string_clean(&msg_id);
    uspl_msghandler_free_unpacked_message(msg);

    return retval;
}

int uspl_get_extract(uspl_rx_t* usp_rx, amxc_var_t* result) {
    int retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    Usp__Get* get = NULL;
    amxc_var_t* paths = NULL;

    when_null(usp_rx, exit);
    when_null(result, exit);

    // Exit if message is invalid or failed to parse
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_REQUEST, exit);
    when_null(usp_rx->msg->body->request, exit);
    when_true(usp_rx->msg->body->request->req_type_case != USP__REQUEST__REQ_TYPE_GET, exit);
    when_null(usp_rx->msg->body->request->get, exit);

    // Exit if there are no parameters to get
    retval = USP_ERR_INVALID_ARGUMENTS;
    get = usp_rx->msg->body->request->get;
    when_true(get->n_param_paths < 1, exit);
    when_null(get->param_paths, exit);

    amxc_var_set_type(result, AMXC_VAR_ID_HTABLE);
    paths = amxc_var_add_key(amxc_llist_t, result, "paths", NULL);
    for(size_t i = 0; i < get->n_param_paths; i++) {
        amxc_var_add(cstring_t, paths, get->param_paths[i]);
    }
    amxc_var_add_key(uint32_t, result, "max_depth", get->max_depth);

    retval = USP_ERR_OK;
exit:
    return retval;
}

int uspl_get_resp_new(uspl_tx_t* usp_tx, amxc_llist_t* resp_list, const char* msg_id) {
    Usp__Msg* resp = NULL;
    int retval = -1;

    when_null(usp_tx, exit);
    when_null(resp_list, exit);
    when_null(msg_id, exit);

    resp = CreateGetResp(msg_id);

    amxc_llist_for_each(it, resp_list) {
        amxc_var_t* reply_data = amxc_llist_it_get_data(it, amxc_var_t, lit);
        retval = uspl_get_resp_process_response(resp, reply_data);
        when_failed(retval, exit);
    }

    retval = uspl_msghandler_pack_protobuf(resp, usp_tx);
    usp_tx->msg_id = USP_STRDUP(resp->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__GET_RESP;

exit:
    uspl_msghandler_free_unpacked_message(resp);

    return retval;
}

int uspl_get_resp_extract(uspl_rx_t* usp_rx, amxc_llist_t* resp_list) {
    int retval = USP_ERR_INTERNAL_ERROR;
    Usp__GetResp* resp = NULL;

    when_null(usp_rx, exit);
    when_null(resp_list, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_RESPONSE, exit);
    when_null(usp_rx->msg->body->response, exit);
    when_true(usp_rx->msg->body->response->resp_type_case != USP__RESPONSE__RESP_TYPE_GET_RESP, exit);
    when_null(usp_rx->msg->body->response->get_resp, exit);

    resp = usp_rx->msg->body->response->get_resp;

    // Exit if there are no results in the response
    retval = USP_ERR_INVALID_ARGUMENTS;
    when_null(resp->req_path_results, exit);
    when_true(resp->n_req_path_results == 0, exit);

    for(size_t i = 0; i < resp->n_req_path_results; i++) {
        uspl_get_resp_append_result(resp->req_path_results[i], resp_list);
    }

    retval = USP_ERR_OK;
exit:
    return retval;
}