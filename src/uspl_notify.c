/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
   @file uspl_notify.c
   @brief
   Create and extract Notify requests and responses
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxj/amxj_variant.h>

#include <debug/sahtrace.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_error.h"
#include "usp/uspl_notify.h"
#include "usp/uspl_msghandler.h"

#include "uspl_msghandler_priv.h"

#include "bbf/usp_mem.h"
#include "bbf/handle_notify.h"

/**
   @brief
   Dynamically creates a generic Notify response object

   @param msg_id string containing the message id of the notify request
   @param subscription_id identifier string which was set by the controller to identify this notification (Device.LocalAgent.Subscription.{i}.ID)

   @return Pointer to a NotifyResp object
 */
static Usp__Msg* CreateNotifyResp(const char* msg_id, const char* subscription_id) {
    Usp__Msg* req;
    Usp__Header* header;
    Usp__Body* body;
    Usp__Response* response;
    Usp__NotifyResp* notify_resp;

    // Allocate and initialise memory to store the parts of the USP message
    req = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(req);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    response = USP_MALLOC(sizeof(Usp__Response));
    usp__response__init(response);

    notify_resp = USP_MALLOC(sizeof(Usp__NotifyResp));
    usp__notify_resp__init(notify_resp);

    // Connect the structures together
    req->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__NOTIFY_RESP;

    req->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    body->response = response;
    response->resp_type_case = USP__RESPONSE__RESP_TYPE_NOTIFY_RESP;
    response->notify_resp = notify_resp;

    notify_resp->subscription_id = USP_STRDUP(subscription_id);

    return req;
}

/**
   @brief
   Add a key-value pair to the event notification

   @note The event variant looks like this:
   ```
    event = {
        path = <string>
        event_name = <string>
        <param_key> = <param_value> (optional),
        ...
    }
   ```

   @param event the Notify Event request that will be extended
   @param key the key that will be added for this key-value pair
   @param entry the variant that will be added as value in (JSON) string format

   @return 0 in case of success, -1 in case of error
 */
static void uspl_notify_event_add_entry(Usp__Notify__Event* event, const char* key, amxc_var_t* entry) {
    Usp__Notify__Event__ParamsEntry* params_entry = NULL;
    int new_num = 0;
    const char* value = NULL;

    // Allocate memory to store the parameter entry
    params_entry = USP_MALLOC(sizeof(Usp__Notify__Event__ParamsEntry));
    usp__notify__event__params_entry__init(params_entry);

    // Increase the size of the vector containing pointers to the map entries
    new_num = event->n_params + 1;
    event->params = USP_REALLOC(event->params, new_num * sizeof(void*));
    event->n_params = new_num;
    event->params[new_num - 1] = params_entry;

    // Add composite parameters in JSON string format
    if((amxc_var_type_of(entry) == AMXC_VAR_ID_HTABLE) ||
       (amxc_var_type_of(entry) == AMXC_VAR_ID_LIST)) {
        amxc_var_cast(entry, AMXC_VAR_ID_JSON);
        value = amxc_var_constcast(jstring_t, entry);
    } else {
        amxc_var_cast(entry, AMXC_VAR_ID_CSTRING);
        value = amxc_var_constcast(cstring_t, entry);
    }

    params_entry->key = USP_STRDUP(key);
    params_entry->value = USP_STRDUP(value);
}

/**
   @brief
   Create an event notify request

   @note The event variant looks like this:
   ```
    event = {
        path = <string>
        event_name = <string>
        <param_key> = <param_value> (optional),
        ...
    }
   ```

   @param msg the Notify request that will be extended
   @param request the variant containing the request information

   @return 0 in case of success, -1 in case of error
 */
static int uspl_notify_event(Usp__Notify* notify, const amxc_var_t* request) {
    int retval = -1;
    const amxc_htable_t* event_table = NULL;
    Usp__Notify__Event* event = NULL;

    event = USP_MALLOC(sizeof(Usp__Notify__Event));
    usp__notify__event__init(event);
    notify->event = event;

    event_table = amxc_var_constcast(amxc_htable_t, GET_ARG(request, "event"));
    when_null(event_table, exit);

    amxc_htable_iterate(it, event_table) {
        amxc_var_t* entry = amxc_var_from_htable_it(it);
        const char* key = amxc_htable_it_get_key(it);

        if(strcmp(key, "path") == 0) {
            event->obj_path = USP_STRDUP(amxc_var_constcast(cstring_t, entry));
        } else if(strcmp(key, "event_name") == 0) {
            event->event_name = USP_STRDUP(amxc_var_constcast(cstring_t, entry));
        } else {
            uspl_notify_event_add_entry(event, key, entry);
        }
    }

    retval = 0;
exit:
    return retval;
}

/**
   @brief
   Create a value change notify request

   @note The value change variant looks like this:
   ```
    value_change {
        param_path = <string>
        param_value = <string
    }
   ```

   @param msg the Notify request that will be extended
   @param request the variant containing the request information

   @return 0 in case of success, -1 in case of error
 */
static int uspl_notify_value_change(Usp__Notify* notify, const amxc_var_t* request) {
    int retval = -1;
    Usp__Notify__ValueChange* val_change = NULL;
    amxc_var_t* vc_var = NULL;
    const char* param_path = NULL;
    const char* param_value = NULL;

    val_change = USP_MALLOC(sizeof(Usp__Notify__ValueChange));
    usp__notify__value_change__init(val_change);
    notify->value_change = val_change;

    vc_var = GET_ARG(request, "value_change");
    when_null(vc_var, exit);

    param_path = GET_CHAR(vc_var, "param_path");
    when_null(param_path, exit);

    param_value = GET_CHAR(vc_var, "param_value");
    when_null(param_value, exit);

    val_change->param_path = USP_STRDUP(param_path);
    val_change->param_value = USP_STRDUP(param_value);

    retval = 0;
exit:
    return retval;
}

/**
   @brief
   Create an object creation notify request

   @note The object creation variant looks like this:
   ```
    obj_creation {
        keys = {
            <key> = <value>,
            ...
        },
        path = <string>
    }
   ```

   @param msg the Notify request that will be extended
   @param request the variant containing the request information

   @return 0 in case of success, -1 in case of error
 */
static int uspl_notify_obj_creation(Usp__Notify* notify, const amxc_var_t* request) {
    int retval = -1;
    int i = 0;
    Usp__Notify__ObjectCreation* obj_creation = NULL;
    amxc_var_t* inst_added = NULL;
    const amxc_htable_t* keys = NULL;
    const char* path = NULL;
    size_t n_keys = 0;

    obj_creation = USP_MALLOC(sizeof(Usp__Notify__ObjectCreation));
    usp__notify__object_creation__init(obj_creation);
    notify->obj_creation = obj_creation;

    inst_added = GET_ARG(request, "obj_creation");
    when_null(inst_added, exit);

    path = GET_CHAR(inst_added, "path");
    when_null(path, exit);

    obj_creation->obj_path = USP_STRDUP(path);
    retval = 0;

    keys = amxc_var_constcast(amxc_htable_t, GET_ARG(inst_added, "keys"));
    when_null(keys, exit);

    n_keys = amxc_htable_size(keys);
    obj_creation->n_unique_keys = n_keys;
    obj_creation->unique_keys = USP_MALLOC(n_keys * sizeof(void*));

    amxc_htable_iterate(it, keys) {
        Usp__Notify__ObjectCreation__UniqueKeysEntry* unique_key = NULL;
        amxc_var_t* var = amxc_var_from_htable_it(it);

        unique_key = USP_MALLOC(sizeof(Usp__Notify__ObjectCreation__UniqueKeysEntry));
        usp__notify__object_creation__unique_keys_entry__init(unique_key);
        obj_creation->unique_keys[i] = unique_key;

        amxc_var_cast(var, AMXC_VAR_ID_CSTRING);
        unique_key->key = USP_STRDUP(amxc_htable_it_get_key(it));
        unique_key->value = USP_STRDUP(amxc_var_constcast(cstring_t, var));
        i++;
    }

exit:
    return retval;
}

/**
   @brief
   Create an object deletion notify request

   @note The object deletion variant looks like this:
   ```
    obj_deletion {
        path = <string>
    }
   ```

   @param msg the Notify request that will be extended
   @param request the variant containing the request information

   @return 0 in case of success, -1 in case of error
 */
static int uspl_notify_obj_deletion(Usp__Notify* notify, const amxc_var_t* request) {
    int retval = -1;
    Usp__Notify__ObjectDeletion* obj_deletion = NULL;
    const char* path = NULL;
    amxc_var_t* obj_deleted = NULL;

    obj_deletion = USP_MALLOC(sizeof(Usp__Notify__ObjectDeletion));
    usp__notify__object_deletion__init(obj_deletion);
    notify->obj_deletion = obj_deletion;

    obj_deleted = GET_ARG(request, "obj_deletion");
    when_null(obj_deleted, exit);

    path = GET_CHAR(obj_deleted, "path");
    when_null(path, exit);

    obj_deletion->obj_path = USP_STRDUP(path);

    retval = 0;
exit:
    return retval;
}

/**
   @brief
   Extract the output arguments from the passed variant and add them to the Notify request

   @param oper_complete part of the Notify request representing the OperationComplete section
   @param oper_var the variant containing the oper_complete information

   @return 0 in case of success, -1 in case of error
 */
static int uspl_notify_oper_complete_success(Usp__Notify__OperationComplete* oper_complete,
                                             const amxc_var_t* oper_var) {
    int i = 0;
    Usp__Notify__OperationComplete__OutputArgs* req_output_args = NULL;
    const amxc_htable_t* out_args = NULL;
    size_t n_args = 0;

    out_args = amxc_var_constcast(amxc_htable_t, GET_ARG(oper_var, "output_args"));
    n_args = amxc_htable_size(out_args);
    req_output_args = USP_MALLOC(sizeof(Usp__Notify__OperationComplete__OutputArgs));
    usp__notify__operation_complete__output_args__init(req_output_args);

    oper_complete->operation_resp_case = USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_REQ_OUTPUT_ARGS;
    oper_complete->req_output_args = req_output_args;
    oper_complete->req_output_args->n_output_args = n_args;
    oper_complete->req_output_args->output_args = USP_MALLOC(n_args * sizeof(void*));

    amxc_htable_iterate(it, out_args) {
        Usp__Notify__OperationComplete__OutputArgs__OutputArgsEntry* entry = NULL;
        amxc_var_t* var = amxc_var_from_htable_it(it);
        const char* key = amxc_var_key(var);
        const char* value = NULL;

        entry = USP_MALLOC(sizeof(Usp__Notify__OperationComplete__OutputArgs__OutputArgsEntry));
        usp__notify__operation_complete__output_args__output_args_entry__init(entry);
        req_output_args->output_args[i] = entry;

        // Add composite parameters in JSON string format
        if((amxc_var_type_of(var) == AMXC_VAR_ID_HTABLE) ||
           (amxc_var_type_of(var) == AMXC_VAR_ID_LIST)) {
            amxc_var_cast(var, AMXC_VAR_ID_JSON);
            value = amxc_var_constcast(jstring_t, var);
        } else {
            amxc_var_cast(var, AMXC_VAR_ID_CSTRING);
            value = amxc_var_constcast(cstring_t, var);
        }

        entry->key = USP_STRDUP(key);
        entry->value = USP_STRDUP(value);
        i++;
    }

    return 0;
}

/**
   @brief
   Extract the error code and message from the passed variant and add them to the Notify request

   @param oper_complete part of the Notify request representing the OperationComplete section
   @param oper_var the variant containing the oper_complete information

   @return 0 in case of success, -1 in case of error
 */
static int uspl_notify_oper_complete_failure(Usp__Notify__OperationComplete* oper_complete,
                                             const amxc_var_t* oper_var) {
    int retval = -1;
    Usp__Notify__OperationComplete__CommandFailure* cmd_failure = NULL;
    amxc_var_t* fail_var = NULL;
    const char* err_msg = NULL;

    fail_var = GET_ARG(oper_var, "cmd_failure");
    when_null(fail_var, exit);

    cmd_failure = USP_MALLOC(sizeof(Usp__Notify__OperationComplete__CommandFailure));
    usp__notify__operation_complete__command_failure__init(cmd_failure);

    oper_complete->operation_resp_case = USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_CMD_FAILURE;
    oper_complete->cmd_failure = cmd_failure;
    oper_complete->cmd_failure->err_code = GET_UINT32(fail_var, "err_code");

    err_msg = GET_CHAR(fail_var, "err_msg");
    when_null(err_msg, exit);

    oper_complete->cmd_failure->err_msg = USP_STRDUP(err_msg);

    retval = 0;
exit:
    return retval;
}

/**
   @brief
   Create an operation complete notify request

   @note The operation complete variant looks like this:
   ```
    oper_complete {
        path = <string>
        cmd_name = <string>
        cmd_key = <string>
        operation_case = <uint32_t>
        output_args (optional) = {
            <key> = <value>,
            ...
        }
        cmd_failure (optional) = {
            err_code = <uint32_t>
            err_msg = <string>
        }
    }
   ```

   @param msg the Notify request that will be extended
   @param request the variant containing the request information

   @return 0 in case of success, -1 in case of error
 */
static int uspl_notify_oper_complete(Usp__Notify* notify, const amxc_var_t* request) {
    int retval = -1;
    Usp__Notify__OperationComplete* oper_complete = NULL;
    uint32_t oper_case = USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP__NOT_SET;
    const char* obj_path = NULL;
    const char* cmd_name = NULL;
    const char* cmd_key = NULL;
    amxc_var_t* oper_var = NULL;

    oper_complete = USP_MALLOC(sizeof(Usp__Notify__OperationComplete));
    usp__notify__operation_complete__init(oper_complete);
    notify->oper_complete = oper_complete;

    oper_var = GET_ARG(request, "oper_complete");
    when_null(oper_var, exit);

    obj_path = GET_CHAR(oper_var, "path");
    when_null(obj_path, exit);

    cmd_name = GET_CHAR(oper_var, "cmd_name");
    when_null(cmd_name, exit);

    cmd_key = GET_CHAR(oper_var, "cmd_key");
    when_null(cmd_key, exit);

    oper_complete->obj_path = USP_STRDUP(obj_path);
    oper_complete->command_name = USP_STRDUP(cmd_name);
    oper_complete->command_key = USP_STRDUP(cmd_key);

    oper_case = GET_UINT32(oper_var, "operation_case");
    switch(oper_case) {
    case USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_REQ_OUTPUT_ARGS:
        retval = uspl_notify_oper_complete_success(oper_complete, oper_var);
        break;
    case USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_CMD_FAILURE:
        retval = uspl_notify_oper_complete_failure(oper_complete, oper_var);
        break;
    default:
        break;
    }

exit:
    return retval;
}

/**
   @brief
   Create an on board request notify request

   @note The on board request variant looks like this:
   ```
    on_board_req {
        oui = <string>
        product_class = <string>
        serial_number = <string>
        agent_supported_protocol_version = <string>
    }
   ```

   @param msg the Notify request that will be extended
   @param request the variant containing the request information

   @return 0 in case of success, -1 in case of error
 */
static int uspl_notify_on_board_req(Usp__Notify* notify, const amxc_var_t* request) {
    int retval = -1;
    Usp__Notify__OnBoardRequest* on_board_req = NULL;
    const char* oui = NULL;
    const char* product_class = NULL;
    const char* serial_number = NULL;
    const char* agent_sup_prot = NULL;
    amxc_var_t* obr_var = NULL;

    on_board_req = USP_MALLOC(sizeof(Usp__Notify__OnBoardRequest));
    usp__notify__on_board_request__init(on_board_req);
    notify->on_board_req = on_board_req;

    obr_var = GET_ARG(request, "on_board_req");
    when_null(obr_var, exit);

    oui = GET_CHAR(obr_var, "oui");
    when_null(oui, exit);

    product_class = GET_CHAR(obr_var, "product_class");
    when_null(product_class, exit);

    serial_number = GET_CHAR(obr_var, "serial_number");
    when_null(serial_number, exit);

    agent_sup_prot = GET_CHAR(obr_var, "agent_supported_protocol_version");
    when_null(agent_sup_prot, exit);

    on_board_req->oui = USP_STRDUP(oui);
    on_board_req->product_class = USP_STRDUP(product_class);
    on_board_req->serial_number = USP_STRDUP(serial_number);
    on_board_req->agent_supported_protocol_versions = USP_STRDUP(agent_sup_prot);

    retval = 0;
exit:
    return retval;
}

/**
   @brief
   Extract the params from the passed variant and add them to the Notify request

   @param any part of the Notify request representing the Any notification
   @param params the variant containing the event parameters

   @return 0 in case of success, -1 in case of error
 */
static int uspl_notify_amx_params(Usp__Notify__AmxNotification* amx_notification,
                                  const amxc_var_t* params) {
    int i = 0;
    const amxc_htable_t* params_table = NULL;
    size_t n_params = 0;

    params_table = amxc_var_constcast(amxc_htable_t, params);
    n_params = amxc_htable_size(params_table);
    amx_notification->n_params = n_params;
    amx_notification->params = USP_MALLOC(n_params * sizeof(void*));

    amxc_htable_iterate(it, params_table) {
        Usp__Notify__AmxNotification__ParamsEntry* entry = NULL;
        amxc_var_t* var = amxc_var_from_htable_it(it);
        const char* key = amxc_var_key(var);
        const char* value = NULL;

        entry = USP_MALLOC(sizeof(Usp__Notify__AmxNotification__ParamsEntry));
        usp__notify__amx_notification__params_entry__init(entry);
        amx_notification->params[i] = entry;

        // Add composite parameters in JSON string format
        if((amxc_var_type_of(var) == AMXC_VAR_ID_HTABLE) ||
           (amxc_var_type_of(var) == AMXC_VAR_ID_LIST)) {
            amxc_var_cast(var, AMXC_VAR_ID_JSON);
            value = amxc_var_constcast(jstring_t, var);
        } else {
            amxc_var_cast(var, AMXC_VAR_ID_CSTRING);
            value = amxc_var_constcast(cstring_t, var);
        }

        entry->key = USP_STRDUP(key);
        entry->value = USP_STRDUP(value);
        i++;
    }

    return 0;
}

/**
   @brief
   Create a notify request of type any

   @note The request variant looks like this:
   ```
    amx_notification {
        <key> = <value>,
        ...
    }
   ```

   @param msg the Notify request that will be extended
   @param request the variant containing the request information

   @return 0 in case of success, -1 in case of error
 */
static int uspl_notify_amx(Usp__Notify* notify, const amxc_var_t* request) {
    int retval = -1;
    Usp__Notify__AmxNotification* amx_notification = NULL;
    amxc_var_t* amx_var = NULL;

    amx_notification = USP_MALLOC(sizeof(Usp__Notify__AmxNotification));
    usp__notify__amx_notification__init(amx_notification);
    notify->amx_notification = amx_notification;

    amx_var = GET_ARG(request, "amx_notification");
    when_null(amx_var, exit);

    uspl_notify_amx_params(amx_notification, amx_var);

    retval = 0;
exit:
    return retval;
}

// Functions for extracting a notify message to a variant

static int uspl_notify_extract_event(Usp__Notify* notify, amxc_var_t* result) {
    amxc_var_t* sub_var = NULL;

    sub_var = amxc_var_add_key(amxc_htable_t, result, "event", NULL);
    amxc_var_add_key(cstring_t, sub_var, "path", notify->event->obj_path);
    amxc_var_add_key(cstring_t, sub_var, "event_name", notify->event->event_name);

    for(size_t i = 0; i < notify->event->n_params; i++) {
        Usp__Notify__Event__ParamsEntry* entry = notify->event->params[i];

        if(*entry->value == 0) {
            amxc_var_add_key(cstring_t, sub_var, entry->key, "");
        } else {
            amxc_var_add_key(cstring_t, sub_var, entry->key, entry->value);
        }
    }

    return 0;
}

static int uspl_notify_extract_value_change(Usp__Notify* notify, amxc_var_t* result) {
    amxc_var_t* sub_var = NULL;

    sub_var = amxc_var_add_key(amxc_htable_t, result, "value_change", NULL);
    amxc_var_add_key(cstring_t, sub_var, "param_path", notify->value_change->param_path);
    amxc_var_add_key(cstring_t, sub_var, "param_value", notify->value_change->param_value);

    return 0;
}

static int uspl_notify_extract_obj_creation(Usp__Notify* notify, amxc_var_t* result) {
    amxc_var_t* sub_var = NULL;
    amxc_var_t* keys = NULL;

    sub_var = amxc_var_add_key(amxc_htable_t, result, "obj_creation", NULL);
    amxc_var_add_key(cstring_t, sub_var, "path", notify->obj_creation->obj_path);

    if(notify->obj_creation->n_unique_keys <= 0) {
        goto exit;
    }

    keys = amxc_var_add_key(amxc_htable_t, sub_var, "keys", NULL);

    for(size_t i = 0; i < notify->obj_creation->n_unique_keys; i++) {
        Usp__Notify__ObjectCreation__UniqueKeysEntry* entry = notify->obj_creation->unique_keys[i];

        if(*entry->value == 0) {
            amxc_var_add_key(cstring_t, keys, entry->key, "");
        } else {
            amxc_var_add_key(cstring_t, keys, entry->key, entry->value);
        }
    }

exit:
    return 0;
}

static int uspl_notify_extract_obj_deletion(Usp__Notify* notify, amxc_var_t* result) {
    amxc_var_t* sub_var = NULL;

    sub_var = amxc_var_add_key(amxc_htable_t, result, "obj_deletion", NULL);
    amxc_var_add_key(cstring_t, sub_var, "path", notify->obj_deletion->obj_path);

    return 0;
}

static int uspl_notify_extract_oper_complete_success(Usp__Notify* notify,
                                                     amxc_var_t* oper_var) {
    Usp__Notify__OperationComplete__OutputArgs* req_out_args = NULL;
    req_out_args = notify->oper_complete->req_output_args;
    amxc_var_t* out_args_var = NULL;

    out_args_var = amxc_var_add_key(amxc_htable_t, oper_var, "output_args", NULL);

    for(size_t i = 0; i < req_out_args->n_output_args; i++) {
        Usp__Notify__OperationComplete__OutputArgs__OutputArgsEntry* entry = req_out_args->output_args[i];

        amxc_var_t* dst_param = amxc_var_add_key(cstring_t, out_args_var, entry->key, "");
        if(amxc_var_set(jstring_t, dst_param, entry->value) != 0) {
            amxc_var_set(cstring_t, dst_param, entry->value);
        }
        amxc_var_cast(dst_param, AMXC_VAR_ID_ANY);
    }

    return 0;
}

static int uspl_notify_extract_oper_complete_failure(Usp__Notify* notify,
                                                     amxc_var_t* oper_var) {
    Usp__Notify__OperationComplete__CommandFailure* cmd_failure = NULL;
    cmd_failure = notify->oper_complete->cmd_failure;
    amxc_var_t* fail_var = NULL;

    fail_var = amxc_var_add_key(amxc_htable_t, oper_var, "cmd_failure", NULL);
    amxc_var_add_key(uint32_t, fail_var, "err_code", cmd_failure->err_code);
    amxc_var_add_key(cstring_t, fail_var, "err_msg", cmd_failure->err_msg);

    return 0;
}

static int uspl_notify_extract_oper_complete(Usp__Notify* notify, amxc_var_t* result) {
    amxc_var_t* sub_var = NULL;
    uint32_t oper_case = USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP__NOT_SET;

    sub_var = amxc_var_add_key(amxc_htable_t, result, "oper_complete", NULL);
    amxc_var_add_key(cstring_t, sub_var, "path", notify->oper_complete->obj_path);
    amxc_var_add_key(cstring_t, sub_var, "cmd_name", notify->oper_complete->command_name);
    amxc_var_add_key(cstring_t, sub_var, "cmd_key", notify->oper_complete->command_key);

    oper_case = notify->oper_complete->operation_resp_case;
    amxc_var_add_key(uint32_t, sub_var, "operation_case", oper_case);

    switch(oper_case) {
    case (USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_REQ_OUTPUT_ARGS):
        uspl_notify_extract_oper_complete_success(notify, sub_var);
        break;
    case (USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_CMD_FAILURE):
        uspl_notify_extract_oper_complete_failure(notify, sub_var);
        break;
    default:
        break;
    }

    return 0;
}

static int uspl_notify_extract_on_board_req(Usp__Notify* notify, amxc_var_t* result) {
    amxc_var_t* sub_var = NULL;

    sub_var = amxc_var_add_key(amxc_htable_t, result, "on_board_req", NULL);
    amxc_var_add_key(cstring_t, sub_var, "oui", notify->on_board_req->oui);
    amxc_var_add_key(cstring_t, sub_var, "product_class", notify->on_board_req->product_class);
    amxc_var_add_key(cstring_t, sub_var, "serial_number", notify->on_board_req->serial_number);
    amxc_var_add_key(cstring_t, sub_var, "agent_supported_protocol_version",
                     notify->on_board_req->agent_supported_protocol_versions);

    return 0;
}

static int uspl_notify_extract_amx(Usp__Notify* notify, amxc_var_t* result) {
    amxc_var_t* amx_notification = amxc_var_add_key(amxc_htable_t, result, "amx_notification", NULL);

    for(size_t i = 0; i < notify->amx_notification->n_params; i++) {
        Usp__Notify__AmxNotification__ParamsEntry* entry = notify->amx_notification->params[i];

        amxc_var_t* dst_param = amxc_var_add_key(cstring_t, amx_notification, entry->key, "");
        if(amxc_var_set(jstring_t, dst_param, entry->value) != 0) {
            amxc_var_set(cstring_t, dst_param, entry->value);
        }
        amxc_var_cast(dst_param, AMXC_VAR_ID_ANY);
    }

    return 0;
}

static int uspl_notify_extract_switch(Usp__Notify* notify,
                                      Usp__Notify__NotificationCase notification_case,
                                      amxc_var_t* result) {
    int retval = USP_ERR_INVALID_ARGUMENTS;

    switch(notification_case) {
    case USP__NOTIFY__NOTIFICATION_EVENT:
        retval = uspl_notify_extract_event(notify, result);
        break;
    case USP__NOTIFY__NOTIFICATION_VALUE_CHANGE:
        retval = uspl_notify_extract_value_change(notify, result);
        break;
    case USP__NOTIFY__NOTIFICATION_OBJ_CREATION:
        retval = uspl_notify_extract_obj_creation(notify, result);
        break;
    case USP__NOTIFY__NOTIFICATION_OBJ_DELETION:
        retval = uspl_notify_extract_obj_deletion(notify, result);
        break;
    case USP__NOTIFY__NOTIFICATION_OPER_COMPLETE:
        retval = uspl_notify_extract_oper_complete(notify, result);
        break;
    case USP__NOTIFY__NOTIFICATION_ON_BOARD_REQ:
        retval = uspl_notify_extract_on_board_req(notify, result);
        break;
    case USP__NOTIFY__NOTIFICATION_AMX_NOTIFICATION:
        retval = uspl_notify_extract_amx(notify, result);
        break;
    default:
        break;
    }
    return retval;
}

/*********************** Start of public functions ****************************/

int uspl_notify_new(uspl_tx_t* usp_tx, const amxc_var_t* request) {
    int retval = -1;
    Usp__Msg* msg = NULL;
    const char* subscription_id = NULL;
    bool send_resp = false;
    Usp__Notify__NotificationCase notification_case = USP__NOTIFY__NOTIFICATION__NOT_SET;
    Usp__Notify* notify = NULL;
    amxc_string_t msg_id;

    amxc_string_init(&msg_id, 0);

    when_null(usp_tx, exit);
    when_null(request, exit);

    amxc_string_setf(&msg_id, "%d", uspl_msghandler_next_msg_id());

    subscription_id = GET_CHAR(request, "subscription_id");
    when_null(subscription_id, exit);
    send_resp = GET_BOOL(request, "send_resp");
    notification_case = GET_UINT32(request, "notification_case");

    msg = CreateNotify(amxc_string_get(&msg_id, 0), subscription_id, send_resp, notification_case);
    notify = msg->body->request->notify;

    switch(notification_case) {
    case USP__NOTIFY__NOTIFICATION_EVENT:
        retval = uspl_notify_event(notify, request);
        break;
    case USP__NOTIFY__NOTIFICATION_VALUE_CHANGE:
        retval = uspl_notify_value_change(notify, request);
        break;
    case USP__NOTIFY__NOTIFICATION_OBJ_CREATION:
        retval = uspl_notify_obj_creation(notify, request);
        break;
    case USP__NOTIFY__NOTIFICATION_OBJ_DELETION:
        retval = uspl_notify_obj_deletion(notify, request);
        break;
    case USP__NOTIFY__NOTIFICATION_OPER_COMPLETE:
        retval = uspl_notify_oper_complete(notify, request);
        break;
    case USP__NOTIFY__NOTIFICATION_ON_BOARD_REQ:
        retval = uspl_notify_on_board_req(notify, request);
        break;
    case USP__NOTIFY__NOTIFICATION_AMX_NOTIFICATION:
        retval = uspl_notify_amx(notify, request);
        break;
    default:
        retval = -1;
        break;
    }
    when_failed(retval, exit);

    retval = uspl_msghandler_pack_protobuf(msg, usp_tx);
    usp_tx->msg_id = USP_STRDUP(msg->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__NOTIFY;

exit:
    amxc_string_clean(&msg_id);
    uspl_msghandler_free_unpacked_message(msg);

    return retval;
}

int uspl_notify_extract(uspl_rx_t* usp_rx, amxc_var_t* result) {
    int retval = USP_ERR_INTERNAL_ERROR;
    Usp__Notify* notify = NULL;
    Usp__Notify__NotificationCase notification_case = USP__NOTIFY__NOTIFICATION__NOT_SET;

    when_null(usp_rx, exit);
    when_null(result, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_REQUEST, exit);
    when_null(usp_rx->msg->body->request, exit);
    when_true(usp_rx->msg->body->request->req_type_case != USP__REQUEST__REQ_TYPE_NOTIFY, exit);
    when_null(usp_rx->msg->body->request->notify, exit);

    notify = usp_rx->msg->body->request->notify;
    notification_case = notify->notification_case;

    amxc_var_set_type(result, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, result, "subscription_id", notify->subscription_id);
    amxc_var_add_key(bool, result, "send_resp", notify->send_resp);
    amxc_var_add_key(uint32_t, result, "notification_case", notification_case);

    retval = uspl_notify_extract_switch(notify, notification_case, result);
    when_failed(retval, exit);

    retval = USP_ERR_OK;
exit:
    return retval;
}

int uspl_notify_resp_new(uspl_tx_t* usp_tx, amxc_llist_t* resp_list, const char* msg_id) {
    int retval = -1;
    Usp__Msg* resp = NULL;
    amxc_var_t* notify_var = NULL;
    const char* subs_id = NULL;

    when_null(usp_tx, exit);
    when_null(resp_list, exit);
    when_null(msg_id, exit);

    notify_var = amxc_var_from_llist_it(amxc_llist_get_first(resp_list));
    when_null(notify_var, exit);

    subs_id = amxc_var_constcast(cstring_t, notify_var);
    when_null(subs_id, exit);

    resp = CreateNotifyResp(msg_id, subs_id);

    retval = uspl_msghandler_pack_protobuf(resp, usp_tx);
    usp_tx->msg_id = USP_STRDUP(resp->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__NOTIFY_RESP;

exit:
    uspl_msghandler_free_unpacked_message(resp);

    return retval;
}

int uspl_notify_resp_extract(uspl_rx_t* usp_rx, amxc_llist_t* resp_list) {
    int retval = USP_ERR_INTERNAL_ERROR;
    amxc_var_t* resp_var = NULL;
    char* subs_id = NULL;

    when_null(usp_rx, exit);
    when_null(resp_list, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_RESPONSE, exit);
    when_null(usp_rx->msg->body->response, exit);
    when_true(usp_rx->msg->body->response->resp_type_case != USP__RESPONSE__RESP_TYPE_NOTIFY_RESP, exit);
    when_null(usp_rx->msg->body->response->notify_resp, exit);

    retval = USP_ERR_INVALID_ARGUMENTS;
    subs_id = usp_rx->msg->body->response->notify_resp->subscription_id;
    when_null(subs_id, exit);

    amxc_var_new(&resp_var);
    amxc_var_set_type(resp_var, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, resp_var, subs_id);
    amxc_llist_append(resp_list, &resp_var->lit);

    retval = USP_ERR_OK;
exit:
    return retval;
}