/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
   @file uspl_get_instances.c
   @brief
   Create and extract GetInstances requests and responses
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>

#include <debug/sahtrace.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_error.h"
#include "usp/uspl_get_instances.h"
#include "usp/uspl_msghandler.h"

#include "uspl_msghandler_priv.h"
#include "uspl_textutils.h"
#include "uspl_private.h"

#include "bbf/usp_mem.h"
#include "bbf/handle_get_instances.h"

/**
   @brief
   Dynamically creates a GetInstances request object

   @note The object should be deleted using usp__msg__free_unpacked()

   @param msg_id string containing the message id of the get_instances request

   @return Pointer to a GetInstances request object
 */
static Usp__Msg* CreateGetInstances(const char* msg_id) {
    Usp__Msg* msg;
    Usp__Header* header;
    Usp__Body* body;
    Usp__Request* request;
    Usp__GetInstances* get_inst;

    // Allocate and initialise memory to store the parts of the USP message
    msg = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(msg);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    request = USP_MALLOC(sizeof(Usp__Request));
    usp__request__init(request);

    get_inst = USP_MALLOC(sizeof(Usp__GetInstances));
    usp__get_instances__init(get_inst);

    // Connect the structures together
    msg->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__GET_INSTANCES;

    msg->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    body->request = request;
    request->req_type_case = USP__REQUEST__REQ_TYPE_GET_INSTANCES;
    request->get_instances = get_inst;

    get_inst->first_level_only = false;
    get_inst->n_obj_paths = 0;
    get_inst->obj_paths = NULL;

    return msg;
}

static void uspl_gi_populate_req(Usp__GetInstances* get_inst, amxc_var_t* obj_paths) {
    const amxc_llist_t* path_list = amxc_var_constcast(amxc_llist_t, obj_paths);
    size_t num_paths = amxc_llist_size(path_list);
    int i = 0;

    get_inst->obj_paths = calloc(num_paths, sizeof(void*));
    get_inst->n_obj_paths = num_paths;

    amxc_llist_iterate(it, path_list) {
        amxc_var_t* var = amxc_var_from_llist_it(it);
        const char* path = amxc_var_constcast(cstring_t, var);
        get_inst->obj_paths[i] = USP_STRDUP(path);
        i++;
    }
}

static void AddRequestedPathResult_CurrInstance(Usp__GetInstancesResp__RequestedPathResult* req_path_res, const char* inst_path, amxc_var_t* unique_keys) {
    Usp__GetInstancesResp__CurrInstance* cur_inst = NULL;
    int new_num = 0;    // new number of entries in the curr_insts array

    // Allocate memory to store the CurrInstance object
    cur_inst = USP_MALLOC(sizeof(Usp__GetInstancesResp__CurrInstance));
    usp__get_instances_resp__curr_instance__init(cur_inst);

    // Increase the size of the vector
    new_num = req_path_res->n_curr_insts + 1;
    req_path_res->curr_insts = USP_REALLOC(req_path_res->curr_insts, new_num * sizeof(void*));
    req_path_res->n_curr_insts = new_num;
    req_path_res->curr_insts[new_num - 1] = cur_inst;

    cur_inst->instantiated_obj_path = USP_STRDUP(inst_path);

    // Fill in the UniqueKeys array
    amxc_var_for_each(entry, unique_keys) {
        Usp__GetInstancesResp__CurrInstance__UniqueKeysEntry* unique_key = NULL;
        const char* key = amxc_var_key(entry);
        char* value = amxc_var_dyncast(cstring_t, entry);

        // Increase size of uniqe key list
        new_num = cur_inst->n_unique_keys + 1;
        cur_inst->unique_keys = USP_REALLOC(cur_inst->unique_keys, new_num * sizeof(void*));
        cur_inst->n_unique_keys = new_num;

        // Allocate memory to store this unique key object
        unique_key = USP_MALLOC(sizeof(Usp__GetInstancesResp__CurrInstance__UniqueKeysEntry));
        usp__get_instances_resp__curr_instance__unique_keys_entry__init(unique_key);

        // Fill in this unique key
        unique_key->key = USP_STRDUP(key);
        unique_key->value = value;

        // Attach this unique key in the unique_keys array (of the CurrInstance object)
        cur_inst->unique_keys[new_num - 1] = unique_key;
    }
}

static int uspl_gi_resp_populate_resp(Usp__GetInstancesResp* resp, amxc_var_t* result) {
    int retval = -1;
    Usp__GetInstancesResp__RequestedPathResult* req_path_res = NULL;
    const char* requested_path = GET_CHAR(result, "requested_path");
    const char* err_msg = GET_CHAR(result, "err_msg");
    uint32_t err_code = GET_UINT32(result, "err_code");
    amxc_var_t* curr_insts = GET_ARG(result, "curr_insts");

    when_str_empty(requested_path, exit);

    req_path_res = AddGetInstances_RequestedPathResult(resp, requested_path, err_code, err_msg);

    amxc_var_for_each(inst, curr_insts) {
        const char* inst_path = GET_CHAR(inst, "inst_path");
        amxc_var_t* unique_keys = GET_ARG(inst, "unique_keys");
        AddRequestedPathResult_CurrInstance(req_path_res, inst_path, unique_keys);
    }

    retval = 0;
exit:
    return retval;
}

static void uspl_gi_resp_add_inst(Usp__GetInstancesResp__CurrInstance* curr_inst,
                                  amxc_var_t* curr_inst_list) {
    amxc_var_t* unique_keys = NULL;
    amxc_var_t* entry = amxc_var_add(amxc_htable_t, curr_inst_list, NULL);
    amxc_var_add_key(cstring_t, entry, "inst_path", curr_inst->instantiated_obj_path);

    when_true(curr_inst->n_unique_keys == 0, exit);

    unique_keys = amxc_var_add_key(amxc_htable_t, entry, "unique_keys", NULL);

    for(size_t i = 0; i < curr_inst->n_unique_keys; i++) {
        amxc_var_add_key(cstring_t, unique_keys,
                         curr_inst->unique_keys[i]->key, curr_inst->unique_keys[i]->value);
    }

exit:
    return;
}

static void uspl_gi_resp_append_result(Usp__GetInstancesResp__RequestedPathResult* req_path_res,
                                       amxc_llist_t* resp_list) {
    amxc_var_t* result = NULL;
    amxc_var_t* curr_insts = NULL;

    amxc_var_new(&result);
    amxc_var_set_type(result, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, result, "requested_path", req_path_res->requested_path);
    amxc_var_add_key(cstring_t, result, "err_msg", req_path_res->err_msg);
    amxc_var_add_key(uint32_t, result, "err_code", req_path_res->err_code);

    when_true(req_path_res->n_curr_insts == 0, exit);

    curr_insts = amxc_var_add_key(amxc_llist_t, result, "curr_insts", NULL);
    for(size_t i = 0; i < req_path_res->n_curr_insts; i++) {
        uspl_gi_resp_add_inst(req_path_res->curr_insts[i], curr_insts);
    }

exit:
    amxc_llist_append(resp_list, &result->lit);
}


/***************** Start of public functions *******************/


int uspl_get_instances_new(uspl_tx_t* usp_tx, const amxc_var_t* request) {
    int retval = -1;
    Usp__Msg* msg = NULL;
    amxc_var_t* obj_paths = NULL;
    bool first_level = false;
    amxc_string_t msg_id;

    amxc_string_init(&msg_id, 0);

    when_null(usp_tx, exit);
    when_null(request, exit);

    amxc_string_setf(&msg_id, "%d", uspl_msghandler_next_msg_id());

    msg = CreateGetInstances(amxc_string_get(&msg_id, 0));

    obj_paths = GET_ARG(request, "obj_paths");
    when_null(obj_paths, exit);

    first_level = GET_BOOL(request, "first_level_only");
    msg->body->request->get_instances->first_level_only = first_level;

    uspl_gi_populate_req(msg->body->request->get_instances, obj_paths);

    retval = uspl_msghandler_pack_protobuf(msg, usp_tx);
    usp_tx->msg_id = USP_STRDUP(msg->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__GET_INSTANCES;

exit:
    amxc_string_clean(&msg_id);
    uspl_msghandler_free_unpacked_message(msg);

    return retval;
}

int uspl_get_instances_extract(uspl_rx_t* usp_rx, amxc_var_t* result) {
    int retval = USP_ERR_INTERNAL_ERROR;
    Usp__GetInstances* get_inst = NULL;
    amxc_var_t* paths = NULL;

    when_null(usp_rx, exit);
    when_null(result, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_REQUEST, exit);
    when_null(usp_rx->msg->body->request, exit);
    when_true(usp_rx->msg->body->request->req_type_case != USP__REQUEST__REQ_TYPE_GET_INSTANCES, exit);
    when_null(usp_rx->msg->body->request->get_instances, exit);

    // Exit if there are no parameters to get
    retval = USP_ERR_INVALID_ARGUMENTS;
    get_inst = usp_rx->msg->body->request->get_instances;
    when_true(get_inst->n_obj_paths < 1, exit);
    when_null(get_inst->obj_paths, exit);

    amxc_var_set_type(result, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, result, "first_level_only", get_inst->first_level_only);
    paths = amxc_var_add_key(amxc_llist_t, result, "obj_paths", NULL);
    for(size_t i = 0; i < get_inst->n_obj_paths; i++) {
        amxc_var_add(cstring_t, paths, get_inst->obj_paths[i]);
    }

    retval = USP_ERR_OK;
exit:
    return retval;
}

int uspl_get_instances_resp_new(uspl_tx_t* usp_tx, amxc_llist_t* resp_list, const char* msg_id) {
    Usp__Msg* resp = NULL;
    int retval = -1;

    when_null(usp_tx, exit);
    when_null(resp_list, exit);
    when_null(msg_id, exit);

    resp = CreateGetInstancesResp(msg_id);

    amxc_llist_for_each(it, resp_list) {
        amxc_var_t* result = amxc_llist_it_get_data(it, amxc_var_t, lit);
        retval = uspl_gi_resp_populate_resp(resp->body->response->get_instances_resp, result);
        when_failed(retval, exit);
    }

    retval = uspl_msghandler_pack_protobuf(resp, usp_tx);
    usp_tx->msg_id = USP_STRDUP(resp->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__GET_INSTANCES_RESP;

exit:
    uspl_msghandler_free_unpacked_message(resp);

    return retval;
}

int uspl_get_instances_resp_extract(uspl_rx_t* usp_rx, amxc_llist_t* resp_list) {
    int retval = USP_ERR_INTERNAL_ERROR;
    Usp__GetInstancesResp* resp = NULL;

    when_null(usp_rx, exit);
    when_null(resp_list, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_RESPONSE, exit);
    when_null(usp_rx->msg->body->response, exit);
    when_true(usp_rx->msg->body->response->resp_type_case != USP__RESPONSE__RESP_TYPE_GET_INSTANCES_RESP, exit);
    when_null(usp_rx->msg->body->response->get_instances_resp, exit);

    resp = usp_rx->msg->body->response->get_instances_resp;
    // Exit if there are no results in the response
    retval = USP_ERR_INVALID_ARGUMENTS;
    when_true(resp->n_req_path_results == 0, exit);
    when_true(resp->req_path_results == NULL, exit);

    for(size_t i = 0; i < resp->n_req_path_results; i++) {
        uspl_gi_resp_append_result(resp->req_path_results[i], resp_list);
    }

    retval = USP_ERR_OK;
exit:
    return retval;
}

int uspl_get_instances_amxd_status_to_usp_err(amxd_status_t status) {
    int retval = -1;

    switch(status) {
    case amxd_status_ok:
        retval = USP_ERR_OK;
        break;
    case amxd_status_not_supported:
        retval = USP_ERR_INVALID_PATH;
        break;
    case amxd_status_not_a_template:
        retval = USP_ERR_NOT_A_TABLE;
        break;
    // R-GIN.0 - Permission denied -> not instantiated
    case amxd_status_permission_denied:
    case amxd_status_not_instantiated:
        retval = USP_ERR_OBJECT_DOES_NOT_EXIST;
        break;
    default:
        retval = USP_ERR_OBJECT_DOES_NOT_EXIST;
        break;
    }

    return retval;
}
