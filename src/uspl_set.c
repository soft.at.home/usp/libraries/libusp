/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
   @file uspl_set.c
   @brief
   Handles the Set message, creating a SetResponse
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>

#include <debug/sahtrace.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_error.h"
#include "usp/uspl_set.h"

#include "uspl_msghandler_priv.h"
#include "uspl_private.h"

#include "bbf/usp_mem.h"
#include "bbf/handle_set.h"

/**
   @brief
   Dynamically creates an Set object

   @note The object is created without any updated_obj_results
   @note The object should be deleted using usp__msg__free_unpacked()

   @param msg_id string containing the message id of the set request, which initiated this response

   @return Pointer to a Set object
 */
static Usp__Msg* CreateSet(const char* msg_id) {
    Usp__Msg* msg;
    Usp__Header* header;
    Usp__Body* body;
    Usp__Request* request;
    Usp__Set* set;

    // Allocate and initialise memory to store the parts of the USP message
    msg = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(msg);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    request = USP_MALLOC(sizeof(Usp__Request));
    usp__request__init(request);

    set = USP_MALLOC(sizeof(Usp__Set));
    usp__set__init(set);

    // Connect the structures together
    msg->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__SET;

    msg->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    body->request = request;
    request->req_type_case = USP__REQUEST__REQ_TYPE_SET;
    request->set = set;
    set->n_update_objs = 0;    // Start from an empty list
    set->update_objs = NULL;

    return msg;
}

/**
   @brief
   Build the UpdateObject structs that belongs to a Set request

   @param set the parent object that is extended
   @param obj_path the path to the object selected in the Set request

   @returns
 */
static Usp__Set__UpdateObject* uspl_set_create_obj(Usp__Set* set, const char* obj_path) {
    Usp__Set__UpdateObject* update_obj = NULL;
    int new_num = 0;    // new number of entries in update_objs array

    // Allocate memory to store the update_object entry
    update_obj = USP_MALLOC(sizeof(Usp__Set__UpdateObject));
    usp__set__update_object__init(update_obj);

    // Increase the size of the array
    new_num = set->n_update_objs + 1;
    set->update_objs = USP_REALLOC(set->update_objs, new_num * sizeof(void*));
    set->n_update_objs = new_num;
    set->update_objs[new_num - 1] = update_obj;

    // Fill up the param_setting entry
    update_obj->obj_path = USP_STRDUP(obj_path);
    update_obj->n_param_settings = 0;
    update_obj->param_settings = NULL;

    return update_obj;
}

/**
   @brief
   Build the UpdateParamSetting objects that belong to a Set request

   @param update_obj the parent object that is extended
   @param param the name of the parameter being set in the Set request
   @param value the value of the parameter being set in the Set request
   @param required boolean indicating whether the parameter is required

   @returns
 */
static void uspl_set_create_param_setting(Usp__Set__UpdateObject* update_obj,
                                          const char* param,
                                          const char* value,
                                          bool required) {
    Usp__Set__UpdateParamSetting* param_setting = NULL;
    int new_num = 0;    // new number of entries in param_settings array

    // Allocate memory to store the param_setting entry
    param_setting = USP_MALLOC(sizeof(Usp__Set__UpdateParamSetting));
    usp__set__update_param_setting__init(param_setting);

    // Increase the size of the array
    new_num = update_obj->n_param_settings + 1;
    update_obj->param_settings = USP_REALLOC(update_obj->param_settings, new_num * sizeof(void*));
    update_obj->n_param_settings = new_num;
    update_obj->param_settings[new_num - 1] = param_setting;

    // Fill up the param_setting entry
    param_setting->param = USP_STRDUP(param);
    param_setting->value = USP_STRDUP(value);
    param_setting->required = required;
}

/**
   @brief
   Build a Set request message based on a list of requests in the form of variants

   @param set the object that is extended
   @param requests the list of requests used to build the Set request

   @returns 0 in case of success, -1 in case of error
 */
static int uspl_set_populate_req(Usp__Set* set, const amxc_var_t* requests) {
    int retval = -1;
    const amxc_llist_t* req_list = NULL;

    req_list = amxc_var_constcast(amxc_llist_t, requests);
    when_true(amxc_llist_size(req_list) == 0, exit);

    amxc_llist_iterate(it, req_list) {
        const amxc_llist_t* params_list = NULL;
        amxc_var_t* req_var = amxc_var_from_llist_it(it);
        const char* obj_path = GET_CHAR(req_var, "object_path");
        when_null(obj_path, exit);

        Usp__Set__UpdateObject* update_obj = uspl_set_create_obj(set, obj_path);

        params_list = amxc_var_constcast(amxc_llist_t, GET_ARG(req_var, "parameters"));
        when_null(params_list, exit);

        retval = -1;
        amxc_llist_iterate(it2, params_list) {
            const char* param = NULL;
            const char* value = NULL;
            bool required = false;
            amxc_var_t* param_info = amxc_var_from_llist_it(it2);

            param = GET_CHAR(param_info, "param");
            when_null(param, exit);
            retval = amxc_var_cast(GET_ARG(param_info, "value"), AMXC_VAR_ID_CSTRING);
            when_failed(retval, exit);
            value = GET_CHAR(param_info, "value");
            required = GET_BOOL(param_info, "required");
            uspl_set_create_param_setting(update_obj, param, value, required);
        }
    }

    retval = 0;
exit:
    return retval;
}

/**
   @brief
   Fill up a variant with the information present in an Set request

   @param set the object that is parsed
   @param table the variant that is filled up with the Set information

   @returns
 */
static void uspl_set_populate_var(Usp__Set* set, amxc_var_t* table) {
    amxc_var_t* requests = NULL;
    amxc_var_set_type(table, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(bool, table, "allow_partial", set->allow_partial);
    requests = amxc_var_add_key(amxc_llist_t, table, "requests", NULL);

    for(size_t i = 0; i < set->n_update_objs; i++) {
        Usp__Set__UpdateObject* update_obj = set->update_objs[i];
        amxc_var_t* request = NULL;
        amxc_var_t* params = NULL;

        request = amxc_var_add(amxc_htable_t, requests, NULL);
        amxc_var_add_key(cstring_t, request, "object_path", update_obj->obj_path);
        params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);
        for(size_t j = 0; j < update_obj->n_param_settings; j++) {
            Usp__Set__UpdateParamSetting* param_setting = update_obj->param_settings[j];
            amxc_var_t* param = NULL;

            param = amxc_var_add(amxc_htable_t, params, NULL);
            amxc_var_add_key(cstring_t, param, "param", param_setting->param);
            amxc_var_add_key(cstring_t, param, "value", param_setting->value);
            amxc_var_add_key(bool, param, "required", param_setting->required);
        }
    }
}

/**
   @brief
   Add all updated parameters to the UpdatedInstanceResult

   @param   updated_inst_result - pointer to updated instance result object to add entries to
   @param   updated_params - variant of type hash table with all parameters

   @return
 */
static void uspl_set_populate_resp_success_params(Usp__SetResp__UpdatedInstanceResult* updated_inst_res,
                                                  amxc_var_t* updated_params) {
    amxc_var_for_each(param, updated_params) {
        const char* param_name = amxc_var_key(param);
        char* param_value = amxc_var_dyncast(cstring_t, param);
        AddUpdatedInstRes_ParamsEntry(updated_inst_res, param_name, param_value);
    }
}

/**
   @brief
   Add all failed parameters to the UpdatedInstanceResult

   @param   updated_inst_result - pointer to updated instance result object to add entries to
   @param   param_errs - variant of type hash table with all parameters errors

   @return
 */
static void uspl_set_populate_resp_success_param_errs(Usp__SetResp__UpdatedInstanceResult* updated_inst_res,
                                                      amxc_var_t* param_errs) {
    amxc_var_for_each(err, param_errs) {
        const char* param_name = GET_CHAR(err, "param");
        const char* err_msg = GET_CHAR(err, "err_msg");
        uint32_t err_code = GET_UINT32(err, "err_code");
        AddUpdatedInstRes_ParamErr(updated_inst_res, param_name, err_code, err_msg);
    }
}

/**
   @brief
   Populate the set response message in case of success

   @param   set_resp - pointer to the set response message to fill up
   @param   requested_path - object path requested with the set request
   @param   reply_data - variant with response information for this updated object

   @return 0 in case of success, -1 in case of error
 */
static int uspl_set_populate_resp_success(Usp__SetResp* set_resp,
                                          const char* requested_path,
                                          amxc_var_t* reply_data) {
    int retval = -1;
    Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationSuccess* oper_success = NULL;
    amxc_var_t* success = GET_ARG(reply_data, "success");
    amxc_var_t* results = GET_ARG(success, "updated_inst_results");

    when_null(success, exit);
    when_null(results, exit);
    when_true(amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, results)), exit);

    oper_success = AddSetResp_OperSuccess(set_resp, requested_path);

    amxc_var_for_each(result, results) {
        Usp__SetResp__UpdatedInstanceResult* updated_inst_res = NULL;
        const char* affected_path = GET_CHAR(result, "affected_path");
        amxc_var_t* updated_params = GET_ARG(result, "updated_params");
        amxc_var_t* param_errs = GET_ARG(result, "param_errs");

        when_null(affected_path, exit);

        updated_inst_res = AddOperSuccess_UpdatedInstRes(oper_success, affected_path);

        uspl_set_populate_resp_success_params(updated_inst_res, updated_params);
        uspl_set_populate_resp_success_param_errs(updated_inst_res, param_errs);
    }

    retval = 0;
exit:
    return retval;
}

/**
   @brief
   Add all failed parameters to the UpdatedInstanceFailure

   @param   updated_inst_failure - pointer to updated instance failure object to add entries to
   @param   param_errs - variant of type hash table with all parameters errors

   @return
 */
static void uspl_set_populate_resp_failure_params(Usp__SetResp__UpdatedInstanceFailure* updated_inst_failure,
                                                  amxc_var_t* param_errs) {
    amxc_var_for_each(err, param_errs) {
        const char* param_name = GET_CHAR(err, "param");
        const char* err_msg = GET_CHAR(err, "err_msg");
        uint32_t err_code = GET_UINT32(err, "err_code");
        AddUpdatedInstFailure_ParamErr(updated_inst_failure, param_name, err_code, err_msg);
    }
}

/**
   @brief
   Populate the set response message in case of failure

   @param   set_resp - pointer to the set response message to fill up
   @param   requested_path - object path requested with the set request
   @param   reply_data - variant with response information for this updated object

   @return 0 in case of success, -1 in case of error
 */
static int uspl_set_populate_resp_failure(Usp__SetResp* set_resp,
                                          const char* requested_path,
                                          amxc_var_t* reply_data) {
    int retval = -1;
    Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationFailure* oper_failure = NULL;
    amxc_var_t* failure = GET_ARG(reply_data, "failure");
    amxc_var_t* inst_failures = GET_ARG(failure, "updated_inst_failures");
    const char* err_msg = GET_CHAR(failure, "err_msg");
    uint32_t err_code = GET_UINT32(failure, "err_code");

    when_null(failure, exit);
    when_str_empty(err_msg, exit);
    when_true(err_code == USP_ERR_OK, exit);

    oper_failure = AddSetResp_OperFailure(set_resp, requested_path, err_code, err_msg);

    amxc_var_for_each(inst, inst_failures) {
        const char* affected_path = GET_CHAR(inst, "affected_path");
        amxc_var_t* param_errs = GET_ARG(inst, "param_errs");
        Usp__SetResp__UpdatedInstanceFailure* updated_inst_failure = NULL;

        when_str_empty(affected_path, exit);

        updated_inst_failure = AddOperFailure_UpdatedInstFailure(oper_failure, affected_path);
        uspl_set_populate_resp_failure_params(updated_inst_failure, param_errs);
    }

    retval = 0;
exit:
    return retval;
}

/**
   @brief
   Populate the set response message

   @param   set_resp - pointer to the set response message to fill up
   @param   reply_data - variant with response information for this updated object

   @return 0 in case of success, -1 in case of error
 */
static int uspl_set_populate_resp(Usp__SetResp* set_resp, amxc_var_t* reply_data) {
    int retval = -1;
    const char* requested_path = GET_CHAR(reply_data, "requested_path");
    uint32_t oper_status = GET_UINT32(reply_data, "oper_status");

    when_str_empty(requested_path, exit);
    when_true(oper_status == USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS__NOT_SET, exit);

    if(oper_status == USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS) {
        retval = uspl_set_populate_resp_success(set_resp, requested_path, reply_data);
    } else {
        retval = uspl_set_populate_resp_failure(set_resp, requested_path, reply_data);
    }

exit:
    return retval;
}

/**
   @brief
   Append the updated instance results to the result variant

   @param updated_inst_res the UpdatedInstanceResult object
   @param updated_inst_var a variant of type list that must be appended with the updated objects

   @returns 0 in case of success, -1 in case of error
 */
static void uspl_set_resp_append_updated_inst(Usp__SetResp__UpdatedInstanceResult* updated_inst_res,
                                              amxc_var_t* updated_inst_var) {
    amxc_var_t* updated_params = NULL;
    amxc_var_t* param_errs = NULL;
    amxc_var_t* entry = NULL;

    entry = amxc_var_add(amxc_htable_t, updated_inst_var, NULL);
    amxc_var_add_key(cstring_t, entry, "affected_path", updated_inst_res->affected_path);

    if(updated_inst_res->n_updated_params > 0) {
        updated_params = amxc_var_add_key(amxc_htable_t, entry, "updated_params", NULL);
    }
    for(size_t i = 0; i < updated_inst_res->n_updated_params; i++) {
        Usp__SetResp__UpdatedInstanceResult__UpdatedParamsEntry* param = NULL;
        amxc_var_t* amx_param = NULL;
        param = updated_inst_res->updated_params[i];
        amx_param = amxc_var_add_key(cstring_t, updated_params, param->key, param->value);
        amxc_var_cast(amx_param, AMXC_VAR_ID_ANY);
    }

    if(updated_inst_res->n_param_errs > 0) {
        param_errs = amxc_var_add_key(amxc_llist_t, entry, "param_errs", NULL);
    }
    for(size_t i = 0; i < updated_inst_res->n_param_errs; i++) {
        amxc_var_t* error_entry = NULL;
        Usp__SetResp__ParameterError* error = updated_inst_res->param_errs[i];

        error_entry = amxc_var_add(amxc_htable_t, param_errs, NULL);
        amxc_var_add_key(cstring_t, error_entry, "param", error->param);
        amxc_var_add_key(cstring_t, error_entry, "err_msg", error->err_msg);
        amxc_var_add_key(uint32_t, error_entry, "err_code", error->err_code);
    }
}

/**
   @brief
   Append the results of a successful Set operation to the list of responses

   @param success an operation success result
   @param resp_list an empty list that will be filled up with variants of the responses
   @param requested_path the original requested path

   @returns 0 in case of success, -1 in case of error
 */
static int uspl_set_resp_append_result_success(Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationSuccess* success,
                                               amxc_llist_t* resp_list,
                                               const char* requested_path) {
    int retval = -1;
    amxc_var_t* entry = NULL;
    amxc_var_t* success_var = NULL;
    amxc_var_t* updated_inst = NULL;

    when_str_empty(requested_path, exit);

    amxc_var_new(&entry);
    amxc_var_set_type(entry, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, entry, "requested_path", requested_path);
    amxc_var_add_key(uint32_t, entry, "oper_status", USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS);
    success_var = amxc_var_add_key(amxc_htable_t, entry, "success", NULL);
    updated_inst = amxc_var_add_key(amxc_llist_t, success_var, "updated_inst_results", NULL);

    for(size_t i = 0; i < success->n_updated_inst_results; i++) {
        uspl_set_resp_append_updated_inst(success->updated_inst_results[i], updated_inst);
    }

    amxc_llist_append(resp_list, &entry->lit);

    retval = 0;
exit:
    return retval;
}

/**
   @brief
   Append the failed parameters to the result variant

   @param updated_inst_failure the UpdatedInstanceFailure object
   @param updated_inst_list a variant of type list that must be appended with the resulting failures

   @returns 0 in case of success, -1 in case of error
 */
static void uspl_set_resp_append_result_failure_params(Usp__SetResp__UpdatedInstanceFailure* updated_inst_failure,
                                                       amxc_var_t* updated_inst_list) {
    amxc_var_t* entry = NULL;
    amxc_var_t* param_errs = NULL;

    entry = amxc_var_add(amxc_htable_t, updated_inst_list, NULL);
    amxc_var_add_key(cstring_t, entry, "affected_path", updated_inst_failure->affected_path);

    if(updated_inst_failure->n_param_errs > 0) {
        param_errs = amxc_var_add_key(amxc_llist_t, entry, "param_errs", NULL);
    }

    for(size_t i = 0; i < updated_inst_failure->n_param_errs; i++) {
        Usp__SetResp__ParameterError* error = updated_inst_failure->param_errs[i];
        amxc_var_t* error_entry = NULL;

        error_entry = amxc_var_add(amxc_htable_t, param_errs, NULL);
        amxc_var_add_key(cstring_t, error_entry, "param", error->param);
        amxc_var_add_key(cstring_t, error_entry, "err_msg", error->err_msg);
        amxc_var_add_key(uint32_t, error_entry, "err_code", error->err_code);
    }
}

/**
   @brief
   Append the results of a failed Set operation to the list of responses

   @param failure an operation failure result
   @param resp_list an empty list that will be filled up with variants of the responses
   @param requested_path the original requested path

   @returns 0 in case of success, -1 in case of error
 */
static int uspl_set_resp_append_result_failure(Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationFailure* failure,
                                               amxc_llist_t* resp_list,
                                               const char* requested_path) {
    int retval = -1;
    amxc_var_t* entry = NULL;
    amxc_var_t* failure_var = NULL;
    amxc_var_t* updated_inst = NULL;

    when_str_empty(requested_path, exit);

    amxc_var_new(&entry);
    amxc_var_set_type(entry, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, entry, "requested_path", requested_path);
    amxc_var_add_key(uint32_t, entry, "oper_status", USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE);
    failure_var = amxc_var_add_key(amxc_htable_t, entry, "failure", NULL);
    amxc_var_add_key(cstring_t, failure_var, "err_msg", failure->err_msg);
    amxc_var_add_key(uint32_t, failure_var, "err_code", failure->err_code);

    if(failure->n_updated_inst_failures > 0) {
        updated_inst = amxc_var_add_key(amxc_llist_t, failure_var, "updated_inst_failures", NULL);
    }

    for(size_t i = 0; i < failure->n_updated_inst_failures; i++) {
        uspl_set_resp_append_result_failure_params(failure->updated_inst_failures[i], updated_inst);
    }

    amxc_llist_append(resp_list, &entry->lit);

    retval = 0;
exit:
    return retval;
}

/**
   @brief

   @param result a UpdatedObjectResult in response to a Set for a single requested path
   @param resp_list an empty list that will be filled up with variants of the responses

   @returns 0 in case of success, -1 in case of error
 */
static int uspl_set_resp_append_result(Usp__SetResp__UpdatedObjectResult* result,
                                       amxc_llist_t* resp_list) {
    int retval = -1;

    if(result->oper_status->oper_status_case == USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS) {
        Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationSuccess* success = NULL;
        success = result->oper_status->oper_success;
        retval = uspl_set_resp_append_result_success(success, resp_list, result->requested_path);
    } else if(result->oper_status->oper_status_case == USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE) {
        Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationFailure* failure = NULL;
        failure = result->oper_status->oper_failure;
        retval = uspl_set_resp_append_result_failure(failure, resp_list, result->requested_path);
    } else {
        SAH_TRACEZ_ERROR(uspl_traceme, "Unknown operation response status");
        goto exit;
    }

exit:
    return retval;
}

/***************** Start of public functions *******************/

int uspl_set_new(uspl_tx_t* usp_tx, const amxc_var_t* request) {
    int retval = -1;
    Usp__Msg* msg = NULL;
    amxc_var_t* requests = NULL;
    amxc_string_t msg_id;

    amxc_string_init(&msg_id, 0);

    when_null(usp_tx, exit);
    when_null(request, exit);

    amxc_string_setf(&msg_id, "%d", uspl_msghandler_next_msg_id());

    msg = CreateSet(amxc_string_get(&msg_id, 0));
    msg->body->request->set->allow_partial = GET_BOOL(request, "allow_partial");

    requests = GET_ARG(request, "requests");
    when_null(requests, exit);

    retval = uspl_set_populate_req(msg->body->request->set, requests);
    when_failed(retval, exit);

    retval = uspl_msghandler_pack_protobuf(msg, usp_tx);
    usp_tx->msg_id = USP_STRDUP(msg->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__SET;

exit:
    amxc_string_clean(&msg_id);
    uspl_msghandler_free_unpacked_message(msg);

    return retval;
}

int uspl_set_extract(uspl_rx_t* usp_rx, amxc_var_t* result) {
    int retval = USP_ERR_INTERNAL_ERROR;
    Usp__Set* set = NULL;

    when_null(usp_rx, exit);
    when_null(result, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_REQUEST, exit);
    when_null(usp_rx->msg->body->request, exit);
    when_true(usp_rx->msg->body->request->req_type_case != USP__REQUEST__REQ_TYPE_SET, exit);
    when_null(usp_rx->msg->body->request->set, exit);

    set = usp_rx->msg->body->request->set;

    uspl_set_populate_var(set, result);

    retval = USP_ERR_OK;
exit:
    return retval;
}

int uspl_set_resp_new(uspl_tx_t* usp_tx, amxc_llist_t* resp_list, const char* msg_id) {
    int retval = -1;
    Usp__Msg* resp = NULL;
    Usp__SetResp* set_resp = NULL;

    when_null(usp_tx, exit);
    when_null(resp_list, exit);
    when_null(msg_id, exit);

    // Create a Set Response message
    resp = CreateSetResp(msg_id);
    set_resp = resp->body->response->set_resp;

    // Iterate over all replies to different Set requests
    amxc_llist_for_each(it, resp_list) {
        amxc_var_t* reply_data = amxc_llist_it_get_data(it, amxc_var_t, lit);
        retval = uspl_set_populate_resp(set_resp, reply_data);
        when_failed(retval, exit);
    }

    retval = uspl_msghandler_pack_protobuf(resp, usp_tx);
    usp_tx->msg_id = USP_STRDUP(resp->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__SET_RESP;

exit:
    uspl_msghandler_free_unpacked_message(resp);
    return retval;
}

int uspl_set_resp_extract(uspl_rx_t* usp_rx, amxc_llist_t* resp_list) {
    int retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    Usp__SetResp* set_resp = NULL;

    when_null(usp_rx, exit);
    when_null(resp_list, exit);
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_RESPONSE, exit);
    when_null(usp_rx->msg->body->response, exit);
    when_true(usp_rx->msg->body->response->resp_type_case != USP__RESPONSE__RESP_TYPE_SET_RESP, exit);
    when_null(usp_rx->msg->body->response->set_resp, exit);
    when_true(usp_rx->msg->body->response->set_resp->n_updated_obj_results == 0, exit);
    when_null(usp_rx->msg->body->response->set_resp->updated_obj_results, exit);

    set_resp = usp_rx->msg->body->response->set_resp;

    for(size_t i = 0; i < set_resp->n_updated_obj_results; i++) {
        uspl_set_resp_append_result(set_resp->updated_obj_results[i], resp_list);
    }

    retval = USP_ERR_OK;
exit:
    return retval;
}
