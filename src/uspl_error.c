/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
   @file uspl_error.c
   @brief
   Functions for creating error responses.
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_error.h"
#include "usp/uspl_msghandler.h"

#include "uspl_msghandler_priv.h"
#include "uspl_private.h"

#include "bbf/usp_mem.h"
#include "bbf/error_resp.h"

int uspl_error_resp_new(uspl_tx_t* usp_tx,
                        amxc_var_t* error,
                        const char* msg_id) {
    Usp__Msg* resp = NULL;
    uint32_t err_code = 0;
    const char* err_msg = NULL;
    int retval = -1;
    amxc_var_t* param_errs = NULL;

    when_null(usp_tx, exit);
    when_null(msg_id, exit);
    when_null(error, exit);

    err_code = GET_UINT32(error, "err_code");
    err_msg = GET_CHAR(error, "err_msg");

    when_str_empty(err_msg, exit);
    when_true(err_code == USP_ERR_OK, exit);

    // Create the new ErrorResponse
    resp = ERROR_RESP_Create(msg_id, err_code, err_msg);
    if(resp == NULL) {
        SAH_TRACEZ_ERROR(uspl_traceme, "Could not create Usp__Msg for error response");
        goto exit;
    }

    param_errs = GET_ARG(error, "param_errs");
    if(param_errs != NULL) {
        amxc_var_for_each(err, param_errs) {
            const char* param_path = GET_CHAR(err, "param_path");
            err_msg = GET_CHAR(err, "err_msg");
            err_code = GET_UINT32(err, "err_code");
            ERROR_RESP_AddParamError(resp, param_path, err_code, err_msg);
        }
    }

    retval = uspl_msghandler_pack_protobuf(resp, usp_tx);
    usp_tx->msg_id = USP_STRDUP(resp->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__ERROR;

exit:
    uspl_msghandler_free_unpacked_message(resp);
    return retval;
}

int uspl_error_resp_extract(uspl_rx_t* usp_rx, amxc_var_t* values) {
    int retval = USP_ERR_INTERNAL_ERROR;
    Usp__Error* error = NULL;

    when_null(usp_rx, exit);
    when_null(values, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_ERROR, exit);
    when_null(usp_rx->msg->body->error, exit);
    when_null(usp_rx->msg->body->error->err_msg, exit);
    when_true(usp_rx->msg->body->error->err_code == 0, exit);

    error = usp_rx->msg->body->error;
    amxc_var_set_type(values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, values, "err_msg", error->err_msg);
    amxc_var_add_key(uint32_t, values, "err_code", error->err_code);

    if(error->n_param_errs != 0) {
        amxc_var_t* param_errs = amxc_var_add_key(amxc_llist_t, values, "param_errs", NULL);
        for(size_t i = 0; i < error->n_param_errs; i++) {
            amxc_var_t* param_var = amxc_var_add(amxc_htable_t, param_errs, NULL);
            amxc_var_add_key(uint32_t, param_var, "err_code", error->param_errs[i]->err_code);
            amxc_var_add_key(cstring_t, param_var, "err_msg", error->param_errs[i]->err_msg);
            amxc_var_add_key(cstring_t, param_var, "param_path", error->param_errs[i]->param_path);
        }
    }

    retval = USP_ERR_OK;
exit:
    return retval;

}

const char* uspl_error_code_to_str(int err) {
    const char* s = NULL;

    switch(err) {
    case USP_ERR_GENERAL_FAILURE:
        s = "General failure";
        break;

    case USP_ERR_MESSAGE_NOT_UNDERSTOOD:
        s = "Message not understood";
        break;

    case USP_ERR_REQUEST_DENIED:
        s = "Request Denied";
        break;

    case USP_ERR_INTERNAL_ERROR:
        s = "Internal error";
        break;

    case USP_ERR_INVALID_ARGUMENTS:
        s = "Invalid arguments";
        break;

    case USP_ERR_RESOURCES_EXCEEDED:
        s = "Resources exceeded";
        break;

    case USP_ERR_PERMISSION_DENIED:
        s = "Permission denied";
        break;

    case USP_ERR_INVALID_CONFIGURATION:
        s = "Invalid Configuration";
        break;

    case USP_ERR_INVALID_PATH_SYNTAX:
        s = "Invalid path syntax";
        break;

    case USP_ERR_PARAM_ACTION_FAILED:
        s = "Parameter action failed";
        break;

    case USP_ERR_UNSUPPORTED_PARAM:
        s = "Unsupported parameter";
        break;

    case USP_ERR_INVALID_TYPE:
        s = "Invalid type";
        break;

    case USP_ERR_INVALID_VALUE:
        s = "Invalid value";
        break;

    case USP_ERR_PARAM_READ_ONLY:
        s = "Parameter read only";
        break;

    case USP_ERR_VALUE_CONFLICT:
        s = "Value conflict";
        break;

    case USP_ERR_CRUD_FAILURE:
        s = "CRUD failure";
        break;

    case USP_ERR_OBJECT_DOES_NOT_EXIST:
        s = "Object does not exist";
        break;

    case USP_ERR_CREATION_FAILURE:
        s = "Creation failure";
        break;

    case USP_ERR_NOT_A_TABLE:
        s = "Not a table";
        break;

    case USP_ERR_OBJECT_NOT_CREATABLE:
        s = "Object not creatable";
        break;

    case USP_ERR_SET_FAILURE:
        s = "Set failure";
        break;

    case USP_ERR_REQUIRED_PARAM_FAILED:
        s = "Required Parameter failed";
        break;

    case USP_ERR_COMMAND_FAILURE:
        s = "Command failure";
        break;

    case USP_ERR_COMMAND_CANCELLED:
        s = "Command cancelled";
        break;

    case USP_ERR_OBJECT_NOT_DELETABLE:
        s = "Object not deletable";
        break;

    case USP_ERR_UNIQUE_KEY_CONFLICT:
        s = "Unique key conflict";
        break;

    case USP_ERR_INVALID_PATH:
        s = "Invalid path";
        break;

    case USP_ERR_RECORD_NOT_PARSED:
        s = "USP Record not parsed";
        break;

    case USP_ERR_SECURE_SESS_REQUIRED:
        s = "Secure seesion required";
        break;

    case USP_ERR_SECURE_SESS_NOT_SUPPORTED:
        s = "Secure session not supported";
        break;

    case USP_ERR_SEG_NOT_SUPPORTED:
        s = "Segmentation and reassembly not supported";
        break;

    case USP_ERR_RECORD_FIELD_INVALID:
        s = "USP Record field invalid";
        break;

    default:
        s = "Unknown error code";
        break;
    }

    return s;
}

int uspl_amxd_status_to_usp_error(amxd_status_t status) {
    int retval = -1;

    // Do nothing if provided value is already a USP error code
    if((status >= USP_ERR_GENERAL_FAILURE) && (status < USP_ERR_GENERAL_FAILURE + 1000)) {
        retval = status;
        goto exit;
    }

    switch(status) {
    case amxd_status_ok:
        retval = USP_ERR_OK;
        break;
    case amxd_status_unknown_error:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_object_not_found:
        retval = USP_ERR_OBJECT_DOES_NOT_EXIST;
        break;
    case amxd_status_function_not_found:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_parameter_not_found:
        retval = USP_ERR_INVALID_PATH;
        break;
    case amxd_status_function_not_implemented:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_invalid_function:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_invalid_function_argument:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_invalid_name:
        retval = USP_ERR_INVALID_VALUE;
        break;
    case amxd_status_invalid_attr:
        retval = USP_ERR_INVALID_ARGUMENTS;
        break;
    case amxd_status_invalid_value:
        retval = USP_ERR_INVALID_VALUE;
        break;
    case amxd_status_invalid_action:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_invalid_type:
        retval = USP_ERR_INVALID_TYPE;
        break;
    case amxd_status_duplicate:
        retval = USP_ERR_UNIQUE_KEY_CONFLICT;
        break;
    case amxd_status_deferred:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_read_only:
        retval = USP_ERR_PARAM_READ_ONLY;
        break;
    case amxd_status_missing_key:
        retval = USP_ERR_INVALID_ARGUMENTS;
        break;
    case amxd_status_file_not_found:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_invalid_arg:
        retval = USP_ERR_INVALID_ARGUMENTS;
        break;
    case amxd_status_out_of_mem:
        retval = USP_ERR_RESOURCES_EXCEEDED;
        break;
    case amxd_status_recursion:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_invalid_path:
        retval = USP_ERR_INVALID_PATH;
        break;
    case amxd_status_invalid_expr:
        retval = USP_ERR_INVALID_PATH;
        break;
    case amxd_status_permission_denied:
        retval = USP_ERR_PERMISSION_DENIED;
        break;
    case amxd_status_not_supported:
        retval = USP_ERR_INVALID_PATH;
        break;
    default:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    }

exit:
    return retval;
}

int uspl_set_amxd_status_to_usp_param_error(amxd_status_t status) {
    int retval = -1;

    // Do nothing if provided value is already a USP error code
    if((status >= USP_ERR_GENERAL_FAILURE) && (status < USP_ERR_GENERAL_FAILURE + 1000)) {
        retval = status;
        goto exit;
    }

    switch(status) {
    case amxd_status_ok:
        retval = USP_ERR_OK;
        break;
    case amxd_status_unknown_error:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_object_not_found:
        retval = USP_ERR_OBJECT_DOES_NOT_EXIST;
        break;
    case amxd_status_function_not_found:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_parameter_not_found:
        retval = USP_ERR_UNSUPPORTED_PARAM;
        break;
    case amxd_status_function_not_implemented:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_invalid_function:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_invalid_function_argument:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_invalid_name:
        retval = USP_ERR_INVALID_VALUE;
        break;
    case amxd_status_invalid_attr:
        retval = USP_ERR_INVALID_ARGUMENTS;
        break;
    case amxd_status_invalid_value:
        retval = USP_ERR_INVALID_VALUE;
        break;
    case amxd_status_invalid_action:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_invalid_type:
        retval = USP_ERR_INVALID_TYPE;
        break;
    case amxd_status_duplicate:
        retval = USP_ERR_INVALID_CONFIGURATION;
        break;
    case amxd_status_deferred:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_read_only:
        retval = USP_ERR_PARAM_READ_ONLY;
        break;
    case amxd_status_missing_key:
        retval = USP_ERR_INVALID_ARGUMENTS;
        break;
    case amxd_status_file_not_found:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_invalid_arg:
        retval = USP_ERR_INVALID_ARGUMENTS;
        break;
    case amxd_status_out_of_mem:
        retval = USP_ERR_RESOURCES_EXCEEDED;
        break;
    case amxd_status_recursion:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    case amxd_status_invalid_path:
        retval = USP_ERR_INVALID_PATH;
        break;
    case amxd_status_invalid_expr:
        retval = USP_ERR_INVALID_PATH;
        break;
    case amxd_status_permission_denied:
        retval = USP_ERR_PERMISSION_DENIED;
        break;
    case amxd_status_not_supported:
        retval = USP_ERR_INVALID_PATH;
        break;
    default:
        retval = USP_ERR_INTERNAL_ERROR;
        break;
    }

exit:
    return retval;
}

amxd_status_t uspl_usp_error_to_amxd_status(int err_code) {
    int retval = err_code;

    // If error code is non zero, but smaller than USP_ERR_GENERAL_FAILURE,
    // it is not a USP error, so do no conversion
    if((err_code >= 0) && (err_code < USP_ERR_GENERAL_FAILURE)) {
        goto exit;
    }

    switch(err_code) {
    case USP_ERR_OK:
        retval = amxd_status_ok;
        break;
    case USP_ERR_INTERNAL_ERROR:
        retval = amxd_status_unknown_error;
        break;
    case USP_ERR_OBJECT_DOES_NOT_EXIST:
        retval = amxd_status_object_not_found;
        break;
    case amxd_status_function_not_found:
        retval = amxd_status_function_not_found;
        break;
    case USP_ERR_UNSUPPORTED_PARAM:
        retval = amxd_status_parameter_not_found;
        break;
    case USP_ERR_COMMAND_FAILURE:
        retval = amxd_status_function_not_implemented;
        break;
    case amxd_status_invalid_function:
        retval = amxd_status_invalid_function;
        break;
    case USP_ERR_INVALID_CMD_ARGS:
        retval = amxd_status_invalid_function_argument;
        break;
    case amxd_status_invalid_name:
        retval = amxd_status_invalid_name;
        break;
    case amxd_status_invalid_attr:
        retval = amxd_status_invalid_attr;
        break;
    case USP_ERR_INVALID_VALUE:
        retval = amxd_status_invalid_value;
        break;
    case USP_ERR_PARAM_ACTION_FAILED:
        retval = amxd_status_invalid_action;
        break;
    case USP_ERR_INVALID_TYPE:
        retval = amxd_status_invalid_type;
        break;
    case amxd_status_duplicate:
        retval = amxd_status_duplicate;
        break;
    case amxd_status_deferred:
        retval = amxd_status_deferred;
        break;
    case USP_ERR_PARAM_READ_ONLY:
        retval = amxd_status_read_only;
        break;
    case USP_ERR_UNIQUE_KEY_CONFLICT:
        retval = amxd_status_missing_key;
        break;
    case amxd_status_file_not_found:
        retval = amxd_status_file_not_found;
        break;
    case USP_ERR_INVALID_ARGUMENTS:
        retval = amxd_status_invalid_arg;
        break;
    case USP_ERR_RESOURCES_EXCEEDED:
        retval = amxd_status_out_of_mem;
        break;
    case amxd_status_recursion:
        retval = amxd_status_recursion;
        break;
    case USP_ERR_INVALID_PATH_SYNTAX:
    case USP_ERR_INVALID_PATH:
        retval = amxd_status_invalid_path;
        break;
    case amxd_status_invalid_expr:
        retval = amxd_status_invalid_expr;
        break;
    case USP_ERR_PERMISSION_DENIED:
        retval = amxd_status_permission_denied;
        break;
    case amxd_status_not_supported:
        retval = amxd_status_not_supported;
        break;
    case -1:
        retval = amxd_status_unknown_error;
        break;
    default:
        retval = amxd_status_unknown_error;
        break;
    }

exit:
    return retval;
}
