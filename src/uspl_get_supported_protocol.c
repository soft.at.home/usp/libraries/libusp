/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
   @file uspl_get_supported_protocol.c
   @brief
   Create and extract Delete requests and responses
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>

#include <debug/sahtrace.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_error.h"
#include "usp/uspl_get_supported_protocol.h"
#include "usp/uspl_msghandler.h"

#include "uspl_msghandler_priv.h"

#include "bbf/usp_mem.h"
#include "bbf/handle_get_supported_protocol.h"

/**
   @brief
   Dynamically creates an GetSupportedProtocol object

   @note: The object should be deleted using usp__msg__free_unpacked()

   @param msg_id string containing the message id of the get request, which initiated this response

   @return Pointer to a GetSupportedProtocol object
 */
static Usp__Msg* CreateGetSupportedProtocol(const char* msg_id) {
    Usp__Msg* msg;
    Usp__Header* header;
    Usp__Body* body;
    Usp__Request* request;
    Usp__GetSupportedProtocol* get_sup_proto;

    // Allocate memory to store the USP message
    msg = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(msg);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    request = USP_MALLOC(sizeof(Usp__Request));
    usp__request__init(request);

    get_sup_proto = USP_MALLOC(sizeof(Usp__GetSupportedProtocol));
    usp__get_supported_protocol__init(get_sup_proto);

    // Connect the structures together
    msg->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO;

    msg->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    body->request = request;
    request->req_type_case = USP__REQUEST__REQ_TYPE_GET_SUPPORTED_PROTOCOL;

    request->get_supported_protocol = get_sup_proto;
    get_sup_proto->controller_supported_protocol_versions = NULL;

    return msg;
}

/*********************** Start of public functions ****************************/

int uspl_get_supported_protocol_new(uspl_tx_t* usp_tx, const amxc_var_t* request) {
    int retval = -1;
    Usp__Msg* msg = NULL;
    Usp__GetSupportedProtocol* gs_proto = NULL;
    const char* versions = NULL;
    amxc_string_t msg_id;

    amxc_string_init(&msg_id, 0);

    when_null(usp_tx, exit);
    when_null(request, exit);

    amxc_string_setf(&msg_id, "%d", uspl_msghandler_next_msg_id());

    msg = CreateGetSupportedProtocol(amxc_string_get(&msg_id, 0));

    versions = amxc_var_constcast(cstring_t, request);
    when_true(versions == NULL || *versions == '\0', exit);

    gs_proto = msg->body->request->get_supported_protocol;
    gs_proto->controller_supported_protocol_versions = USP_STRDUP(versions);

    retval = uspl_msghandler_pack_protobuf(msg, usp_tx);
    usp_tx->msg_id = USP_STRDUP(msg->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO;

exit:
    amxc_string_clean(&msg_id);
    uspl_msghandler_free_unpacked_message(msg);

    return retval;
}

int uspl_get_supported_protocol_extract(uspl_rx_t* usp_rx, amxc_var_t* result) {
    int retval = USP_ERR_INTERNAL_ERROR;
    Usp__GetSupportedProtocol* gs_proto = NULL;

    when_null(usp_rx, exit);
    when_null(result, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_REQUEST, exit);
    when_null(usp_rx->msg->body->request, exit);
    when_true(usp_rx->msg->body->request->req_type_case != USP__REQUEST__REQ_TYPE_GET_SUPPORTED_PROTOCOL, exit);
    when_null(usp_rx->msg->body->request->get_supported_protocol, exit);

    // Exit if the controller protocol is not present
    gs_proto = usp_rx->msg->body->request->get_supported_protocol;
    retval = USP_ERR_INVALID_ARGUMENTS;
    when_null(gs_proto->controller_supported_protocol_versions, exit);

    amxc_var_set_type(result, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, result, gs_proto->controller_supported_protocol_versions);

    retval = USP_ERR_OK;
exit:
    return retval;
}

int uspl_get_supported_protocol_resp_new(uspl_tx_t* usp_tx, amxc_llist_t* resp_list, const char* msg_id) {
    int retval = -1;
    Usp__Msg* resp = NULL;
    Usp__GetSupportedProtocolResp* gs_proto_resp = NULL;
    amxc_var_t* response = NULL;
    const char* versions = NULL;

    when_null(usp_tx, exit);
    when_null(resp_list, exit);
    when_null(msg_id, exit);

    resp = CreateGetSupportedProtocolResp(msg_id);
    gs_proto_resp = resp->body->response->get_supported_protocol_resp;

    response = amxc_var_from_llist_it(amxc_llist_get_first(resp_list));
    when_null(response, exit);

    versions = amxc_var_constcast(cstring_t, response);
    when_null(versions, exit);

    gs_proto_resp->agent_supported_protocol_versions = USP_STRDUP(versions);

    retval = uspl_msghandler_pack_protobuf(resp, usp_tx);
    usp_tx->msg_id = USP_STRDUP(resp->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO_RESP;

exit:
    uspl_msghandler_free_unpacked_message(resp);

    return retval;
}

int uspl_get_supported_protocol_resp_extract(uspl_rx_t* usp_rx, amxc_llist_t* resp_list) {
    int retval = USP_ERR_INTERNAL_ERROR;
    Usp__GetSupportedProtocolResp* resp = NULL;
    amxc_var_t* resp_var = NULL;

    when_null(usp_rx, exit);
    when_null(resp_list, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_RESPONSE, exit);
    when_null(usp_rx->msg->body->response, exit);
    when_true(usp_rx->msg->body->response->resp_type_case != USP__RESPONSE__RESP_TYPE_GET_SUPPORTED_PROTOCOL_RESP, exit);
    when_null(usp_rx->msg->body->response->get_supported_protocol_resp, exit);

    // Exit if the controller protocol is not present
    resp = usp_rx->msg->body->response->get_supported_protocol_resp;
    retval = USP_ERR_INVALID_ARGUMENTS;
    when_null(resp->agent_supported_protocol_versions, exit);

    amxc_var_new(&resp_var);
    amxc_var_set_type(resp_var, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, resp_var, resp->agent_supported_protocol_versions);
    amxc_llist_append(resp_list, &resp_var->lit);

    retval = USP_ERR_OK;
exit:
    return retval;
}