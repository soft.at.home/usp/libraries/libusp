/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
   @file uspl_add.c
   @brief
   Create and extract Add requests and responses
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>

#include <debug/sahtrace.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_error.h"
#include "usp/uspl_add.h"
#include "usp/uspl_msghandler.h"

#include "uspl_msghandler_priv.h"
#include "uspl_textutils.h"
#include "uspl_private.h"

#include "bbf/usp_mem.h"
#include "bbf/handle_add.h"

/**
   @brief
   Dynamically creates an Add request object

   @note The object should be deleted using usp__msg__free_unpacked()

   @param msg_id string containing the message id of the add request

   @return Pointer to an Add request object
 */
static Usp__Msg* CreateAdd(const char* msg_id) {
    Usp__Msg* msg;
    Usp__Header* header;
    Usp__Body* body;
    Usp__Request* request;
    Usp__Add* add;

    // Allocate and initialise memory to store the parts of the USP message
    msg = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(msg);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    request = USP_MALLOC(sizeof(Usp__Request));
    usp__request__init(request);

    add = USP_MALLOC(sizeof(Usp__Add));
    usp__add__init(add);

    // Connect the structures together
    msg->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__ADD;

    msg->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    body->request = request;
    request->req_type_case = USP__REQUEST__REQ_TYPE_ADD;
    request->add = add;

    add->allow_partial = false;
    add->n_create_objs = 0;    // Start from an empty list
    add->create_objs = NULL;

    return msg;
}

/**
   @brief
   Add the unique keys that were added during the add request to the response message

   @param oper_success pointer to oper success object to add this unique key map to
   @param params htable of key value pairs

   @return None
 */
static void AddOperSuccess_UniqueKeys(Usp__AddResp__CreatedObjectResult__OperationStatus__OperationSuccess* oper_success,
                                      amxc_var_t* params) {
    Usp__AddResp__CreatedObjectResult__OperationStatus__OperationSuccess__UniqueKeysEntry* entry = NULL;
    int i = 0;
    const amxc_htable_t* table = amxc_var_constcast(amxc_htable_t, params);

    // Allocate the unique key map vector
    oper_success->n_unique_keys = amxc_htable_size(table);
    oper_success->unique_keys = USP_MALLOC(oper_success->n_unique_keys * sizeof(void*));

    // Add all unique keys to the unique key map
    amxc_htable_iterate(it, table) {
        amxc_var_t* param_var = amxc_var_from_htable_it(it);
        entry = USP_MALLOC(sizeof(Usp__AddResp__CreatedObjectResult__OperationStatus__OperationSuccess__UniqueKeysEntry));
        usp__add_resp__created_object_result__operation_status__operation_success__unique_keys_entry__init(entry);
        oper_success->unique_keys[i] = entry;

        entry->key = USP_STRDUP(amxc_htable_it_get_key(it));
        entry->value = amxc_var_dyncast(cstring_t, param_var);
        i++;
    }
}

/**
   @brief
   Build the CreateObject structs that belong to an Add request

   @param add the parent object that is extended
   @param obj_path the path to the object being added in the Add request

   @returns
 */
static Usp__Add__CreateObject* uspl_add_create_obj(Usp__Add* add, const char* obj_path) {
    Usp__Add__CreateObject* create_obj = NULL;
    int new_num = 0;    // new number of entries in create_objs array

    // Allocate memory to store the create_object entry
    create_obj = USP_MALLOC(sizeof(Usp__Add__CreateObject));
    usp__add__create_object__init(create_obj);

    // Increase the size of the array
    new_num = add->n_create_objs + 1;
    add->create_objs = USP_REALLOC(add->create_objs, new_num * sizeof(void*));
    add->n_create_objs = new_num;
    add->create_objs[new_num - 1] = create_obj;

    // Fill up the param_setting entry
    create_obj->obj_path = USP_STRDUP(obj_path);
    create_obj->n_param_settings = 0;
    create_obj->param_settings = NULL;

    return create_obj;
}

/**
   @brief
   Build the CreateParamSetting objects that belong to an Add request

   @param create_obj the parent object that is extended
   @param param the name of the parameter being added in the Add request
   @param value the value of the parameter being added in the Add request
   @param required boolean indicating whether the parameter is required

   @returns
 */
static void uspl_add_create_param_setting(Usp__Add__CreateObject* create_obj,
                                          const char* param,
                                          const char* value,
                                          bool required) {
    Usp__Add__CreateParamSetting* param_setting = NULL;
    int new_num = 0;    // new number of entries in param_settings array

    // Allocate memory to store the param_setting entry
    param_setting = USP_MALLOC(sizeof(Usp__Add__CreateParamSetting));
    usp__add__create_param_setting__init(param_setting);

    // Increase the size of the array
    new_num = create_obj->n_param_settings + 1;
    create_obj->param_settings = USP_REALLOC(create_obj->param_settings, new_num * sizeof(void*));
    create_obj->n_param_settings = new_num;
    create_obj->param_settings[new_num - 1] = param_setting;

    // Fill up the param_setting entry
    param_setting->param = USP_STRDUP(param);
    param_setting->value = USP_STRDUP(value);
    param_setting->required = required;
}

/**
   @brief
   Build an Add request message based on a list of requests in the form of variants

   @param add the object that is extended
   @param requests the list of requests used to build the Add request

   @returns 0 in case of success, -1 in case of error
 */
static int uspl_add_populate_req(Usp__Add* add, const amxc_var_t* requests) {
    int retval = -1;
    const amxc_llist_t* req_list = NULL;

    req_list = amxc_var_constcast(amxc_llist_t, requests);
    when_true(amxc_llist_size(req_list) == 0, exit);

    amxc_llist_iterate(it, req_list) {
        const amxc_llist_t* params_list = NULL;
        amxc_var_t* req_var = amxc_var_from_llist_it(it);
        const char* obj_path = GET_CHAR(req_var, "object_path");
        when_null(obj_path, exit);

        Usp__Add__CreateObject* create_obj = uspl_add_create_obj(add, obj_path);
        retval = 0;

        params_list = amxc_var_constcast(amxc_llist_t, GET_ARG(req_var, "parameters"));
        when_null(params_list, exit);

        retval = -1;
        amxc_llist_iterate(it2, params_list) {
            const char* param = NULL;
            const char* value = NULL;
            bool required = false;
            amxc_var_t* param_info = amxc_var_from_llist_it(it2);

            param = GET_CHAR(param_info, "param");
            when_null(param, exit);
            retval = amxc_var_cast(GET_ARG(param_info, "value"), AMXC_VAR_ID_CSTRING);
            when_failed(retval, exit);
            value = GET_CHAR(param_info, "value");
            required = GET_BOOL(param_info, "required");
            uspl_add_create_param_setting(create_obj, param, value, required);
        }
    }

    retval = 0;
exit:
    return retval;
}

/**
   @brief
   Fill up a variant with the information present in an Add request

   @param add the object that is parsed
   @param table the variant that is filled up with the Add information

   @returns
 */
static void uspl_add_populate_var(Usp__Add* add, amxc_var_t* table) {
    amxc_var_t* requests = NULL;
    amxc_var_set_type(table, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(bool, table, "allow_partial", add->allow_partial);
    requests = amxc_var_add_key(amxc_llist_t, table, "requests", NULL);

    for(size_t i = 0; i < add->n_create_objs; i++) {
        Usp__Add__CreateObject* create_obj = add->create_objs[i];
        amxc_var_t* request = NULL;
        amxc_var_t* params = NULL;

        request = amxc_var_add(amxc_htable_t, requests, NULL);
        amxc_var_add_key(cstring_t, request, "object_path", create_obj->obj_path);
        params = amxc_var_add_key(amxc_llist_t, request, "parameters", NULL);
        for(size_t j = 0; j < create_obj->n_param_settings; j++) {
            Usp__Add__CreateParamSetting* param_setting = create_obj->param_settings[j];
            amxc_var_t* param = NULL;

            param = amxc_var_add(amxc_htable_t, params, NULL);
            amxc_var_add_key(cstring_t, param, "param", param_setting->param);
            amxc_var_add_key(cstring_t, param, "value", param_setting->value);
            amxc_var_add_key(bool, param, "required", param_setting->required);
        }
    }
}

/**
   @brief
   Fill up an AddResp message in case of a successful Add

   @param add_resp the object that will be extended
   @param req_path the requested path that corresponds to this reply
   @param reply_data a variant with the data model reply to an Add request

   @returns 0 in case of success, -1 in case of error
 */
static int uspl_add_populate_resp_success(Usp__AddResp* add_resp,
                                          const char* req_path,
                                          amxc_var_t* reply_data) {
    int retval = -1;
    Usp__AddResp__CreatedObjectResult__OperationStatus__OperationSuccess* success = NULL;
    const char* inst_path = NULL;
    amxc_var_t* params = NULL;

    inst_path = GET_CHAR(reply_data, "path");
    when_null(inst_path, exit);
    params = GET_ARG(reply_data, "parameters");
    when_null(params, exit);
    success = AddResp_OperSuccess(add_resp, req_path, inst_path);
    AddOperSuccess_UniqueKeys(success, params);

    retval = 0;
exit:
    return retval;
}

/**
   @brief
   Fill up an AddResp message in case of a Failed Add

   @param add_resp the object that will be extended
   @param req_path the requested path that corresponds to this reply
   @param reply_data a variant with the data model reply to an Add request

   @returns 0 in case of success, -1 in case of error
 */
static int uspl_add_populate_resp_failure(Usp__AddResp* add_resp,
                                          const char* req_path,
                                          uint32_t err_code,
                                          const char* err_msg) {
    int retval = -1;
    Usp__AddResp__CreatedObjectResult__OperationStatus__OperationFailure* failure = NULL;

    if((err_msg == NULL) || (*err_msg == 0)) {
        err_msg = uspl_error_code_to_str(err_code);
    }

    failure = AddResp_OperFailure(add_resp, req_path, err_code, err_msg);
    when_null(failure, exit);

    retval = 0;
exit:
    return retval;
}

/**
   @brief
   Fill up an AddResp message

   @param add_resp the object that will be extended
   @param reply_data a variant with the data model reply to an Add request

   @returns 0 in case of success, -1 in case of error
 */
static int uspl_add_populate_resp(Usp__AddResp* add_resp, amxc_var_t* reply_data) {
    int retval = -1;
    uint32_t err_code = 999;
    amxc_var_t* result = NULL;
    const char* req_path = NULL;

    result = GET_ARG(reply_data, "result");
    when_null(result, exit);
    req_path = GET_CHAR(result, "requested_path");
    when_null(req_path, exit);

    err_code = GET_UINT32(result, "err_code");
    if(err_code == USP_ERR_OK) {
        retval = uspl_add_populate_resp_success(add_resp, req_path, reply_data);
    } else {
        retval = uspl_add_populate_resp_failure(add_resp, req_path, err_code,
                                                GET_CHAR(result, "err_msg"));
    }

exit:
    return retval;
}

/**
   @brief
   Append the results of a successful Add operation to the list of responses

   @param success an operation success result
   @param resp_list an empty list that will be filled up with variants of the responses
   @param requested_path the original requested path

   @returns 0 in case of success, -1 in case of error
 */
static int uspl_add_resp_append_result_success(Usp__AddResp__CreatedObjectResult__OperationStatus__OperationSuccess* success,
                                               amxc_llist_t* resp_list,
                                               const char* requested_path) {
    int retval = -1;
    amxc_var_t* entry = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* name = NULL;
    amxc_var_t* result = NULL;

    amxc_var_new(&entry);
    amxc_var_set_type(entry, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, entry, "path", success->instantiated_path);
    amxc_var_add_key(cstring_t, entry, "object", success->instantiated_path);
    result = amxc_var_add_key(amxc_htable_t, entry, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", requested_path);
    amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_OK);

    params = amxc_var_add_key(amxc_htable_t, entry, "parameters", NULL);
    for(size_t i = 0; i < success->n_unique_keys; i++) {
        char* key = success->unique_keys[i]->key;
        char* value = success->unique_keys[i]->value;
        amxc_var_t* amx_param = amxc_var_add_key(cstring_t, params, key, value);
        amxc_var_cast(amx_param, AMXC_VAR_ID_ANY);

        if(strcmp(key, "Alias") == 0) {
            name = amxc_var_add_key(cstring_t, entry, "name", value);
        }
    }

    retval = uspl_textutils_get_index(success->instantiated_path);
    if(retval < 0) {
        goto exit;
    }
    amxc_var_add_key(uint32_t, entry, "index", retval);

    // If no Alias was found, use index as name
    if(name == NULL) {
        amxc_string_t index;
        amxc_string_init(&index, 0);
        amxc_string_setf(&index, "%d", retval);
        amxc_var_add_key(cstring_t, entry, "name", amxc_string_get(&index, 0));
        amxc_string_clean(&index);
    }
    amxc_llist_append(resp_list, &entry->lit);

    retval = 0;
exit:
    return retval;
}

/**
   @brief
   Append the results of a failed Add operation to the list of responses

   @param failure an operation failure result
   @param resp_list an empty list that will be filled up with variants of the responses
   @param requested_path the original requested path

   @returns 0
 */
static int uspl_add_resp_append_result_failure(Usp__AddResp__CreatedObjectResult__OperationStatus__OperationFailure* failure,
                                               amxc_llist_t* resp_list,
                                               const char* requested_path) {
    amxc_var_t* entry = NULL;
    amxc_var_t* result = NULL;

    amxc_var_new(&entry);
    amxc_var_set_type(entry, AMXC_VAR_ID_HTABLE);

    result = amxc_var_add_key(amxc_htable_t, entry, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", requested_path);
    amxc_var_add_key(uint32_t, result, "err_code", failure->err_code);
    amxc_var_add_key(cstring_t, result, "err_msg", failure->err_msg);

    amxc_llist_append(resp_list, &entry->lit);

    return 0;
}

/**
   @brief

   @param result a CreatedObjectResult in response to an Add for a single requested path
   @param resp_list an empty list that will be filled up with variants of the responses

   @returns 0 in case of success, -1 in case of error
 */
static int uspl_add_resp_append_result(Usp__AddResp__CreatedObjectResult* result,
                                       amxc_llist_t* resp_list) {
    int retval = -1;

    if(result->oper_status->oper_status_case == USP__ADD_RESP__CREATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS) {
        Usp__AddResp__CreatedObjectResult__OperationStatus__OperationSuccess* success = NULL;
        success = result->oper_status->oper_success;
        retval = uspl_add_resp_append_result_success(success, resp_list, result->requested_path);
    } else if(result->oper_status->oper_status_case == USP__ADD_RESP__CREATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE) {
        Usp__AddResp__CreatedObjectResult__OperationStatus__OperationFailure* failure = NULL;
        failure = result->oper_status->oper_failure;
        retval = uspl_add_resp_append_result_failure(failure, resp_list, result->requested_path);
        goto exit;
    } else {
        SAH_TRACEZ_ERROR(uspl_traceme, "Unknown operation response status");
        goto exit;
    }

exit:
    return retval;
}

/*********************** Start of public functions ****************************/

int uspl_add_new(uspl_tx_t* usp_tx, const amxc_var_t* request) {
    int retval = -1;
    Usp__Msg* msg = NULL;
    amxc_var_t* requests = NULL;
    amxc_string_t msg_id;

    amxc_string_init(&msg_id, 0);

    when_null(usp_tx, exit);
    when_null(request, exit);

    amxc_string_setf(&msg_id, "%d", uspl_msghandler_next_msg_id());

    msg = CreateAdd(amxc_string_get(&msg_id, 0));

    msg->body->request->add->allow_partial = GET_BOOL(request, "allow_partial");

    requests = GET_ARG(request, "requests");
    when_null(requests, exit);

    retval = uspl_add_populate_req(msg->body->request->add, requests);
    when_failed(retval, exit);

    retval = uspl_msghandler_pack_protobuf(msg, usp_tx);
    usp_tx->msg_id = USP_STRDUP(msg->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__ADD;

exit:
    amxc_string_clean(&msg_id);
    uspl_msghandler_free_unpacked_message(msg);

    return retval;
}

int uspl_add_extract(uspl_rx_t* usp_rx, amxc_var_t* result) {
    int retval = USP_ERR_INTERNAL_ERROR;
    Usp__Add* add = NULL;

    when_null(usp_rx, exit);
    when_null(result, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_REQUEST, exit);
    when_null(usp_rx->msg->body->request, exit);
    when_true(usp_rx->msg->body->request->req_type_case != USP__REQUEST__REQ_TYPE_ADD, exit);
    when_null(usp_rx->msg->body->request->add, exit);

    add = usp_rx->msg->body->request->add;

    // Log error message when allow_partial is false, because it is not supported yet
    if(!add->allow_partial) {
        SAH_TRACEZ_ERROR(uspl_traceme, "allow_partial=false is not yet supported and will be ignored");
    }

    uspl_add_populate_var(add, result);

    retval = USP_ERR_OK;
exit:
    return retval;
}

int uspl_add_resp_new(uspl_tx_t* usp_tx, amxc_llist_t* resp_list, const char* msg_id) {
    int retval = -1;
    Usp__Msg* resp = NULL;
    Usp__AddResp* add_resp = NULL;

    when_null(usp_tx, exit);
    when_null(resp_list, exit);
    when_null(msg_id, exit);

    resp = CreateAddResp(msg_id);
    add_resp = resp->body->response->add_resp;

    // Iterate over all replies to different Add requests
    amxc_llist_for_each(it, resp_list) {
        amxc_var_t* reply_data = amxc_llist_it_get_data(it, amxc_var_t, lit);
        retval = uspl_add_populate_resp(add_resp, reply_data);
        when_failed(retval, exit);
    }

    retval = uspl_msghandler_pack_protobuf(resp, usp_tx);
    usp_tx->msg_id = USP_STRDUP(resp->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__ADD_RESP;

exit:
    uspl_msghandler_free_unpacked_message(resp);

    return retval;
}

int uspl_add_resp_extract(uspl_rx_t* usp_rx, amxc_llist_t* resp_list) {
    int retval = USP_ERR_INTERNAL_ERROR;
    Usp__AddResp* resp = NULL;

    when_null(usp_rx, exit);
    when_null(resp_list, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_RESPONSE, exit);
    when_null(usp_rx->msg->body->response, exit);
    when_true(usp_rx->msg->body->response->resp_type_case != USP__RESPONSE__RESP_TYPE_ADD_RESP, exit);
    when_null(usp_rx->msg->body->response->add_resp, exit);

    resp = usp_rx->msg->body->response->add_resp;
    // Exit if there are no results in the response
    retval = USP_ERR_INVALID_ARGUMENTS;
    when_true(resp->n_created_obj_results == 0, exit);
    when_true(resp->created_obj_results == NULL, exit);

    for(size_t i = 0; i < resp->n_created_obj_results; i++) {
        uspl_add_resp_append_result(resp->created_obj_results[i], resp_list);
    }

    retval = USP_ERR_OK;
exit:
    return retval;
}

