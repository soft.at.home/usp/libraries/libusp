/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
   @file uspl_operate.c
   @brief
   Create and extract Operate requests and responses
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxj/amxj_variant.h>

#include <debug/sahtrace.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_error.h"
#include "usp/uspl_operate.h"
#include "usp/uspl_msghandler.h"

#include "uspl_msghandler_priv.h"

#include "bbf/usp_mem.h"
#include "bbf/handle_operate.h"

/**
   @brief
   Dynamically creates an OperRequest object

   @note The object should be deleted using usp__msg__free_unpacked()

   @param msg_id string containing the message id of the operate request

   @return Pointer to the OperRequest object
 */
static Usp__Msg* uspl_operate_create(const char* msg_id, const char* cmd, const char* cmd_key, bool send_resp) {
    Usp__Msg* msg = NULL;
    Usp__Header* header = NULL;
    Usp__Body* body = NULL;
    Usp__Request* request = NULL;
    Usp__Operate* oper = NULL;

    // Allocate and initialise memory to store the parts of the USP message
    msg = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(msg);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    request = USP_MALLOC(sizeof(Usp__Request));
    usp__request__init(request);

    oper = USP_MALLOC(sizeof(Usp__Operate));
    usp__operate__init(oper);

    // Connect the structures together
    msg->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__OPERATE;

    msg->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    body->request = request;
    request->req_type_case = USP__REQUEST__REQ_TYPE_OPERATE;
    request->operate = oper;

    oper->command = USP_STRDUP(cmd);
    oper->command_key = USP_STRDUP(cmd_key);
    oper->send_resp = send_resp;
    oper->n_input_args = 0;
    oper->input_args = NULL;

    return msg;
}

/**
   @brief
   Add an input argument to the Operate request

   @param oper the USP operate request that will be extended
   @param hit a hash table iterator corresponding to a hash table key-value pair

   @return Pointer to an OperResponse object
 */
static void uspl_operate_add_input_arg(Usp__Operate* oper, amxc_htable_it_t* hit) {
    Usp__Operate__InputArgsEntry* entry = NULL;
    int new_num = 0;
    amxc_var_t* arg = amxc_var_from_htable_it(hit);
    const char* value = NULL;

    // Allocate memory to store the create_object entry
    entry = USP_MALLOC(sizeof(Usp__Operate__InputArgsEntry));
    usp__operate__input_args_entry__init(entry);

    new_num = oper->n_input_args + 1;
    oper->input_args = USP_REALLOC(oper->input_args, new_num * sizeof(void*));
    oper->n_input_args = new_num;
    oper->input_args[new_num - 1] = entry;

    // Add composite parameters in JSON string format
    if((amxc_var_type_of(arg) == AMXC_VAR_ID_HTABLE) ||
       (amxc_var_type_of(arg) == AMXC_VAR_ID_LIST)) {
        amxc_var_cast(arg, AMXC_VAR_ID_JSON);
        value = amxc_var_constcast(jstring_t, arg);
    } else {
        amxc_var_cast(arg, AMXC_VAR_ID_CSTRING);
        value = amxc_var_constcast(cstring_t, arg);
    }

    // Fill up the input_args entry
    entry->key = USP_STRDUP(amxc_htable_it_get_key(hit));
    entry->value = USP_STRDUP(value);
}

/**
   @brief
   Build the variant that corresponds to an operate request

   @param oper the USP operate request that will be parsed
   @param result the variant that will be extended

   @return USP specific error code
 */
static int uspl_operate_populate_var(Usp__Operate* operate, amxc_var_t* result) {
    int retval = USP_ERR_INVALID_ARGUMENTS;
    amxc_var_t* input_args = NULL;

    when_true(operate->command == NULL || *operate->command == 0, exit);
    when_true(operate->command_key == NULL || *operate->command_key == 0, exit);

    amxc_var_set_type(result, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, result, "command", operate->command);
    amxc_var_add_key(cstring_t, result, "command_key", operate->command_key);
    amxc_var_add_key(bool, result, "send_resp", operate->send_resp);

    if((operate->n_input_args == 0) || (operate->input_args == NULL)) {
        retval = USP_ERR_OK;
        goto exit;
    }

    input_args = amxc_var_add_key(amxc_htable_t, result, "input_args", NULL);
    for(size_t i = 0; i < operate->n_input_args; i++) {
        Usp__Operate__InputArgsEntry* entry = operate->input_args[i];
        when_null(entry, exit);

        if(*entry->value == 0) {
            amxc_var_add_key(cstring_t, input_args, entry->key, "");
        } else {
            amxc_var_t* dst_param = amxc_var_add_key(cstring_t, input_args, entry->key, "");
            if(amxc_var_set(jstring_t, dst_param, entry->value) != 0) {
                amxc_var_set(cstring_t, dst_param, entry->value);
            }
            amxc_var_cast(dst_param, AMXC_VAR_ID_ANY);
        }
    }

    retval = USP_ERR_OK;
exit:
    return retval;
}

/**
   @brief
   Adds the request object path of an asynchronous command to an OperationResponse object

   @param oper_resp pointer to operation response object to add this entry to
   @param cmd path to asynchronous operation which was started
   @param reply_data variant containing data for a single operate response entry

   @return None
 */
static int uspl_operate_resp_req_obj_path(Usp__OperateResp* oper_resp, const char* cmd, amxc_var_t* reply_data) {
    Usp__OperateResp__OperationResult* oper_result = NULL;
    size_t new_num = 0;    // new number of entries in the operation_result array
    int retval = -1;
    const char* req_obj_path = GET_CHAR(reply_data, "req_obj_path");

    when_null(req_obj_path, exit);

    // Allocate memory to store the structures
    oper_result = USP_MALLOC(sizeof(Usp__OperateResp__OperationResult));
    usp__operate_resp__operation_result__init(oper_result);

    // Increase the size of the vector
    new_num = oper_resp->n_operation_results + 1;
    oper_resp->operation_results = USP_REALLOC(oper_resp->operation_results, new_num * sizeof(void*));
    oper_resp->n_operation_results = new_num;
    oper_resp->operation_results[new_num - 1] = oper_result;

    // Initialise the operation result
    oper_result->executed_command = USP_STRDUP(cmd);
    oper_result->operation_resp_case = USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OBJ_PATH;
    oper_result->req_obj_path = USP_STRDUP(req_obj_path);

    retval = 0;
exit:
    return retval;
}

static void uspl_operate_resp_req_output_args_entries(Usp__OperateResp__OperationResult__OutputArgs* output_args,
                                                      const amxc_htable_t* args_table) {
    int i = 0;

    when_null(args_table, exit);

    // Iterate over all output arguments, adding them to the operate result object
    amxc_htable_iterate(it, args_table) {
        amxc_var_t* arg = amxc_var_from_htable_it(it);
        Usp__OperateResp__OperationResult__OutputArgs__OutputArgsEntry* entry = NULL;
        const char* value = NULL;

        entry = USP_MALLOC(sizeof(Usp__OperateResp__OperationResult__OutputArgs__OutputArgsEntry));
        usp__operate_resp__operation_result__output_args__output_args_entry__init(entry);
        output_args->output_args[i] = entry;

        // Add composite parameters in JSON string format
        if((amxc_var_type_of(arg) == AMXC_VAR_ID_HTABLE) ||
           (amxc_var_type_of(arg) == AMXC_VAR_ID_LIST)) {
            amxc_var_cast(arg, AMXC_VAR_ID_JSON);
            value = amxc_var_constcast(jstring_t, arg);
        } else {
            amxc_var_cast(arg, AMXC_VAR_ID_CSTRING);
            value = amxc_var_constcast(cstring_t, arg);
        }

        entry->key = USP_STRDUP(amxc_htable_it_get_key(it));
        entry->value = USP_STRDUP(value);
        i++;
    }

exit:
    return;
}

/**
   @brief
   Adds the output argument response of a synchronous command to an OperationResponse object

   @param oper_resp pointer to operation response object to add this entry to
   @param cmd path to synchronous operation which succeeded
   @param reply_data variant containing data for a single operate response entry

   @return None
 */
static int uspl_operate_resp_req_output_args(Usp__OperateResp* oper_resp, const char* cmd, amxc_var_t* reply_data) {
    Usp__OperateResp__OperationResult* oper_res = NULL;
    Usp__OperateResp__OperationResult__OutputArgs* output_args = NULL;
    int new_num = 0;    // new number of entries in the operation_result array
    int num_args = 0;
    const amxc_htable_t* args_table = amxc_var_constcast(amxc_htable_t, GET_ARG(reply_data, "output_args"));

    // Allocate memory to store the structures
    oper_res = USP_MALLOC(sizeof(Usp__OperateResp__OperationResult));
    usp__operate_resp__operation_result__init(oper_res);

    output_args = USP_MALLOC(sizeof(Usp__OperateResp__OperationResult__OutputArgs));
    usp__operate_resp__operation_result__output_args__init(output_args);

    // Increase the size of the vector
    new_num = oper_resp->n_operation_results + 1;
    oper_resp->operation_results = USP_REALLOC(oper_resp->operation_results, new_num * sizeof(void*));
    oper_resp->n_operation_results = new_num;
    oper_resp->operation_results[new_num - 1] = oper_res;

    // Initialise the operation result
    oper_res->executed_command = USP_STRDUP(cmd);
    oper_res->operation_resp_case = USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OUTPUT_ARGS;
    oper_res->req_output_args = output_args;

    // Initialise the output args object
    num_args = amxc_htable_size(args_table);
    output_args->n_output_args = num_args;
    output_args->output_args = USP_MALLOC(num_args * sizeof(void*));

    uspl_operate_resp_req_output_args_entries(output_args, args_table);

    return 0;
}

/**
   @brief
   Adds a command failure to an OperationResponse object

   @param oper_resp pointer to operation response object to add this entry to
   @param path path to operation which failed
   @param reply_data variant containing data for a single operate response entry

   @return  None
 */
static int uspl_operate_resp_cmd_failure(Usp__OperateResp* oper_resp, const char* cmd, amxc_var_t* reply_data) {
    Usp__OperateResp__OperationResult* oper_res = NULL;
    Usp__OperateResp__OperationResult__CommandFailure* cmd_failure = NULL;
    int new_num = 0;    // new number of entries in the operation_result array
    int retval = -1;
    amxc_var_t* failure_var = NULL;
    const char* err_msg = NULL;

    failure_var = GET_ARG(reply_data, "cmd_failure");
    when_null(failure_var, exit);

    err_msg = GET_CHAR(failure_var, "err_msg");
    when_null(err_msg, exit);

    // Allocate memory to store the structures
    oper_res = USP_MALLOC(sizeof(Usp__OperateResp__OperationResult));
    usp__operate_resp__operation_result__init(oper_res);

    cmd_failure = USP_MALLOC(sizeof(Usp__OperateResp__OperationResult__CommandFailure));
    usp__operate_resp__operation_result__command_failure__init(cmd_failure);

    // Increase the size of the vector
    new_num = oper_resp->n_operation_results + 1;
    oper_resp->operation_results = USP_REALLOC(oper_resp->operation_results, new_num * sizeof(void*));
    oper_resp->n_operation_results = new_num;
    oper_resp->operation_results[new_num - 1] = oper_res;

    // Initialise the operation result
    oper_res->executed_command = USP_STRDUP(cmd);
    oper_res->operation_resp_case = USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_CMD_FAILURE;
    oper_res->cmd_failure = cmd_failure;

    // Initialise the command failure
    cmd_failure->err_code = amxc_var_dyncast(uint32_t, GET_ARG(failure_var, "err_code"));
    cmd_failure->err_msg = USP_STRDUP(err_msg);

    retval = 0;
exit:
    return retval;
}

static int uspl_operate_resp_populate(Usp__OperateResp* oper_resp, amxc_var_t* reply_data) {
    int retval = -1;
    const char* executed_cmd = NULL;
    uint32_t oper_case = 999;

    executed_cmd = GET_CHAR(reply_data, "executed_command");
    when_null(executed_cmd, exit);

    oper_case = amxc_var_dyncast(uint32_t, GET_ARG(reply_data, "operation_resp_case"));
    switch(oper_case) {
    case USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OBJ_PATH:
        retval = uspl_operate_resp_req_obj_path(oper_resp, executed_cmd, reply_data);
        break;
    case USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OUTPUT_ARGS:
        retval = uspl_operate_resp_req_output_args(oper_resp, executed_cmd, reply_data);
        break;
    case USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_CMD_FAILURE:
        retval = uspl_operate_resp_cmd_failure(oper_resp, executed_cmd, reply_data);
        break;
    default:
        goto exit;
    }

exit:
    return retval;
}

static void uspl_operate_resp_append_result_req_obj_path(const char* req_obj_path,
                                                         const char* cmd,
                                                         amxc_llist_t* resp_list) {
    amxc_var_t* entry = NULL;

    amxc_var_new(&entry);
    amxc_var_set_type(entry, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, entry, "executed_command", cmd);
    amxc_var_add_key(uint32_t, entry, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OBJ_PATH);
    amxc_var_add_key(cstring_t, entry, "req_obj_path", req_obj_path);

    amxc_llist_append(resp_list, &entry->lit);
}

static void uspl_operate_resp_append_result_req_output_args(Usp__OperateResp__OperationResult__OutputArgs* output_args,
                                                            const char* cmd,
                                                            amxc_llist_t* resp_list) {
    amxc_var_t* entry = NULL;
    amxc_var_t* args_var = NULL;

    amxc_var_new(&entry);
    amxc_var_set_type(entry, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, entry, "executed_command", cmd);
    amxc_var_add_key(uint32_t, entry, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OUTPUT_ARGS);
    args_var = amxc_var_add_key(amxc_htable_t, entry, "output_args", NULL);

    for(size_t i = 0; i < output_args->n_output_args; i++) {
        Usp__OperateResp__OperationResult__OutputArgs__OutputArgsEntry* out_arg = output_args->output_args[i];
        when_null(out_arg, exit);

        if(*out_arg->value == 0) {
            amxc_var_add_key(cstring_t, args_var, out_arg->key, "");
        } else {
            amxc_var_t* dst_param = amxc_var_add_key(cstring_t, args_var, out_arg->key, "");
            if(amxc_var_set(jstring_t, dst_param, out_arg->value) != 0) {
                amxc_var_set(cstring_t, dst_param, out_arg->value);
            }
            amxc_var_cast(dst_param, AMXC_VAR_ID_ANY);
        }
    }

exit:
    amxc_llist_append(resp_list, &entry->lit);
}

static void uspl_operate_resp_append_result_cmd_failure(Usp__OperateResp__OperationResult__CommandFailure* cmd_failure,
                                                        const char* cmd,
                                                        amxc_llist_t* resp_list) {
    amxc_var_t* entry = NULL;
    amxc_var_t* failure_var = NULL;

    amxc_var_new(&entry);
    amxc_var_set_type(entry, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, entry, "executed_command", cmd);
    amxc_var_add_key(uint32_t, entry, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_CMD_FAILURE);
    failure_var = amxc_var_add_key(amxc_htable_t, entry, "cmd_failure", NULL);

    amxc_var_add_key(cstring_t, failure_var, "err_msg", cmd_failure->err_msg);
    amxc_var_add_key(uint32_t, failure_var, "err_code", cmd_failure->err_code);

    amxc_llist_append(resp_list, &entry->lit);
}

/**
   @brief

   @param result a OperateResp__OperationResult in response to a single executed operation
   @param resp_list an empty list that will be filled up with variants of the responses

   @returns 0 in case of success, -1 in case of error
 */
static int uspl_operate_resp_append_result(Usp__OperateResp__OperationResult* result,
                                           amxc_llist_t* resp_list) {
    int retval = 0;

    switch(result->operation_resp_case) {
    case USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OBJ_PATH:
        uspl_operate_resp_append_result_req_obj_path(result->req_obj_path, result->executed_command, resp_list);
        break;
    case USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OUTPUT_ARGS:
        uspl_operate_resp_append_result_req_output_args(result->req_output_args, result->executed_command, resp_list);
        break;
    case USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_CMD_FAILURE:
        uspl_operate_resp_append_result_cmd_failure(result->cmd_failure, result->executed_command, resp_list);
        break;
    default:
        SAH_TRACEZ_ERROR(uspl_traceme, "Unknown operation response status");
        retval = USP_ERR_INVALID_ARGUMENTS;
        goto exit;
    }

exit:
    return retval;
}

/*********************** Start of public functions ****************************/

int uspl_operate_new(uspl_tx_t* usp_tx, const amxc_var_t* request) {
    int retval = -1;
    Usp__Msg* msg = NULL;
    bool send_resp = false;
    const char* cmd = NULL;
    const char* cmd_key = NULL;
    const amxc_htable_t* input_args = NULL;
    Usp__Operate* oper = NULL;
    amxc_string_t msg_id;

    amxc_string_init(&msg_id, 0);

    when_null(usp_tx, exit);
    when_null(request, exit);

    amxc_string_setf(&msg_id, "%d", uspl_msghandler_next_msg_id());

    send_resp = GET_BOOL(request, "send_resp");
    cmd = GET_CHAR(request, "command");
    when_null(cmd, exit);
    cmd_key = GET_CHAR(request, "command_key");
    when_null(cmd_key, exit);

    msg = uspl_operate_create(amxc_string_get(&msg_id, 0), cmd, cmd_key, send_resp);
    oper = msg->body->request->operate;

    input_args = amxc_var_constcast(amxc_htable_t, GET_ARG(request, "input_args"));
    amxc_htable_iterate(it, input_args) {
        uspl_operate_add_input_arg(oper, it);
    }

    retval = uspl_msghandler_pack_protobuf(msg, usp_tx);
    usp_tx->msg_id = USP_STRDUP(msg->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__OPERATE;

exit:
    amxc_string_clean(&msg_id);
    uspl_msghandler_free_unpacked_message(msg);

    return retval;
}

int uspl_operate_extract(uspl_rx_t* usp_rx, amxc_var_t* result) {
    int retval = USP_ERR_INTERNAL_ERROR;

    when_null(usp_rx, exit);
    when_null(result, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_REQUEST, exit);
    when_null(usp_rx->msg->body->request, exit);
    when_true(usp_rx->msg->body->request->req_type_case != USP__REQUEST__REQ_TYPE_OPERATE, exit);
    when_null(usp_rx->msg->body->request->operate, exit);

    retval = uspl_operate_populate_var(usp_rx->msg->body->request->operate, result);
    when_failed(retval, exit);

    retval = USP_ERR_OK;
exit:
    return retval;
}

int uspl_operate_resp_new(uspl_tx_t* usp_tx, amxc_llist_t* resp_list, const char* msg_id) {
    int retval = -1;
    Usp__Msg* resp = NULL;
    Usp__OperateResp* oper_resp = NULL;

    when_null(usp_tx, exit);
    when_null(resp_list, exit);
    when_null(msg_id, exit);

    resp = CreateOperResp(msg_id);
    oper_resp = resp->body->response->operate_resp;

    // Iterate over all replies to different Operate requests
    amxc_llist_for_each(it, resp_list) {
        amxc_var_t* reply_data = amxc_llist_it_get_data(it, amxc_var_t, lit);
        retval = uspl_operate_resp_populate(oper_resp, reply_data);
        when_failed(retval, exit);
    }

    retval = uspl_msghandler_pack_protobuf(resp, usp_tx);
    usp_tx->msg_id = USP_STRDUP(resp->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__OPERATE_RESP;

exit:
    uspl_msghandler_free_unpacked_message(resp);

    return retval;
}

int uspl_operate_resp_extract(uspl_rx_t* usp_rx, amxc_llist_t* resp_list) {
    int retval = USP_ERR_INTERNAL_ERROR;
    Usp__OperateResp* resp = NULL;

    when_null(usp_rx, exit);
    when_null(resp_list, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_RESPONSE, exit);
    when_null(usp_rx->msg->body->response, exit);
    when_true(usp_rx->msg->body->response->resp_type_case != USP__RESPONSE__RESP_TYPE_OPERATE_RESP, exit);
    when_null(usp_rx->msg->body->response->operate_resp, exit);

    resp = usp_rx->msg->body->response->operate_resp;
    // Exit if there are no results in the response
    retval = USP_ERR_INVALID_ARGUMENTS;
    when_true(resp->n_operation_results == 0, exit);
    when_true(resp->operation_results == NULL, exit);

    for(size_t i = 0; i < resp->n_operation_results; i++) {
        retval = uspl_operate_resp_append_result(resp->operation_results[i], resp_list);
        when_failed(retval, exit);
    }

    retval = USP_ERR_OK;
exit:
    return retval;
}
