/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
   @file uspl_get_supported_dm.c
   @brief
   Create a GetSupportedDM message or a GetSupportedDMResponse
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>

#include <debug/sahtrace.h>

#include "usp/usp_err_codes.h"
#include "usp/uspl_error.h"
#include "usp/uspl_get_supported_dm.h"
#include "usp/uspl_msghandler.h"

#include "uspl_msghandler_priv.h"
#include "uspl_private.h"

#include "bbf/usp_mem.h"
#include "bbf/handle_get_supported_dm.h"

// URI of data model implemented by USP Agent
#define BBF_DATA_MODEL_URI "urn:broadband-forum-org:tr-181-2-16-0"

/**
   @brief
   Dynamically creates a GetSupportedDM object

   @note The object should be deleted using usp__msg__free_unpacked()

   @param msg_id string containing the message id of the request, which initiated this response

   @return Pointer to a GetSupportedDM object
 */
static Usp__Msg* CreateGetSupportedDM(const char* msg_id) {
    Usp__Msg* msg;
    Usp__Header* header;
    Usp__Body* body;
    Usp__Request* request;
    Usp__GetSupportedDM* get_sup;

    // Allocate memory to store the USP message
    msg = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(msg);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    request = USP_MALLOC(sizeof(Usp__Request));
    usp__request__init(request);

    get_sup = USP_MALLOC(sizeof(Usp__GetSupportedDM));
    usp__get_supported_dm__init(get_sup);

    // Connect the structures together
    msg->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM;

    msg->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_REQUEST;
    body->request = request;

    request->req_type_case = USP__REQUEST__REQ_TYPE_GET_SUPPORTED_DM;
    request->get_supported_dm = get_sup;

    return msg;
}

/**
   @brief
   Dynamically adds a SupportedObjResult to a RequestedObjResult object

   @param ror pointer to RequestedObjResult object
   @param node Data model node containing details to add
   @param permissions permissions bitmask for the node

   @return Pointer to a SupportedObjResult object
 */
static Usp__GetSupportedDMResp__SupportedObjectResult*
AddReqObjResult_SupportedObjResult(Usp__GetSupportedDMResp__RequestedObjectResult* ror, const char* obj_path, amxc_var_t* obj_var) {
    Usp__GetSupportedDMResp__SupportedObjectResult* sor;
    int new_num;    // new number of entries in the requested obj result array

    // Allocate memory to store the SupportedObjResult object
    sor = USP_MALLOC(sizeof(Usp__GetSupportedDMResp__SupportedObjectResult));
    usp__get_supported_dmresp__supported_object_result__init(sor);

    // Increase the size of the vector
    new_num = ror->n_supported_objs + 1;
    ror->supported_objs = USP_REALLOC(ror->supported_objs, new_num * sizeof(void*));
    ror->n_supported_objs = new_num;
    ror->supported_objs[new_num - 1] = sor;

    // Fill in the SupportedObjResult object. Path must include a trailing '.'
    sor->supported_obj_path = USP_STRDUP(obj_path);

    // Determine properties
    sor->access = GET_UINT32(obj_var, "access");
    sor->is_multi_instance = GET_BOOL(obj_var, "is_multi_instance");

    return sor;
}


static uint32_t uspl_param_amx_type_to_usp_type(uint32_t amx_type) {
    uint32_t retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_UNKNOWN;

    switch(amx_type) {
    case AMXC_VAR_ID_CSTRING:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_STRING;
        break;
    case AMXC_VAR_ID_INT8:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_INT;
        break;
    case AMXC_VAR_ID_INT16:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_INT;
        break;
    case AMXC_VAR_ID_INT32:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_INT;
        break;
    case AMXC_VAR_ID_INT64:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_LONG;
        break;
    case AMXC_VAR_ID_UINT8:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_UNSIGNED_INT;
        break;
    case AMXC_VAR_ID_UINT16:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_UNSIGNED_INT;
        break;
    case AMXC_VAR_ID_UINT32:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_UNSIGNED_INT;
        break;
    case AMXC_VAR_ID_UINT64:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_UNSIGNED_LONG;
        break;
    case AMXC_VAR_ID_FLOAT:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_DECIMAL;
        break;
    case AMXC_VAR_ID_DOUBLE:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_DECIMAL;
        break;
    case AMXC_VAR_ID_BOOL:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_BOOLEAN;
        break;
    case AMXC_VAR_ID_LIST:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_STRING;
        break;
    case AMXC_VAR_ID_HTABLE:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_STRING;
        break;
    case AMXC_VAR_ID_FD:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_INT;
        break;
    case AMXC_VAR_ID_TIMESTAMP:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_DATE_TIME;
        break;
    case AMXC_VAR_ID_CSV_STRING:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_STRING;
        break;
    case AMXC_VAR_ID_SSV_STRING:
        retval = USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_STRING;
        break;
    default:
        break;
    }

    return retval;
}

static uint32_t uspl_param_usp_type_to_amx_type(uint32_t usp_type) {
    uint32_t retval = AMXC_VAR_ID_CSTRING;

    switch(usp_type) {
    case USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_BASE_64:
        retval = AMXC_VAR_ID_CSTRING;
        break;
    case USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_BOOLEAN:
        retval = AMXC_VAR_ID_BOOL;
        break;
    case USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_DATE_TIME:
        retval = AMXC_VAR_ID_TIMESTAMP;
        break;
    case USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_DECIMAL:
        retval = AMXC_VAR_ID_FLOAT;
        break;
    case USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_HEX_BINARY:
        retval = AMXC_VAR_ID_CSTRING;
        break;
    case USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_INT:
        retval = AMXC_VAR_ID_INT32;
        break;
    case USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_LONG:
        retval = AMXC_VAR_ID_INT64;
        break;
    case USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_STRING:
        retval = AMXC_VAR_ID_CSTRING;
        break;
    case USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_UNSIGNED_INT:
        retval = AMXC_VAR_ID_UINT32;
        break;
    case USP__GET_SUPPORTED_DMRESP__PARAM_VALUE_TYPE__PARAM_UNSIGNED_LONG:
        retval = AMXC_VAR_ID_UINT64;
        break;
    default:
        break;
    }

    return retval;
}

/**
   @brief
   Dynamically adds a SupportedParamResult to a SupportedObjResult object

   @param sor pointer to the SupportedObjResult object
   @param node Data model node containing details to add
   @param permissions permissions bitmask for the node

   @return  None
 */
static void AddSupportedObjResult_SupportedParamResult(Usp__GetSupportedDMResp__SupportedObjectResult* sor, amxc_var_t* param_var) {
    Usp__GetSupportedDMResp__SupportedParamResult* pr;
    int new_num;    // new number of entries in the requested obj result array

    // Allocate memory to store the SupportedParamResult object
    pr = USP_MALLOC(sizeof(Usp__GetSupportedDMResp__SupportedParamResult));
    usp__get_supported_dmresp__supported_param_result__init(pr);

    // Increase the size of the vector
    new_num = sor->n_supported_params + 1;
    sor->supported_params = USP_REALLOC(sor->supported_params, new_num * sizeof(void*));
    sor->n_supported_params = new_num;
    sor->supported_params[new_num - 1] = pr;

    // Fill in the SupportedCommandResult object
    pr->param_name = USP_STRDUP(GET_CHAR(param_var, "param_name"));
    pr->access = GET_UINT32(param_var, "access");
    pr->value_change = GET_UINT32(param_var, "value_change");
    pr->value_type = uspl_param_amx_type_to_usp_type(GET_UINT32(param_var, "type"));
}

/**
   @brief
   Dynamically adds a SupportedCommandResult to a SupportedObjResult object

   @param sor pointer to the SupportedObjResult object
   @param node Data model node containing details to add

   @return  None
 */
static void AddSupportedObjResult_SupportedCommandResult(Usp__GetSupportedDMResp__SupportedObjectResult* sor, amxc_var_t* cmd_var) {
    Usp__GetSupportedDMResp__SupportedCommandResult* cr = NULL;
    int new_num = 0;    // new number of entries in the requested obj result array
    int i = 0;
    const amxc_llist_t* inputs_list = NULL;
    const amxc_llist_t* outputs_list = NULL;

    // Allocate memory to store the SupportedCommandResult object
    cr = USP_MALLOC(sizeof(Usp__GetSupportedDMResp__SupportedCommandResult));
    usp__get_supported_dmresp__supported_command_result__init(cr);

    // Increase the size of the vector
    new_num = sor->n_supported_commands + 1;
    sor->supported_commands = USP_REALLOC(sor->supported_commands, new_num * sizeof(void*));
    sor->n_supported_commands = new_num;
    sor->supported_commands[new_num - 1] = cr;

    // Fill in the SupportedCommandResult object
    cr->command_name = USP_STRDUP(GET_CHAR(cmd_var, "command_name"));
    cr->command_type = GET_UINT32(cmd_var, "command_type");

    // Copy the command's input arguments into the SupportedCommandResult
    inputs_list = amxc_var_constcast(amxc_llist_t, GET_ARG(cmd_var, "input_arg_names"));
    cr->n_input_arg_names = amxc_llist_size(inputs_list);
    cr->input_arg_names = USP_MALLOC(cr->n_input_arg_names * sizeof(void*));
    i = 0;
    amxc_llist_iterate(it, inputs_list) {
        const char* param_name = amxc_var_constcast(cstring_t, amxc_var_from_llist_it(it));
        cr->input_arg_names[i] = USP_STRDUP(param_name);
        i++;
    }
    // Copy the command's input arguments into the SupportedCommandResult
    outputs_list = amxc_var_constcast(amxc_llist_t, GET_ARG(cmd_var, "output_arg_names"));
    cr->n_output_arg_names = amxc_llist_size(outputs_list);
    cr->output_arg_names = USP_MALLOC(cr->n_output_arg_names * sizeof(void*));
    i = 0;
    amxc_llist_iterate(it, outputs_list) {
        const char* param_name = amxc_var_constcast(cstring_t, amxc_var_from_llist_it(it));
        cr->output_arg_names[i] = USP_STRDUP(param_name);
        i++;
    }
}

/**
   @brief
   Dynamically adds a SupportedEventResult to a SupportedObjResult object

   @param sor pointer to the SupportedObjResult object
   @param node Data model node containing details to add

   @return None
 */
static void AddSupportedObjResult_SupportedEventResult(Usp__GetSupportedDMResp__SupportedObjectResult* sor, amxc_var_t* event_var) {
    Usp__GetSupportedDMResp__SupportedEventResult* er = NULL;
    int new_num = 0;    // new number of entries in the requested obj result array
    int i = 0;
    const amxc_llist_t* args_list = NULL;

    // Allocate memory to store the SupportedEventResult object
    er = USP_MALLOC(sizeof(Usp__GetSupportedDMResp__SupportedEventResult));
    usp__get_supported_dmresp__supported_event_result__init(er);

    // Increase the size of the vector
    new_num = sor->n_supported_events + 1;
    sor->supported_events = USP_REALLOC(sor->supported_events, new_num * sizeof(void*));
    sor->n_supported_events = new_num;
    sor->supported_events[new_num - 1] = er;

    // Fill in the SupportedCommandResult object
    er->event_name = USP_STRDUP(amxc_var_key(event_var));

    // Copy the event's arguments into the SupportedEventResult
    args_list = amxc_var_constcast(amxc_llist_t, event_var);
    er->n_arg_names = amxc_llist_size(args_list);
    er->arg_names = USP_MALLOC(er->n_arg_names * sizeof(void*));
    i = 0;
    amxc_llist_iterate(it, args_list) {
        const char* arg_name = amxc_var_constcast(cstring_t, amxc_var_from_llist_it(it));
        er->arg_names[i] = USP_STRDUP(arg_name);
        i++;
    }
}

static void WalkSchema(Usp__GetSupportedDMResp__RequestedObjectResult* ror, amxc_var_t* dm_response) {
    Usp__GetSupportedDMResp__SupportedObjectResult* sor = NULL;

    const amxc_htable_t* obj_table = amxc_var_constcast(amxc_htable_t, dm_response);
    amxc_htable_iterate(it, obj_table) {
        amxc_var_t* object_var = amxc_var_from_htable_it(it);
        const char* object_path = amxc_htable_it_get_key(it);
        const amxc_llist_t* param_list = NULL;
        const amxc_llist_t* cmd_list = NULL;
        amxc_var_t* event_table = NULL;

        if(strcmp(object_path, "result") == 0) {
            continue;
        }

        sor = AddReqObjResult_SupportedObjResult(ror, object_path, object_var);

        param_list = amxc_var_constcast(amxc_llist_t, GET_ARG(object_var, "supported_params"));
        amxc_llist_iterate(it2, param_list) {
            amxc_var_t* param_var = amxc_var_from_llist_it(it2);
            AddSupportedObjResult_SupportedParamResult(sor, param_var);
        }

        cmd_list = amxc_var_constcast(amxc_llist_t, GET_ARG(object_var, "supported_commands"));
        amxc_llist_iterate(it2, cmd_list) {
            amxc_var_t* cmd_var = amxc_var_from_llist_it(it2);
            AddSupportedObjResult_SupportedCommandResult(sor, cmd_var);
        }

        event_table = GET_ARG(object_var, "supported_events");
        amxc_var_for_each(event, event_table) {
            AddSupportedObjResult_SupportedEventResult(sor, event);
        }
    }
}

/**
   @brief
   Process each requested path

   @param gs_resp pointer to GetSupportedDMResponse object of USP response message
   @param dm_response pointer to variant containing data model response for this requested path

   @return None - This code must handle any errors by reporting errors in the response message
 */
static void ProcessSupportedPathInstances(Usp__GetSupportedDMResp* gs_resp, amxc_var_t* dm_response) {
    Usp__GetSupportedDMResp__RequestedObjectResult* ror;
    amxc_var_t* err_code_var = NULL;
    amxc_var_t* result_var = NULL;
    const char* req_path = NULL;
    uint32_t err_code = 999;

    result_var = GET_ARG(dm_response, "result");
    when_null(result_var, exit);

    req_path = GET_CHAR(result_var, "requested_path");
    when_null(req_path, exit);

    err_code_var = GET_ARG(result_var, "err_code");
    when_null(err_code_var, exit);

    err_code = amxc_var_dyncast(uint32_t, err_code_var);

    // Exit if there was an error in the datamodel for this requested path
    if(err_code != 0) {
        const char* err_msg = uspl_error_code_to_str(err_code);
        ror = AddGetSupportedDM_ReqObjResult(gs_resp, req_path, err_code, err_msg, BBF_DATA_MODEL_URI);
        (void) ror; // Keep Clang static analyser happy
        return;
    }

    // Add a requested object result, since we will have at least one object
    ror = AddGetSupportedDM_ReqObjResult(gs_resp, req_path, USP_ERR_OK, "", BBF_DATA_MODEL_URI);

    // Recurse through the schema, building up the response
    WalkSchema(ror, dm_response);

exit:
    return;
}

/**
   @brief

   @param scr a single supported command result that is part of the get supported dm response
   @param cmd_list the list that will be filled up with the supported command information

   @returns
 */
static void uspl_get_supported_dm_resp_append_cmd(Usp__GetSupportedDMResp__SupportedCommandResult* scr,
                                                  amxc_var_t* cmd_list) {
    amxc_var_t* cmd_var = NULL;
    amxc_var_t* inputs_list = NULL;
    amxc_var_t* outputs_list = NULL;

    cmd_var = amxc_var_add(amxc_htable_t, cmd_list, NULL);
    amxc_var_add_key(cstring_t, cmd_var, "command_name", scr->command_name);
    amxc_var_add_key(uint32_t, cmd_var, "command_type", scr->command_type);
    inputs_list = amxc_var_add_key(amxc_llist_t, cmd_var, "input_arg_names", NULL);
    outputs_list = amxc_var_add_key(amxc_llist_t, cmd_var, "output_arg_names", NULL);

    for(size_t i = 0; i < scr->n_input_arg_names; i++) {
        amxc_var_add(cstring_t, inputs_list, scr->input_arg_names[i]);
    }

    for(size_t i = 0; i < scr->n_output_arg_names; i++) {
        amxc_var_add(cstring_t, outputs_list, scr->output_arg_names[i]);
    }
}

/**
   @brief

   @param spr a single supported param result that is part of the get supported dm response
   @param cmd_list the list that will be filled up with the supported param information

   @returns
 */
static void uspl_get_supported_dm_resp_append_param(Usp__GetSupportedDMResp__SupportedParamResult* spr,
                                                    amxc_var_t* param_list) {
    amxc_var_t* param_var = amxc_var_add(amxc_htable_t, param_list, NULL);
    amxc_var_add_key(uint32_t, param_var, "access", spr->access);
    amxc_var_add_key(cstring_t, param_var, "param_name", spr->param_name);
    amxc_var_add_key(uint32_t, param_var, "type", uspl_param_usp_type_to_amx_type(spr->value_type));
    amxc_var_add_key(uint32_t, param_var, "value_change", spr->value_change);
}

/**
   @brief

   @param ser a single supported event result that is part of the get supported dm response
   @param cmd_list the list that will be filled up with the supported event information

   @returns
 */
static void uspl_get_supported_dm_resp_append_event(Usp__GetSupportedDMResp__SupportedEventResult* ser,
                                                    amxc_var_t* event_table) {
    amxc_var_t* event = amxc_var_add_key(amxc_llist_t, event_table, ser->event_name, NULL);

    for(size_t i = 0; i < ser->n_arg_names; i++) {
        amxc_var_add(cstring_t, event, ser->arg_names[i]);
    }
}

/**
   @brief

   @param result the result of a get supported dm response for a single requested path
   @param resp_list an empty list that will be filled up with variants of the responses

   @returns
 */
static void uspl_get_supported_dm_resp_append_result(Usp__GetSupportedDMResp__RequestedObjectResult* result,
                                                     amxc_llist_t* resp_list) {
    amxc_var_t* entry = NULL;
    amxc_var_t* result_var = NULL;

    amxc_var_new(&entry);
    amxc_var_set_type(entry, AMXC_VAR_ID_HTABLE);

    result_var = amxc_var_add_key(amxc_htable_t, entry, "result", NULL);
    amxc_var_add_key(cstring_t, result_var, "requested_path", result->req_obj_path);
    amxc_var_add_key(uint32_t, result_var, "err_code", result->err_code);

    if(result->err_msg != NULL) {
        amxc_var_add_key(cstring_t, result_var, "err_msg", result->err_msg);
    }
    if(result->err_code != 0) {
        amxc_llist_append(resp_list, &entry->lit);
        goto exit;
    }

    for(size_t i = 0; i < result->n_supported_objs; i++) {
        Usp__GetSupportedDMResp__SupportedObjectResult* sor = result->supported_objs[i];
        amxc_var_t* sor_table = NULL;
        amxc_var_t* cmd_list = NULL;
        amxc_var_t* param_list = NULL;
        amxc_var_t* event_table = NULL;

        sor_table = amxc_var_add_key(amxc_htable_t, entry, sor->supported_obj_path, NULL);
        amxc_var_add_key(bool, sor_table, "is_multi_instance", sor->is_multi_instance);
        amxc_var_add_key(uint32_t, sor_table, "access", sor->access);
        cmd_list = amxc_var_add_key(amxc_llist_t, sor_table, "supported_commands", NULL);
        param_list = amxc_var_add_key(amxc_llist_t, sor_table, "supported_params", NULL);
        event_table = amxc_var_add_key(amxc_htable_t, sor_table, "supported_events", NULL);
        for(size_t j = 0; j < sor->n_supported_commands; j++) {
            uspl_get_supported_dm_resp_append_cmd(sor->supported_commands[j], cmd_list);
        }
        for(size_t j = 0; j < sor->n_supported_params; j++) {
            uspl_get_supported_dm_resp_append_param(sor->supported_params[j], param_list);
        }
        for(size_t j = 0; j < sor->n_supported_events; j++) {
            uspl_get_supported_dm_resp_append_event(sor->supported_events[j], event_table);
        }
        amxc_llist_append(resp_list, &entry->lit);
    }

exit:
    return;
}

/*********************** Start of public functions ****************************/

int uspl_get_supported_dm_new(uspl_tx_t* usp_tx, const amxc_var_t* request) {
    int retval = -1;
    int i = 0;
    Usp__Msg* msg = NULL;
    Usp__GetSupportedDM* gs = NULL;
    const amxc_llist_t* paths_list = NULL;
    size_t num_paths = 0;
    amxc_var_t* flags = NULL;
    amxc_var_t* paths = NULL;
    amxc_string_t msg_id;

    amxc_string_init(&msg_id, 0);

    when_null(usp_tx, exit);
    when_null(request, exit);

    amxc_string_setf(&msg_id, "%d", uspl_msghandler_next_msg_id());

    msg = CreateGetSupportedDM(amxc_string_get(&msg_id, 0));

    flags = GET_ARG(request, "flags");
    gs = msg->body->request->get_supported_dm;
    gs->first_level_only = GET_BOOL(flags, "first_level_only");
    gs->return_commands = GET_BOOL(flags, "return_commands");
    gs->return_events = GET_BOOL(flags, "return_events");
    gs->return_params = GET_BOOL(flags, "return_params");

    paths = GET_ARG(request, "paths");
    paths_list = amxc_var_constcast(amxc_llist_t, paths);
    num_paths = amxc_llist_size(paths_list);

    gs->obj_paths = calloc(num_paths, sizeof(char*));
    gs->n_obj_paths = num_paths;
    amxc_llist_iterate(it, paths_list) {
        amxc_var_t* var = amxc_var_from_llist_it(it);
        const char* path = amxc_var_constcast(cstring_t, var);
        gs->obj_paths[i] = USP_STRDUP(path);
        i++;
    }

    retval = uspl_msghandler_pack_protobuf(msg, usp_tx);
    usp_tx->msg_id = USP_STRDUP(msg->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM;

exit:
    amxc_string_clean(&msg_id);
    uspl_msghandler_free_unpacked_message(msg);

    return retval;
}

int uspl_get_supported_dm_extract(uspl_rx_t* usp_rx, amxc_var_t* result) {
    Usp__GetSupportedDM* gs = NULL;
    int retval = USP_ERR_INTERNAL_ERROR;
    amxc_var_t* flags = NULL;
    amxc_var_t* paths = NULL;

    when_null(usp_rx, exit);
    when_null(result, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_REQUEST, exit);
    when_null(usp_rx->msg->body->request, exit);
    when_true(usp_rx->msg->body->request->req_type_case != USP__REQUEST__REQ_TYPE_GET_SUPPORTED_DM, exit);
    when_null(usp_rx->msg->body->request->get_supported_dm, exit);

    // Exit if there are no parameters to get
    retval = USP_ERR_INVALID_ARGUMENTS;
    gs = usp_rx->msg->body->request->get_supported_dm;
    when_true(gs->n_obj_paths < 1, exit);
    when_null(gs->obj_paths, exit);

    amxc_var_set_type(result, AMXC_VAR_ID_HTABLE);
    flags = amxc_var_add_key(amxc_htable_t, result, "flags", NULL);
    amxc_var_add_key(bool, flags, "first_level_only", gs->first_level_only);
    amxc_var_add_key(bool, flags, "return_events", gs->return_events);
    amxc_var_add_key(bool, flags, "return_params", gs->return_params);
    amxc_var_add_key(bool, flags, "return_commands", gs->return_commands);

    paths = amxc_var_add_key(amxc_llist_t, result, "paths", NULL);
    for(size_t i = 0; i < gs->n_obj_paths; i++) {
        amxc_var_add(cstring_t, paths, gs->obj_paths[i]);
    }

    retval = USP_ERR_OK;
exit:
    return retval;
}

int uspl_get_supported_dm_resp_new(uspl_tx_t* usp_tx, amxc_llist_t* resp_list, const char* msg_id) {
    int retval = -1;
    int i = 0;
    Usp__Msg* resp = NULL;
    Usp__GetSupportedDMResp* gs_resp = NULL;

    when_null(usp_tx, exit);
    when_null(resp_list, exit);
    when_null(msg_id, exit);

    // Create a GetSupportedDM Response message
    resp = CreateGetSupportedDMResp(msg_id);
    gs_resp = resp->body->response->get_supported_dm_resp;

    // Iterate over all replies to different GetSupported requests
    amxc_llist_for_each(it, resp_list) {
        amxc_var_t* reply_data = amxc_llist_it_get_data(it, amxc_var_t, lit);
        ProcessSupportedPathInstances(gs_resp, reply_data);
        i++;
    }

    retval = uspl_msghandler_pack_protobuf(resp, usp_tx);
    usp_tx->msg_id = USP_STRDUP(resp->header->msg_id);
    usp_tx->msg_type = USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM_RESP;

exit:
    uspl_msghandler_free_unpacked_message(resp);

    return retval;
}

int uspl_get_supported_dm_resp_extract(uspl_rx_t* usp_rx, amxc_llist_t* resp_list) {
    int retval = USP_ERR_INTERNAL_ERROR;
    Usp__GetSupportedDMResp* resp = NULL;

    when_null(usp_rx, exit);
    when_null(resp_list, exit);

    // Exit if message is invalid or failed to parse
    retval = USP_ERR_MESSAGE_NOT_UNDERSTOOD;
    when_null(usp_rx->msg, exit);
    when_null(usp_rx->msg->header, exit);
    when_null(usp_rx->msg->body, exit);
    when_true(usp_rx->msg->body->msg_body_case != USP__BODY__MSG_BODY_RESPONSE, exit);
    when_null(usp_rx->msg->body->response, exit);
    when_true(usp_rx->msg->body->response->resp_type_case != USP__RESPONSE__RESP_TYPE_GET_SUPPORTED_DM_RESP, exit);
    when_null(usp_rx->msg->body->response->get_supported_dm_resp, exit);

    resp = usp_rx->msg->body->response->get_supported_dm_resp;
    // Exit if there are no results in the response
    if((resp->n_req_obj_results == 0) || (resp->req_obj_results == NULL)) {
        retval = USP_ERR_INVALID_ARGUMENTS;
        goto exit;
    }

    for(size_t i = 0; i < resp->n_req_obj_results; i++) {
        uspl_get_supported_dm_resp_append_result(resp->req_obj_results[i], resp_list);
    }

    retval = USP_ERR_OK;
exit:
    return retval;
}