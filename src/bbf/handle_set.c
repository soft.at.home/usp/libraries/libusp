/*
 *
 * Copyright (C) 2019-2023, Broadband Forum
 * Copyright (C) 2016-2023  CommScope, Inc
 * Copyright (C) 2020,  BT PLC
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "bbf/usp_mem.h"
#include "bbf/handle_set.h"

Usp__Msg* CreateSetResp(const char* msg_id) {
    Usp__Msg* resp;
    Usp__Header* header;
    Usp__Body* body;
    Usp__Response* response;
    Usp__SetResp* set_resp;

    // Allocate and initialise memory to store the parts of the USP message
    resp = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(resp);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    response = USP_MALLOC(sizeof(Usp__Response));
    usp__response__init(response);

    set_resp = USP_MALLOC(sizeof(Usp__SetResp));
    usp__set_resp__init(set_resp);

    // Connect the structures together
    resp->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__SET_RESP;

    resp->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    body->response = response;
    response->resp_type_case = USP__RESPONSE__RESP_TYPE_SET_RESP;
    response->set_resp = set_resp;
    set_resp->n_updated_obj_results = 0;    // Start from an empty list
    set_resp->updated_obj_results = NULL;

    return resp;
}

Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationSuccess*
AddSetResp_OperSuccess(Usp__SetResp* set_resp, const char* path) {
    Usp__SetResp__UpdatedObjectResult* updated_obj_res = NULL;
    Usp__SetResp__UpdatedObjectResult__OperationStatus* oper_status = NULL;
    Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationSuccess* oper_success = NULL;
    int new_num = 0;    // new number of entries in the updated object result array

    // Allocate memory to store the updated object result
    updated_obj_res = USP_MALLOC(sizeof(Usp__SetResp__UpdatedObjectResult));
    usp__set_resp__updated_object_result__init(updated_obj_res);

    oper_status = USP_MALLOC(sizeof(Usp__SetResp__UpdatedObjectResult__OperationStatus));
    usp__set_resp__updated_object_result__operation_status__init(oper_status);

    oper_success = USP_MALLOC(sizeof(Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationSuccess));
    usp__set_resp__updated_object_result__operation_status__operation_success__init(oper_success);

    // Increase the size of the vector
    new_num = set_resp->n_updated_obj_results + 1;
    set_resp->updated_obj_results = USP_REALLOC(set_resp->updated_obj_results, new_num * sizeof(void*));
    set_resp->n_updated_obj_results = new_num;
    set_resp->updated_obj_results[new_num - 1] = updated_obj_res;

    // Connect all objects together, and fill in their members
    updated_obj_res->requested_path = USP_STRDUP(path);
    updated_obj_res->oper_status = oper_status;

    oper_status->oper_status_case = USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS;
    oper_status->oper_success = oper_success;

    oper_success->n_updated_inst_results = 0;
    oper_success->updated_inst_results = NULL;

    return oper_success;
}

Usp__SetResp__UpdatedInstanceResult*
AddOperSuccess_UpdatedInstRes(Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationSuccess* oper_success,
                              const char* affected_path) {
    Usp__SetResp__UpdatedInstanceResult* updated_inst_result = NULL;
    int new_num = 0;    // new number of entries in the updated instance result array

    // Allocate memory to store the updated instance result entry
    updated_inst_result = USP_MALLOC(sizeof(Usp__SetResp__UpdatedInstanceResult));
    usp__set_resp__updated_instance_result__init(updated_inst_result);

    // Increase the size of the vector
    new_num = oper_success->n_updated_inst_results + 1;
    oper_success->updated_inst_results = USP_REALLOC(oper_success->updated_inst_results, new_num * sizeof(void*));
    oper_success->n_updated_inst_results = new_num;
    oper_success->updated_inst_results[new_num - 1] = updated_inst_result;

    // Initialise the updated instance result
    updated_inst_result->n_updated_params = 0;
    updated_inst_result->updated_params = NULL;
    updated_inst_result->n_param_errs = 0;
    updated_inst_result->param_errs = NULL;

    // Add the object path with a trailing '.'
    updated_inst_result->affected_path = USP_STRDUP(affected_path);

    return updated_inst_result;
}

Usp__SetResp__UpdatedInstanceResult__UpdatedParamsEntry*
AddUpdatedInstRes_ParamsEntry(Usp__SetResp__UpdatedInstanceResult* updated_inst_result,
                              const char* key,
                              char* value) {
    Usp__SetResp__UpdatedInstanceResult__UpdatedParamsEntry* entry = NULL;
    int new_num = 0;    // new number of entries in the updated instance result array

    // Allocate memory to store the updated instance result entry
    entry = USP_MALLOC(sizeof(Usp__SetResp__UpdatedInstanceResult__UpdatedParamsEntry));
    usp__set_resp__updated_instance_result__updated_params_entry__init(entry);

    // Increase the size of the vector
    new_num = updated_inst_result->n_updated_params + 1;
    updated_inst_result->updated_params = USP_REALLOC(updated_inst_result->updated_params, new_num * sizeof(void*));
    updated_inst_result->n_updated_params = new_num;
    updated_inst_result->updated_params[new_num - 1] = entry;

    // Initialise the result param map entry
    entry->key = USP_STRDUP(key);
    entry->value = value; // already dyncast

    return entry;
}

Usp__SetResp__ParameterError*
AddUpdatedInstRes_ParamErr(Usp__SetResp__UpdatedInstanceResult* updated_inst_result,
                           const char* path,
                           int err_code,
                           const char* err_msg) {
    Usp__SetResp__ParameterError* param_err_entry = NULL;
    int new_num = 0;    // new number of entries in the param_err array

    // Allocate memory to store the param_err entry
    param_err_entry = USP_MALLOC(sizeof(Usp__SetResp__ParameterError));
    usp__set_resp__parameter_error__init(param_err_entry);

    // Increase the size of the vector
    new_num = updated_inst_result->n_param_errs + 1;
    updated_inst_result->param_errs = USP_REALLOC(updated_inst_result->param_errs, new_num * sizeof(void*));
    updated_inst_result->n_param_errs = new_num;
    updated_inst_result->param_errs[new_num - 1] = param_err_entry;

    // Initialise the param_err_entry
    param_err_entry->param = USP_STRDUP(path);
    param_err_entry->err_code = err_code;
    param_err_entry->err_msg = USP_STRDUP(err_msg);

    return param_err_entry;
}

Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationFailure*
AddSetResp_OperFailure(Usp__SetResp* set_resp, const char* path, int err_code, const char* err_msg) {
    Usp__SetResp__UpdatedObjectResult* updated_obj_res = NULL;
    Usp__SetResp__UpdatedObjectResult__OperationStatus* oper_status = NULL;
    Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationFailure* oper_failure = NULL;
    int new_num = 0;    // new number of entries in the updated object result array

    // Allocate memory to store the updated object result
    updated_obj_res = USP_MALLOC(sizeof(Usp__SetResp__UpdatedObjectResult));
    usp__set_resp__updated_object_result__init(updated_obj_res);

    oper_status = USP_MALLOC(sizeof(Usp__SetResp__UpdatedObjectResult__OperationStatus));
    usp__set_resp__updated_object_result__operation_status__init(oper_status);

    oper_failure = USP_MALLOC(sizeof(Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationFailure));
    usp__set_resp__updated_object_result__operation_status__operation_failure__init(oper_failure);

    // Increase the size of the vector
    new_num = set_resp->n_updated_obj_results + 1;
    set_resp->updated_obj_results = USP_REALLOC(set_resp->updated_obj_results, new_num * sizeof(void*));
    set_resp->n_updated_obj_results = new_num;
    set_resp->updated_obj_results[new_num - 1] = updated_obj_res;

    // Connect all objects together, and fill in their members
    updated_obj_res->requested_path = USP_STRDUP(path);
    updated_obj_res->oper_status = oper_status;

    oper_status->oper_status_case = USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE;
    oper_status->oper_failure = oper_failure;

    oper_failure->err_code = err_code;
    oper_failure->err_msg = USP_STRDUP(err_msg);
    oper_failure->n_updated_inst_failures = 0;
    oper_failure->updated_inst_failures = NULL;

    return oper_failure;
}

Usp__SetResp__UpdatedInstanceFailure*
AddOperFailure_UpdatedInstFailure(Usp__SetResp__UpdatedObjectResult__OperationStatus__OperationFailure* oper_failure,
                                  const char* affected_path) {
    Usp__SetResp__UpdatedInstanceFailure* updated_inst_failure = NULL;
    int new_num = 0;    // new number of entries in the updated instance failure array

    // Allocate memory to store the updated instance failure entry
    updated_inst_failure = USP_MALLOC(sizeof(Usp__SetResp__UpdatedInstanceFailure));
    usp__set_resp__updated_instance_failure__init(updated_inst_failure);

    // Increase the size of the vector
    new_num = oper_failure->n_updated_inst_failures + 1;
    oper_failure->updated_inst_failures = USP_REALLOC(oper_failure->updated_inst_failures, new_num * sizeof(void*));
    oper_failure->n_updated_inst_failures = new_num;
    oper_failure->updated_inst_failures[new_num - 1] = updated_inst_failure;

    // Initialise the updated instance failure
    updated_inst_failure->n_param_errs = 0;
    updated_inst_failure->param_errs = NULL;

    // Add the object path with a trailing '.'
    updated_inst_failure->affected_path = USP_STRDUP(affected_path);

    return updated_inst_failure;
}

Usp__SetResp__ParameterError*
AddUpdatedInstFailure_ParamErr(Usp__SetResp__UpdatedInstanceFailure* updated_inst_failure,
                               const char* path,
                               int err_code,
                               const char* err_msg) {
    Usp__SetResp__ParameterError* param_err_entry = NULL;
    int new_num = 0;    // new number of entries in the param_err array

    // Allocate memory to store the param_err entry
    param_err_entry = USP_MALLOC(sizeof(Usp__SetResp__ParameterError));
    usp__set_resp__parameter_error__init(param_err_entry);

    // Increase the size of the vector
    new_num = updated_inst_failure->n_param_errs + 1;
    updated_inst_failure->param_errs = USP_REALLOC(updated_inst_failure->param_errs, new_num * sizeof(void*));
    updated_inst_failure->n_param_errs = new_num;
    updated_inst_failure->param_errs[new_num - 1] = param_err_entry;

    // Initialise the param_err_entry
    param_err_entry->param = USP_STRDUP(path);
    param_err_entry->err_code = err_code;
    param_err_entry->err_msg = USP_STRDUP(err_msg);

    return param_err_entry;
}
