/*
 *
 * Copyright (C) 2019-2023, Broadband Forum
 * Copyright (C) 2016-2023  CommScope, Inc
 * Copyright (C) 2020,  BT PLC
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "bbf/usp_mem.h"
#include "bbf/handle_get_supported_dm.h"

Usp__Msg* CreateGetSupportedDMResp(const char* msg_id) {
    Usp__Msg* resp;
    Usp__Header* header;
    Usp__Body* body;
    Usp__Response* response;
    Usp__GetSupportedDMResp* get_sup_resp;

    // Allocate memory to store the USP message
    resp = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(resp);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    response = USP_MALLOC(sizeof(Usp__Response));
    usp__response__init(response);

    get_sup_resp = USP_MALLOC(sizeof(Usp__GetSupportedDMResp));
    usp__get_supported_dmresp__init(get_sup_resp);

    // Connect the structures together
    resp->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM_RESP;

    resp->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    body->response = response;
    response->resp_type_case = USP__RESPONSE__RESP_TYPE_GET_SUPPORTED_DM_RESP;

    response->get_supported_dm_resp = get_sup_resp;

    return resp;
}

Usp__GetSupportedDMResp__RequestedObjectResult*
AddGetSupportedDM_ReqObjResult(Usp__GetSupportedDMResp* gs_resp, const char* requested_path, uint32_t err, const char* err_msg, const char* bbf_uri) {
    Usp__GetSupportedDMResp__RequestedObjectResult* ror;
    int new_num;    // new number of entries in the requested obj result array

    // Allocate memory to store the RequestedObjResult object
    ror = USP_MALLOC(sizeof(Usp__GetSupportedDMResp__RequestedObjectResult));
    usp__get_supported_dmresp__requested_object_result__init(ror);

    // Increase the size of the vector
    new_num = gs_resp->n_req_obj_results + 1;
    gs_resp->req_obj_results = USP_REALLOC(gs_resp->req_obj_results, new_num * sizeof(void*));
    gs_resp->n_req_obj_results = new_num;
    gs_resp->req_obj_results[new_num - 1] = ror;

    // Fill in the RequestedObjResult object
    ror->req_obj_path = USP_STRDUP(requested_path);
    ror->err_code = err;
    ror->err_msg = USP_STRDUP(err_msg);
    ror->data_model_inst_uri = USP_STRDUP(bbf_uri);

    return ror;
}
