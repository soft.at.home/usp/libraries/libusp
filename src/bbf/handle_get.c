/*
 *
 * Copyright (C) 2019-2023, Broadband Forum
 * Copyright (C) 2016-2023  CommScope, Inc
 * Copyright (C) 2020,  BT PLC
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "bbf/usp_mem.h"
#include "bbf/handle_get.h"

Usp__Msg* CreateGetResp(const char* msg_id) {
    Usp__Msg* resp;
    Usp__Header* header;
    Usp__Body* body;
    Usp__Response* response;
    Usp__GetResp* get_resp;

    // Allocate memory to store the USP message
    resp = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(resp);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    response = USP_MALLOC(sizeof(Usp__Response));
    usp__response__init(response);

    get_resp = USP_MALLOC(sizeof(Usp__GetResp));
    usp__get_resp__init(get_resp);

    // Connect the structures together
    resp->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__GET_RESP;

    resp->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    body->response = response;
    response->resp_type_case = USP__RESPONSE__RESP_TYPE_GET_RESP;
    response->get_resp = get_resp;
    get_resp->n_req_path_results = 0;    // Start from an empty response list
    get_resp->req_path_results = NULL;

    return resp;
}

Usp__GetResp__RequestedPathResult*
AddGetResp_ReqPathRes(Usp__Msg* resp, const char* requested_path, int err_code, const char* err_msg) {
    Usp__GetResp* get_resp;
    Usp__GetResp__RequestedPathResult* req_path_result;
    int new_num;    // new number of requested_path_results

    // Allocate memory to store the requested_path_result
    req_path_result = USP_MALLOC(sizeof(Usp__GetResp__RequestedPathResult));
    usp__get_resp__requested_path_result__init(req_path_result);

    // Increase the size of the vector containing pointers to the requested_path_results
    get_resp = resp->body->response->get_resp;
    new_num = get_resp->n_req_path_results + 1;
    get_resp->req_path_results = USP_REALLOC(get_resp->req_path_results, new_num * sizeof(void*));
    get_resp->n_req_path_results = new_num;
    get_resp->req_path_results[new_num - 1] = req_path_result;

    // Initialise the requested_path_result
    req_path_result->requested_path = USP_STRDUP(requested_path);
    req_path_result->err_code = err_code;
    req_path_result->err_msg = USP_STRDUP(err_msg);
    req_path_result->n_resolved_path_results = 0;     // Start from an empty list
    req_path_result->resolved_path_results = NULL;

    return req_path_result;
}


Usp__GetResp__ResolvedPathResult* AddReqPathRes_ResolvedPathResult(Usp__GetResp__RequestedPathResult* req_path_result, const char* obj_path) {
    Usp__GetResp__ResolvedPathResult* resolved_path_res_entry;

    int new_num;    // new number of entries in the result_params

    // Allocate memory to store the resolved_path_result entry
    resolved_path_res_entry = USP_MALLOC(sizeof(Usp__GetResp__ResolvedPathResult));
    usp__get_resp__resolved_path_result__init(resolved_path_res_entry);

    // Increase the size of the vector containing pointers to the map entries
    new_num = req_path_result->n_resolved_path_results + 1;
    req_path_result->resolved_path_results = USP_REALLOC(req_path_result->resolved_path_results, new_num * sizeof(void*));
    req_path_result->n_resolved_path_results = new_num;
    req_path_result->resolved_path_results[new_num - 1] = resolved_path_res_entry;

    // Initialise the resolved_path_result
    resolved_path_res_entry->resolved_path = USP_STRDUP(obj_path);
    resolved_path_res_entry->n_result_params = 0;
    resolved_path_res_entry->result_params = NULL;

    return resolved_path_res_entry;
}

Usp__GetResp__ResolvedPathResult__ResultParamsEntry*
AddResolvedPathRes_ParamsEntry(Usp__GetResp__ResolvedPathResult* resolved_path_res, const char* param_name, const char* value) {
    Usp__GetResp__ResolvedPathResult__ResultParamsEntry* res_params_entry;

    int new_num;    // new number of entries in the result_params

    // Allocate memory to store the result_params entry
    res_params_entry = USP_MALLOC(sizeof(Usp__GetResp__ResolvedPathResult__ResultParamsEntry));
    usp__get_resp__resolved_path_result__result_params_entry__init(res_params_entry);

    // Increase the size of the vector containing pointers to the map entries
    new_num = resolved_path_res->n_result_params + 1;
    resolved_path_res->result_params = USP_REALLOC(resolved_path_res->result_params, new_num * sizeof(void*));
    resolved_path_res->n_result_params = new_num;
    resolved_path_res->result_params[new_num - 1] = res_params_entry;

    // Initialise the result_params_entry
    res_params_entry->key = USP_STRDUP(param_name);
    if(value != NULL) {
        res_params_entry->value = USP_STRDUP(value);
    } else {
        res_params_entry->value = NULL;
    }

    return res_params_entry;
}
