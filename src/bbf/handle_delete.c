/*
 *
 * Copyright (C) 2019-2023, Broadband Forum
 * Copyright (C) 2016-2023  CommScope, Inc
 * Copyright (C) 2020,  BT PLC
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "bbf/usp_mem.h"
#include "bbf/handle_delete.h"

Usp__Msg* CreateDeleteResp(const char* msg_id) {
    Usp__Msg* resp;
    Usp__Header* header;
    Usp__Body* body;
    Usp__Response* response;
    Usp__DeleteResp* del_resp;

    // Allocate and initialise memory to store the parts of the USP message
    resp = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(resp);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    response = USP_MALLOC(sizeof(Usp__Response));
    usp__response__init(response);

    del_resp = USP_MALLOC(sizeof(Usp__DeleteResp));
    usp__delete_resp__init(del_resp);

    // Connect the structures together
    resp->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__DELETE_RESP;

    resp->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    body->response = response;
    response->resp_type_case = USP__RESPONSE__RESP_TYPE_DELETE_RESP;
    response->delete_resp = del_resp;

    del_resp->n_deleted_obj_results = 0;    // Start from an empty list
    del_resp->deleted_obj_results = NULL;

    return resp;
}

Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationSuccess*
AddDeleteResp_OperSuccess(Usp__DeleteResp* del_resp, const char* path) {
    Usp__DeleteResp__DeletedObjectResult* deleted_obj_res;
    Usp__DeleteResp__DeletedObjectResult__OperationStatus* oper_status;
    Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationSuccess* oper_success;
    int new_num;    // new number of entries in the created object result array

    // Allocate memory to store the created object result
    deleted_obj_res = USP_MALLOC(sizeof(Usp__DeleteResp__DeletedObjectResult));
    usp__delete_resp__deleted_object_result__init(deleted_obj_res);

    // Allocate memory to store the created oper status object
    oper_status = USP_MALLOC(sizeof(Usp__DeleteResp__DeletedObjectResult__OperationStatus));
    usp__delete_resp__deleted_object_result__operation_status__init(oper_status);

    // Allocate memory to store the created oper success object
    oper_success = USP_MALLOC(sizeof(Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationSuccess));
    usp__delete_resp__deleted_object_result__operation_status__operation_success__init(oper_success);

    // Increase the size of the vector
    new_num = del_resp->n_deleted_obj_results + 1;
    del_resp->deleted_obj_results = USP_REALLOC(del_resp->deleted_obj_results, new_num * sizeof(void*));
    del_resp->n_deleted_obj_results = new_num;
    del_resp->deleted_obj_results[new_num - 1] = deleted_obj_res;

    // Fill in its members
    deleted_obj_res->requested_path = USP_STRDUP(path);
    deleted_obj_res->oper_status = oper_status;

    oper_status->oper_status_case = USP__DELETE_RESP__DELETED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS;
    oper_status->oper_success = oper_success;

    oper_success->n_affected_paths = 0;
    oper_success->affected_paths = NULL;
    oper_success->n_unaffected_path_errs = 0;
    oper_success->unaffected_path_errs = NULL;

    return oper_success;
}

void AddOperSuccess_AffectedPath(Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationSuccess* oper_success,
                                 const char* path) {
    int new_num;    // new number of entries in the affected path list

    // Increase the size of the vector
    new_num = oper_success->n_affected_paths + 1;
    oper_success->affected_paths = USP_REALLOC(oper_success->affected_paths, new_num * sizeof(void*));
    oper_success->n_affected_paths = new_num;

    // Add the path to the vector
    oper_success->affected_paths[new_num - 1] = USP_STRDUP(path);
}

void AddOperSuccess_UnaffectedPathError(Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationSuccess* oper_success,
                                        const char* path, uint32_t err_code, const char* err_msg) {
    int new_num;    // new number of entries in the unaffected path error list
    Usp__DeleteResp__UnaffectedPathError* unaffected_path_err;

    // Allocate memory to store the unaffected path error
    unaffected_path_err = USP_MALLOC(sizeof(Usp__DeleteResp__UnaffectedPathError));
    usp__delete_resp__unaffected_path_error__init(unaffected_path_err);

    // Increase the size of the vector
    new_num = oper_success->n_unaffected_path_errs + 1;
    oper_success->unaffected_path_errs = USP_REALLOC(oper_success->unaffected_path_errs, new_num * sizeof(void*));
    oper_success->n_unaffected_path_errs = new_num;
    oper_success->unaffected_path_errs[new_num - 1] = unaffected_path_err;

    // Fill in its members
    unaffected_path_err = oper_success->unaffected_path_errs[new_num - 1];
    unaffected_path_err->unaffected_path = USP_STRDUP(path);
    unaffected_path_err->err_code = err_code;
    unaffected_path_err->err_msg = USP_STRDUP(err_msg);
}

Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationFailure*
AddDeleteResp_OperFailure(Usp__DeleteResp* del_resp, const char* path, uint32_t err_code, const char* err_msg) {
    Usp__DeleteResp__DeletedObjectResult* deleted_obj_res;
    Usp__DeleteResp__DeletedObjectResult__OperationStatus* oper_status;
    Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationFailure* oper_failure;
    int new_num;    // new number of entries in the created object result array

    // Allocate memory to store the created object result
    deleted_obj_res = USP_MALLOC(sizeof(Usp__DeleteResp__DeletedObjectResult));
    usp__delete_resp__deleted_object_result__init(deleted_obj_res);

    // Allocate memory to store the created oper status object
    oper_status = USP_MALLOC(sizeof(Usp__DeleteResp__DeletedObjectResult__OperationStatus));
    usp__delete_resp__deleted_object_result__operation_status__init(oper_status);

    // Allocate memory to store the created oper failure object
    oper_failure = USP_MALLOC(sizeof(Usp__DeleteResp__DeletedObjectResult__OperationStatus__OperationFailure));
    usp__delete_resp__deleted_object_result__operation_status__operation_failure__init(oper_failure);

    // Increase the size of the vector
    new_num = del_resp->n_deleted_obj_results + 1;
    del_resp->deleted_obj_results = USP_REALLOC(del_resp->deleted_obj_results, new_num * sizeof(void*));
    del_resp->n_deleted_obj_results = new_num;
    del_resp->deleted_obj_results[new_num - 1] = deleted_obj_res;

    // Fill in its members
    deleted_obj_res->requested_path = USP_STRDUP(path);
    deleted_obj_res->oper_status = oper_status;

    oper_status->oper_status_case = USP__DELETE_RESP__DELETED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE;
    oper_status->oper_failure = oper_failure;

    oper_failure->err_code = err_code;
    oper_failure->err_msg = USP_STRDUP(err_msg);

    return oper_failure;
}
