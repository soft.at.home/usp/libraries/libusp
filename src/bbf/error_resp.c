/*
 *
 * Copyright (C) 2019-2023, Broadband Forum
 * Copyright (C) 2016-2023  CommScope, Inc
 * Copyright (C) 2020,  BT PLC
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "bbf/usp_mem.h"
#include "bbf/error_resp.h"

Usp__Msg* ERROR_RESP_Create(const char* msg_id, int err_code, const char* err_msg) {
    Usp__Msg* resp;
    Usp__Header* header;
    Usp__Body* body;
    Usp__Error* error;

    // Allocate memory to store the USP message
    resp = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(resp);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    error = USP_MALLOC(sizeof(Usp__Error));
    usp__error__init(error);

    // Connect the structures together
    resp->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__ERROR;

    resp->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_ERROR;
    body->error = error;

    error->err_code = err_code;
    error->err_msg = USP_STRDUP(err_msg);
    error->n_param_errs = 0;
    error->param_errs = NULL;

    return resp;
}

Usp__Error__ParamError*
ERROR_RESP_AddParamError(Usp__Msg* resp, const char* path, int err_code, const char* err_msg) {
    Usp__Error* error;
    Usp__Error__ParamError* param_err;
    int new_num;    // new number of param_error

    // Allocate memory to store the param_error
    param_err = USP_MALLOC(sizeof(Usp__Error__ParamError));
    usp__error__param_error__init(param_err);

    // Increase the size of the vector containing pointers to the param_errors
    error = resp->body->error;
    new_num = error->n_param_errs + 1;
    error->param_errs = USP_REALLOC(error->param_errs, new_num * sizeof(void*));
    error->n_param_errs = new_num;
    error->param_errs[new_num - 1] = param_err;

    // Initialise the param_error
    param_err->param_path = USP_STRDUP(path);
    param_err->err_code = err_code;
    param_err->err_msg = USP_STRDUP(err_msg);

    return param_err;
}
