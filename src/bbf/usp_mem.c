/*
 *
 * Copyright (C) 2019-2023, Broadband Forum
 * Copyright (C) 2016-2023  CommScope, Inc
 * Copyright (C) 2020,  BT PLC
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
   @file usp_mem.c
   @brief
   Wrapper functions for allocating and deallocating memory on USP Agent
   The allocation functions terminate (logging an error message), if out of memory
   It is the intention that all allocation/deallocation in USP Agent goes through functions in this file
   This will allow us to debug memory leaks etc more easily
 */

#include <stdlib.h>
#include <string.h>

#include <protobuf-c/protobuf-c.h>

#include <debug/sahtrace.h>

#include "uspl_private.h"

#include "bbf/usp_mem.h"

/**
   @brief
   Wrapper around malloc() that terminates with error message if it failed

   @param func name of caller
   @param line line number of caller
   @param size number of bytes to allocate

   @return None
 */
void* USP_MEM_Malloc(UNUSED const char* func, UNUSED int line, int size) {
    void* ptr;

    // Terminate if out of memory
    ptr = malloc(size);
    if(ptr == NULL) {
        SAH_TRACEZ_ERROR(uspl_traceme, "%s (%d): malloc(%d bytes) failed", func, line, size);
    }

    return ptr;
}

/**
   @brief
   Wrapper around free()

   @param func name of caller
   @param line line number of caller
   @param ptr pointer to dynamically allocated memory buffer to free

   @return None
 */
void USP_MEM_Free(UNUSED const char* func, UNUSED int line, void* ptr) {
    // Free the memory
    free(ptr);
}

/**
   @brief
   Wrapper around realloc() that terminates with error message if it failed

   @param func name of caller
   @param line line number of caller
   @param ptr pointer to current buffer than needs reallocating
   @param size number of bytes to reallocate

   @return None
 */
void* USP_MEM_Realloc(UNUSED const char* func, UNUSED int line, void* ptr, int size) {
    void* new_ptr;

    // Terminate if out of memory
    new_ptr = realloc(ptr, size);
    if(new_ptr == NULL) {
        SAH_TRACEZ_ERROR(uspl_traceme, "%s (%d): realloc(%d bytes) failed", func, line, size);
    }

    return new_ptr;
}

/**
   @brief
   Wrapper around strdup() that terminates with error message if it failed

   @note This function treats a NULL input string, as a NULL output

   @param func name of caller
   @param line line number of caller
   @param ptr pointer to buffer containing string to copy

   @return None
 */
void* USP_MEM_Strdup(UNUSED const char* func, UNUSED int line, const char* ptr) {
    void* new_ptr;

    // Exit if nothing to copy
    if(ptr == NULL) {
        return NULL;
    }

    // Terminate if out of memory
    new_ptr = strdup(ptr);
    if(new_ptr == NULL) {
        SAH_TRACEZ_ERROR(uspl_traceme, "%s (%d): strdup(%d bytes) failed", func, line,
                         (int) strlen(ptr) + 1);
    }

    return new_ptr;
}
