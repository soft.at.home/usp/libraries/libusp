/*
 *
 * Copyright (C) 2019-2023, Broadband Forum
 * Copyright (C) 2016-2023  CommScope, Inc
 * Copyright (C) 2020,  BT PLC
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "bbf/usp_mem.h"
#include "bbf/handle_add.h"

Usp__Msg* CreateAddResp(const char* msg_id) {
    Usp__Msg* resp;
    Usp__Header* header;
    Usp__Body* body;
    Usp__Response* response;
    Usp__AddResp* add_resp;

    // Allocate and initialise memory to store the parts of the USP message
    resp = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(resp);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    response = USP_MALLOC(sizeof(Usp__Response));
    usp__response__init(response);

    add_resp = USP_MALLOC(sizeof(Usp__AddResp));
    usp__add_resp__init(add_resp);

    // Connect the structures together
    resp->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__ADD_RESP;

    resp->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    body->response = response;
    response->resp_type_case = USP__RESPONSE__RESP_TYPE_ADD_RESP;
    response->add_resp = add_resp;

    add_resp->n_created_obj_results = 0;    // Start from an empty list
    add_resp->created_obj_results = NULL;

    return resp;
}

Usp__AddResp__CreatedObjectResult__OperationStatus__OperationSuccess*
AddResp_OperSuccess(Usp__AddResp* add_resp, const char* req_path, const char* path) {
    Usp__AddResp__CreatedObjectResult* created_obj_res = NULL;
    Usp__AddResp__CreatedObjectResult__OperationStatus* oper_status = NULL;
    Usp__AddResp__CreatedObjectResult__OperationStatus__OperationSuccess* oper_success = NULL;
    int new_num = -1;    // new number of entries in the created object result array

    // Allocate memory to store the created object result
    created_obj_res = USP_MALLOC(sizeof(Usp__AddResp__CreatedObjectResult));
    usp__add_resp__created_object_result__init(created_obj_res);

    oper_status = USP_MALLOC(sizeof(Usp__AddResp__CreatedObjectResult__OperationStatus));
    usp__add_resp__created_object_result__operation_status__init(oper_status);

    oper_success = USP_MALLOC(sizeof(Usp__AddResp__CreatedObjectResult__OperationStatus__OperationSuccess));
    usp__add_resp__created_object_result__operation_status__operation_success__init(oper_success);

    // Increase the size of the vector
    new_num = add_resp->n_created_obj_results + 1;
    add_resp->created_obj_results = USP_REALLOC(add_resp->created_obj_results, new_num * sizeof(void*));
    add_resp->n_created_obj_results = new_num;
    add_resp->created_obj_results[new_num - 1] = created_obj_res;

    // Connect all objects together, and fill in their members
    created_obj_res->requested_path = USP_STRDUP(req_path);
    created_obj_res->oper_status = oper_status;

    oper_status->oper_status_case = USP__ADD_RESP__CREATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS;
    oper_status->oper_success = oper_success;

    oper_success->n_param_errs = 0;

    oper_success->instantiated_path = USP_STRDUP(path);
    oper_success->param_errs = NULL;             // Start from an empty list
    oper_success->n_param_errs = 0;

    oper_success->unique_keys = NULL;   // Start from an empty list
    oper_success->n_unique_keys = 0;

    return oper_success;
}

Usp__AddResp__CreatedObjectResult__OperationStatus__OperationFailure*
AddResp_OperFailure(Usp__AddResp* add_resp, const char* path, int err_code, const char* err_msg) {
    Usp__AddResp__CreatedObjectResult* created_obj_res;
    Usp__AddResp__CreatedObjectResult__OperationStatus* oper_status;
    Usp__AddResp__CreatedObjectResult__OperationStatus__OperationFailure* oper_failure;
    int new_num;    // new number of entries in the created object result array

    // Allocate memory to store the created object result
    created_obj_res = USP_MALLOC(sizeof(Usp__AddResp__CreatedObjectResult));
    usp__add_resp__created_object_result__init(created_obj_res);

    oper_status = USP_MALLOC(sizeof(Usp__AddResp__CreatedObjectResult__OperationStatus));
    usp__add_resp__created_object_result__operation_status__init(oper_status);

    oper_failure = USP_MALLOC(sizeof(Usp__AddResp__CreatedObjectResult__OperationStatus__OperationFailure));
    usp__add_resp__created_object_result__operation_status__operation_failure__init(oper_failure);

    // Increase the size of the vector
    new_num = add_resp->n_created_obj_results + 1;
    add_resp->created_obj_results = USP_REALLOC(add_resp->created_obj_results, new_num * sizeof(void*));
    add_resp->n_created_obj_results = new_num;
    add_resp->created_obj_results[new_num - 1] = created_obj_res;

    // Connect all objects together, and fill in their members
    created_obj_res->requested_path = USP_STRDUP(path);
    created_obj_res->oper_status = oper_status;

    oper_status->oper_status_case = USP__ADD_RESP__CREATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE;
    oper_status->oper_failure = oper_failure;

    oper_failure->err_code = err_code;
    oper_failure->err_msg = USP_STRDUP(err_msg);

    return oper_failure;
}
