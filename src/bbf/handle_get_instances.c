/*
 *
 * Copyright (C) 2019-2023, Broadband Forum
 * Copyright (C) 2016-2023  CommScope, Inc
 * Copyright (C) 2020,  BT PLC
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "bbf/usp_mem.h"
#include "bbf/handle_get_instances.h"

Usp__Msg* CreateGetInstancesResp(const char* msg_id) {
    Usp__Msg* resp = NULL;
    Usp__Header* header = NULL;
    Usp__Body* body = NULL;
    Usp__Response* response = NULL;
    Usp__GetInstancesResp* get_inst_resp = NULL;

    // Allocate memory to store the USP message
    resp = USP_MALLOC(sizeof(Usp__Msg));
    usp__msg__init(resp);

    header = USP_MALLOC(sizeof(Usp__Header));
    usp__header__init(header);

    body = USP_MALLOC(sizeof(Usp__Body));
    usp__body__init(body);

    response = USP_MALLOC(sizeof(Usp__Response));
    usp__response__init(response);

    get_inst_resp = USP_MALLOC(sizeof(Usp__GetInstancesResp));
    usp__get_instances_resp__init(get_inst_resp);

    // Connect the structures together
    resp->header = header;
    header->msg_id = USP_STRDUP(msg_id);
    header->msg_type = USP__HEADER__MSG_TYPE__GET_INSTANCES_RESP;

    resp->body = body;
    body->msg_body_case = USP__BODY__MSG_BODY_RESPONSE;
    body->response = response;
    response->resp_type_case = USP__RESPONSE__RESP_TYPE_GET_INSTANCES_RESP;

    response->get_instances_resp = get_inst_resp;

    return resp;
}

Usp__GetInstancesResp__RequestedPathResult*
AddGetInstances_RequestedPathResult(Usp__GetInstancesResp* gi_resp, const char* requested_path, uint32_t err, const char* err_msg) {
    Usp__GetInstancesResp__RequestedPathResult* req_path_res = NULL;
    int new_num = 0;    // new number of entries in the requested path result array

    // Allocate memory to store the RequestedPathResult object
    req_path_res = USP_MALLOC(sizeof(Usp__GetInstancesResp__RequestedPathResult));
    usp__get_instances_resp__requested_path_result__init(req_path_res);

    // Increase the size of the vector
    new_num = gi_resp->n_req_path_results + 1;
    gi_resp->req_path_results = USP_REALLOC(gi_resp->req_path_results, new_num * sizeof(void*));
    gi_resp->n_req_path_results = new_num;
    gi_resp->req_path_results[new_num - 1] = req_path_res;

    // Fill in the RequestedPathResult object
    req_path_res->requested_path = USP_STRDUP(requested_path);
    req_path_res->err_code = err;
    req_path_res->err_msg = USP_STRDUP(err_msg);

    return req_path_res;
}

